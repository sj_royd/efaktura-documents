<?php

include '../vendor/autoload.php';

use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\PEPPOL as P;
use SJRoyd\PEF\BIS\CreditNote;

$note = new CreditNote('NK/121/8/18/MPK', '2018-08-31');
$note
	->setNote('registration court')// * uwagi tekstowe do faktury
	->setAccountingCost('Project cost code 123')// * znak stanowiska kosztów u nabywcy
	->setContractReference('Contract321');// * numer kontraktu

// -----------------------------------------------------------------------------
// dane sprzedawcy
$supplier = $note->supplier('Nazwa wystawcy noty')// nazwa sprzedawcy
	->setEndpoint('1234567890123', P\EAS::GLN)// identyfikator „Endpoint” – punktu dostępu w sieci (Sprzedawca)
	->setIdentification('Supp123')// identyfikator sprzedawcy (zgodny z listą PEPPOL)
    ->addTaxScheme('PL1234567890')// numer rejestracji podatkowej VAT (NIP) sprzedawcy
    // dane podmiotu prawnego
    // nazwa rejestrowa sprzedawcy
    // identyfikator rejestrowy sprzedawcy
    // informacje: kapitał, ...
    ->setLegalEntity('Nazwa firmy', '5402697509', 'Forma prawna');
// adres sprzedawcy
$supplier->address(P\Country::PL)// kod kraju
	->setStreet('Nazwa ulicy')// * linia adresu cz.1 (nazwa ulicy)
	->setAdditionalStreet('131/133')// * linia adresu cz.2 (c.d. nazwa ulicy)
	->setCity('Nazwa miejscowości')// * nazwa miejscowości
	->setPostalCode('54-321')// * kod pocztowy
	->setCountrySubentity('Region')// * nazwa regionu
	->setAddressLine('A');// * linia adresu cz.3 (dodatkowe informacje adresu)
$supplier->contact()// dane kontaktowe siedziby sprzedawcy
	->setName('Imię Nazwisko')// imię i nazwisko osoby kontaktowej
	->setPhone('614621230')// numer telefonu osoby kontaktowej
	->setEmail('aaaaa@dostawca.eu');// adres e-mail osoby kontaktowej

// -----------------------------------------------------------------------------
// dane klienta
$customer = $note->customer('Miasto Poznań')// nazwa nabywcy
	->setEndpoint('1234567890123', P\EAS::GLN)// identyfikator „Endpoint” (zgodny z listą PEPPOL) – punktu dostępu w sieci (Odbiorca)
	->setIdentification('Client123')// identyfikator sprzedawcy (zgodny z listą PEPPOL)
    ->setTaxScheme('PL1234567890')// numer rejestracji podatkowej VAT (NIP) nabywcy (Miasto Poznań)
    // dane podmiotu prawnego
    // nazwa rejestrowa nabywcy
    // identyfikator rejestrowy nabywcy
    ->setLegalEntity('Nazwa firmy', '5402697509');
// adres nabywcy
$customer->address(P\Country::PL)// kod kraju
	->setStreet('Nazwa ulicy') // * linia adresu cz.1 (nazwa ulicy)
	->setAdditionalStreet('17')// * linia adresu cz.2 (c.d. nazwa ulicy)
	->setCity('Nazwa miejscowości')// * nazwa miejscowości
	->setPostalCode('54-321')// * kod pocztowy
	->setCountrySubentity('Region')// * nazwa regionu
	->setAddressLine('Poziom C');// * linia adresu cz.3 (dodatkowe informacje adresu)
$customer->contact()// dane kontaktowe siedziby sprzedawcy
	->setName('Imię Nazwisko')// imię i nazwisko osoby kontaktowej
	->setPhone('614621230')// numer telefonu osoby kontaktowej
	->setEmail('aaaaa@dostawca.eu');// adres e-mail osoby kontaktowej

// -----------------------------------------------------------------------------
// pozycja faktury
// nazwa pozycji
// cena netto dla pozycji
$note->line('Drzwi', 1275)
    ->setAccountingCost('Koszty001');// * pozycja kosztów u nabywcy

// -----------------------------------------------------------------------------
$note->line('Okna', 749);

// -----------------------------------------------------------------------------
$note->line('Usługa montażu', 800);

// -----------------------------------------------------------------------------
$note->line('Powietrze atm.', 500);

//
//print_r($invoice);die;
//echo $note->xml();
//Helper\CodeList::setLang('pl');
//CreditNote::$previewDateFormat = 'j.m.Y';
echo $note->html();