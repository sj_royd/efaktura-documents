<?php

include '../vendor/autoload.php';

use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Invoice;

$invoice = Invoice::parse(file_get_contents('gen_f.xml'));
//print_r($invoice);die;
//Helper\CodeList::setLang('pl');
Invoice::$previewDateFormat = 'j.m.Y';
echo $invoice->html();