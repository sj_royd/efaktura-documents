<?php

include '../vendor/autoload.php';

use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\PEPPOL as P;
use SJRoyd\PEF\BIS\Invoice;
use SJRoyd\PEF\BIS\InvoiceCorrection;

$invoice = Invoice::parse(file_get_contents('f.xml'));

$correction = new InvoiceCorrection('FK/121/8/18/MPK', '2018-08-31');
$correction
    ->setOriginInvoice($invoice)
	->setTaxPointDate('2018-09-30')// termin płatności faktury
	->setNote('registration court')// * uwagi tekstowe do faktury
	->setTaxOtherCurrency(P\Currency::EUR, 503)// * kod waluty podatku VAT
	->setAccountingCost('koszty 123')// * znak stanowiska kosztów u nabywcy
	->setBuyerReference('12345')// * referencja kupującego
	->setPeriod('2018-08-01', '2018-08-31')// * okres fakturowany
	->setDespatchReference('D12345')// * numer dokumentu awiza wysyłki
	->setReceiptReference('R12345')// * numer dokumentu potwierdzenia odbioru
	->setOriginatorReference('O12345');// * znak ogłoszenia przetargowego lub jego

// -----------------------------------------------------------------------------

$reference = $correction->additionalReference('1234', P\InvoicedObjectId::ZZZ);// * referencje do dowodu dostawy
$reference->setDescription('receiving advice');

$reference = $correction->additionalReference('Doc1', P\InvoicedObjectId::ZZZ);// * referencje do harmonogramu dostaw
$reference->setDescription('Tabela opisowa');
$reference->attachment()->setExternalUri('http://www.Sprzedawca.eu/sheet001.html');// link do harmonogramu

$reference = $correction->additionalReference('Doc2', P\InvoicedObjectId::ZZZ);// * referencje do zamówienia u dostawcy
$reference->setDescription('order supplier');
$reference->attachment()->setEmbedded(// załącznik do zamówienia
							file_get_contents('invoice-gen.php'),
							'plain/text',
							'faktura.php'
						);

// -----------------------------------------------------------------------------
// *+ sposób zapłaty
$correction->copyPaymentMeans();

// -----------------------------------------------------------------------------
// * warunki płatności
// opis warunków płatności
$correction->setPaymentTerms('2 % upustu przy płatności do 2 dni');

// -----------------------------------------------------------------------------
// * obciążenia i upusty kopiowane z faktury pierwotnej
$correction->copyAllowancesCharges();
$x = $correction->addAllowanceCharge(5, 5, P\TaxCategory::STANDARD);
$x->setCharge();

// -----------------------------------------------------------------------------
// MonetaryTotal automatycznie
$correction->total()->setPrepaidAmount(300.25);

// -----------------------------------------------------------------------------
// pozycja faktury
// nazwa pozycji
// ilość
// jednostka
// cena netto dla pozycji
// procent opodatkowania [23%, 8%, 5%]
// kod kategorii opodatkowania [VAT]
$line = $correction->line('Drzwi', 8, P\UnitCode20::UNIT, 1275, 23, P\TaxCategory::STANDARD)
    ->setNote('Oklejone taśmą')// * dodatkowa informacja lub uwaga
    ->setAccountingCost('Koszty001')// * pozycja kosztów u nabywcy
    ->setPeriod('2018-08-01', '2018-08-31')// * okres fakturowany
    ->setOrderLineReference(1)// * numer odnośnej pozycji zamówienia
    ->setDocumentReference('tajne-2', P\InvoicedObjectId::ASO);// * Referencja do dokumentu powiązanego
// * upust
// kwota upustu
$line->addAllowanceCharge(10)
    ->setAllowance()// wskaźnik upustu
    ->setBaseAmount(1000)// * niewymagana wartość bazowa do wyliczenia upustu
//    ->setMultiplierFactor(1)// * lub procent upustu
    ->setReason('zapłata gotówką', P\AllowanceReason::DISCOUNT);// * opis tekstowy powodu upustu oraz kod
//// * obciążenie
//// kwota obciążenia
$line->addAllowanceCharge(15)
    ->setCharge()// wskaźnik obciążenia
//    ->setBaseAmount(1000)// * niewymagana wartość bazowa do wyliczenia obciążenia
//    ->setMultiplierFactor(1)// * lub procent obciążenia
    ->setReason('koszty pakowania', P\ChargeReason::PACKING);// * opis tekstowy powodu obciążenia oraz kod
$line->item
    ->setDescription('Drzwi lewe, wymiar 80x210, wzmocnione, antywłamaniowe klasy C')// * opis pozycji
    ->setBuyerIdentification('ID007')// * identyfikator wg kupującego
    ->setSellerIdentification('SID008')// * identyfikator wg sprzedającego
    ->setStandardIdentification('5900070011115', P\ICD::EAN_LOCATION)// * identyfikator standardowy [np EAN]
    ->setOriginCountry(P\Country::PL)// * kod kraju pochodzenia pozycji (ISO 3166)
    ->addClassificationCode('43211503', P\ItemTypeId::MUTUALLY_DEFINED, 'v1')// * kod systemu klasyfikacji oraz kod klasyfikacji pozycji
    ->addClassificationCode('30213100-6', P\ItemTypeId::MUTUALLY_DEFINED, 'v1')//
    ->addClassificationCode('8501131148741', P\ItemTypeId::EAN)//
    ->addProperty('Kolor', 'machoń');// *+ Dodatkowe parametry pozycji
$line->price->setQuantity(2);//
$line->price->setAllowance(275);// obniżka ceny jednostkowej

// -----------------------------------------------------------------------------
$line = $correction->line('Okna', 4, P\UnitCode20::UNIT, 749, 23, P\TaxCategory::STANDARD)
    ->setAccountingCost('Koszty001')// * pozycja kosztów u nabywcy
    ->setPeriod('2018-08-01', '2018-08-31')// * okres fakturowany
    ->setOrderLineReference(2)// * numer odnośnej pozycji zamówienia
    ->setDocumentReference('AVX', P\InvoicedObjectId::ZZZ);// * Referencja do dokumentu powiązanego
// * upust
// kwota upustu
$line->addAllowanceCharge(10)
    ->setAllowance()// wskaźnik upustu
    ->setBaseAmount(1000)// * niewymagana wartość bazowa do wyliczenia upustu
//    ->setMultiplierFactor(1)// * lub procent upustu
    ->setReason('zapłata gotówką', P\AllowanceReason::DISCOUNT);// * opis tekstowy powodu upustu oraz kod
//// * obciążenie
//// kwota obciążenia
$line->addAllowanceCharge(10)
    ->setCharge()// wskaźnik obciążenia
    ->setBaseAmount(1000)// * niewymagana wartość bazowa do wyliczenia obciążenia
//    ->setMultiplierFactor(1)// * lub procent obciążenia
    ->setReason('koszty pakowania', P\ChargeReason::PACKING);// * opis tekstowy powodu obciążenia oraz kod
$line->item
    ->setDescription('Okna symetryczne, wymiar 120x80')// * opis pozycji
    ->setStandardIdentification('5900070012117', P\ICD::GTIN_GS1)// * identyfikator standardowy [np GS1 GTIN]
    ->setOriginCountry(P\Country::PL)// * kod kraju pochodzenia pozycji (ISO 3166)
    ->addProperty('Kolor', 'biały');// *+ Dodatkowe parametry pozycji
$line->price->setAllowance(120);// obniżka ceny jednostkowej

// -----------------------------------------------------------------------------
$line = $correction->line('Usługa montażu', 1, P\UnitCode20::SERVICE_UNIT, 800, 8, P\TaxCategory::STANDARD);

// -----------------------------------------------------------------------------
$line = $correction->line('Powietrze atm.', 1, P\UnitCode20::AIR_DRY_TON, 480, 0, P\TaxCategory::ZERO_RATED);

// -----------------------------------------------------------------------------
$correction->taxTotal()->subtotal['Z-0']->category
        ->setExtemptionReasonCode('ZZZ')
        ->setExtemptionReason('Zwolnienie ze względu na nieprzekroczenie 200 000 PLN obrotu');
//
//print_r($correction);die;
//echo $correction->xml();
//Helper\CodeList::setLang('pl');
$correction::$previewDateFormat = 'j.m.Y';
echo $correction->html();