<?php

include '../vendor/autoload.php';

use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Order;

$order = Order::parse(file_get_contents('order_example.xml'));
//print_r($order);die;
//Helper\CodeList::setLang('pl');
Order::$previewDateFormat = 'j.m.Y';
echo $order->html();