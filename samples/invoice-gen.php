<?php

include '../vendor/autoload.php';

use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\PEPPOL as P;
use SJRoyd\PEF\BIS\Invoice;

$invoice = new Invoice('FV/121/8/18/MPK', '2018-08-31');
$invoice
//    ->setTypeCode(P\InvoiceTypeCode::INVOICE)// * identyfikator typu faktury
	->setDueDate('2018-09-30')// termin płatności faktury
	->setNote('registration court')// * uwagi tekstowe do faktury
	->setTaxOtherCurrency(P\Currency::EUR, 501)// * kod waluty podatku VAT
	->setAccountingCost('koszty 123')// * znak stanowiska kosztów u nabywcy
	->setBuyerReference('12345')// * referencja kupującego
	->setPeriod('2018-08-01', '2018-08-31')// * okres fakturowany
	// * referencje do zamówienia
	// identyfikator (numer) zamówienia
	// numer zamówienia wg dostawcy
	->setOrderReference('123', 'SO123')
	// * referencje do faktury
	// numer faktury poprzedzającej (powiązanej)
	// data faktury poprzedzającej (powiązanej)
	->addBillingReference('F12345', '2018-06-04')
	->addBillingReference('F12345', '2018-06-04')
	->addBillingReference('F12345', '2018-06-04')
	->setDespatchReference('D12345')// * numer dokumentu awiza wysyłki
	->setReceiptReference('R12345')// * numer dokumentu potwierdzenia odbioru
	->setOriginatorReference('O12345')// * znak ogłoszenia przetargowego lub jego części
	->setContractReference('Contract321')// * numer kontraktu
	->setProjectReference('PROJ01');// * numer projektu

// -----------------------------------------------------------------------------

$reference = $invoice->additionalReference('1234', P\InvoicedObjectId::ZZZ);// * referencje do dowodu dostawy
$reference->setDescription('receiving advice');

$reference = $invoice->additionalReference('Doc1', P\InvoicedObjectId::ZZZ);// * referencje do harmonogramu dostaw
$reference->setDescription('Tabela opisowa');
$reference->attachment()->setExternalUri('http://www.Sprzedawca.eu/sheet001.html');// link do harmonogramu

$reference = $invoice->additionalReference('Doc2', P\InvoicedObjectId::ZZZ);// * referencje do zamówienia u dostawcy
$reference->setDescription('order supplier');
$reference->attachment()->setEmbedded(// załącznik do zamówienia
							file_get_contents('invoice-gen.php'),
							'plain/text',
							'faktura.php'
						);
// -----------------------------------------------------------------------------
// dane sprzedawcy
$supplier = $invoice->supplier('MPK w Poznaniu Sp. z o.o.')// nazwa sprzedawcy
	->setEndpoint('7770005132', P\EAS::VAT_PL)// identyfikator „Endpoint” – punktu dostępu w sieci (Sprzedawca) - MPK
	->setIdentification('7770005132', P\ICD::GLOBAL_AESA)// identyfikator sprzedawcy (zgodny z listą PEPPOL)
    ->addTaxScheme('PL7770005132')// numer rejestracji podatkowej VAT (NIP) sprzedawcy
    // dane podmiotu prawnego
    // nazwa rejestrowa sprzedawcy
    // identyfikator rejestrowy sprzedawcy
    // informacje: kapitał, ...
    ->setLegalEntity('MPK w Poznaniu Sp. z o.o.', '7770005132', 'Forma prawna', P\ICD::GS1_GTIN);
// adres sprzedawcy
$supplier->address(P\Country::PL)// kod kraju
	->setStreet('Głogowska')// * linia adresu cz.1 (nazwa ulicy)
	->setAdditionalStreet('131/133')// * linia adresu cz.2 (c.d. nazwa ulicy)
	->setCity('Poznań')// * nazwa miejscowości
	->setPostalCode('60-244')// * kod pocztowy
	->setCountrySubentity('Wlkp')// * nazwa regionu
	->setAddressLine('ptr. 3');// * linia adresu cz.3 (dodatkowe informacje adresu)
$supplier->contact()// dane kontaktowe siedziby sprzedawcy
	->setName('Imię Nazwisko')// imię i nazwisko osoby kontaktowej
	->setPhone('614621230')// numer telefonu osoby kontaktowej
	->setEmail('aaaaa@dostawca.eu');// adres e-mail osoby kontaktowej

// -----------------------------------------------------------------------------
// dane klienta
$customer = $invoice->customer('Miasto Poznań')// nazwa nabywcy
	->setEndpoint('7792346900', P\EAS::VAT_PL)// identyfikator „Endpoint” (zgodny z listą PEPPOL) – punktu dostępu w sieci (Odbiorca) - ZTM
	->setIdentification('2090001440', P\ICD::CERN)// identyfikator sprzedawcy (zgodny z listą PEPPOL)
    ->setTaxScheme('PL2090001440')// numer rejestracji podatkowej VAT (NIP) nabywcy (Miasto Poznań)
    // dane podmiotu prawnego
    // nazwa rejestrowa nabywcy
    // identyfikator rejestrowy nabywcy
    ->setLegalEntity('Miasto Poznań', '2090001440');
// adres nabywcy
$customer->address(P\Country::PL)// kod kraju
	->setStreet('Plac Kolegiacki') // * linia adresu cz.1 (nazwa ulicy)
	->setAdditionalStreet('17')// * linia adresu cz.2 (c.d. nazwa ulicy)
	->setCity('Poznań')// * nazwa miejscowości
	->setPostalCode('61-841')// * kod pocztowy
	->setCountrySubentity('Region')// * nazwa regionu
	->setAddressLine('Parter');// * linia adresu cz.3 (dodatkowe informacje adresu)
$customer->contact()// dane kontaktowe siedziby sprzedawcy
	->setName('Imię Nazwisko')// imię i nazwisko osoby kontaktowej
	->setPhone('614621230')// numer telefonu osoby kontaktowej
	->setEmail('aaaaa@dostawca.eu');// adres e-mail osoby kontaktowej

// -----------------------------------------------------------------------------
// * dane odbiorcy płatności
$payee = $invoice->payee('MPK w Poznaniu Sp. z o.o')// nazwa odbiorcy płatności
	->setIdentification('098740918237')// identyfikator odbiorcy płatności (zgodny z listą PEPPOL) MPK
	->setLegalEntity('6411982340');// identyfikator odbiorcy płatności

// -----------------------------------------------------------------------------
// *  dane przedstawiciela podatkowego
$taxRepresentative = $invoice->taxRepresentative('Nazwa')// nazwa przedstawiciela podatkowego
    ->setTaxScheme('PL7770020410')// NIP przedstawiciela podatk.
    ->address(P\Country::PL)
        ->setStreet('Nazwa ulicy')// * linia adresu cz.1 (nazwa ulicy)
        ->setAdditionalStreet('123/1')// * linia adresu cz.2 (c.d. nazwa ulicy)
        ->setCity('Nazwa miejscowości')// * nazwa miejscowości
        ->setPostalCode('54-321')// * kod pocztowy
        ->setCountrySubentity('Region')// * nazwa regionu
        ->setAddressLine('A');// * linia adresu cz.3 (dodatkowe informacje adresu)

// -----------------------------------------------------------------------------
// * dane odbiorcy faktury (jeśli inny niż Kupujący np. ztm) i dostawy
$invoice->delivery()
    ->setDate('2013-05-31')// * faktyczna data dostawy
    ->setReceipientName('Zarząd Transportu Miejskiego')// nazwa odbiorcy (ZTM)
    ->setLocationId('5790000436118', P\ICD::EAN_LOCATION)// identyfikator odbiorcy dostawy (zgodny z listą PEPPOL) MPK
    ->address(P\Country::PL)
        ->setStreet('ul. Matejki') // * linia adresu cz.1 (nazwa ulicy)
        ->setAdditionalStreet('59')// * linia adresu cz.2 (c.d. nazwa ulicy)
        ->setCity('Poznań')// * nazwa miejscowości
        ->setPostalCode('60-770')// * kod pocztowy
        ->setCountrySubentity('Region')// * nazwa regionu
        ->setAddressLine('Piętro');// * linia adresu cz.3 (dodatkowe informacje adresu)

// -----------------------------------------------------------------------------
// *+ sposób zapłaty
// kod typu płatności
$invoice->addPaymentMeans(P\PaymentMeans::IN_CASH, 'Tekst opisowy')
    ->setId('Payref1');// identyfikator płatności

$invoice->addPaymentMeans(P\PaymentMeans::PROMISSORY_NOTE, 'Tekst opisowy')
    ->setId('Payref1');// identyfikator płatności

// * Dane karty płatniczej
// numer karty
$invoice->addPaymentMeans(P\PaymentMeans::BANK_CARD, 'Tekst opisowy')
// nazwa wydawcy karty
    ->card('123455', 'VISA')
    ->setHolderName('Imię Nazwisko');// nazwa właściciela karty

$invoice->addPaymentMeans(P\PaymentMeans::DEBIT_TRANSFER, 'Tekst opisowy')
    ->setId('Payref1')// identyfikator płatności
// * Dane konta do przelewu
// numer konta do wpłaty należności za fakturę (np. IBAN)
    ->account('PL12123412341234121212121212')
        ->setName('Konto dla płatnosci')// nazwa konta dla płatności
        ->setInstitutionName('Bank Sp. z o.o.'); // nazwa operatora płatności

$invoice->addPaymentMeans(P\PaymentMeans::DIRECT_DEBIT, 'Tekst opisowy')
// * Dane do polecenia zapłaty
    ->mandate()
        ->setId('M01')// Niepowtarzalny identyfikator płatności przypisany przez odbiorcę płatności do określenia szczegółów polecenia zapłaty
        ->setAccountNumber('PL12123412341234121212121212');// numer obciążanego konta płatnika

// -----------------------------------------------------------------------------
// * warunki płatności
// opis warunków płatności
$invoice->setPaymentTerms('2 % upustu przy płatności do 2 dni');

// -----------------------------------------------------------------------------
// * obciążenia
// kwota obciążenia
// procent opodatkowania
// kod kategorii opodatkowania
$invoice->addAllowanceCharge(10, 23, P\TaxCategory::STANDARD)
    ->setCharge()// wskaźnik obciążenia
    ->setBaseAmount(1000)// * niewymagana wartość bazowa do wyliczenia obciążenia
//    ->setMultiplierFactor(1)// * lub procent obciążenia
    ->setReason('Koszty pakowania x', P\ChargeReason::PACKING);// * opis tekstowy powodu obciążenia oraz kod
//
//// * upusty
//// kwota upustu
//// procent opodatkowania
//// kod kategorii opodatkowania
$invoice->addAllowanceCharge(20, 23, P\TaxCategory::STANDARD)
    ->setAllowance()// wskaźnik upustu
    ->setBaseAmount(1000)//*  niewymagana wartość bazowa do wyliczenia upustu
//    ->setMultiplierFactor(2)// * lub procent upustu
    ->setReason('Zapłata gotówką x', P\AllowanceReason::NEW_OUTLET_DISCOUNT);// * opis tekstowy powodu upustu oraz kod

// -----------------------------------------------------------------------------
// TaxTotal automatycznie + dla TaxCurrency samo TaxAmount
// MonetaryTotal automatycznie
$invoice->total()->setPrepaidAmount(300);

// -----------------------------------------------------------------------------
// pozycja faktury
// nazwa pozycji
// ilość
// jednostka
// cena jednostkowa netto
// procent opodatkowania [23%, 8%, 5%]
// kod kategorii opodatkowania [VAT]
$line = $invoice->line('Drzwi', 8, P\UnitCode20::UNIT, 1275, 23, P\TaxCategory::STANDARD)
    ->setNote('Oklejone taśmą')// * dodatkowa informacja lub uwaga
    ->setAccountingCost('Koszty001')// * pozycja kosztów u nabywcy
    ->setPeriod('2018-08-01', '2018-08-31')// * okres fakturowany
    ->setOrderLineReference(1)// * numer odnośnej pozycji zamówienia
    ->setDocumentReference('tajne-2', P\InvoicedObjectId::ASO);// * Referencja do dokumentu powiązanego
// * upust
// kwota upustu
$line->addAllowanceCharge(10)
    ->setAllowance()// wskaźnik upustu
    ->setBaseAmount(1000)// * niewymagana wartość bazowa do wyliczenia upustu
//    ->setMultiplierFactor(1)// * lub procent upustu
    ->setReason('zapłata gotówką', P\AllowanceReason::DISCOUNT);// * opis tekstowy powodu upustu oraz kod
//// * obciążenie
//// kwota obciążenia
$line->addAllowanceCharge(15)
    ->setCharge()// wskaźnik obciążenia
//    ->setBaseAmount(1000)// * niewymagana wartość bazowa do wyliczenia obciążenia
//    ->setMultiplierFactor(1)// * lub procent obciążenia
    ->setReason('koszty pakowania', P\ChargeReason::PACKING);// * opis tekstowy powodu obciążenia oraz kod
$line->item
    ->setDescription('Drzwi lewe, wymiar 80x210, wzmocnione, antywłamaniowe klasy C')// * opis pozycji
    ->setBuyerIdentification('ID007')// * identyfikator wg kupującego
    ->setSellerIdentification('SID008')// * identyfikator wg sprzedającego
    ->setStandardIdentification('5900070011115', P\ICD::EAN_LOCATION)// * identyfikator standardowy [np EAN]
    ->setOriginCountry(P\Country::PL)// * kod kraju pochodzenia pozycji (ISO 3166)
    ->addClassificationCode('43211503', P\ItemTypeId::MUTUALLY_DEFINED, 'v1')// * kod systemu klasyfikacji oraz kod klasyfikacji pozycji
    ->addClassificationCode('30213100-6', P\ItemTypeId::MUTUALLY_DEFINED, 'v1')//
    ->addClassificationCode('8501131148741', P\ItemTypeId::EAN)//
    ->addProperty('Kolor', 'machoń');// *+ Dodatkowe parametry pozycji
$line->price->setQuantity(2);//
$line->price->setAllowance(275);// obniżka ceny jednostkowej

// -----------------------------------------------------------------------------
$line = $invoice->line('Okna', 4, P\UnitCode20::UNIT, 749, 23, P\TaxCategory::STANDARD)
    ->setAccountingCost('Koszty001')// * pozycja kosztów u nabywcy
    ->setPeriod('2018-08-01', '2018-08-31')// * okres fakturowany
    ->setOrderLineReference(2)// * numer odnośnej pozycji zamówienia
    ->setDocumentReference('AVX', P\InvoicedObjectId::ZZZ);// * Referencja do dokumentu powiązanego
// * upust
// kwota upustu
$line->addAllowanceCharge(10)
    ->setAllowance()// wskaźnik upustu
    ->setBaseAmount(1000)// * niewymagana wartość bazowa do wyliczenia upustu
//    ->setMultiplierFactor(1)// * lub procent upustu
    ->setReason('zapłata gotówką', P\AllowanceReason::DISCOUNT);// * opis tekstowy powodu upustu oraz kod
//// * obciążenie
//// kwota obciążenia
$line->addAllowanceCharge(10)
    ->setCharge()// wskaźnik obciążenia
    ->setBaseAmount(1000)// * niewymagana wartość bazowa do wyliczenia obciążenia
//    ->setMultiplierFactor(1)// * lub procent obciążenia
    ->setReason('koszty pakowania', P\ChargeReason::PACKING);// * opis tekstowy powodu obciążenia oraz kod
$line->item
    ->setDescription('Okna symetryczne, wymiar 120x80')// * opis pozycji
    ->setStandardIdentification('5900070012117', P\ICD::GTIN_GS1)// * identyfikator standardowy [np GS1 GTIN]
    ->setOriginCountry(P\Country::PL)// * kod kraju pochodzenia pozycji (ISO 3166)
    ->addProperty('Kolor', 'biały');// *+ Dodatkowe parametry pozycji
$line->price->setAllowance(120);// obniżka ceny jednostkowej

// -----------------------------------------------------------------------------
$line = $invoice->line('Usługa montażu', 1, P\UnitCode20::SERVICE_UNIT, 800, 8, P\TaxCategory::STANDARD);

// -----------------------------------------------------------------------------
$line = $invoice->line('Powietrze atm.', 1, P\UnitCode20::AIR_DRY_TON, 500, 0, P\TaxCategory::EXEMPT);

// -----------------------------------------------------------------------------
$invoice->taxTotal()->subtotal['E-0']->category
        ->setExtemptionReasonCode('ZZZ')
        ->setExtemptionReason('Zwolnienie ze względu na nieprzekroczenie 200 000 PLN obrotu');
//
//print_r($invoice);die;
//echo $invoice->xml();
//Helper\CodeList::setLang('pl');
$invoice::$previewDateFormat = 'j.m.Y';
echo $invoice->html();