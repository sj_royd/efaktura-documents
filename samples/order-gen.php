<?php

include '../vendor/autoload.php';

use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\PEPPOL as P;
use SJRoyd\PEF\BIS\Order;

$order = new Order('34', '2018-01-20 12:30:00');
$order
    ->setTypeCode(P\OrderTypeCode::METERED_SERVICE_INVOICE)// * identyfikator typu zamówienia
	->setNote('sąd rejestrowy, kapitał')// * uwagi tekstowe do faktury
	->setCustomerReference('12345')// * referencja klienta
	->setAccountingCost('Projekt 123')// * znak stanowiska kosztów u nabywcy
	->setValidityPeriod('2018-08-31')// * data ważności zamówienia
	->setQuotationReference('PromocjaID123') // * referencja do promocji
	->setOrderReference('RejectedOrderID123') // * referencja do wcześniejszego zamówienia
	->setOriginatorReference('MAFO') // * referencja do zamówienia
	->setContract('34322');// * numer kontraktu

// -----------------------------------------------------------------------------

$reference = $order->additionalReference('1234', P\InvoicedObjectId::ZZZ);
$reference->setDescription('receiving advice');

$reference = $order->additionalReference('Doc1', P\InvoicedObjectId::ZZZ);
$reference->setDescription('Tabela opisowa');
$reference->attachment()->setExternalUri('http://www.Sprzedawca.eu/sheet001.html');// link do harmonogramu

$reference = $order->additionalReference('Doc2', P\InvoicedObjectId::ZZZ);
$reference->setDescription('załącznik');
$reference->attachment()->setEmbedded(// załącznik do zamówienia
							file_get_contents('order-gen.php'),
							'plain/text',
							'zamówienie.php'
						);

// -----------------------------------------------------------------------------
// odbiorca
// -----------------------------------------------------------------------------
$buyer = $order->buyer('Nazwa podmiotu')// nazwa odbiorcy
	->setEndpoint('5909000840010', P\EAS::EAN)// adres w sieci (zgodny z listą PEPPOL)
	->setIdentification('7300070011115', P\ICD::GS1_GTIN)// identyfikator GLN odbiorcy
    ->setTaxScheme('PL7770020410')// numer NIP odbiorcy
    // dane podmiotu prawnego
    // nazwa rejestrowa
    // identyfikator rejestrowy
    // miasto siedziby
    ->setLegalEntity('Instytut Logistyki i Magazynowania', '000018603', null, 'Poznań', P\Country::PL);
//    ->setLegalEntity('Instytut Logistyki i Magazynowania', '000018603', null, null, P\Country::PL);
//    ->setLegalEntity('Instytut Logistyki i Magazynowania', '000018603');
// adres odbiorcy
$buyer->address(P\Country::PL)// kod kraju
	->setStreet('nazwa ulicy') // * linia adresu cz.1 (nazwa ulicy)
	->setAdditionalStreet('123/1r')// * linia adresu cz.2 (c.d. nazwa ulicy)
	->setAddressLine('III piętro')// * linia adresu cz.3 (dodatkowe informacje adresu)
	->setCity('Poznań')// * nazwa miejscowości
	->setPostalCode('61-755')// * kod pocztowy
	->setCountrySubentity('Wielkopolska');// * nazwa regionu
$buyer->contact()// dane kontaktowe odbiorcy
	->setName('Imię Nazwisko')// imię i nazwisko osoby kontaktowej
	->setPhone('123456')// numer telefonu osoby kontaktowej
	->setEmail('pelle@johnsson.se');// adres e-mail osoby kontaktowej
// -----------------------------------------------------------------------------
// dostawca
// -----------------------------------------------------------------------------
$seller = $order->seller('Moderna Produkter AB')// nazwa nabywcy
	->setEndpoint('7302347231111', P\EAS::EAN)// adres w sieci (zgodny z listą PEPPOL)
	->setIdentification('SellerPartyID123')// identyfikator  (zgodny z listą PEPPOL)
    // dane podmiotu prawnego
    // nazwa rejestrowa
    // identyfikator rejestrowy
    // miasto siedziby
    ->setLegalEntity('Moderna Produkter AB', '000018603', null, 'Stockholm', P\Country::SE);
// adres nabywcy
$seller->address(P\Country::SE)// kod kraju
	->setStreet('Kungsgatan')// * linia adresu cz.1 (nazwa ulicy)
	->setAdditionalStreet('suite12')// * linia adresu cz.2 (c.d. nazwa ulicy)
	->setCity('Stockholm')// * nazwa miejscowości
	->setPostalCode('11000')// * kod pocztowy
	->setCountrySubentity('RegionX');// * nazwa regionu
$seller->contact()// dane kontaktowe
	->setName('Imię Nazwisko')// imię i nazwisko osoby kontaktowej
	->setPhone('123456')// numer telefonu osoby kontaktowej
	->setEmail('pelle@johnsson.se');// adres e-mail osoby kontaktowej
// -----------------------------------------------------------------------------
// originator zamawiający
// -----------------------------------------------------------------------------
$originator = $order->originator('Instytut Logistyki i Magazynowania')// nazwa
        ->setIdentification('0987678321123');// Identyfikator zamawiającego (zgodny z listą PEPPOL)
$originator->contact()
    ->setName('Nazwisko i imię')
    ->setPhone('346788')
    ->setEmail('office@ilim.poznan.pl');
// -----------------------------------------------------------------------------
// customer nabywca
// -----------------------------------------------------------------------------
$customer = $order->customer('Nazwa podmiotu')// nazwa
    ->setIdentification('7300070011115')
    ->setTaxScheme('PL7770020410')
    ->setLegalEntity('Instytut Logistyki i Magazynowania', '000018603', null, 'Poznań', P\Country::PL);
$customer->address(P\Country::PL)
	->setStreet('nazwa ulicy')// * linia adresu cz.1 (nazwa ulicy)
	->setAdditionalStreet('123/1r')// * linia adresu cz.2 (c.d. nazwa ulicy)
	->setAddressLine('piętro A')// * linia adresu cz.3 (dodatkowe informacje adresu)
	->setCity('Poznań')// * nazwa miejscowości
	->setPostalCode('61-755')// * kod pocztowy
	->setCountrySubentity('Wielkopolska');// * nazwa regionu
$customer->contact()// dane kontaktowe
	->setName('Imię Nazwisko')// imię i nazwisko osoby kontaktowej
	->setPhone('346788')// numer telefonu osoby kontaktowej
	->setEmail('office@ilim.poznan.pl');// adres e-mail osoby kontaktowej
// -----------------------------------------------------------------------------
// delivery dostawa
// -----------------------------------------------------------------------------
$delivery = $order->delivery('Swedish trucking')// nazwa przewoźnika
    ->setLocationId('5909000840003', P\ICD::EAN_LOCATION)// identyfikator odbiorcy dostawy (zgodny z listą PEPPOL)
    ->setRequestedPeriod('2018-10-28', '2018-10-25');// wymagana data dostawy
$delivery->address(P\Country::PL)
    ->setStreet('Estkowskiego') // * linia adresu cz.1 (nazwa ulicy)
    ->setAdditionalStreet('6')// * linia adresu cz.2 (c.d. nazwa ulicy)
    ->setCity('Poznań')// * nazwa miejscowości
    ->setPostalCode('61-755')// * kod pocztowy
    ->setCountrySubentity('Wielkopolska');// * nazwa regionu
$delivery->party// dane przewoźnika
    ->setIdentification('67654328394567');// identyfikator przewoźnika (zgodny z listą PEPPOL)
$delivery->party->contact()
    ->setName('Per')
    ->setPhone('987098709')
    ->setEmail('bill@svetruck.se');

// -----------------------------------------------------------------------------
// * warunki dostawy
$order->deliveryTerms()
        ->setId('FOT')
        ->setSpecialTerms('CAD')
        ->setLocation('ILiM');

// -----------------------------------------------------------------------------
// * warunki płatności
// opis warunków płatności
$order->setPaymentTerms('2 % upustu przy płatności do 2 dni');

// -----------------------------------------------------------------------------
// * obciążenia
// kwota obciążenia
// procent opodatkowania
// kod kategorii opodatkowania
$order->addAllowanceCharge(100)
    ->setCharge()// wskaźnik obciążenia
    ->setReason('Transport documents');// * opis tekstowy powodu obciążenia oraz kod
//
//// * upusty
//// kwota upustu
//// procent opodatkowania
//// kod kategorii opodatkowania
$order->addAllowanceCharge(100)
    ->setAllowance()// wskaźnik upustu
//    ->setMultiplierFactor(2)// * lub procent upustu
    ->setReason('Upust od całości zamówienia');// * opis tekstowy powodu upustu oraz kod

// -----------------------------------------------------------------------------
// TaxTotal automatycznie
// AnticipatedMonetaryTotal automatycznie przewidywane sumy dla zamówienia

// -----------------------------------------------------------------------------
// pozycja faktury
// nazwa pozycji
// ilość
// jednostka
// cena jednostkowa netto
// procent opodatkowania [23%, 8%, 5%]
// kod kategorii opodatkowania [VAT]
$line = $order->line('Falu Rödfärg', 120, P\UnitCode20::LITRE, 60, 23, P\TaxCategory::STANDARD)
    ->setNote('uwaga 1')// * dodatkowa informacja lub uwaga
    ->setPartialDelivery(false)
    ->setAccountingCost('ProjectID123')// * pozycja kosztów u nabywcy
    ->setPeriod('2018-10-25', '2018-10-28')// * wymagana data dostawy
    ->setOriginator('Imię i nazwisko', 'EmployeeXXX');// * inicjujący zamówienie towaru
//// * obciążenie
//// kwota obciążenia
$line->addAllowanceCharge(120)
    ->setCharge()// wskaźnik obciążenia
    ->setBaseAmount(60)// * niewymagana wartość bazowa do wyliczenia obciążenia
//    ->setMultiplierFactor(1)// * lub procent obciążenia
    ->setReason('koszty pakowania');// * opis tekstowy powodu obciążenia oraz kod
$line->item
    ->setDescription('Czerwona kredka')// * opis pozycji
    ->setBuyerIdentification('SItemNo001')// * identyfikator wg kupującego
    ->setSellerIdentification('SItemNo001')// * identyfikator wg sprzedającego
    ->setStandardIdentification('5909000840058', P\ICD::GS1_GTIN)// * identyfikator standardowy [np EAN]
    ->addSpecificationReference('Dok1')
    ->addClassificationCode('43211503', P\ItemTypeId::GS1_POLAND)// * kod systemu klasyfikacji oraz kod klasyfikacji pozycji
    ->addProperty('typ kredki', 'akrylowa');// *+ Dodatkowe parametry pozycji
$line->price->setQuantity(1);//
$line->price->setAllowance(10);// obniżka ceny jednostkowej

// -----------------------------------------------------------------------------
$line = $order->line('Pensel 20 mm', 15, P\UnitCode20::UNIT, 20, 23, P\TaxCategory::STANDARD)
    ->setNote('uwaga 2')// * dodatkowa informacja lub uwaga
    ->setAccountingCost('ProjectID123')// * pozycja kosztów u nabywcy
    ->setPeriod('2018-10-25', '2018-10-28')// * wymagana data dostawy
    ->setOriginator('Imię i nazwisko', 'EmployeeXXX');// * inicjujący zamówienie towaru
$line->item
    ->setDescription('wkłady kolorowe')// * opis pozycji
    ->setSellerIdentification('SItemNo011')// * identyfikator wg sprzedającego
    ->setStandardIdentification('5909000840065', P\ICD::GS1_GTIN)// * identyfikator standardowy [np EAN]
    ->addSpecificationReference('Dok1')
    ->addClassificationCode('43211503', P\ItemTypeId::GS1_POLAND)// * kod systemu klasyfikacji oraz kod klasyfikacji pozycji
    ->addProperty('Width', '20mm');// *+ Dodatkowe parametry pozycji
$line->price->setQuantity(1);//
$line->price->setAllowance(5);// obniżka ceny jednostkowej
//
//print_r($order);die;
//echo $order->xml();
//Helper\CodeList::setLang('pl');
$order::$previewDateFormat = 'j.m.Y';
echo $order->html();