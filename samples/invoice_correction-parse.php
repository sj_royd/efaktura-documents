<?php

include '../vendor/autoload.php';

use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Exception;
use SJRoyd\PEF\BIS\InvoiceCorrection;

try {
    $invoice = InvoiceCorrection::parse(file_get_contents('fk.xml'));
    //print_r($invoice);die;
    //Helper\CodeList::setLang('pl');
    InvoiceCorrection::$previewDateFormat = 'j.m.Y';
//    print_r($invoice);die;
    $html = $invoice->html();
    echo $html;

} catch (Exception\EmptyDocumentException $ex) {
    echo $ex->getMessage();
} catch (Exception\WrongDocumentException $ex) {
    echo $ex->getMessage();
} catch (Exception $ex) {

}