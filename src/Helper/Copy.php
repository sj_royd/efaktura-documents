<?php

namespace SJRoyd\PEF\BIS\Helper;

interface Copy
{
    public static function copy($item);
}
