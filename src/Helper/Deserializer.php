<?php

namespace SJRoyd\PEF\BIS\Helper;

use Illuminate\Support\Arr;
use Sabre\Xml\Reader;

class Deserializer
{

    const CAST_INT = 'i';
    const CAST_FLOAT = 'f';
    const CAST_DOUBLE = 'd';
    const CAST_BOOL = 'b';

    private static $data = [];

    /**
     *
     * @param Reader $reader
     */
    public static function prepare(Reader $reader)
    {
        self::$data = self::addVals($reader->parseInnerTree());
    }

    /**
     *
     * @param type $data
     * @return type
     */
    private static function addVals($data)
    {
        $output = [];
        foreach($data as $v){
            $values = [];
            if(is_array($v['value'])){
                $values = [
                    'value' => self::addVals($v['value'])
                ];
            } else {
                $values = [
                    'value' => $v['value']
                ];
            }
            self::addAttrs($v['attributes'], $values);
            $output[$v['name']][] = $values;
        }
        return $output;
    }

    /**
     *
     * @param type $data
     * @param type $values
     */
    private static function addAttrs($data, &$values)
    {
        foreach($data as $k => $v){
            $values["@{$k}"] = $v;
        }
    }

    /**
     *
     * @param string $path
     * @param string $cast
     * @return mixed
     */
    public static function get($path, $cast = null)
    {
        $p = explode('/', $path);

        $attr = null;
        if(preg_match('~^@~', $p[count($p) -1])){
            $attr = array_pop($p);
        }
        $count = count($p) - 1;

        $arrayPath = [];

        foreach($p as $i => $part){
            $proprerty = ($attr && $i == $count) ? $attr : 'value';
            $arrayPath[] = $part.'.0.'.$proprerty;
        }

        $fullPath = implode('.', $arrayPath);

        $value = Arr::get(self::$data, $fullPath);

        return ($cast && $value) ? new $cast($value) : $value;
    }

    /**
     *
     * @param string $path
     * @return mixed
     */
    public static function getArray($path, $subpath = null)
    {
        $p = explode('/', $path);
        $count = count($p) - 1;

        $arrayPath = [];

        foreach($p as $i => $part){
            $x = ($i == $count) ? '' : '.0.value';
            $arrayPath[] = $part.$x;
        }

        $fullPath = implode('.', $arrayPath);

        $result = [];
        foreach(Arr::get(self::$data, $fullPath, []) as $v){
            $result[] = $v['value'];
        }

        if($subpath){
            $result = self::getArraySubpath($result, $subpath);
        }

        return $result;
    }

    /**
     *
     * @param array $array A result from getArray
     * @param string $path
     * @return mixed
     */
    public static function getArraySubpath($array, $path)
    {
        $p = explode('/', $path);

        $attr = null;
        if(preg_match('~^@~', $p[count($p) -1])){
            $attr = array_pop($p);
        }
        $count = count($p) - 1;

        $arrayPath = [];

        foreach($p as $i => $part){
            $proprerty = ($attr && $i == $count) ? $attr : 'value';
            $arrayPath[] = $part.'.0.'.$proprerty;
        }

        $fullPath = implode('.', $arrayPath);

        $result = [];
        foreach($array as $a){
            $result[] = Arr::get($a, $fullPath);
        }

        return $result;
    }

    /**
     *
     * @param type $val
     * @param type $type
     * @return type
     */
    public static function cast($val, $type)
    {
        if(is_null($val)){
            return;
        }
        switch($type){
            case self::CAST_INT:
                $val = (int)$val; break;
            case self::CAST_FLOAT:
                $val = (float)$val; break;
            case self::CAST_DOUBLE:
                $val = (double)$val; break;
            case self::CAST_BOOL:
                $val = $val == 'true'; break;
        }
        return $val;
    }

    public static function debug()
    {
        return self::$data;
    }
}
