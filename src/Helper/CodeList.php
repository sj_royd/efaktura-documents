<?php

namespace SJRoyd\PEF\BIS\Helper;


class CodeList
{
    /**
     * Reference name
     * @var string
     */
    private static $identifier;

    /**
     * Cache
     * @var array
     */
    private static $cache = [];

    private static $translate = 'pl';

    public static function setLang($lang)
    {
        if(file_exists(__DIR__.'/PEPPOL/translate/'.$lang)){
            self::$translate = $lang;
        }
    }

    /**
     *
     * @param string $identifier
     * @param string $findCode The code
     * @return string|array
     */
    private static function getCode($identifier, $findCode = null)
    {
        self::$identifier = $identifier;
        if(!array_key_exists($identifier, self::$cache)){
            self::$cache[$identifier]
                    = include_once __DIR__.'/PEPPOL/translate/'.self::$translate.'/'.$identifier.'.php';
        }

        if($findCode){
            if(!array_key_exists($findCode, self::$cache[$identifier])){
                return;
            }
            return self::$cache[$identifier][$findCode];
        }

		return self::$cache[$identifier];
    }

    public static function getCodesFromXml($file)
    {
        $dir = __DIR__.'/../../resource/codelist/';
        $dom = new \DOMDocument();
        $dom->load($dir.$file.'.xml');
        $result = [];

        $codes = $dom->getElementsByTagName('Code');
        foreach ($codes as $code)
        {
            $_code = $code->getElementsByTagName('Id')[0]->textContent;
            $_name = $code->getElementsByTagName('Name')[0]->textContent;

            $result[$_code] = $_name;
        }

        return $result;
    }

    /**
     *
     * @param type $list
     * @return type
     * @throws \InvalidArgumentException
     *
     */
    public static function validate($list)
    {
        $args = func_get_args();
        $code = array_pop($args);
        foreach($args as $arg){
            if($arg){
                return;
            }
        }
        $list = self::$identifier;
        throw new \InvalidArgumentException("Code ID \"{$code}\" is not valid in {$list} codes list");
    }

    /**
     * Reference name
     * @return string
     */
    public static function getRefName()
    {
        return self::$refName;
    }

    /**
     * ISO 3166-1:Alpha2 Country codes
     * Usage
     * /ubl:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cac:Country/cbc:IdentificationCode
     * /ubl:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cac:Country/cbc:IdentificationCode
     * /ubl:Invoice/cac:TaxRepresentativeParty/cac:PostalAddress/cac:Country/cbc:IdentificationCode
     * /ubl:Invoice/cac:Delivery/cac:DeliveryLocation/cac:Address/cac:Country/cbc:IdentificationCode
     * /ubl:Invoice/cac:InvoiceLine/cac:Item/cac:OriginCountry/cbc:IdentificationCode
     * /ubl:CreditNote/cac:AccountingSupplierParty/cac:Party/cac:PostalAddress/cac:Country/cbc:IdentificationCode
     * /ubl:CreditNote/cac:AccountingCustomerParty/cac:Party/cac:PostalAddress/cac:Country/cbc:IdentificationCode
     * /ubl:CreditNote/cac:TaxRepresentativeParty/cac:PostalAddress/cac:Country/cbc:IdentificationCode
     * /ubl:CreditNote/cac:Delivery/cac:DeliveryLocation/cac:Address/cac:Country/cbc:IdentificationCode
     * /ubl:CreditNote/cac:CreditNoteLine/cac:Item/cac:OriginCountry/cbc:IdentificationCode
     * @param string $code
     * @return string|array
     */
    public static function getCountryAlpha2($code = null)
    {
        return self::getCode('Country', strtoupper($code));
    }

    /**
     * ISO 4217 Currency codes
     * Usage
     * /ubl:Invoice/cbc:DocumentCurrencyCode
     * /ubl:Invoice/cbc:TaxCurrencyCode
     * /ubl:Invoice/cac:AllowanceCharge/cbc:Amount/@currencyID
     * /ubl:Invoice/cac:AllowanceCharge/cbc:BaseAmount/@currencyID
     * /ubl:Invoice/cac:TaxTotal/cbc:TaxAmount/@currencyID
     * /ubl:Invoice/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxableAmount/@currencyID
     * /ubl:Invoice/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxAmount/@currencyID
     * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount/@currencyID
     * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:TaxExclusiveAmount/@currencyID
     * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID
     * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount/@currencyID
     * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:ChargeTotalAmount/@currencyID
     * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:PrepaidAmount/@currencyID
     * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:PayableRoundingAmount/@currencyID
     * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID
     * /ubl:Invoice/cac:InvoiceLine/cbc:LineExtensionAmount/@currencyID
     * /ubl:Invoice/cac:InvoiceLine/cac:AllowanceCharge/cbc:Amount/@currencyID
     * /ubl:Invoice/cac:InvoiceLine/cac:AllowanceCharge/cbc:BaseAmount/@currencyID
     * /ubl:Invoice/cac:InvoiceLine/cac:Price/cbc:PriceAmount/@currencyID
     * /ubl:Invoice/cac:InvoiceLine/cac:Price/cac:AllowanceCharge/cbc:Amount/@currencyID
     * /ubl:Invoice/cac:InvoiceLine/cac:Price/cac:AllowanceCharge/cbc:BaseAmount/@currencyID
     * /ubl:CreditNote/cbc:DocumentCurrencyCode
     * /ubl:CreditNote/cbc:TaxCurrencyCode
     * /ubl:CreditNote/cac:AllowanceCharge/cbc:Amount/@currencyID
     * /ubl:CreditNote/cac:AllowanceCharge/cbc:BaseAmount/@currencyID
     * /ubl:CreditNote/cac:TaxTotal/cbc:TaxAmount/@currencyID
     * /ubl:CreditNote/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxableAmount/@currencyID
     * /ubl:CreditNote/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxAmount/@currencyID
     * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:LineExtensionAmount/@currencyID
     * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:TaxExclusiveAmount/@currencyID
     * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID
     * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount/@currencyID
     * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:ChargeTotalAmount/@currencyID
     * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:PrepaidAmount/@currencyID
     * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:PayableRoundingAmount/@currencyID
     * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID
     * /ubl:CreditNote/cac:CreditNoteLine/cbc:LineExtensionAmount/@currencyID
     * /ubl:CreditNote/cac:CreditNoteLine/cac:AllowanceCharge/cbc:Amount/@currencyID
     * /ubl:CreditNote/cac:CreditNoteLine/cac:AllowanceCharge/cbc:BaseAmount/@currencyID
     * /ubl:CreditNote/cac:CreditNoteLine/cac:Price/cbc:PriceAmount/@currencyID
     * /ubl:CreditNote/cac:CreditNoteLine/cac:Price/cac:AllowanceCharge/cbc:Amount/@currencyID
     * /ubl:CreditNote/cac:CreditNoteLine/cac:Price/cac:AllowanceCharge/cbc:BaseAmount/@currencyID
     * @param string $code
     * @return string|array
     */
    public static function getCurrency($code = null)
    {
        return self::getCode('Currency', strtoupper($code));
    }

    /**
     * Mime code (subset of IANA code list)
     * Usage
     * /ubl:Invoice/cac:AdditionalDocumentReference/cac:Attachment/cbc:EmbeddedDocumentBinaryObject/@mimeCode
     * /ubl:CreditNote/cac:AdditionalDocumentReference/cac:Attachment/cbc:EmbeddedDocumentBinaryObject/@mimeCode
     * @param string $code
     * @return string|array
     */
    public static function getMimeCode($code = null)
    {
        return self::getCode('MimeCode', $code);
    }

    /**
     * Credit note type code (UNCL1001 subset)
     * Usage
     * /ubl:CreditNote/cbc:CreditNoteTypeCode
     * @param string $code
     * @return string|array
     */
    public static function getCreditNote($code = null)
    {
        return self::getCode('CreditNoteTypeCode', $code);
    }

    /**
     * Invoice type code (UNCL1001 subset)
     * Usage
     * /ubl:Invoice/cbc:InvoiceTypeCode
     * @param string $code
     * @return string|array
     */
    public static function getInvoice($code = null)
    {
        return self::getCode('InvoiceTypeCode', $code);
    }

    /**
     * Order type code (UNCL1001 subset)
     * @param string $code
     * @return string|array
     */
    public static function getOrder($code = null)
    {
        return self::getCode('OrderTypeCode', $code);
    }

    /**
     * Invoiced object identifier scheme (UNCL 1153)
     * Usage
     * /ubl:Invoice/cac:AdditionalDocumentReference/cbc:ID/@schemeID
     * /ubl:Invoice/cac:InvoiceLine/cac:DocumentReference/cbc:ID/@schemeID
     * /ubl:CreditNote/cac:AdditionalDocumentReference/cbc:ID/@schemeID
     * /ubl:CreditNote/cac:CreditNoteLine/cac:DocumentReference/cbc:ID/@schemeID
     * @param string $code
     * @return string|array
     */
    public static function getInvoicedObjectIdentifier($code = null)
    {
        return self::getCode('InvoicedObjectId', strtoupper($code));
    }

    /**
     * VAT date code (UNCL2005 subset)
     * Usage
     * /ubl:Invoice/cac:InvoicePeriod/cbc:DescriptionCode
     * /ubl:CreditNote/cac:InvoicePeriod/cbc:DescriptionCode
     * @param string $code
     * @return string|array
     */
    public static function getVatDate($code = null)
    {
        return self::getCode('VatDate', $code);
    }

    /**
     * Payment means code (UNCL4461)
     * Usage
     * /ubl:Invoice/cac:PaymentMeans/cbc:PaymentMeansCode
     * /ubl:CreditNote/cac:PaymentMeans/cbc:PaymentMeansCode
     * @param string $code
     * @return string|array
     */
    public static function getPaymentMeans($code = null)
    {
        return self::getCode('PaymentMeans', strtoupper($code));
    }

    /**
     * Allowance reason codes (UNCL5189 subset)
     * Usage
     * /ubl:Invoice/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
     * /ubl:Invoice/cac:InvoiceLine/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
     * /ubl:CreditNote/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
     * /ubl:CreditNote/cac:CreditNoteLine/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
     * @param string $code
     * @return string|array
     */
    public static function getAllowanceReason($code = null)
    {
        return self::getCode('AllowanceReason', $code);
    }

    /**
     * Duty or tax or fee category code (Subset of UNCL5305)
     * Usage
     * /ubl:Invoice/cac:AllowanceCharge/cac:TaxCategory/cbc:ID
     * /ubl:Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cbc:ID
     * /ubl:Invoice/cac:InvoiceLine/cac:Item/cac:ClassifiedTaxCategory/cbc:ID
     * /ubl:CreditNote/cac:AllowanceCharge/cac:TaxCategory/cbc:ID
     * /ubl:CreditNote/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cbc:ID
     * /ubl:CreditNote/cac:CreditNoteLine/cac:Item/cac:ClassifiedTaxCategory/cbc:ID
     * @param string $code
     * @return string|array
     */
    public static function getTaxCategory($code = null)
    {
        return self::getCode('TaxCategory', strtoupper($code));
    }

    /**
     * Item type identification code (UNCL7143)
     * Usage
     * /ubl:Invoice/cac:InvoiceLine/cac:Item/cac:CommodityClassification/cbc:ItemClassificationCode/@listID
     * /ubl:CreditNote/cac:CreditNoteLine/cac:Item/cac:CommodityClassification/cbc:ItemClassificationCode/@listID
     * @param string $code
     * @return string|array
     */
    public static function getItemTypeId($code = null)
    {
        return self::getCode('ItemTypeId', strtoupper($code));
    }

    /**
     * Charge reason code (UNCL7161)
     * Usage
     * /ubl:Invoice/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
     * /ubl:Invoice/cac:InvoiceLine/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
     * /ubl:CreditNote/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
     * /ubl:CreditNote/cac:CreditNoteLine/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
     * @param string $code
     * @return string|array
     */
    public static function getChargeReason($code = null)
    {
        return self::getCode('ChargeReason', strtoupper($code));
    }

    /**
     * UN/ECE Recommandation 20
     * UN/ECE Recommandation 21
     * Usage
     * /ubl:Invoice/cac:InvoiceLine/cbc:InvoicedQuantity/@unitCode
     * /ubl:Invoice/cac:InvoiceLine/cac:Price/cbc:BaseQuantity/@unitCode
     * /ubl:CreditNote/cac:CreditNoteLine/cbc:CreditedQuantity/@unitCode
     * /ubl:CreditNote/cac:CreditNoteLine/cac:Price/cbc:BaseQuantity/@unitCode
     * @param string $code
     * @return string|array
     */
    public static function getUnitCode($code = null)
    {
        return self::getCode('UnitCode', strtoupper($code));
    }

    /**
     * ISO 6523 ICD list
     * Usage
     * /ubl:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification/cbc:ID/@schemeID
     * /ubl:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cbc:CompanyID/@schemeID
     * /ubl:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID/@schemeID
     * /ubl:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cbc:CompanyID/@schemeID
     * /ubl:Invoice/cac:PayeeParty/cac:PartyIdentification/cbc:ID/@schemeID
     * /ubl:Invoice/cac:PayeeParty/cac:PartyLegalEntity/cbc:CompanyID/@schemeID
     * /ubl:Invoice/cac:Delivery/cac:DeliveryLocation/cbc:ID/@schemeID
     * /ubl:Invoice/cac:InvoiceLine/cac:Item/cac:StandardItemIdentification/cbc:ID/@schemeID
     * /ubl:CreditNote/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification/cbc:ID/@schemeID
     * /ubl:CreditNote/cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cbc:CompanyID/@schemeID
     * /ubl:CreditNote/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID/@schemeID
     * /ubl:CreditNote/cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cbc:CompanyID/@schemeID
     * /ubl:CreditNote/cac:PayeeParty/cac:PartyIdentification/cbc:ID/@schemeID
     * /ubl:CreditNote/cac:PayeeParty/cac:PartyLegalEntity/cbc:CompanyID/@schemeID
     * /ubl:CreditNote/cac:Delivery/cac:DeliveryLocation/cbc:ID/@schemeID
     * /ubl:CreditNote/cac:CreditNoteLine/cac:Item/cac:StandardItemIdentification/cbc:ID/@schemeID
     * @param string $code
     * @return string|array
     */
    public static function getIcd($code = null)
    {
        return self::getCode('ICD', $code);
    }

    /**
     * Electronic Address Scheme (EAS)
     * Usage
     * /ubl:Invoice/cac:AccountingSupplierParty/cac:Party/cbc:EndpointID/@schemeID
     * /ubl:Invoice/cac:AccountingCustomerParty/cac:Party/cbc:EndpointID/@schemeID
     * /ubl:CreditNote/cac:AccountingSupplierParty/cac:Party/cbc:EndpointID/@schemeID
     * /ubl:CreditNote/cac:AccountingCustomerParty/cac:Party/cbc:EndpointID/@schemeID
     * @param string $code
     * @return string|array
     */
    public static function getEas($code = null)
    {
        return self::getCode('EAS', $code);
    }

    /**
     * SEPA indicator
     * Usage
     * /ubl:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification/cbc:ID/@schemeID
     * /ubl:Invoice/cac:PayeeParty/cac:PartyIdentification/cbc:ID/@schemeID
     * /ubl:CreditNote/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification/cbc:ID/@schemeID
     * /ubl:CreditNote/cac:PayeeParty/cac:PartyIdentification/cbc:ID/@schemeID
     * @param string $code
     * @return string|array
     */
    public static function getSepa($code = null)
    {
        return self::getCode('SEPA', $code);
    }

}
