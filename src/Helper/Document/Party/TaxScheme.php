<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Party;

use SJRoyd\PEF\BIS\Document\Party as P;

trait TaxScheme
{
    /**
     *
     * @param string $companyId numer rejestracji podatkowej VAT (NIP) nabywcy
     * / the VAT registration number of the customer
     * @return $this
     */
    public function setTaxScheme($companyId)
    {
        $this->taxScheme = new P\TaxScheme($companyId, 'VAT');
        return $this;
    }
}
