<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Party;

use SJRoyd\PEF\BIS\Helper;

trait Identification
{
    /**
     * Identyfikator nabywcy (zgodny z listą PEPPOL) / Customer's identifier (PePPOL Id)
     * @param string $id
     * @param string $schemeId
     * @return $this
     */
    public function setIdentification($id, $schemeId = null)
    {
        $this->identification = $id;
        $this->identificationScheme = $schemeId;
        // ICD
        Helper\CodeList::validate(
            Helper\CodeList::getIcd($schemeId),
            $schemeId
        );

        return $this;
    }
}
