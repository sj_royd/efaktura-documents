<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Party;

use SJRoyd\PEF\BIS\Document\Party as P;
use SJRoyd\PEF\BIS\Helper;

trait OrderLegalEntity
{
    /**
     * Dane rejestracyjne sprzedawcy / supplier legal registration data
     * @param string $name nazwa rejestrowa sprzedawcy / supplier registration name
     * @param string $companyId identyfikator rejestrowy sprzedawcy / supplier registration identifier
     * @param string $companyIdScheme ICD Seller legal registration identifier identification scheme identifier
     * @param string $city
     * @param string $countryCode kod kraju / Country code default PL if $city set
     * @return $this
     */
    public function setLegalEntity($name, $companyId = null, $companyIdScheme = null, $city = null, $countryCode = null)
    {
        !$this->legalEntity && $this->legalEntity = new P\LegalEntity;

        $this->legalEntity->setName($name);
        $companyId && $this->legalEntity->setCompanyId($companyId, $companyIdScheme);
        if($city && !$countryCode){
            $countryCode = Helper\PEPPOL\Country::PL;
        }
        $countryCode && $this->legalEntity->setAddress($countryCode, $city);

        return $this;
    }
}
