<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Party;

use SJRoyd\PEF\BIS\Document\Party as P;

trait Contact
{
    /**
     * dane kontaktowe nabywcy / customer contact data
     * @return P\Contact
     */
    public function contact()
    {
        !$this->contact && $this->contact = new P\Contact();
        return $this->contact;
    }
}
