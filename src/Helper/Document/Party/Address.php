<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Party;

use SJRoyd\PEF\BIS\Document\Party as P;

trait Address
{
    /**
     * adres nabywcy / customer address
     * @param string $countryId
     * @return P\Address
     */
    public function address($countryId)
    {
        !$this->address && $this->address = new P\Address($countryId);
        return $this->address;
    }
}
