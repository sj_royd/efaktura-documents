<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Party;

use SJRoyd\PEF\BIS\Helper;

trait EndPoint
{
    /**
     * identyfikator „Endpoint” (zgodny z listą PEPPOL) – punktu dostępu w sieci (Odbiorca)
     * / "Endpoint" identifier – Deliver to access point in the network
     * @param string $endpoint
     * @param string $scheme
     * @return $this
     */
    public function setEndpoint($endpoint, $scheme)
    {
        $this->endpoint = $endpoint;
        $this->endpointScheme = $scheme;
        // EAS
        Helper\CodeList::validate(
            Helper\CodeList::getEas($scheme), $scheme
        );

        return $this;
    }
}
