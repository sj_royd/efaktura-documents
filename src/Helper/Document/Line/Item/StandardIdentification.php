<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Line\Item;

use SJRoyd\PEF\BIS\Helper;

trait StandardIdentification
{
    /**
     *
     * @param string $identification
     * @param string $schemeId ICD
     * @return $this
     */
    public function setStandardIdentification($identification, $schemeId)
    {
        $this->standardIdentification = $identification;
        $this->standardIdentificationSchemeId = $schemeId;
        // ICD
        Helper\CodeList::validate(
            Helper\CodeList::getIcd($schemeId), $schemeId
        );
        return $this;
    }
}
