<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Line\Item;

trait BuyerIdentification
{
    /**
     *
     * @param string $identification
     * @return $this
     */
    public function setBuyerIdentification($identification)
    {
        $this->buyerIdentification = $identification;
        return $this;
    }
}
