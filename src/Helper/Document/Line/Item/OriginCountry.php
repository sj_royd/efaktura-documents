<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Line\Item;

use SJRoyd\PEF\BIS\Helper;

trait OriginCountry
{
    /**
     *
     * @param string $originCountry ISO3166
     * @return $this
     */
    public function setOriginCountry($originCountry)
    {
        $this->originCountry = $originCountry;
        // ISO3166
        Helper\CodeList::validate(
            Helper\CodeList::getCountryAlpha2($originCountry), $originCountry
        );
        return $this;
    }
}
