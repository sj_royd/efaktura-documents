<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Line\Item;

use SJRoyd\PEF\BIS\Helper;

trait ClassificationCode
{
    /**
     * kod systemu klasyfikacji oraz kod klasyfikacji pozycji
     * / Item classification system code and item classification code (e.g. CPV)
     * @param string $code
     * @param string $listId UNCL7143
     * @param string $listVersionId
     * @return $this
     */
    public function addClassificationCode($code, $listId, $listVersionId = null)
    {
        $this->classificationCode[] = $code;
        $this->classificationCodeListId[] = $listId;
        // UNCL7143
        Helper\CodeList::validate(
            Helper\CodeList::getItemTypeId($listId), $listId
        );
        $this->classificationCodeListVersion[] = $listVersionId;
        return $this;
    }
}
