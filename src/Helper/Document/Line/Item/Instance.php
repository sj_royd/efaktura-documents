<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Line\Item;

trait Instance
{
    /**
     *
     * @param string $reference
     * @return ItemInstance
     */
    public function addInstance()
    {
        $instance = new ItemInstance();
        $this->instances[] = $instance;
        return $instance;
    }
}
