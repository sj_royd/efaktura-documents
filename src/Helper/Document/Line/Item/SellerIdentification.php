<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Line\Item;

trait SellerIdentification
{
    /**
     *
     * @param string $identification
     * @return $this
     */
    public function setSellerIdentification($identification)
    {
        $this->sellerIdentification = $identification;
        return $this;
    }
}
