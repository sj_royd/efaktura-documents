<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Line\Item;

trait Description
{
    /**
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
}
