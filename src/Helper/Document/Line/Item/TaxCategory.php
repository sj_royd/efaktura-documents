<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Line\Item;

use SJRoyd\PEF\BIS\Document;

trait TaxCategory
{
    /**
     *
     * @param string $taxCode
     * @param int $percent
     * @return Document\TaxCategory
     */
    protected function newTaxCategory($taxCode, $percent)
    {
        return new Document\TaxCategory($taxCode, $percent);
    }

    /**
     *
     * @param string $taxCode
     * @param int $percent
     * @return Document\TaxCategory
     */
    public function taxCategory($taxCode, $percent)
    {
        !$this->taxCategory && $this->taxCategory = $this->newTaxCategory($taxCode, $percent);
        return $this->taxCategory;
    }
}
