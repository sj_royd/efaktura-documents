<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Line\Item;

trait SpecificationReference
{
    /**
     *
     * @param string $reference
     * @return $this
     */
    public function addSpecificationReference($reference)
    {
        $this->specificationReference[] = $reference;
        return $this;
    }
}
