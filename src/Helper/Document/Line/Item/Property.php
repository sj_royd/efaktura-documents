<?php

namespace SJRoyd\PEF\BIS\Helper\Document\Line\Item;

use SJRoyd\PEF\BIS\Document\Line\ItemProperty;

trait Property
{
    /**
     * Dodatkowe parametry pozycji / Additional item attributes
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function addProperty($name, $value)
    {
        $property = new ItemProperty($name, $value);
        $this->property[] = $property;
        return $this;
    }
}
