<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * Item type identification code (UNCL7143)
 * usage
 * /ubl:Invoice/cac:InvoiceLine/cac:Item/cac:CommodityClassification/cbc:ItemClassificationCode/@listID
 * /ubl:CreditNote/cac:CreditNoteLine/cac:Item/cac:CommodityClassification/cbc:ItemClassificationCode/@listID
 */
class ItemTypeId
{

    /**
     * Product version number
     */
    const PRODUCT_VERSION = 'AA';

    /**
     * Assembly
     */
    const ASSEMBLY = 'AB';

    /**
     * HIBC (Health Industry Bar Code)
     */
    const HIBC = 'AC';

    /**
     * Cold roll number
     */
    const COLD_ROLL = 'AD';

    /**
     * Hot roll number
     */
    const HOT_ROLL = 'AE';

    /**
     * Slab number
     */
    const SLAB = 'AF';

    /**
     * Software revision number
     */
    const SOFTWARE_REVISION = 'AG';

    /**
     * UPC (Universal Product Code) Consumer package code (1-5-5)
     */
    const UPC_1_5_5 = 'AH';

    /**
     * UPC (Universal Product Code) Consumer package code (1-5-5-1)
     */
    const UPC_1_5_5_1 = 'AI';

    /**
     * Sample number
     */
    const SAMPLE = 'AJ';

    /**
     * Pack number
     */
    const PACK = 'AK';

    /**
     * UPC (Universal Product Code) Shipping container code (1-2-5-5)
     */
    const UPC_1_2_5_5 = 'AL';

    /**
     * UPC (Universal Product Code)/EAN (European article number)
     */
    const UPC_EAN = 'AM';

    /**
     * UPC (Universal Product Code)/EAN (European article number)
     */
    const EAN_UPC = 'AM';

    /**
     * UPC (Universal Product Code) suffix
     */
    const UPC_SUFFIX = 'AN';

    /**
     * State label code
     */
    const STATE_LABEL = 'AO';

    /**
     * Heat number
     */
    const HEAT = 'AP';

    /**
     * Coupon number
     */
    const COUPON = 'AQ';

    /**
     * Resource number
     */
    const RESOURCE = 'AR';

    /**
     * Work task number
     */
    const WORK_TASK = 'AS';

    /**
     * Price look up number
     */
    const PLU_NUMBER = 'AT';

    /**
     * NSN (North Atlantic Treaty Organization Stock Number)
     */
    const NSN = 'AU';

    /**
     * Refined product code
     */
    const REFINED_PRODUCT = 'AV';

    /**
     * Exhibit
     */
    const EXHIBIT = 'AW';

    /**
     * End item
     */
    const END_ITEM = 'AX';

    /**
     * Federal supply classification
     */
    const FEDERAL_SUPPLY_CLASSIFICATION = 'AY';

    /**
     * Engineering data list
     */
    const ENGINEERING_DATA_LIST = 'AZ';

    /**
     * Milestone event number
     */
    const MILESTONE_EVENT = 'BA';

    /**
     * Lot number
     */
    const LOT = 'BB';

    /**
     * National drug code 4-4-2 format
     */
    const NATIONAL_DRUG_4_4_2 = 'BC';

    /**
     * National drug code 5-3-2 format
     */
    const NATIONAL_DRUG_5_3_2 = 'BD';

    /**
     * National drug code 5-4-1 format
     */
    const NATIONAL_DRUG_5_4_1 = 'BE';

    /**
     * National drug code 5-4-2 format
     */
    const NATIONAL_DRUG_5_4_2 = 'BF';

    /**
     * National drug code
     */
    const NATIONAL_DRUG = 'BG';

    /**
     * Part number
     */
    const PART = 'BH';

    /**
     * Local Stock Number (LSN)
     */
    const LSN = 'BI';

    /**
     * Next higher assembly number
     */
    const NEXT_HIGHER_ASSEMBLY = 'BJ';

    /**
     * Data category
     */
    const DATA_CATEGORY = 'BK';

    /**
     * Control number
     */
    const CONTROL = 'BL';

    /**
     * Special material identification code
     */
    const SPECIAL_MATERIAL_IDENTIFICATION = 'BM';

    /**
     * Locally assigned control number
     */
    const LOCALLY_ASSIGNED_CONTROL = 'BN';

    /**
     * Buyer's colour
     */
    const BUYERS_COLOUR = 'BO';

    /**
     * Buyer's part number
     */
    const BUYERS_PART = 'BP';

    /**
     * Variable measure product code
     */
    const VARIABLE_MEASURE_PRODUCT = 'BQ';

    /**
     * Financial phase
     */
    const FINANCIAL_PHASE = 'BR';

    /**
     * Contract breakdown
     */
    const CONTRACT_BREAKDOWN = 'BS';

    /**
     * Technical phase
     */
    const TECHNICAL_PHASE = 'BT';

    /**
     * Dye lot number
     */
    const DYE_LOT = 'BU';

    /**
     * Daily statement of activities
     */
    const DAILY_STATEMENT_OF_ACTIVITIES = 'BV';

    /**
     * Periodical statement of activities within a bilaterally agreed time period
     */
    const PERIODICAL_STATEMENT_OF_ACTIVITIES_WITHIN_A_BILATERALLY_AGREED_TIME_PERIOD = 'BW';

    /**
     * Calendar week statement of activities
     */
    const CALENDAR_WEEK_STATEMENT_OF_ACTIVITIES = 'BX';

    /**
     * Calendar month statement of activities
     */
    const CALENDAR_MONTH_STATEMENT_OF_ACTIVITIES = 'BY';

    /**
     * Original equipment number
     */
    const ORIGINAL_EQUIPMENT = 'BZ';

    /**
     * Industry commodity code
     */
    const INDUSTRY_COMMODITY = 'CC';

    /**
     * Commodity grouping
     */
    const COMMODITY_GROUPING = 'CG';

    /**
     * Colour number
     */
    const COLOUR = 'CL';

    /**
     * Contract number
     */
    const CONTRACT = 'CR';

    /**
     * Customs article number
     */
    const CUSTOMS_ARTICLE = 'CV';

    /**
     * Drawing revision number
     */
    const DRAWING_REVISION = 'DR';

    /**
     * Drawing
     */
    const DRAWING = 'DW';

    /**
     * Engineering change level
     */
    const ENGINEERING_CHANGE_LEVEL = 'EC';

    /**
     * Material code
     */
    const MATERIAL = 'EF';

    /**
     * International Article Numbering Association (EAN)
     */
    const EAN = 'EN';

    /**
     * Fish species
     */
    const FISH_SPECIES = 'FS';

    /**
     * Buyer's internal product group code
     */
    const BUYERS_INTERNAL_PRODUCT_GROUP = 'GB';

    /**
     * National product group code
     */
    const NATIONAL_PRODUCT_GROUP = 'GN';

    /**
     * General specification number
     */
    const GENERAL_SPECIFICATION = 'GS';

    /**
     * Harmonised system
     */
    const HARMONISED_SYSTEM = 'HS';

    /**
     * ISBN (International Standard Book Number)
     */
    const ISBN = 'IB';

    /**
     * Buyer's item number
     */
    const BUYERS_ITEM = 'IN';

    /**
     * ISSN (International Standard Serial Number)
     */
    const ISSN = 'IS';

    /**
     * Buyer's style number
     */
    const BUYERS_STYLE = 'IT';

    /**
     * Buyer's size code
     */
    const BUYERS_SIZE = 'IZ';

    /**
     * Machine number
     */
    const MACHINE = 'MA';

    /**
     * Manufacturer's (producer's) article number
     */
    const MANUFACTURERS_ARTICLE = 'MF';

    /**
     * Model number
     */
    const MODEL = 'MN';

    /**
     * Product/service identification number
     */
    const PRODUCT_IDENTIFICATION = 'MP';

    /**
     * Product/service identification number
     */
    const SERVICE_IDENTIFICATION = 'MP';

    /**
     * Batch number
     */
    const BATCH = 'NB';

    /**
     * Customer order number
     */
    const CUSTOMER_ORDER = 'ON';

    /**
     * Part number description
     */
    const PART_DESCRIPTION = 'PD';

    /**
     * Purchaser's order line number
     */
    const PURCHASERS_ORDER_LINE = 'PL';

    /**
     * Purchase order number
     */
    const PURCHASE_ORDER = 'PO';

    /**
     * Promotional variant number
     */
    const PROMOTIONAL_VARIANT = 'PV';

    /**
     * Buyer's qualifier for size
     */
    const BUYERS_QUALIFIER_FOR_SIZE = 'QS';

    /**
     * Returnable container number
     */
    const RETURNABLE_CONTAINER = 'RC';

    /**
     * Release number
     */
    const RELEASE = 'RN';

    /**
     * Run number
     */
    const RUN = 'RU';

    /**
     * Record keeping of model year
     */
    const RECORD_KEEPING_OF_MODEL_YEAR = 'RY';

    /**
     * Supplier's article number
     */
    const SUPPLIERS_ARTICLE = 'SA';

    /**
     * Standard group of products (mixed assortment)
     */
    const STANDARD_GROUP_OF_PRODUCTS = 'SG';

    /**
     * SKU (Stock keeping unit)
     */
    const SKU = 'SK';

    /**
     * Serial number
     */
    const SERIAL = 'SN';

    /**
     * RSK number
     */
    const RSK = 'SRS';

    /**
     * IFLS (Institut Francais du Libre Service) 5 digit product
     */
    const IFLS_5 = 'SRT';

    /**
     * IFLS (Institut Francais du Libre Service) 9 digit product
     */
    const IFLS_9 = 'SRU';

    /**
     * GS1 Global Trade Item Number
     */
    const GS1 = 'SRV';

    /**
     * EDIS (Energy Data Identification System)
     */
    const EDIS = 'SRW';

    /**
     * Slaughter number
     */
    const SLAUGHTER = 'SRX';

    /**
     * Official animal number
     */
    const ANIMAL = 'SRY';

    /**
     * Harmonized tariff schedule
     */
    const HARMONIZED_TARIFF_SCHEDULE = 'SRZ';

    /**
     * Supplier's supplier article number
     */
    const SUPPLIERS_SUPPLIER_ARTICLE = 'SS';

    /**
     * 46 Level DOT Code
     */
    const DOT_46 = 'SSA';

    /**
     * Airline Tariff 6D
     */
    const AIRLINE_TARIFF_6D = 'SSB';

    /**
     * Title 49 Code of Federal Regulations
     */
    const TITLE_49_CODE_OF_FEDERAL_REGULATIONS = 'SSC';

    /**
     * International Civil Aviation Administration code
     */
    const ICAA = 'SSD';

    /**
     * Hazardous Materials ID DOT
     */
    const HAZARDOUS_MATERIALS_ID_DOT = 'SSE';

    /**
     * Endorsement
     */
    const ENDORSEMENT = 'SSF';

    /**
     * Air Force Regulation 71-4
     */
    const AF_REGULATION_71_4 = 'SSG';

    /**
     * Breed
     */
    const BREED = 'SSH';

    /**
     * Chemical Abstract Service (CAS) registry number
     */
    const CAS_REGISTRY = 'SSI';

    /**
     * Engine model designation
     */
    const ENGINE_MODEL_DESIGNATION = 'SSJ';

    /**
     * Institutional Meat Purchase Specifications (IMPS) Number
     */
    const IMPS = 'SSK';

    /**
     * Price Look-Up code (PLU)
     */
    const PLU_CODE = 'SSL';

    /**
     * International Maritime Organization (IMO) Code
     */
    const IMO = 'SSM';

    /**
     * Bureau of Explosives 600-A (rail)
     */
    const BUREAU_OF_EXPLOSIVES_600_A = 'SSN';

    /**
     * United Nations Dangerous Goods List
     */
    const UNITED_NATIONS_DANGEROUS_GOODS_LIST = 'SSO';

    /**
     * International Code of Botanical Nomenclature (ICBN)
     */
    const ICBN = 'SSP';

    /**
     * International Code of Zoological Nomenclature (ICZN)
     */
    const ICZN = 'SSQ';

    /**
     * International Code of Nomenclature for Cultivated Plants (ICNCP)
     */
    const ICNCP = 'SSR';

    /**
     * Distributor’s article identifier
     */
    const DISTRIBUTORS_ARTICLE_IDENTIFIER = 'SSS';

    /**
     * Norwegian Classification system ENVA
     */
    const ENVA = 'SST';

    /**
     * Supplier assigned classification
     */
    const SUPPLIER_ASSIGNED_CLASSIFICATION = 'SSU';

    /**
     * Mexican classification system AMECE
     */
    const AMECE = 'SSV';

    /**
     * German classification system CCG
     */
    const CCG = 'SSW';

    /**
     * Finnish classification system EANFIN
     */
    const EANFIN = 'SSX';

    /**
     * Canadian classification system ICC
     */
    const ICC = 'SSY';

    /**
     * French classification system IFLS5
     */
    const IFLS5 = 'SSZ';

    /**
     * Style number
     */
    const STYLE = 'ST';

    /**
     * Dutch classification system CBL
     */
    const CBL = 'STA';

    /**
     * Japanese classification system JICFS
     */
    const JICFS = 'STB';

    /**
     * European Union dairy subsidy eligibility classification
     */
    const EU_DAIRY_SUBSIDY_ELIGIBILITY_CLASSIFICATION = 'STC';

    /**
     * GS1 Spain classification system
     */
    const GS1_SPAIN = 'STD';

    /**
     * GS1 Poland classification system
     */
    const GS1_POLAND = 'STE';

    /**
     * Federal Agency on Technical Regulating and Metrology of the Russian Federation
     */
    const ROSSTANDART = 'STF';

    /**
     * Efficient Consumer Response (ECR) Austria classification system
     */
    const ECR_AUSTRIA = 'STG';

    /**
     * GS1 Italy classification system
     */
    const GS1_ITALY = 'STH';

    /**
     * CPV (Common Procurement Vocabulary)
     */
    const CPV = 'STI';

    /**
     * IFDA (International Foodservice Distributors Association)
     */
    const IFDA = 'STJ';

    /**
     * AHFS (American Hospital Formulary Service) pharmacologic -therapeutic
                classification
     */
    const AHFS = 'STK';

    /**
     * ATC (Anatomical Therapeutic Chemical) classification system
     */
    const ATC = 'STL';

    /**
     * CLADIMED (Classification des Dispositifs Médicaux)
     */
    const CLADIMED = 'STM';

    /**
     * CMDR (Canadian Medical Device Regulations) classification system
     */
    const CMDR = 'STN';

    /**
     * CNDM (Classificazione Nazionale dei Dispositivi Medici)
     */
    const CNDM = 'STO';

    /**
     * UK DM&D (Dictionary of Medicines & Devices) standard coding scheme
     */
    const UK_DMD = 'STP';

    /**
     * eCl@ss
     */
    const ECLASS = 'STQ';

    /**
     * EDMA (European Diagnostic Manufacturers Association)
     */
    const EDMA = 'STR';

    /**
     * EGAR (European Generic Article Register)
     */
    const EGAR = 'STS';

    /**
     * GMDN (Global Medical Devices Nomenclature)
     */
    const GMDN = 'STT';

    /**
     * GPI (Generic Product Identifier)
     */
    const GPI = 'STU';

    /**
     * HCPCS (Healthcare Common Procedure Coding System)
     */
    const HCPCS = 'STV';

    /**
     * ICPS (International Classification for Patient Safety)
     */
    const ICPS = 'STW';

    /**
     * MedDRA (Medical Dictionary for Regulatory Activities)
     */
    const MEDDRA = 'STX';

    /**
     * Medical Columbus
     */
    const MEDICAL_COLUMBUS = 'STY';

    /**
     * NAPCS (North American Product Classification System)
     */
    const NAPCS = 'STZ';

    /**
     * NHS (National Health Services) eClass
     */
    const NHS = 'SUA';

    /**
     * US FDA (Food and Drug Administration) Product Code Classification Database
     */
    const US_FDA = 'SUB';

    /**
     * SNOMED CT (Systematized Nomenclature of Medicine-Clinical Terms)
     */
    const SNOMED_CT = 'SUC';

    /**
     * UMDNS (Universal Medical Device Nomenclature System)
     */
    const UMDNS = 'SUD';

    /**
     * GS1 Global Returnable Asset Identifier, non-serialised
     */
    const GS1_GLOBAL = 'SUE';

    /**
     * IMEI
     */
    const IMEI = 'SUF';

    /**
     * Waste Type (EMSA)
     */
    const EMSA = 'SUG';

    /**
     * Ship's store classification type
     */
    const SHIPS_STORE_CLASSIFICATION_TYPE = 'SUH';

    /**
     * Emergency fire code
     */
    const EMERGENCY_FIRE = 'SUI';

    /**
     * Emergency spillage code
     */
    const EMERGENCY_SPILLAGE = 'SUJ';

    /**
     * IMDG packing group
     */
    const IMDG_PACKING_GROUP = 'SUK';

    /**
     * MARPOL Code IBC
     */
    const MARPOL_IBC = 'SUL';

    /**
     * IMDG subsidiary risk class
     */
    const IMDG_SUBSIDIARY_RISK_CLASS = 'SUM';

    /**
     * Transport group number
     */
    const TRANSPORT_GROUP = 'TG';

    /**
     * Taxonomic Serial Number
     */
    const TAXONOMIC_SERIAL = 'TSN';

    /**
     * IMDG main hazard class
     */
    const IMDG_MAIN_HAZARD_CLASS = 'TSO';

    /**
     * EU Combined Nomenclature
     */
    const EU_COMBINED_NOMENCLATURE = 'TSP';

    /**
     * Ultimate customer's article number
     */
    const ULTIMATE_CUSTOMERS_ARTICLE = 'UA';

    /**
     * UPC (Universal product code)
     */
    const UPC = 'UP';

    /**
     * Vendor item number
     */
    const VENDOR_ITEM = 'VN';

    /**
     * Vendor's (seller's) part number
     */
    const VENDOR_PART = 'VP';

    /**
     * Vendor's supplemental item number
     */
    const VENDORS_SUPPLEMENTAL_ITEM = 'VS';

    /**
     * Vendor specification number
     */
    const VENDOR_SPECIFICATION = 'VX';

    /**
     * Mutually defined
     */
    const MUTUALLY_DEFINED = 'ZZZ';


}
