<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * Invoiced object identifier scheme (UNCL 1153)
 * usage
 * /ubl:Invoice/cac:AdditionalDocumentReference/cbc:ID/@schemeID
 * /ubl:Invoice/cac:InvoiceLine/cac:DocumentReference/cbc:ID/@schemeID
 * /ubl:CreditNote/cac:AdditionalDocumentReference/cbc:ID/@schemeID
 * /ubl:CreditNote/cac:CreditNoteLine/cac:DocumentReference/cbc:ID/@schemeID
 */
class InvoicedObjectId
{
    /**
    * Order acknowledgement document identifier
    */
   const AAA = 'AAA';

   /**
    * Proforma invoice document identifier
    */
   const AAB = 'AAB';

   /**
    * Documentary credit identifier
    */
   const AAC = 'AAC';

   /**
    * Contract document addendum identifier
    */
   const AAD = 'AAD';

   /**
    * Goods declaration number
    */
   const AAE = 'AAE';

   /**
    * Debit card number
    */
   const AAF = 'AAF';

   /**
    * Offer number
    */
   const AAG = 'AAG';

   /**
    * Bank's batch interbank transaction reference number
    */
   const AAH = 'AAH';

   /**
    * Bank's individual interbank transaction reference number
    */
   const AAI = 'AAI';

   /**
    * Delivery order number
    */
   const AAJ = 'AAJ';

   /**
    * Despatch advice number
    */
   const AAK = 'AAK';

   /**
    * Drawing number
    */
   const AAL = 'AAL';

   /**
    * Waybill number
    */
   const AAM = 'AAM';

   /**
    * Delivery schedule number
    */
   const AAN = 'AAN';

   /**
    * Consignment identifier, consignee assigned
    */
   const AAO = 'AAO';

   /**
    * Partial shipment identifier
    */
   const AAP = 'AAP';

   /**
    * Transport equipment identifier
    */
   const AAQ = 'AAQ';

   /**
    * Municipality assigned business registry number
    */
   const AAR = 'AAR';

   /**
    * Transport contract document identifier
    */
   const AAS = 'AAS';

   /**
    * Master label number
    */
   const AAT = 'AAT';

   /**
    * Despatch note document identifier
    */
   const AAU = 'AAU';

   /**
    * Enquiry number
    */
   const AAV = 'AAV';

   /**
    * Docket number
    */
   const AAW = 'AAW';

   /**
    * Civil action number
    */
   const AAX = 'AAX';

   /**
    * Carrier's agent reference number
    */
   const AAY = 'AAY';

   /**
    * Standard Carrier Alpha Code (SCAC) number
    */
   const AAZ = 'AAZ';

   /**
    * Customs valuation decision number
    */
   const ABA = 'ABA';

   /**
    * End use authorization number
    */
   const ABB = 'ABB';

   /**
    * Anti-dumping case number
    */
   const ABC = 'ABC';

   /**
    * Customs tariff number
    */
   const ABD = 'ABD';

   /**
    * Declarant's reference number
    */
   const ABE = 'ABE';

   /**
    * Repair estimate number
    */
   const ABF = 'ABF';

   /**
    * Customs decision request number
    */
   const ABG = 'ABG';

   /**
    * Sub-house bill of lading number
    */
   const ABH = 'ABH';

   /**
    * Tax payment identifier
    */
   const ABI = 'ABI';

   /**
    * Quota number
    */
   const ABJ = 'ABJ';

   /**
    * Transit (onward carriage) guarantee (bond) number
    */
   const ABK = 'ABK';

   /**
    * Customs guarantee number
    */
   const ABL = 'ABL';

   /**
    * Replacing part number
    */
   const ABM = 'ABM';

   /**
    * Seller's catalogue number
    */
   const ABN = 'ABN';

   /**
    * Originator's reference
    */
   const ABO = 'ABO';

   /**
    * Declarant's Customs identity number
    */
   const ABP = 'ABP';

   /**
    * Importer reference number
    */
   const ABQ = 'ABQ';

   /**
    * Export clearance instruction reference number
    */
   const ABR = 'ABR';

   /**
    * Import clearance instruction reference number
    */
   const ABS = 'ABS';

   /**
    * Goods declaration document identifier, Customs
    */
   const ABT = 'ABT';

   /**
    * Article number
    */
   const ABU = 'ABU';

   /**
    * Intra-plant routing
    */
   const ABV = 'ABV';

   /**
    * Stock keeping unit number
    */
   const ABW = 'ABW';

   /**
    * Text Element Identifier deletion reference
    */
   const ABX = 'ABX';

   /**
    * Allotment identification (Air)
    */
   const ABY = 'ABY';

   /**
    * Vehicle licence number
    */
   const ABZ = 'ABZ';

   /**
    * Air cargo transfer manifest
    */
   const AC = 'AC';

   /**
    * Cargo acceptance order reference number
    */
   const ACA = 'ACA';

   /**
    * US government agency number
    */
   const ACB = 'ACB';

   /**
    * Shipping unit identification
    */
   const ACC = 'ACC';

   /**
    * Additional reference number
    */
   const ACD = 'ACD';

   /**
    * Related document number
    */
   const ACE = 'ACE';

   /**
    * Addressee reference
    */
   const ACF = 'ACF';

   /**
    * ATA carnet number
    */
   const ACG = 'ACG';

   /**
    * Packaging unit identification
    */
   const ACH = 'ACH';

   /**
    * Outerpackaging unit identification
    */
   const ACI = 'ACI';

   /**
    * Customer material specification number
    */
   const ACJ = 'ACJ';

   /**
    * Bank reference
    */
   const ACK = 'ACK';

   /**
    * Principal reference number
    */
   const ACL = 'ACL';

   /**
    * Collection advice document identifier
    */
   const ACN = 'ACN';

   /**
    * Iron charge number
    */
   const ACO = 'ACO';

   /**
    * Hot roll number
    */
   const ACP = 'ACP';

   /**
    * Cold roll number
    */
   const ACQ = 'ACQ';

   /**
    * Railway wagon number
    */
   const ACR = 'ACR';

   /**
    * Unique claims reference number of the sender
    */
   const ACT = 'ACT';

   /**
    * Loss/event number
    */
   const ACU = 'ACU';

   /**
    * Estimate order reference number
    */
   const ACV = 'ACV';

   /**
    * Reference number to previous message
    */
   const ACW = 'ACW';

   /**
    * Banker's acceptance
    */
   const ACX = 'ACX';

   /**
    * Duty memo number
    */
   const ACY = 'ACY';

   /**
    * Equipment transport charge number
    */
   const ACZ = 'ACZ';

   /**
    * Buyer's item number
    */
   const ADA = 'ADA';

   /**
    * Matured certificate of deposit
    */
   const ADB = 'ADB';

   /**
    * Loan
    */
   const ADC = 'ADC';

   /**
    * Analysis number/test number
    */
   const ADD = 'ADD';

   /**
    * Account number
    */
   const ADE = 'ADE';

   /**
    * Treaty number
    */
   const ADF = 'ADF';

   /**
    * Catastrophe number
    */
   const ADG = 'ADG';

   /**
    * Bureau signing (statement reference)
    */
   const ADI = 'ADI';

   /**
    * Company / syndicate reference 1
    */
   const ADJ = 'ADJ';

   /**
    * Company / syndicate reference 2
    */
   const ADK = 'ADK';

   /**
    * Ordering customer consignment reference number
    */
   const ADL = 'ADL';

   /**
    * Shipowner's authorization number
    */
   const ADM = 'ADM';

   /**
    * Inland transport order number
    */
   const ADN = 'ADN';

   /**
    * Container work order reference number
    */
   const ADO = 'ADO';

   /**
    * Statement number
    */
   const ADP = 'ADP';

   /**
    * Unique market reference
    */
   const ADQ = 'ADQ';

   /**
    * Group accounting
    */
   const ADT = 'ADT';

   /**
    * Broker reference 1
    */
   const ADU = 'ADU';

   /**
    * Broker reference 2
    */
   const ADV = 'ADV';

   /**
    * Lloyd's claims office reference
    */
   const ADW = 'ADW';

   /**
    * Secure delivery terms and conditions agreement reference
    */
   const ADX = 'ADX';

   /**
    * Report number
    */
   const ADY = 'ADY';

   /**
    * Trader account number
    */
   const ADZ = 'ADZ';

   /**
    * Authorization for expense (AFE) number
    */
   const AE = 'AE';

   /**
    * Government agency reference number
    */
   const AEA = 'AEA';

   /**
    * Assembly number
    */
   const AEB = 'AEB';

   /**
    * Symbol number
    */
   const AEC = 'AEC';

   /**
    * Commodity number
    */
   const AED = 'AED';

   /**
    * Eur 1 certificate number
    */
   const AEE = 'AEE';

   /**
    * Customer process specification number
    */
   const AEF = 'AEF';

   /**
    * Customer specification number
    */
   const AEG = 'AEG';

   /**
    * Applicable instructions or standards
    */
   const AEH = 'AEH';

   /**
    * Registration number of previous Customs declaration
    */
   const AEI = 'AEI';

   /**
    * Post-entry reference
    */
   const AEJ = 'AEJ';

   /**
    * Payment order number
    */
   const AEK = 'AEK';

   /**
    * Delivery number (transport)
    */
   const AEL = 'AEL';

   /**
    * Transport route
    */
   const AEM = 'AEM';

   /**
    * Customer's unit inventory number
    */
   const AEN = 'AEN';

   /**
    * Product reservation number
    */
   const AEO = 'AEO';

   /**
    * Project number
    */
   const AEP = 'AEP';

   /**
    * Drawing list number
    */
   const AEQ = 'AEQ';

   /**
    * Project specification number
    */
   const AER = 'AER';

   /**
    * Primary reference
    */
   const AES = 'AES';

   /**
    * Request for cancellation number
    */
   const AET = 'AET';

   /**
    * Supplier's control number
    */
   const AEU = 'AEU';

   /**
    * Shipping note number
    */
   const AEV = 'AEV';

   /**
    * Empty container bill number
    */
   const AEW = 'AEW';

   /**
    * Non-negotiable maritime transport document number
    */
   const AEX = 'AEX';

   /**
    * Substitute air waybill number
    */
   const AEY = 'AEY';

   /**
    * Despatch note (post parcels) number
    */
   const AEZ = 'AEZ';

   /**
    * Airlines flight identification number
    */
   const AF = 'AF';

   /**
    * Through bill of lading number
    */
   const AFA = 'AFA';

   /**
    * Cargo manifest number
    */
   const AFB = 'AFB';

   /**
    * Bordereau number
    */
   const AFC = 'AFC';

   /**
    * Customs item number
    */
   const AFD = 'AFD';

   /**
    * Export Control Commodity number (ECCN)
    */
   const AFE = 'AFE';

   /**
    * Marking/label reference
    */
   const AFF = 'AFF';

   /**
    * Tariff number
    */
   const AFG = 'AFG';

   /**
    * Replenishment purchase order number
    */
   const AFH = 'AFH';

   /**
    * Immediate transportation no. for in bond movement
    */
   const AFI = 'AFI';

   /**
    * Transportation exportation no. for in bond movement
    */
   const AFJ = 'AFJ';

   /**
    * Immediate exportation no. for in bond movement
    */
   const AFK = 'AFK';

   /**
    * Associated invoices
    */
   const AFL = 'AFL';

   /**
    * Secondary Customs reference
    */
   const AFM = 'AFM';

   /**
    * Account party's reference
    */
   const AFN = 'AFN';

   /**
    * Beneficiary's reference
    */
   const AFO = 'AFO';

   /**
    * Second beneficiary's reference
    */
   const AFP = 'AFP';

   /**
    * Applicant's bank reference
    */
   const AFQ = 'AFQ';

   /**
    * Issuing bank's reference
    */
   const AFR = 'AFR';

   /**
    * Beneficiary's bank reference
    */
   const AFS = 'AFS';

   /**
    * Direct payment valuation number
    */
   const AFT = 'AFT';

   /**
    * Direct payment valuation request number
    */
   const AFU = 'AFU';

   /**
    * Quantity valuation number
    */
   const AFV = 'AFV';

   /**
    * Quantity valuation request number
    */
   const AFW = 'AFW';

   /**
    * Bill of quantities number
    */
   const AFX = 'AFX';

   /**
    * Payment valuation number
    */
   const AFY = 'AFY';

   /**
    * Situation number
    */
   const AFZ = 'AFZ';

   /**
    * Agreement to pay number
    */
   const AGA = 'AGA';

   /**
    * Contract party reference number
    */
   const AGB = 'AGB';

   /**
    * Account party's bank reference
    */
   const AGC = 'AGC';

   /**
    * Agent's bank reference
    */
   const AGD = 'AGD';

   /**
    * Agent's reference
    */
   const AGE = 'AGE';

   /**
    * Applicant's reference
    */
   const AGF = 'AGF';

   /**
    * Dispute number
    */
   const AGG = 'AGG';

   /**
    * Credit rating agency's reference number
    */
   const AGH = 'AGH';

   /**
    * Request number
    */
   const AGI = 'AGI';

   /**
    * Single transaction sequence number
    */
   const AGJ = 'AGJ';

   /**
    * Application reference number
    */
   const AGK = 'AGK';

   /**
    * Delivery verification certificate
    */
   const AGL = 'AGL';

   /**
    * Number of temporary importation document
    */
   const AGM = 'AGM';

   /**
    * Reference number quoted on statement
    */
   const AGN = 'AGN';

   /**
    * Sender's reference to the original message
    */
   const AGO = 'AGO';

   /**
    * Company issued equipment ID
    */
   const AGP = 'AGP';

   /**
    * Domestic flight number
    */
   const AGQ = 'AGQ';

   /**
    * International flight number
    */
   const AGR = 'AGR';

   /**
    * Employer identification number of service bureau
    */
   const AGS = 'AGS';

   /**
    * Service group identification number
    */
   const AGT = 'AGT';

   /**
    * Member number
    */
   const AGU = 'AGU';

   /**
    * Previous member number
    */
   const AGV = 'AGV';

   /**
    * Scheme/plan number
    */
   const AGW = 'AGW';

   /**
    * Previous scheme/plan number
    */
   const AGX = 'AGX';

   /**
    * Receiving party's member identification
    */
   const AGY = 'AGY';

   /**
    * Payroll number
    */
   const AGZ = 'AGZ';

   /**
    * Packaging specification number
    */
   const AHA = 'AHA';

   /**
    * Authority issued equipment identification
    */
   const AHB = 'AHB';

   /**
    * Training flight number
    */
   const AHC = 'AHC';

   /**
    * Fund code number
    */
   const AHD = 'AHD';

   /**
    * Signal code number
    */
   const AHE = 'AHE';

   /**
    * Major force program number
    */
   const AHF = 'AHF';

   /**
    * Nomination number
    */
   const AHG = 'AHG';

   /**
    * Laboratory registration number
    */
   const AHH = 'AHH';

   /**
    * Transport contract reference number
    */
   const AHI = 'AHI';

   /**
    * Payee's reference number
    */
   const AHJ = 'AHJ';

   /**
    * Payer's reference number
    */
   const AHK = 'AHK';

   /**
    * Creditor's reference number
    */
   const AHL = 'AHL';

   /**
    * Debtor's reference number
    */
   const AHM = 'AHM';

   /**
    * Joint venture reference number
    */
   const AHN = 'AHN';

   /**
    * Chamber of Commerce registration number
    */
   const AHO = 'AHO';

   /**
    * Tax registration number
    */
   const AHP = 'AHP';

   /**
    * Wool identification number
    */
   const AHQ = 'AHQ';

   /**
    * Wool tax reference number
    */
   const AHR = 'AHR';

   /**
    * Meat processing establishment registration number
    */
   const AHS = 'AHS';

   /**
    * Quarantine/treatment status reference number
    */
   const AHT = 'AHT';

   /**
    * Request for quote number
    */
   const AHU = 'AHU';

   /**
    * Manual processing authority number
    */
   const AHV = 'AHV';

   /**
    * Rate note number
    */
   const AHX = 'AHX';

   /**
    * Freight Forwarder number
    */
   const AHY = 'AHY';

   /**
    * Customs release code
    */
   const AHZ = 'AHZ';

   /**
    * Compliance code number
    */
   const AIA = 'AIA';

   /**
    * Department of transportation bond number
    */
   const AIB = 'AIB';

   /**
    * Export establishment number
    */
   const AIC = 'AIC';

   /**
    * Certificate of conformity
    */
   const AID = 'AID';

   /**
    * Ministerial certificate of homologation
    */
   const AIE = 'AIE';

   /**
    * Previous delivery instruction number
    */
   const AIF = 'AIF';

   /**
    * Passport number
    */
   const AIG = 'AIG';

   /**
    * Common transaction reference number
    */
   const AIH = 'AIH';

   /**
    * Bank's common transaction reference number
    */
   const AII = 'AII';

   /**
    * Customer's individual transaction reference number
    */
   const AIJ = 'AIJ';

   /**
    * Bank's individual transaction reference number
    */
   const AIK = 'AIK';

   /**
    * Customer's common transaction reference number
    */
   const AIL = 'AIL';

   /**
    * Individual transaction reference number
    */
   const AIM = 'AIM';

   /**
    * Product sourcing agreement number
    */
   const AIN = 'AIN';

   /**
    * Customs transhipment number
    */
   const AIO = 'AIO';

   /**
    * Customs preference inquiry number
    */
   const AIP = 'AIP';

   /**
    * Packing plant number
    */
   const AIQ = 'AIQ';

   /**
    * Original certificate number
    */
   const AIR = 'AIR';

   /**
    * Processing plant number
    */
   const AIS = 'AIS';

   /**
    * Slaughter plant number
    */
   const AIT = 'AIT';

   /**
    * Charge card account number
    */
   const AIU = 'AIU';

   /**
    * Event reference number
    */
   const AIV = 'AIV';

   /**
    * Transport section reference number
    */
   const AIW = 'AIW';

   /**
    * Referred product for mechanical analysis
    */
   const AIX = 'AIX';

   /**
    * Referred product for chemical analysis
    */
   const AIY = 'AIY';

   /**
    * Consolidated invoice number
    */
   const AIZ = 'AIZ';

   /**
    * Part reference indicator in a drawing
    */
   const AJA = 'AJA';

   /**
    * U.S. Code of Federal Regulations (CFR)
    */
   const AJB = 'AJB';

   /**
    * Purchasing activity clause number
    */
   const AJC = 'AJC';

   /**
    * U.S. Defense Federal Acquisition Regulation Supplement
    */
   const AJD = 'AJD';

   /**
    * Agency clause number
    */
   const AJE = 'AJE';

   /**
    * Circular publication number
    */
   const AJF = 'AJF';

   /**
    * U.S. Federal Acquisition Regulation
    */
   const AJG = 'AJG';

   /**
    * U.S. General Services Administration Regulation
    */
   const AJH = 'AJH';

   /**
    * U.S. Federal Information Resources Management Regulation
    */
   const AJI = 'AJI';

   /**
    * Paragraph
    */
   const AJJ = 'AJJ';

   /**
    * Special instructions number
    */
   const AJK = 'AJK';

   /**
    * Site specific procedures, terms, and conditions number
    */
   const AJL = 'AJL';

   /**
    * Master solicitation procedures, terms, and conditions
    */
   const AJM = 'AJM';

   /**
    * U.S. Department of Veterans Affairs Acquisition Regulation
    */
   const AJN = 'AJN';

   /**
    * Military Interdepartmental Purchase Request (MIPR) number
    */
   const AJO = 'AJO';

   /**
    * Foreign military sales number
    */
   const AJP = 'AJP';

   /**
    * Defense priorities allocation system priority rating
    */
   const AJQ = 'AJQ';

   /**
    * Wage determination number
    */
   const AJR = 'AJR';

   /**
    * Agreement number
    */
   const AJS = 'AJS';

   /**
    * Standard Industry Classification (SIC) number
    */
   const AJT = 'AJT';

   /**
    * End item number
    */
   const AJU = 'AJU';

   /**
    * Federal supply schedule item number
    */
   const AJV = 'AJV';

   /**
    * Technical document number
    */
   const AJW = 'AJW';

   /**
    * Technical order number
    */
   const AJX = 'AJX';

   /**
    * Suffix
    */
   const AJY = 'AJY';

   /**
    * Transportation account number
    */
   const AJZ = 'AJZ';

   /**
    * Container disposition order reference number
    */
   const AKA = 'AKA';

   /**
    * Container prefix
    */
   const AKB = 'AKB';

   /**
    * Transport equipment return reference
    */
   const AKC = 'AKC';

   /**
    * Transport equipment survey reference
    */
   const AKD = 'AKD';

   /**
    * Transport equipment survey report number
    */
   const AKE = 'AKE';

   /**
    * Transport equipment stuffing order
    */
   const AKF = 'AKF';

   /**
    * Vehicle Identification Number (VIN)
    */
   const AKG = 'AKG';

   /**
    * Government bill of lading
    */
   const AKH = 'AKH';

   /**
    * Ordering customer's second reference number
    */
   const AKI = 'AKI';

   /**
    * Direct debit reference
    */
   const AKJ = 'AKJ';

   /**
    * Meter reading at the beginning of the delivery
    */
   const AKK = 'AKK';

   /**
    * Meter reading at the end of delivery
    */
   const AKL = 'AKL';

   /**
    * Replenishment purchase order range start number
    */
   const AKM = 'AKM';

   /**
    * Third bank's reference
    */
   const AKN = 'AKN';

   /**
    * Action authorization number
    */
   const AKO = 'AKO';

   /**
    * Appropriation number
    */
   const AKP = 'AKP';

   /**
    * Product change authority number
    */
   const AKQ = 'AKQ';

   /**
    * General cargo consignment reference number
    */
   const AKR = 'AKR';

   /**
    * Catalogue sequence number
    */
   const AKS = 'AKS';

   /**
    * Forwarding order number
    */
   const AKT = 'AKT';

   /**
    * Transport equipment survey reference number
    */
   const AKU = 'AKU';

   /**
    * Lease contract reference
    */
   const AKV = 'AKV';

   /**
    * Transport costs reference number
    */
   const AKW = 'AKW';

   /**
    * Transport equipment stripping order
    */
   const AKX = 'AKX';

   /**
    * Prior policy number
    */
   const AKY = 'AKY';

   /**
    * Policy number
    */
   const AKZ = 'AKZ';

   /**
    * Procurement budget number
    */
   const ALA = 'ALA';

   /**
    * Domestic inventory management code
    */
   const ALB = 'ALB';

   /**
    * Customer reference number assigned to previous balance of
    */
   const ALC = 'ALC';

   /**
    * Previous credit advice reference number
    */
   const ALD = 'ALD';

   /**
    * Reporting form number
    */
   const ALE = 'ALE';

   /**
    * Authorization number for exception to dangerous goods
    */
   const ALF = 'ALF';

   /**
    * Dangerous goods security number
    */
   const ALG = 'ALG';

   /**
    * Dangerous goods transport licence number
    */
   const ALH = 'ALH';

   /**
    * Previous rental agreement number
    */
   const ALI = 'ALI';

   /**
    * Next rental agreement reason number
    */
   const ALJ = 'ALJ';

   /**
    * Consignee's invoice number
    */
   const ALK = 'ALK';

   /**
    * Message batch number
    */
   const ALL = 'ALL';

   /**
    * Previous delivery schedule number
    */
   const ALM = 'ALM';

   /**
    * Physical inventory recount reference number
    */
   const ALN = 'ALN';

   /**
    * Receiving advice number
    */
   const ALO = 'ALO';

   /**
    * Returnable container reference number
    */
   const ALP = 'ALP';

   /**
    * Returns notice number
    */
   const ALQ = 'ALQ';

   /**
    * Sales forecast number
    */
   const ALR = 'ALR';

   /**
    * Sales report number
    */
   const ALS = 'ALS';

   /**
    * Previous tax control number
    */
   const ALT = 'ALT';

   /**
    * AGERD (Aerospace Ground Equipment Requirement Data) number
    */
   const ALU = 'ALU';

   /**
    * Registered capital reference
    */
   const ALV = 'ALV';

   /**
    * Standard number of inspection document
    */
   const ALW = 'ALW';

   /**
    * Model
    */
   const ALX = 'ALX';

   /**
    * Financial management reference
    */
   const ALY = 'ALY';

   /**
    * NOTIfication for COLlection number (NOTICOL)
    */
   const ALZ = 'ALZ';

   /**
    * Previous request for metered reading reference number
    */
   const AMA = 'AMA';

   /**
    * Next rental agreement number
    */
   const AMB = 'AMB';

   /**
    * Reference number of a request for metered reading
    */
   const AMC = 'AMC';

   /**
    * Hastening number
    */
   const AMD = 'AMD';

   /**
    * Repair data request number
    */
   const AME = 'AME';

   /**
    * Consumption data request number
    */
   const AMF = 'AMF';

   /**
    * Profile number
    */
   const AMG = 'AMG';

   /**
    * Case number
    */
   const AMH = 'AMH';

   /**
    * Government quality assurance and control level Number
    */
   const AMI = 'AMI';

   /**
    * Payment plan reference
    */
   const AMJ = 'AMJ';

   /**
    * Replaced meter unit number
    */
   const AMK = 'AMK';

   /**
    * Replenishment purchase order range end number
    */
   const AML = 'AML';

   /**
    * Insurer assigned reference number
    */
   const AMM = 'AMM';

   /**
    * Canadian excise entry number
    */
   const AMN = 'AMN';

   /**
    * Premium rate table
    */
   const AMO = 'AMO';

   /**
    * Advise through bank's reference
    */
   const AMP = 'AMP';

   /**
    * US, Department of Transportation bond surety code
    */
   const AMQ = 'AMQ';

   /**
    * US, Food and Drug Administration establishment indicator
    */
   const AMR = 'AMR';

   /**
    * US, Federal Communications Commission (FCC) import
    */
   const AMS = 'AMS';

   /**
    * Goods and Services Tax identification number
    */
   const AMT = 'AMT';

   /**
    * Integrated logistic support cross reference number
    */
   const AMU = 'AMU';

   /**
    * Department number
    */
   const AMV = 'AMV';

   /**
    * Buyer's catalogue number
    */
   const AMW = 'AMW';

   /**
    * Financial settlement party's reference number
    */
   const AMX = 'AMX';

   /**
    * Standard's version number
    */
   const AMY = 'AMY';

   /**
    * Pipeline number
    */
   const AMZ = 'AMZ';

   /**
    * Account servicing bank's reference number
    */
   const ANA = 'ANA';

   /**
    * Completed units payment request reference
    */
   const ANB = 'ANB';

   /**
    * Payment in advance request reference
    */
   const ANC = 'ANC';

   /**
    * Parent file
    */
   const AND = 'AND';

   /**
    * Sub file
    */
   const ANE = 'ANE';

   /**
    * CAD file layer convention
    */
   const ANF = 'ANF';

   /**
    * Technical regulation
    */
   const ANG = 'ANG';

   /**
    * Plot file
    */
   const ANH = 'ANH';

   /**
    * File conversion journal
    */
   const ANI = 'ANI';

   /**
    * Authorization number
    */
   const ANJ = 'ANJ';

   /**
    * Reference number assigned by third party
    */
   const ANK = 'ANK';

   /**
    * Deposit reference number
    */
   const ANL = 'ANL';

   /**
    * Named bank's reference
    */
   const ANM = 'ANM';

   /**
    * Drawee's reference
    */
   const ANN = 'ANN';

   /**
    * Case of need party's reference
    */
   const ANO = 'ANO';

   /**
    * Collecting bank's reference
    */
   const ANP = 'ANP';

   /**
    * Remitting bank's reference
    */
   const ANQ = 'ANQ';

   /**
    * Principal's bank reference
    */
   const ANR = 'ANR';

   /**
    * Presenting bank's reference
    */
   const ANS = 'ANS';

   /**
    * Consignee's reference
    */
   const ANT = 'ANT';

   /**
    * Financial transaction reference number
    */
   const ANU = 'ANU';

   /**
    * Credit reference number
    */
   const ANV = 'ANV';

   /**
    * Receiving bank's authorization number
    */
   const ANW = 'ANW';

   /**
    * Clearing reference
    */
   const ANX = 'ANX';

   /**
    * Sending bank's reference number
    */
   const ANY = 'ANY';

   /**
    * Documentary payment reference
    */
   const AOA = 'AOA';

   /**
    * Accounting file reference
    */
   const AOD = 'AOD';

   /**
    * Sender's file reference number
    */
   const AOE = 'AOE';

   /**
    * Receiver's file reference number
    */
   const AOF = 'AOF';

   /**
    * Source document internal reference
    */
   const AOG = 'AOG';

   /**
    * Principal's reference
    */
   const AOH = 'AOH';

   /**
    * Debit reference number
    */
   const AOI = 'AOI';

   /**
    * Calendar
    */
   const AOJ = 'AOJ';

   /**
    * Work shift
    */
   const AOK = 'AOK';

   /**
    * Work breakdown structure
    */
   const AOL = 'AOL';

   /**
    * Organisation breakdown structure
    */
   const AOM = 'AOM';

   /**
    * Work task charge number
    */
   const AON = 'AON';

   /**
    * Functional work group
    */
   const AOO = 'AOO';

   /**
    * Work team
    */
   const AOP = 'AOP';

   /**
    * Department
    */
   const AOQ = 'AOQ';

   /**
    * Statement of work
    */
   const AOR = 'AOR';

   /**
    * Work package
    */
   const AOS = 'AOS';

   /**
    * Planning package
    */
   const AOT = 'AOT';

   /**
    * Cost account
    */
   const AOU = 'AOU';

   /**
    * Work order
    */
   const AOV = 'AOV';

   /**
    * Transportation Control Number (TCN)
    */
   const AOW = 'AOW';

   /**
    * Constraint notation
    */
   const AOX = 'AOX';

   /**
    * ETERMS reference
    */
   const AOY = 'AOY';

   /**
    * Implementation version number
    */
   const AOZ = 'AOZ';

   /**
    * Accounts receivable number
    */
   const AP = 'AP';

   /**
    * Incorporated legal reference
    */
   const APA = 'APA';

   /**
    * Payment instalment reference number
    */
   const APB = 'APB';

   /**
    * Equipment owner reference number
    */
   const APC = 'APC';

   /**
    * Cedent's claim number
    */
   const APD = 'APD';

   /**
    * Reinsurer's claim number
    */
   const APE = 'APE';

   /**
    * Price/sales catalogue response reference number
    */
   const APF = 'APF';

   /**
    * General purpose message reference number
    */
   const APG = 'APG';

   /**
    * Invoicing data sheet reference number
    */
   const APH = 'APH';

   /**
    * Inventory report reference number
    */
   const API = 'API';

   /**
    * Ceiling formula reference number
    */
   const APJ = 'APJ';

   /**
    * Price variation formula reference number
    */
   const APK = 'APK';

   /**
    * Reference to account servicing bank's message
    */
   const APL = 'APL';

   /**
    * Party sequence number
    */
   const APM = 'APM';

   /**
    * Purchaser's request reference
    */
   const APN = 'APN';

   /**
    * Contractor request reference
    */
   const APO = 'APO';

   /**
    * Accident reference number
    */
   const APP = 'APP';

   /**
    * Commercial account summary reference number
    */
   const APQ = 'APQ';

   /**
    * Contract breakdown reference
    */
   const APR = 'APR';

   /**
    * Contractor registration number
    */
   const APS = 'APS';

   /**
    * Applicable coefficient identification number
    */
   const APT = 'APT';

   /**
    * Special budget account number
    */
   const APU = 'APU';

   /**
    * Authorisation for repair reference
    */
   const APV = 'APV';

   /**
    * Manufacturer defined repair rates reference
    */
   const APW = 'APW';

   /**
    * Original submitter log number
    */
   const APX = 'APX';

   /**
    * Original submitter, parent Data Maintenance Request (DMR)
    */
   const APY = 'APY';

   /**
    * Original submitter, child Data Maintenance Request (DMR)
    */
   const APZ = 'APZ';

   /**
    * Entry point assessment log number
    */
   const AQA = 'AQA';

   /**
    * Entry point assessment log number, parent DMR
    */
   const AQB = 'AQB';

   /**
    * Entry point assessment log number, child DMR
    */
   const AQC = 'AQC';

   /**
    * Data structure tag
    */
   const AQD = 'AQD';

   /**
    * Central secretariat log number
    */
   const AQE = 'AQE';

   /**
    * Central secretariat log number, parent Data Maintenance
    */
   const AQF = 'AQF';

   /**
    * Central secretariat log number, child Data Maintenance
    */
   const AQG = 'AQG';

   /**
    * International assessment log number
    */
   const AQH = 'AQH';

   /**
    * International assessment log number, parent Data
    */
   const AQI = 'AQI';

   /**
    * International assessment log number, child Data Maintenance
    */
   const AQJ = 'AQJ';

   /**
    * Status report number
    */
   const AQK = 'AQK';

   /**
    * Message design group number
    */
   const AQL = 'AQL';

   /**
    * US Customs Service (USCS) entry code
    */
   const AQM = 'AQM';

   /**
    * Beginning job sequence number
    */
   const AQN = 'AQN';

   /**
    * Sender's clause number
    */
   const AQO = 'AQO';

   /**
    * Dun and Bradstreet Canada's 8 digit Standard Industrial
    */
   const AQP = 'AQP';

   /**
    * Activite Principale Exercee (APE) identifier
    */
   const AQQ = 'AQQ';

   /**
    * Dun and Bradstreet US 8 digit Standard Industrial
    */
   const AQR = 'AQR';

   /**
    * Nomenclature Activity Classification Economy (NACE)
    */
   const AQS = 'AQS';

   /**
    * Norme Activite Francaise (NAF) identifier
    */
   const AQT = 'AQT';

   /**
    * Registered contractor activity type
    */
   const AQU = 'AQU';

   /**
    * Statistic Bundes Amt (SBA) identifier
    */
   const AQV = 'AQV';

   /**
    * State or province assigned entity identification
    */
   const AQW = 'AQW';

   /**
    * Institute of Security and Future Market Development (ISFMD)
    */
   const AQX = 'AQX';

   /**
    * File identification number
    */
   const AQY = 'AQY';

   /**
    * Bankruptcy procedure number
    */
   const AQZ = 'AQZ';

   /**
    * National government business identification number
    */
   const ARA = 'ARA';

   /**
    * Prior Data Universal Number System (DUNS) number
    */
   const ARB = 'ARB';

   /**
    * Companies Registry Office (CRO) number
    */
   const ARC = 'ARC';

   /**
    * Costa Rican judicial number
    */
   const ARD = 'ARD';

   /**
    * Numero de Identificacion Tributaria (NIT)
    */
   const ARE = 'ARE';

   /**
    * Patron number
    */
   const ARF = 'ARF';

   /**
    * Registro Informacion Fiscal (RIF) number
    */
   const ARG = 'ARG';

   /**
    * Registro Unico de Contribuyente (RUC) number
    */
   const ARH = 'ARH';

   /**
    * Tokyo SHOKO Research (TSR) business identifier
    */
   const ARI = 'ARI';

   /**
    * Personal identity card number
    */
   const ARJ = 'ARJ';

   /**
    * Systeme Informatique pour le Repertoire des ENtreprises
    */
   const ARK = 'ARK';

   /**
    * Systeme Informatique pour le Repertoire des ETablissements
    */
   const ARL = 'ARL';

   /**
    * Publication issue number
    */
   const ARM = 'ARM';

   /**
    * Original filing number
    */
   const ARN = 'ARN';

   /**
    * Document page identifier
    */
   const ARO = 'ARO';

   /**
    * Public filing registration number
    */
   const ARP = 'ARP';

   /**
    * Regiristo Federal de Contribuyentes
    */
   const ARQ = 'ARQ';

   /**
    * Social security number
    */
   const ARR = 'ARR';

   /**
    * Document volume number
    */
   const ARS = 'ARS';

   /**
    * Book number
    */
   const ART = 'ART';

   /**
    * Stock exchange company identifier
    */
   const ARU = 'ARU';

   /**
    * Imputation account
    */
   const ARV = 'ARV';

   /**
    * Financial phase reference
    */
   const ARW = 'ARW';

   /**
    * Technical phase reference
    */
   const ARX = 'ARX';

   /**
    * Prior contractor registration number
    */
   const ARY = 'ARY';

   /**
    * Stock adjustment number
    */
   const ARZ = 'ARZ';

   /**
    * Dispensation reference
    */
   const ASA = 'ASA';

   /**
    * Investment reference number
    */
   const ASB = 'ASB';

   /**
    * Assuming company
    */
   const ASC = 'ASC';

   /**
    * Budget chapter
    */
   const ASD = 'ASD';

   /**
    * Duty free products security number
    */
   const ASE = 'ASE';

   /**
    * Duty free products receipt authorisation number
    */
   const ASF = 'ASF';

   /**
    * Party information message reference
    */
   const ASG = 'ASG';

   /**
    * Formal statement reference
    */
   const ASH = 'ASH';

   /**
    * Proof of delivery reference number
    */
   const ASI = 'ASI';

   /**
    * Supplier's credit claim reference number
    */
   const ASJ = 'ASJ';

   /**
    * Picture of actual product
    */
   const ASK = 'ASK';

   /**
    * Picture of a generic product
    */
   const ASL = 'ASL';

   /**
    * Trading partner identification number
    */
   const ASM = 'ASM';

   /**
    * Prior trading partner identification number
    */
   const ASN = 'ASN';

   /**
    * Password
    */
   const ASO = 'ASO';

   /**
    * Formal report number
    */
   const ASP = 'ASP';

   /**
    * Fund account number
    */
   const ASQ = 'ASQ';

   /**
    * Safe custody number
    */
   const ASR = 'ASR';

   /**
    * Master account number
    */
   const ASS = 'ASS';

   /**
    * Group reference number
    */
   const AST = 'AST';

   /**
    * Accounting transmission number
    */
   const ASU = 'ASU';

   /**
    * Product data file number
    */
   const ASV = 'ASV';

   /**
    * Cadastro Geral do Contribuinte (CGC)
    */
   const ASW = 'ASW';

   /**
    * Foreign resident identification number
    */
   const ASX = 'ASX';

   /**
    * CD-ROM
    */
   const ASY = 'ASY';

   /**
    * Physical medium
    */
   const ASZ = 'ASZ';

   /**
    * Financial cancellation reference number
    */
   const ATA = 'ATA';

   /**
    * Purchase for export Customs agreement number
    */
   const ATB = 'ATB';

   /**
    * Judgment number
    */
   const ATC = 'ATC';

   /**
    * Secretariat number
    */
   const ATD = 'ATD';

   /**
    * Previous banking status message reference
    */
   const ATE = 'ATE';

   /**
    * Last received banking status message reference
    */
   const ATF = 'ATF';

   /**
    * Bank's documentary procedure reference
    */
   const ATG = 'ATG';

   /**
    * Customer's documentary procedure reference
    */
   const ATH = 'ATH';

   /**
    * Safe deposit box number
    */
   const ATI = 'ATI';

   /**
    * Receiving Bankgiro number
    */
   const ATJ = 'ATJ';

   /**
    * Sending Bankgiro number
    */
   const ATK = 'ATK';

   /**
    * Bankgiro reference
    */
   const ATL = 'ATL';

   /**
    * Guarantee number
    */
   const ATM = 'ATM';

   /**
    * Collection instrument number
    */
   const ATN = 'ATN';

   /**
    * Converted Postgiro number
    */
   const ATO = 'ATO';

   /**
    * Cost centre alignment number
    */
   const ATP = 'ATP';

   /**
    * Kamer Van Koophandel (KVK) number
    */
   const ATQ = 'ATQ';

   /**
    * Institut Belgo-Luxembourgeois de Codification (IBLC) number
    */
   const ATR = 'ATR';

   /**
    * External object reference
    */
   const ATS = 'ATS';

   /**
    * Exceptional transport authorisation number
    */
   const ATT = 'ATT';

   /**
    * Clave Unica de Identificacion Tributaria (CUIT)
    */
   const ATU = 'ATU';

   /**
    * Registro Unico Tributario (RUT)
    */
   const ATV = 'ATV';

   /**
    * Flat rack container bundle identification number
    */
   const ATW = 'ATW';

   /**
    * Transport equipment acceptance order reference
    */
   const ATX = 'ATX';

   /**
    * Transport equipment release order reference
    */
   const ATY = 'ATY';

   /**
    * Ship's stay reference number
    */
   const ATZ = 'ATZ';

   /**
    * Authorization to meet competition number
    */
   const AU = 'AU';

   /**
    * Place of positioning reference
    */
   const AUA = 'AUA';

   /**
    * Party reference
    */
   const AUB = 'AUB';

   /**
    * Issued prescription identification
    */
   const AUC = 'AUC';

   /**
    * Collection reference
    */
   const AUD = 'AUD';

   /**
    * Travel service
    */
   const AUE = 'AUE';

   /**
    * Consignment stock contract
    */
   const AUF = 'AUF';

   /**
    * Importer's letter of credit reference
    */
   const AUG = 'AUG';

   /**
    * Performed prescription identification
    */
   const AUH = 'AUH';

   /**
    * Image reference
    */
   const AUI = 'AUI';

   /**
    * Proposed purchase order reference number
    */
   const AUJ = 'AUJ';

   /**
    * Application for financial support reference number
    */
   const AUK = 'AUK';

   /**
    * Manufacturing quality agreement number
    */
   const AUL = 'AUL';

   /**
    * Software editor reference
    */
   const AUM = 'AUM';

   /**
    * Software reference
    */
   const AUN = 'AUN';

   /**
    * Software quality reference
    */
   const AUO = 'AUO';

   /**
    * Consolidated orders' reference
    */
   const AUP = 'AUP';

   /**
    * Customs binding ruling number
    */
   const AUQ = 'AUQ';

   /**
    * Customs non-binding ruling number
    */
   const AUR = 'AUR';

   /**
    * Delivery route reference
    */
   const AUS = 'AUS';

   /**
    * Net area supplier reference
    */
   const AUT = 'AUT';

   /**
    * Time series reference
    */
   const AUU = 'AUU';

   /**
    * Connecting point to central grid
    */
   const AUV = 'AUV';

   /**
    * Marketing plan identification number (MPIN)
    */
   const AUW = 'AUW';

   /**
    * Entity reference number, previous
    */
   const AUX = 'AUX';

   /**
    * International Standard Industrial Classification (ISIC)
    */
   const AUY = 'AUY';

   /**
    * Customs pre-approval ruling number
    */
   const AUZ = 'AUZ';

   /**
    * Account payable number
    */
   const AV = 'AV';

   /**
    * First financial institution's transaction reference
    */
   const AVA = 'AVA';

   /**
    * Product characteristics directory
    */
   const AVB = 'AVB';

   /**
    * Supplier's customer reference number
    */
   const AVC = 'AVC';

   /**
    * Inventory report request number
    */
   const AVD = 'AVD';

   /**
    * Metering point
    */
   const AVE = 'AVE';

   /**
    * Passenger reservation number
    */
   const AVF = 'AVF';

   /**
    * Slaughterhouse approval number
    */
   const AVG = 'AVG';

   /**
    * Meat cutting plant approval number
    */
   const AVH = 'AVH';

   /**
    * Customer travel service identifier
    */
   const AVI = 'AVI';

   /**
    * Export control classification number
    */
   const AVJ = 'AVJ';

   /**
    * Broker reference 3
    */
   const AVK = 'AVK';

   /**
    * Consignment information
    */
   const AVL = 'AVL';

   /**
    * Goods item information
    */
   const AVM = 'AVM';

   /**
    * Dangerous Goods information
    */
   const AVN = 'AVN';

   /**
    * Pilotage services exemption number
    */
   const AVO = 'AVO';

   /**
    * Person registration number
    */
   const AVP = 'AVP';

   /**
    * Place of packing approval number
    */
   const AVQ = 'AVQ';

   /**
    * Original Mandate Reference
    */
   const AVR = 'AVR';

   /**
    * Mandate Reference
    */
   const AVS = 'AVS';

   /**
    * Reservation station indentifier
    */
   const AVT = 'AVT';

   /**
    * Unique goods shipment identifier
    */
   const AVU = 'AVU';

   /**
    * Framework Agreement Number
    */
   const AVV = 'AVV';

   /**
    * Hash value
    */
   const AVW = 'AVW';

   /**
    * Movement reference number
    */
   const AVX = 'AVX';

   /**
    * Economic Operators Registration and Identification Number
    */
   const AVY = 'AVY';

   /**
    * Local Reference Number
    */
   const AVZ = 'AVZ';

   /**
    * Rate code number
    */
   const AWA = 'AWA';

   /**
    * Air waybill number
    */
   const AWB = 'AWB';

   /**
    * Documentary credit amendment number
    */
   const AWC = 'AWC';

   /**
    * Advising bank's reference
    */
   const AWD = 'AWD';

   /**
    * Cost centre
    */
   const AWE = 'AWE';

   /**
    * Work item quantity determination
    */
   const AWF = 'AWF';

   /**
    * Internal data process number
    */
   const AWG = 'AWG';

   /**
    * Category of work reference
    */
   const AWH = 'AWH';

   /**
    * Policy form number
    */
   const AWI = 'AWI';

   /**
    * Net area
    */
   const AWJ = 'AWJ';

   /**
    * Service provider
    */
   const AWK = 'AWK';

   /**
    * Error position
    */
   const AWL = 'AWL';

   /**
    * Service category reference
    */
   const AWM = 'AWM';

   /**
    * Connected location
    */
   const AWN = 'AWN';

   /**
    * Related party
    */
   const AWO = 'AWO';

   /**
    * Latest accounting entry record reference
    */
   const AWP = 'AWP';

   /**
    * Accounting entry
    */
   const AWQ = 'AWQ';

   /**
    * Document reference, original
    */
   const AWR = 'AWR';

   /**
    * Hygienic Certificate number, national
    */
   const AWS = 'AWS';

   /**
    * Administrative Reference Code
    */
   const AWT = 'AWT';

   /**
    * Pick-up sheet number
    */
   const AWU = 'AWU';

   /**
    * Phone number
    */
   const AWV = 'AWV';

   /**
    * Buyer's fund number
    */
   const AWW = 'AWW';

   /**
    * Company trading account number
    */
   const AWX = 'AWX';

   /**
    * Reserved goods identifier
    */
   const AWY = 'AWY';

   /**
    * Handling and movement reference number
    */
   const AWZ = 'AWZ';

   /**
    * Instruction to despatch reference number
    */
   const AXA = 'AXA';

   /**
    * Instruction for returns number
    */
   const AXB = 'AXB';

   /**
    * Metered services consumption report number
    */
   const AXC = 'AXC';

   /**
    * Order status enquiry number
    */
   const AXD = 'AXD';

   /**
    * Firm booking reference number
    */
   const AXE = 'AXE';

   /**
    * Product inquiry number
    */
   const AXF = 'AXF';

   /**
    * Split delivery number
    */
   const AXG = 'AXG';

   /**
    * Service relation number
    */
   const AXH = 'AXH';

   /**
    * Serial shipping container code
    */
   const AXI = 'AXI';

   /**
    * Test specification number
    */
   const AXJ = 'AXJ';

   /**
    * Transport status report number
    */
   const AXK = 'AXK';

   /**
    * Tooling contract number
    */
   const AXL = 'AXL';

   /**
    * Formula reference number
    */
   const AXM = 'AXM';

   /**
    * Pre-agreement number
    */
   const AXN = 'AXN';

   /**
    * Product certification number
    */
   const AXO = 'AXO';

   /**
    * Consignment contract number
    */
   const AXP = 'AXP';

   /**
    * Product specification reference number
    */
   const AXQ = 'AXQ';

   /**
    * Payroll deduction advice reference
    */
   const AXR = 'AXR';

   /**
    * Beginning meter reading actual
    */
   const BA = 'BA';

   /**
    * Buyer's contract number
    */
   const BC = 'BC';

   /**
    * Bid number
    */
   const BD = 'BD';

   /**
    * Beginning meter reading estimated
    */
   const BE = 'BE';

   /**
    * House bill of lading number
    */
   const BH = 'BH';

   /**
    * Bill of lading number
    */
   const BM = 'BM';

   /**
    * Consignment identifier, carrier assigned
    */
   const BN = 'BN';

   /**
    * Blanket order number
    */
   const BO = 'BO';

   /**
    * Broker or sales office number
    */
   const BR = 'BR';

   /**
    * Batch number/lot number
    */
   const BT = 'BT';

   /**
    * Battery and accumulator producer registration number
    */
   const BTP = 'BTP';

   /**
    * Blended with number
    */
   const BW = 'BW';

   /**
    * IATA Cargo Agent CASS Address number
    */
   const CAS = 'CAS';

   /**
    * Matching of entries, balanced
    */
   const CAT = 'CAT';

   /**
    * Entry flagging
    */
   const CAU = 'CAU';

   /**
    * Matching of entries, unbalanced
    */
   const CAV = 'CAV';

   /**
    * Document reference, internal
    */
   const CAW = 'CAW';

   /**
    * European Value Added Tax identification
    */
   const CAX = 'CAX';

   /**
    * Cost accounting document
    */
   const CAY = 'CAY';

   /**
    * Grid operator's customer reference number
    */
   const CAZ = 'CAZ';

   /**
    * Ticket control number
    */
   const CBA = 'CBA';

   /**
    * Order shipment grouping reference
    */
   const CBB = 'CBB';

   /**
    * Credit note number
    */
   const CD = 'CD';

   /**
    * Ceding company
    */
   const CEC = 'CEC';

   /**
    * Debit letter number
    */
   const CED = 'CED';

   /**
    * Consignee's further order
    */
   const CFE = 'CFE';

   /**
    * Animal farm licence number
    */
   const CFF = 'CFF';

   /**
    * Consignor's further order
    */
   const CFO = 'CFO';

   /**
    * Consignee's order number
    */
   const CG = 'CG';

   /**
    * Customer catalogue number
    */
   const CH = 'CH';

   /**
    * Cheque number
    */
   const CK = 'CK';

   /**
    * Checking number
    */
   const CKN = 'CKN';

   /**
    * Credit memo number
    */
   const CM = 'CM';

   /**
    * Road consignment note number
    */
   const CMR = 'CMR';

   /**
    * Carrier's reference number
    */
   const CN = 'CN';

   /**
    * Charges note document attachment indicator
    */
   const CNO = 'CNO';

   /**
    * Call off order number
    */
   const COF = 'COF';

   /**
    * Condition of purchase document number
    */
   const CP = 'CP';

   /**
    * Customer reference number
    */
   const CR = 'CR';

   /**
    * Transport means journey identifier
    */
   const CRN = 'CRN';

   /**
    * Condition of sale document number
    */
   const CS = 'CS';

   /**
    * Team assignment number
    */
   const CST = 'CST';

   /**
    * Contract number
    */
   const CT = 'CT';

   /**
    * Consignment identifier, consignor assigned
    */
   const CU = 'CU';

   /**
    * Container operators reference number
    */
   const CV = 'CV';

   /**
    * Package number
    */
   const CW = 'CW';

   /**
    * Cooperation contract number
    */
   const CZ = 'CZ';

   /**
    * Deferment approval number
    */
   const DA = 'DA';

   /**
    * Debit account number
    */
   const DAN = 'DAN';

   /**
    * Buyer's debtor number
    */
   const DB = 'DB';

   /**
    * Distributor invoice number
    */
   const DI = 'DI';

   /**
    * Debit note number
    */
   const DL = 'DL';

   /**
    * Document identifier
    */
   const DM = 'DM';

   /**
    * Delivery note number
    */
   const DQ = 'DQ';

   /**
    * Dock receipt number
    */
   const DR = 'DR';

   /**
    * Ending meter reading actual
    */
   const EA = 'EA';

   /**
    * Embargo permit number
    */
   const EB = 'EB';

   /**
    * Export declaration
    */
   const ED = 'ED';

   /**
    * Ending meter reading estimated
    */
   const EE = 'EE';

   /**
    * Electrical and electronic equipment producer registration
    */
   const EEP = 'EEP';

   /**
    * Employer's identification number
    */
   const EI = 'EI';

   /**
    * Embargo number
    */
   const EN = 'EN';

   /**
    * Equipment number
    */
   const EQ = 'EQ';

   /**
    * Container/equipment receipt number
    */
   const ER = 'ER';

   /**
    * Exporter's reference number
    */
   const ERN = 'ERN';

   /**
    * Excess transportation number
    */
   const ET = 'ET';

   /**
    * Export permit identifier
    */
   const EX = 'EX';

   /**
    * Fiscal number
    */
   const FC = 'FC';

   /**
    * Consignment identifier, freight forwarder assigned
    */
   const FF = 'FF';

   /**
    * File line identifier
    */
   const FI = 'FI';

   /**
    * Flow reference number
    */
   const FLW = 'FLW';

   /**
    * Freight bill number
    */
   const FN = 'FN';

   /**
    * Foreign exchange
    */
   const FO = 'FO';

   /**
    * Final sequence number
    */
   const FS = 'FS';

   /**
    * Free zone identifier
    */
   const FT = 'FT';

   /**
    * File version number
    */
   const FV = 'FV';

   /**
    * Foreign exchange contract number
    */
   const FX = 'FX';

   /**
    * Standard's number
    */
   const GA = 'GA';

   /**
    * Government contract number
    */
   const GC = 'GC';

   /**
    * Standard's code number
    */
   const GD = 'GD';

   /**
    * General declaration number
    */
   const GDN = 'GDN';

   /**
    * Government reference number
    */
   const GN = 'GN';

   /**
    * Harmonised system number
    */
   const HS = 'HS';

   /**
    * House waybill number
    */
   const HWB = 'HWB';

   /**
    * Internal vendor number
    */
   const IA = 'IA';

   /**
    * In bond number
    */
   const IB = 'IB';

   /**
    * IATA cargo agent code number
    */
   const ICA = 'ICA';

   /**
    * Insurance certificate reference number
    */
   const ICE = 'ICE';

   /**
    * Insurance contract reference number
    */
   const ICO = 'ICO';

   /**
    * Initial sample inspection report number
    */
   const II = 'II';

   /**
    * Internal order number
    */
   const IL = 'IL';

   /**
    * Intermediary broker
    */
   const INB = 'INB';

   /**
    * Interchange number new
    */
   const INN = 'INN';

   /**
    * Interchange number old
    */
   const INO = 'INO';

   /**
    * Import permit identifier
    */
   const IP = 'IP';

   /**
    * Invoice number suffix
    */
   const IS = 'IS';

   /**
    * Internal customer number
    */
   const IT = 'IT';

   /**
    * Invoice document identifier
    */
   const IV = 'IV';

   /**
    * Job number
    */
   const JB = 'JB';

   /**
    * Ending job sequence number
    */
   const JE = 'JE';

   /**
    * Shipping label serial number
    */
   const LA = 'LA';

   /**
    * Loading authorisation identifier
    */
   const LAN = 'LAN';

   /**
    * Lower number in range
    */
   const LAR = 'LAR';

   /**
    * Lockbox
    */
   const LB = 'LB';

   /**
    * Letter of credit number
    */
   const LC = 'LC';

   /**
    * Document line identifier
    */
   const LI = 'LI';

   /**
    * Load planning number
    */
   const LO = 'LO';

   /**
    * Reservation office identifier
    */
   const LRC = 'LRC';

   /**
    * Bar coded label serial number
    */
   const LS = 'LS';

   /**
    * Ship notice/manifest number
    */
   const MA = 'MA';

   /**
    * Master bill of lading number
    */
   const MB = 'MB';

   /**
    * Manufacturer's part number
    */
   const MF = 'MF';

   /**
    * Meter unit number
    */
   const MG = 'MG';

   /**
    * Manufacturing order number
    */
   const MH = 'MH';

   /**
    * Message recipient
    */
   const MR = 'MR';

   /**
    * Mailing reference number
    */
   const MRN = 'MRN';

   /**
    * Message sender
    */
   const MS = 'MS';

   /**
    * Manufacturer's material safety data sheet number
    */
   const MSS = 'MSS';

   /**
    * Master air waybill number
    */
   const MWB = 'MWB';

   /**
    * North American hazardous goods classification number
    */
   const NA = 'NA';

   /**
    * Nota Fiscal
    */
   const NF = 'NF';

   /**
    * Current invoice number
    */
   const OH = 'OH';

   /**
    * Previous invoice number
    */
   const OI = 'OI';

   /**
    * Order document identifier, buyer assigned
    */
   const ON = 'ON';

   /**
    * Original purchase order
    */
   const OP = 'OP';

   /**
    * General order number
    */
   const OR = 'OR';

   /**
    * Payer's financial institution account number
    */
   const PB = 'PB';

   /**
    * Production code
    */
   const PC = 'PC';

   /**
    * Promotion deal number
    */
   const PD = 'PD';

   /**
    * Plant number
    */
   const PE = 'PE';

   /**
    * Prime contractor contract number
    */
   const PF = 'PF';

   /**
    * Price list version number
    */
   const PI = 'PI';

   /**
    * Packing list number
    */
   const PK = 'PK';

   /**
    * Price list number
    */
   const PL = 'PL';

   /**
    * Purchase order response number
    */
   const POR = 'POR';

   /**
    * Purchase order change number
    */
   const PP = 'PP';

   /**
    * Payment reference
    */
   const PQ = 'PQ';

   /**
    * Price quote number
    */
   const PR = 'PR';

   /**
    * Purchase order number suffix
    */
   const PS = 'PS';

   /**
    * Prior purchase order number
    */
   const PW = 'PW';

   /**
    * Payee's financial institution account number
    */
   const PY = 'PY';

   /**
    * Remittance advice number
    */
   const RA = 'RA';

   /**
    * Rail/road routing code
    */
   const RC = 'RC';

   /**
    * Railway consignment note number
    */
   const RCN = 'RCN';

   /**
    * Release number
    */
   const RE = 'RE';

   /**
    * Consignment receipt identifier
    */
   const REN = 'REN';

   /**
    * Export reference number
    */
   const RF = 'RF';

   /**
    * Payer's financial institution transit routing No.(ACH transfers)
    */
   const RR = 'RR';

   /**
    * Payee's financial institution transit routing No.
    */
   const RT = 'RT';

   /**
    * Sales person number
    */
   const SA = 'SA';

   /**
    * Sales region number
    */
   const SB = 'SB';

   /**
    * Sales department number
    */
   const SD = 'SD';

   /**
    * Serial number
    */
   const SE = 'SE';

   /**
    * Allocated seat
    */
   const SEA = 'SEA';

   /**
    * Ship from
    */
   const SF = 'SF';

   /**
    * Previous highest schedule number
    */
   const SH = 'SH';

   /**
    * SID (Shipper's identifying number for shipment)
    */
   const SI = 'SI';

   /**
    * Sales office number
    */
   const SM = 'SM';

   /**
    * Transport equipment seal identifier
    */
   const SN = 'SN';

   /**
    * Scan line
    */
   const SP = 'SP';

   /**
    * Equipment sequence number
    */
   const SQ = 'SQ';

   /**
    * Shipment reference number
    */
   const SRN = 'SRN';

   /**
    * Sellers reference number
    */
   const SS = 'SS';

   /**
    * Station reference number
    */
   const STA = 'STA';

   /**
    * Swap order number
    */
   const SW = 'SW';

   /**
    * Specification number
    */
   const SZ = 'SZ';

   /**
    * Trucker's bill of lading
    */
   const TB = 'TB';

   /**
    * Terminal operator's consignment reference
    */
   const TCR = 'TCR';

   /**
    * Telex message number
    */
   const TE = 'TE';

   /**
    * Transfer number
    */
   const TF = 'TF';

   /**
    * TIR carnet number
    */
   const TI = 'TI';

   /**
    * Transport instruction number
    */
   const TIN = 'TIN';

   /**
    * Tax exemption licence number
    */
   const TL = 'TL';

   /**
    * Transaction reference number
    */
   const TN = 'TN';

   /**
    * Test report number
    */
   const TP = 'TP';

   /**
    * Upper number of range
    */
   const UAR = 'UAR';

   /**
    * Ultimate customer's reference number
    */
   const UC = 'UC';

   /**
    * Unique consignment reference number
    */
   const UCN = 'UCN';

   /**
    * United Nations Dangerous Goods identifier
    */
   const UN = 'UN';

   /**
    * Ultimate customer's order number
    */
   const UO = 'UO';

   /**
    * Uniform Resource Identifier
    */
   const URI = 'URI';

   /**
    * VAT registration number
    */
   const VA = 'VA';

   /**
    * Vendor contract number
    */
   const VC = 'VC';

   /**
    * Transport equipment gross mass verification reference
    */
   const VGR = 'VGR';

   /**
    * Vessel identifier
    */
   const VM = 'VM';

   /**
    * Order number (vendor)
    */
   const VN = 'VN';

   /**
    * Voyage number
    */
   const VON = 'VON';

   /**
    * Transport equipment gross mass verification order reference
    */
   const VOR = 'VOR';

   /**
    * Vendor product number
    */
   const VP = 'VP';

   /**
    * Vendor ID number
    */
   const VR = 'VR';

   /**
    * Vendor order number suffix
    */
   const VS = 'VS';

   /**
    * Motor vehicle identification number
    */
   const VT = 'VT';

   /**
    * Voucher number
    */
   const VV = 'VV';

   /**
    * Warehouse entry number
    */
   const WE = 'WE';

   /**
    * Weight agreement number
    */
   const WM = 'WM';

   /**
    * Well number
    */
   const WN = 'WN';

   /**
    * Warehouse receipt number
    */
   const WR = 'WR';

   /**
    * Warehouse storage location number
    */
   const WS = 'WS';

   /**
    * Rail waybill number
    */
   const WY = 'WY';

   /**
    * Company/place registration number
    */
   const XA = 'XA';

   /**
    * Cargo control number
    */
   const XC = 'XC';

   /**
    * Previous cargo control number
    */
   const XP = 'XP';

   /**
    * Mutually defined reference number
    */
   const ZZZ = 'ZZZ';


}
