<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * Credit note type code (UNCL1001 subset)
 * Usage
 * /ubl:CreditNote/cbc:CreditNoteTypeCode
 */
class CreditNoteTypeCode
{

    /**
     * Credit note related to goods or services
     * Document message used to provide credit information related to a
     * transaction for goods or services to the relevant party.
     */
    const CREDIT_NOTE_GOODS_SERVICES = 81;

    /**
     * Credit note related to financial adjustments
     * Document message for providing credit information related to financial
     *  adjustments to the relevant party, e.g., bonuses.
     */
    const CREDIT_NOTE_FINANCIAL = 82;

    /**
     * Credit note
     * (1113) Document/message for providing credit information to the relevant party.
     */
    const CREDIT_NOTE = 381;

    /**
     * Factored credit note
     * Credit note related to assigned invoice(s).
     */
    const FACTORED_CREDIT_NOTE = 396;

    /**
     * Forwarder's credit note
     * Document/message for providing credit information to the relevant party.
     */
    const FORWARDERS_CREDIT_NOTE = 532;

}
