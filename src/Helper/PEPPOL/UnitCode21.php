<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * UN/ECE Recommandation 21
 * Usage
 * /ubl:Invoice/cac:InvoiceLine/cbc:InvoicedQuantity/@unitCode
 * /ubl:Invoice/cac:InvoiceLine/cac:Price/cbc:BaseQuantity/@unitCode
 * /ubl:CreditNote/cac:CreditNoteLine/cbc:CreditedQuantity/@unitCode
 * /ubl:CreditNote/cac:CreditNoteLine/cac:Price/cbc:BaseQuantity/@unitCode
 */
class UnitCode21
{
    /**
    * Drum, steel
    */
   const DRUM_STEEL = '1A';

   /**
    * Drum, aluminium
    */
   const DRUM_ALUMINIUM = '1B';

   /**
    * Drum, plywood
    */
   const DRUM_PLYWOOD = '1D';

   /**
    * Container, flexible
    */
   const CONTAINER_FLEXIBLE = '1F';

   /**
    * Drum, fibre
    */
   const DRUM_FIBRE = '1G';

   /**
    * Drum, wooden
    */
   const DRUM_WOODEN = '1W';

   /**
    * Barrel, wooden
    */
   const BARREL_WOODEN = '2C';

   /**
    * Jerrican, steel
    */
   const JERRICAN_STEEL = '3A';

   /**
    * Jerrican, plastic
    */
   const JERRICAN_PLASTIC = '3H';

   /**
    * Bag, super bulk
    */
   const BAG_SUPER_BULK = '43';

   /**
    * Bag, polybag
    */
   const BAG_POLYBAG = '44';

   /**
    * Box, steel
    */
   const BOX_STEEL = '4A';

   /**
    * Box, aluminium
    */
   const BOX_ALUMINIUM = '4B';

   /**
    * Box, natural wood
    */
   const BOX_NATURAL_WOOD = '4C';

   /**
    * Box, plywood
    */
   const BOX_PLYWOOD = '4D';

   /**
    * Box, reconstituted wood
    */
   const BOX_RECONSTITUTED_WOOD = '4F';

   /**
    * Box, fibreboard
    */
   const BOX_FIBREBOARD = '4G';

   /**
    * Box, plastic
    */
   const BOX_PLASTIC = '4H';

   /**
    * Bag, woven plastic
    */
   const BAG_WOVEN_PLASTIC = '5H';

   /**
    * Bag, textile
    */
   const BAG_TEXTILE = '5L';

   /**
    * Bag, paper
    */
   const BAG_PAPER = '5M';

   /**
    * Composite packaging, plastic receptacle
    */
   const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE = '6H';

   /**
    * Composite packaging, glass receptacle
    */
   const COMPOSITE_PACKAGING_GLASS_RECEPTACLE = '6P';

   /**
    * Case, car
    */
   const CASE_CAR = '7A';

   /**
    * Case, wooden
    */
   const CASE_WOODEN = '7B';

   /**
    * Pallet, wooden
    */
   const PALLET_WOODEN = '8A';

   /**
    * Crate, wooden
    */
   const CRATE_WOODEN = '8B';

   /**
    * Bundle, wooden
    */
   const BUNDLE_WOODEN = '8C';

   /**
    * Intermediate bulk container, rigid plastic
    */
   const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC = 'AA';

   /**
    * Receptacle, fibre
    */
   const RECEPTACLE_FIBRE = 'AB';

   /**
    * Receptacle, paper
    */
   const RECEPTACLE_PAPER = 'AC';

   /**
    * Receptacle, wooden
    */
   const RECEPTACLE_WOODEN = 'AD';

   /**
    * Aerosol
    */
   const AEROSOL = 'AE';

   /**
    * Pallet, modular, collars 80cms * 60cms
    */
   const PALLET_MODULAR_COLLARS_80CM_60CM = 'AF';

   /**
    * Pallet, shrinkwrapped
    */
   const PALLET_SHRINKWRAPPED = 'AG';

   /**
    * Pallet, 100cms * 110cms
    */
   const PALLET_100CM_110CM = 'AH';

   /**
    * Clamshell
    */
   const CLAMSHELL = 'AI';

   /**
    * Cone
    */
   const CONE = 'AJ';

   /**
    * Ball
    */
   const BALL = 'AL';

   /**
    * Ampoule, non-protected
    */
   const AMPOULE_NON_PROTECTED = 'AM';

   /**
    * Ampoule, protected
    */
   const AMPOULE_PROTECTED = 'AP';

   /**
    * Atomizer
    */
   const ATOMIZER = 'AT';

   /**
    * Capsule
    */
   const CAPSULE = 'AV';

   /**
    * Belt
    */
   const BELT = 'B4';

   /**
    * Barrel
    */
   const BARREL = 'BA';

   /**
    * Bobbin
    */
   const BOBBIN = 'BB';

   /**
    * Bottlecrate / bottlerack
    */
   const BOTTLECRATE = 'BC';

   /**
    * Bottlecrate / bottlerack
    */
   const BOTTLERACK = 'BC';

   /**
    * Board
    */
   const BOARD = 'BD';

   /**
    * Bundle
    */
   const BUNDLE = 'BE';

   /**
    * Balloon, non-protected
    */
   const BALLOON_NON_PROTECTED = 'BF';

   /**
    * Bag
    */
   const BAG = 'BG';

   /**
    * Bunch
    */
   const BUNCH = 'BH';

   /**
    * Bin
    */
   const BIN = 'BI';

   /**
    * Bucket
    */
   const BUCKET = 'BJ';

   /**
    * Basket
    */
   const BASKET = 'BK';

   /**
    * Bale, compressed
    */
   const BALE_COMPRESSED = 'BL';

   /**
    * Basin
    */
   const BASIN = 'BM';

   /**
    * Bale, non-compressed
    */
   const BALE_NON_COMPRESSED = 'BN';

   /**
    * Bottle, non-protected, cylindrical
    */
   const BOTTLE_NON_PROTECTED_CYLINDRICAL = 'BO';

   /**
    * Balloon, protected
    */
   const BALLOON_PROTECTED = 'BP';

   /**
    * Bottle, protected cylindrical
    */
   const BOTTLE_PROTECTED_CYLINDRICAL = 'BQ';

   /**
    * Bar
    */
   const BAR = 'BR';

   /**
    * Bottle, non-protected, bulbous
    */
   const BOTTLE_NON_PROTECTED_BULBOUS = 'BS';

   /**
    * Bolt
    */
   const BOLT = 'BT';

   /**
    * Butt
    */
   const BUTT = 'BU';

   /**
    * Bottle, protected bulbous
    */
   const BOTTLE_PROTECTED_BULBOUS = 'BV';

   /**
    * Box, for liquids
    */
   const BOX_FOR_LIQUIDS = 'BW';

   /**
    * Box
    */
   const BOX = 'BX';

   /**
    * Board, in bundle/bunch/truss
    */
   const BOARD_IN_BUNDLE_BUNCH_TRUSS = 'BY';

   /**
    * Bars, in bundle/bunch/truss
    */
   const BARS_IN_BUNDLE_BUNCH_TRUSS = 'BZ';

   /**
    * Can, rectangular
    */
   const CAN_RECTANGULAR = 'CA';

   /**
    * Crate, beer
    */
   const CRATE_BEER = 'CB';

   /**
    * Churn
    */
   const CHURN = 'CC';

   /**
    * Can, with handle and spout
    */
   const CAN_WITH_HANDLE_AND_SPOUT = 'CD';

   /**
    * Creel
    */
   const CREEL = 'CE';

   /**
    * Coffer
    */
   const COFFER = 'CF';

   /**
    * Cage
    */
   const CAGE = 'CG';

   /**
    * Chest
    */
   const CHEST = 'CH';

   /**
    * Canister
    */
   const CANISTER = 'CI';

   /**
    * Coffin
    */
   const COFFIN = 'CJ';

   /**
    * Cask
    */
   const CASK = 'CK';

   /**
    * Coil
    */
   const COIL = 'CL';

   /**
    * Card
    */
   const CARD = 'CM';

   /**
    * Container, not otherwise specified as transport equipment
    */
   const CONTAINER_TRANSPORT_EQUIPMENT = 'CN';

   /**
    * Carboy, non-protected
    */
   const CARBOY_NON_PROTECTED = 'CO';

   /**
    * Carboy, protected
    */
   const CARBOY_PROTECTED = 'CP';

   /**
    * Cartridge
    */
   const CARTRIDGE = 'CQ';

   /**
    * Crate
    */
   const CRATE = 'CR';

   /**
    * Case
    */
   const CASE_UNIT = 'CS';

   /**
    * Carton
    */
   const CARTON = 'CT';

   /**
    * Cup
    */
   const CUP = 'CU';

   /**
    * Cover
    */
   const COVER = 'CV';

   /**
    * Cage, roll
    */
   const CAGE_ROLL = 'CW';

   /**
    * Can, cylindrical
    */
   const CAN_CYLINDRICAL = 'CX';

   /**
    * Cylinder
    */
   const CYLINDER = 'CY';

   /**
    * Canvas
    */
   const CANVAS = 'CZ';

   /**
    * Crate, multiple layer, plastic
    */
   const CRATE_MULTIPLE_LAYER_PLASTIC = 'DA';

   /**
    * Crate, multiple layer, wooden
    */
   const CRATE_MULTIPLE_LAYER_WOODEN = 'DB';

   /**
    * Crate, multiple layer, cardboard
    */
   const CRATE_MULTIPLE_LAYER_CARDBOARD = 'DC';

   /**
    * Cage, Commonwealth Handling Equipment Pool (CHEP)
    */
   const CAGE_CHEP = 'DG';

   /**
    * Box, Commonwealth Handling Equipment Pool (CHEP), Eurobox
    */
   const BOX_CHEP_EUROBOX = 'DH';

   /**
    * Drum, iron
    */
   const DRUM_IRON = 'DI';

   /**
    * Demijohn, non-protected
    */
   const DEMIJOHN_NON_PROTECTED = 'DJ';

   /**
    * Crate, bulk, cardboard
    */
   const CRATE_BULK_CARDBOARD = 'DK';

   /**
    * Crate, bulk, plastic
    */
   const CRATE_BULK_PLASTIC = 'DL';

   /**
    * Crate, bulk, wooden
    */
   const CRATE_BULK_WOODEN = 'DM';

   /**
    * Dispenser
    */
   const DISPENSER = 'DN';

   /**
    * Demijohn, protected
    */
   const DEMIJOHN_PROTECTED = 'DP';

   /**
    * Drum
    */
   const DRUM = 'DR';

   /**
    * Tray, one layer no cover, plastic
    */
   const TRAY_ONE_LAYER_NO_COVER_PLASTIC = 'DS';

   /**
    * Tray, one layer no cover, wooden
    */
   const TRAY_ONE_LAYER_NO_COVER_WOODEN = 'DT';

   /**
    * Tray, one layer no cover, polystyrene
    */
   const TRAY_ONE_LAYER_NO_COVER_POLYSTYRENE = 'DU';

   /**
    * Tray, one layer no cover, cardboard
    */
   const TRAY_ONE_LAYER_NO_COVER_CARDBOARD = 'DV';

   /**
    * Tray, two layers no cover, plastic tray
    */
   const TRAY_TWO_LAYERS_NO_COVER_PLASTIC_TRAY = 'DW';

   /**
    * Tray, two layers no cover, wooden
    */
   const TRAY_TWO_LAYERS_NO_COVER_WOODEN = 'DX';

   /**
    * Tray, two layers no cover, cardboard
    */
   const TRAY_TWO_LAYERS_NO_COVER_CARDBOARD = 'DY';

   /**
    * Bag, plastic
    */
   const BAG_PLASTIC = 'EC';

   /**
    * Case, with pallet base
    */
   const CASE_WITH_PALLET_BASE = 'ED';

   /**
    * Case, with pallet base, wooden
    */
   const CASE_WITH_PALLET_BASE_WOODEN = 'EE';

   /**
    * Case, with pallet base, cardboard
    */
   const CASE_WITH_PALLET_BASE_CARDBOARD = 'EF';

   /**
    * Case, with pallet base, plastic
    */
   const CASE_WITH_PALLET_BASE_PLASTIC = 'EG';

   /**
    * Case, with pallet base, metal
    */
   const CASE_WITH_PALLET_BASE_METAL = 'EH';

   /**
    * Case, isothermic
    */
   const CASE_ISOTHERMIC = 'EI';

   /**
    * Envelope
    */
   const ENVELOPE = 'EN';

   /**
    * Flexibag
    */
   const FLEXIBAG = 'FB';

   /**
    * Crate, fruit
    */
   const CRATE_FRUIT = 'FC';

   /**
    * Crate, framed
    */
   const CRATE_FRAMED = 'FD';

   /**
    * Flexitank
    */
   const FLEXITANK = 'FE';

   /**
    * Firkin
    */
   const FIRKIN = 'FI';

   /**
    * Flask
    */
   const FLASK = 'FL';

   /**
    * Footlocker
    */
   const FOOTLOCKER = 'FO';

   /**
    * Filmpack
    */
   const FILMPACK = 'FP';

   /**
    * Frame
    */
   const FRAME = 'FR';

   /**
    * Foodtainer
    */
   const FOODTAINER = 'FT';

   /**
    * Cart, flatbed
    */
   const CART_FLATBED = 'FW';

   /**
    * Bag, flexible container
    */
   const BAG_FLEXIBLE_CONTAINER = 'FX';

   /**
    * Bottle, gas
    */
   const BOTTLE_GAS = 'GB';

   /**
    * Girder
    */
   const GIRDER = 'GI';

   /**
    * Container, gallon
    */
   const CONTAINER_GALLON = 'GL';

   /**
    * Receptacle, glass
    */
   const RECEPTACLE_GLASS = 'GR';

   /**
    * Tray, containing horizontally stacked flat items
    */
   const TRAY_CONTAINING_HORIZONTALLY_STACKED_FLAT_ITEMS = 'GU';

   /**
    * Bag, gunny
    */
   const BAG_GUNNY = 'GY';

   /**
    * Girders, in bundle/bunch/truss
    */
   const GIRDERS_IN_BUNDLE_BUNCH_TRUSS = 'GZ';

   /**
    * Basket, with handle, plastic
    */
   const BASKET_WITH_HANDLE_PLASTIC = 'HA';

   /**
    * Basket, with handle, wooden
    */
   const BASKET_WITH_HANDLE_WOODEN = 'HB';

   /**
    * Basket, with handle, cardboard
    */
   const BASKET_WITH_HANDLE_CARDBOARD = 'HC';

   /**
    * Hogshead
    */
   const HOGSHEAD = 'HG';

   /**
    * Hanger
    */
   const HANGER = 'HN';

   /**
    * Hamper
    */
   const HAMPER = 'HR';

   /**
    * Package, display, wooden
    */
   const PACKAGE_DISPLAY_WOODEN = 'IA';

   /**
    * Package, display, cardboard
    */
   const PACKAGE_DISPLAY_CARDBOARD = 'IB';

   /**
    * Package, display, plastic
    */
   const PACKAGE_DISPLAY_PLASTIC = 'IC';

   /**
    * Package, display, metal
    */
   const PACKAGE_DISPLAY_METAL = 'ID';

   /**
    * Package, show
    */
   const PACKAGE_SHOW = 'IE';

   /**
    * Package, flow
    */
   const PACKAGE_FLOW = 'IF';

   /**
    * Package, paper wrapped
    */
   const PACKAGE_PAPER_WRAPPED = 'IG';

   /**
    * Drum, plastic
    */
   const DRUM_PLASTIC = 'IH';

   /**
    * Package, cardboard, with bottle grip-holes
    */
   const PACKAGE_CARDBOARD_WITH_BOTTLE_GRIP_HOLES = 'IK';

   /**
    * Tray, rigid, lidded stackable (CEN TS 14482:2002)
    */
   const TRAY_RIGID_LIDDED_STACKABLE = 'IL';

   /**
    * Ingot
    */
   const INGOT = 'IN';

   /**
    * Ingots, in bundle/bunch/truss
    */
   const INGOTS_IN_BUNDLE_BUNCH_TRUSS = 'IZ';

   /**
    * Bag, jumbo
    */
   const BAG_JUMBO = 'JB';

   /**
    * Jerrican, rectangular
    */
   const JERRICAN_RECTANGULAR = 'JC';

   /**
    * Jug
    */
   const JUG = 'JG';

   /**
    * Jar
    */
   const JAR = 'JR';

   /**
    * Jutebag
    */
   const JUTEBAG = 'JT';

   /**
    * Jerrican, cylindrical
    */
   const JERRICAN_CYLINDRICAL = 'JY';

   /**
    * Keg
    */
   const KEG = 'KG';

   /**
    * Kit
    */
   const KIT = 'KI';

   /**
    * Luggage
    */
   const LUGGAGE = 'LE';

   /**
    * Log
    */
   const LOG = 'LG';

   /**
    * Lot
    */
   const LOT = 'LT';

   /**
    * Lug
    */
   const LUG = 'LU';

   /**
    * Liftvan
    */
   const LIFTVAN = 'LV';

   /**
    * Logs, in bundle/bunch/truss
    */
   const LOGS_IN_BUNDLE_BUNCH_TRUSS = 'LZ';

   /**
    * Crate, metal
    */
   const CRATE_METAL = 'MA';

   /**
    * Bag, multiply
    */
   const BAG_MULTIPLY = 'MB';

   /**
    * Crate, milk
    */
   const CRATE_MILK = 'MC';

   /**
    * Container, metal
    */
   const CONTAINER_METAL = 'ME';

   /**
    * Receptacle, metal
    */
   const RECEPTACLE_METAL = 'MR';

   /**
    * Sack, multi-wall
    */
   const SACK_MULTI_WALL = 'MS';

   /**
    * Mat
    */
   const MAT = 'MT';

   /**
    * Receptacle, plastic wrapped
    */
   const RECEPTACLE_PLASTIC_WRAPPED = 'MW';

   /**
    * Matchbox
    */
   const MATCHBOX = 'MX';

   /**
    * Not available
    */
   const NOT_AVAILABLE = 'NA';

   /**
    * Unpacked or unpackaged
    */
   const UNPACKED_OR_UNPACKAGED = 'NE';

   /**
    * Unpacked or unpackaged, single unit
    */
   const UNPACKED_OR_UNPACKAGED_SINGLE_UNIT = 'NF';

   /**
    * Unpacked or unpackaged, multiple units
    */
   const UNPACKED_OR_UNPACKAGED_MULTIPLE_UNITS = 'NG';

   /**
    * Nest
    */
   const NEST = 'NS';

   /**
    * Net
    */
   const NET = 'NT';

   /**
    * Net, tube, plastic
    */
   const NET_TUBE_PLASTIC = 'NU';

   /**
    * Net, tube, textile
    */
   const NET_TUBE_TEXTILE = 'NV';

   /**
    * Pallet, CHEP 40 cm x 60 cm
    */
   const PALLET_CHEP_40_CM_X_60_CM = 'OA';

   /**
    * Pallet, CHEP 80 cm x 120 cm
    */
   const PALLET_CHEP_80_CM_X_120_CM = 'OB';

   /**
    * Pallet, CHEP 100 cm x 120 cm
    */
   const PALLET_CHEP_100_CM_X_120_CM = 'OC';

   /**
    * Pallet, AS 4068-1993
    */
   const PALLET_4068_1993 = 'OD';

   /**
    * Pallet, ISO T11
    */
   const PALLET_ISO_T11 = 'OE';

   /**
    * Platform, unspecified weight or dimension
    */
   const PLATFORM_UNSPECIFIED_WEIGHT_OR_DIMENSION = 'OF';

   /**
    * Block
    */
   const BLOCK = 'OK';

   /**
    * Octabin
    */
   const OCTABIN = 'OT';

   /**
    * Container, outer
    */
   const CONTAINER_OUTER = 'OU';

   /**
    * Pan
    */
   const PAN = 'P2';

   /**
    * Packet
    */
   const PACKET = 'PA';

   /**
    * Pallet, box Combined open-ended box and pallet
    */
   const PALLET_BOX_COMBINED_OPEN_ENDED_BOX_AND_PALLET = 'PB';

   /**
    * Parcel
    */
   const PARCEL = 'PC';

   /**
    * Pallet, modular, collars 80cms * 100cms
    */
   const PALLET_MODULAR_COLLARS_80CM_100CM = 'PD';

   /**
    * Pallet, modular, collars 80cms * 120cms
    */
   const PALLET_MODULAR_COLLARS_80CM_120CM = 'PE';

   /**
    * Pen
    */
   const PEN = 'PF';

   /**
    * Plate
    */
   const PLATE = 'PG';

   /**
    * Pitcher
    */
   const PITCHER = 'PH';

   /**
    * Pipe
    */
   const PIPE = 'PI';

   /**
    * Punnet
    */
   const PUNNET = 'PJ';

   /**
    * Package
    */
   const PACKAGE = 'PK';

   /**
    * Pail
    */
   const PAIL = 'PL';

   /**
    * Plank
    */
   const PLANK = 'PN';

   /**
    * Pouch
    */
   const POUCH = 'PO';

   /**
    * Piece
    */
   const PIECE = 'PP';

   /**
    * Receptacle, plastic
    */
   const RECEPTACLE_PLASTIC = 'PR';

   /**
    * Pot
    */
   const POT = 'PT';

   /**
    * Tray
    */
   const TRAY = 'PU';

   /**
    * Pipes, in bundle/bunch/truss
    */
   const PIPES_IN_BUNDLE_BUNCH_TRUSS = 'PV';

   /**
    * Pallet
    */
   const PALLET = 'PX';

   /**
    * Plates, in bundle/bunch/truss
    */
   const PLATES_IN_BUNDLE_BUNCH_TRUSS = 'PY';

   /**
    * Planks, in bundle/bunch/truss
    */
   const PLANKS_IN_BUNDLE_BUNCH_TRUSS = 'PZ';

   /**
    * Drum, steel, non-removable head
    */
   const DRUM_STEEL_NON_REMOVABLE_HEAD = 'QA';

   /**
    * Drum, steel, removable head
    */
   const DRUM_STEEL_REMOVABLE_HEAD = 'QB';

   /**
    * Drum, aluminium, non-removable head
    */
   const DRUM_ALUMINIUM_NON_REMOVABLE_HEAD = 'QC';

   /**
    * Drum, aluminium, removable head
    */
   const DRUM_ALUMINIUM_REMOVABLE_HEAD = 'QD';

   /**
    * Drum, plastic, non-removable head
    */
   const DRUM_PLASTIC_NON_REMOVABLE_HEAD = 'QF';

   /**
    * Drum, plastic, removable head
    */
   const DRUM_PLASTIC_REMOVABLE_HEAD = 'QG';

   /**
    * Barrel, wooden, bung type
    */
   const BARREL_WOODEN_BUNG_TYPE = 'QH';

   /**
    * Barrel, wooden, removable head
    */
   const BARREL_WOODEN_REMOVABLE_HEAD = 'QJ';

   /**
    * Jerrican, steel, non-removable head
    */
   const JERRICAN_STEEL_NON_REMOVABLE_HEAD = 'QK';

   /**
    * Jerrican, steel, removable head
    */
   const JERRICAN_STEEL_REMOVABLE_HEAD = 'QL';

   /**
    * Jerrican, plastic, non-removable head
    */
   const JERRICAN_PLASTIC_NON_REMOVABLE_HEAD = 'QM';

   /**
    * Jerrican, plastic, removable head
    */
   const JERRICAN_PLASTIC_REMOVABLE_HEAD = 'QN';

   /**
    * Box, wooden, natural wood, ordinary
    */
   const BOX_WOODEN_NATURAL_WOOD_ORDINARY = 'QP';

   /**
    * Box, wooden, natural wood, with sift proof walls
    */
   const BOX_WOODEN_NATURAL_WOOD_WITH_SIFT_PROOF_WALLS = 'QQ';

   /**
    * Box, plastic, expanded
    */
   const BOX_PLASTIC_EXPANDED = 'QR';

   /**
    * Box, plastic, solid
    */
   const BOX_PLASTIC_SOLID = 'QS';

   /**
    * Rod
    */
   const ROD = 'RD';

   /**
    * Ring
    */
   const RING = 'RG';

   /**
    * Rack, clothing hanger
    */
   const RACK_CLOTHING_HANGER = 'RJ';

   /**
    * Rack
    */
   const RACK = 'RK';

   /**
    * Reel
    */
   const REEL = 'RL';

   /**
    * Roll
    */
   const ROLL = 'RO';

   /**
    * Rednet
    */
   const REDNET = 'RT';

   /**
    * Rods, in bundle/bunch/truss
    */
   const RODS_IN_BUNDLE_BUNCH_TRUSS = 'RZ';

   /**
    * Sack
    */
   const SACK = 'SA';

   /**
    * Slab
    */
   const SLAB = 'SB';

   /**
    * Crate, shallow
    */
   const CRATE_SHALLOW = 'SC';

   /**
    * Spindle
    */
   const SPINDLE = 'SD';

   /**
    * Sea-chest
    */
   const SEA_CHEST = 'SE';

   /**
    * Sachet
    */
   const SACHET = 'SH';

   /**
    * Skid
    */
   const SKID = 'SI';

   /**
    * Case, skeleton
    */
   const CASE_SKELETON = 'SK';

   /**
    * Slipsheet
    */
   const SLIPSHEET = 'SL';

   /**
    * Sheetmetal
    */
   const SHEETMETAL = 'SM';

   /**
    * Spool
    */
   const SPOOL = 'SO';

   /**
    * Sheet, plastic wrapping
    */
   const SHEET_PLASTIC_WRAPPING = 'SP';

   /**
    * Case, steel
    */
   const CASE_STEEL = 'SS';

   /**
    * Sheet
    */
   const SHEET = 'ST';

   /**
    * Suitcase
    */
   const SUITCASE = 'SU';

   /**
    * Envelope, steel
    */
   const ENVELOPE_STEEL = 'SV';

   /**
    * Shrinkwrapped
    */
   const SHRINKWRAPPED = 'SW';

   /**
    * Sleeve
    */
   const SLEEVE = 'SY';

   /**
    * Sheets, in bundle/bunch/truss
    */
   const SHEETS_IN_BUNDLE_BUNCH_TRUSS = 'SZ';

   /**
    * Tablet
    */
   const TABLET = 'T1';

   /**
    * Tub
    */
   const TUB = 'TB';

   /**
    * Tea-chest
    */
   const TEA_CHEST = 'TC';

   /**
    * Tube, collapsible
    */
   const TUBE_COLLAPSIBLE = 'TD';

   /**
    * Tyre
    */
   const TYRE = 'TE';

   /**
    * Tank container, generic
    */
   const TANK_CONTAINER_GENERIC = 'TG';

   /**
    * Tierce
    */
   const TIERCE = 'TI';

   /**
    * Tank, rectangular
    */
   const TANK_RECTANGULAR = 'TK';

   /**
    * Tub, with lid
    */
   const TUB_WITH_LID = 'TL';

   /**
    * Tin
    */
   const TIN = 'TN';

   /**
    * Tun
    */
   const TUN = 'TO';

   /**
    * Trunk
    */
   const TRUNK = 'TR';

   /**
    * Truss
    */
   const TRUSS = 'TS';

   /**
    * Bag, tote
    */
   const BAG_TOTE = 'TT';

   /**
    * Tube
    */
   const TUBE = 'TU';

   /**
    * Tube, with nozzle
    */
   const TUBE_WITH_NOZZLE = 'TV';

   /**
    * Pallet, triwall
    */
   const PALLET_TRIWALL = 'TW';

   /**
    * Tank, cylindrical
    */
   const TANK_CYLINDRICAL = 'TY';

   /**
    * Tubes, in bundle/bunch/truss
    */
   const TUBES_IN_BUNDLE_BUNCH_TRUSS = 'TZ';

   /**
    * Uncaged
    */
   const UNCAGED = 'UC';

   /**
    * Unit
    */
   const UNIT = 'UN';

   /**
    * Vat
    */
   const VAT = 'VA';

   /**
    * Bulk, gas (at 1031 mbar and 15°C)
    */
   const BULK_GAS = 'VG';

   /**
    * Vial
    */
   const VIAL = 'VI';

   /**
    * Vanpack
    */
   const VANPACK = 'VK';

   /**
    * Bulk, liquid
    */
   const BULK_LIQUID = 'VL';

   /**
    * Bulk, solid, large particles (“nodules”)
    */
   const BULK_SOLID_LARGE_PARTICLES_NODULES = 'VO';

   /**
    * Vacuum-packed
    */
   const VACUUM_PACKED = 'VP';

   /**
    * Bulk, liquefied gas (at abnormal temperature/pressure)
    */
   const BULK_LIQUEFIED_GAS_ABNORMAL = 'VQ';

   /**
    * Vehicle
    */
   const VEHICLE = 'VN';

   /**
    * Bulk, solid, granular particles (“grains”)
    */
   const BULK_SOLID_GRANULAR_PARTICLES_GRAINS = 'VR';

   /**
    * Bulk, scrap metal
    */
   const BULK_SCRAP_METAL = 'VS';

   /**
    * Bulk, solid, fine particles (“powders”)
    */
   const BULK_SOLID_FINE_PARTICLES_POWDERS = 'VY';

   /**
    * Intermediate bulk container
    */
   const INTERMEDIATE_BULK_CONTAINER = 'WA';

   /**
    * Wickerbottle
    */
   const WICKERBOTTLE = 'WB';

   /**
    * Intermediate bulk container, steel
    */
   const INTERMEDIATE_BULK_CONTAINER_STEEL = 'WC';

   /**
    * Intermediate bulk container, aluminium
    */
   const INTERMEDIATE_BULK_CONTAINER_ALUMINIUM = 'WD';

   /**
    * Intermediate bulk container, metal
    */
   const INTERMEDIATE_BULK_CONTAINER_METAL = 'WF';

   /**
    * Intermediate bulk container, steel, pressurised > 10 kpa
    */
   const INTERMEDIATE_BULK_CONTAINER_STEEL_PRESSURISED_UP_10_KPA = 'WG';

   /**
    * Intermediate bulk container, aluminium, pressurised > 10 kpa
    */
   const INTERMEDIATE_BULK_CONTAINER_ALUMINIUM_PRESSURISED_UP_10_KPA = 'WH';

   /**
    * Intermediate bulk container, metal, pressure 10 kpa
    */
   const INTERMEDIATE_BULK_CONTAINER_METAL_PRESSURE_10_KPA = 'WJ';

   /**
    * Intermediate bulk container, steel, liquid
    */
   const INTERMEDIATE_BULK_CONTAINER_STEEL_LIQUID = 'WK';

   /**
    * Intermediate bulk container, aluminium, liquid
    */
   const INTERMEDIATE_BULK_CONTAINER_ALUMINIUM_LIQUID = 'WL';

   /**
    * Intermediate bulk container, metal, liquid
    */
   const INTERMEDIATE_BULK_CONTAINER_METAL_LIQUID = 'WM';

   /**
    * Intermediate bulk container, woven plastic, without coat/liner
    */
   const INTERMEDIATE_BULK_CONTAINER_WOVEN_PLASTIC_WITHOUT_COAT = 'WN';

   /**
    * Intermediate bulk container, woven plastic, coated
    */
   const INTERMEDIATE_BULK_CONTAINER_WOVEN_PLASTIC_COATED = 'WP';

   /**
    * Intermediate bulk container, woven plastic, with liner
    */
   const INTERMEDIATE_BULK_CONTAINER_WOVEN_PLASTIC_WITH_LINER = 'WQ';

   /**
    * Intermediate bulk container, woven plastic, coated and liner
    */
   const INTERMEDIATE_BULK_CONTAINER_WOVEN_PLASTIC_COATED_AND_LINER = 'WR';

   /**
    * Intermediate bulk container, plastic film
    */
   const INTERMEDIATE_BULK_CONTAINER_PLASTIC_FILM = 'WS';

   /**
    * Intermediate bulk container, textile with out coat/liner
    */
   const INTERMEDIATE_BULK_CONTAINER_TEXTILE_WITH_OUT_COAT = 'WT';

   /**
    * Intermediate bulk container, natural wood, with inner liner
    */
   const INTERMEDIATE_BULK_CONTAINER_NATURAL_WOOD_WITH_INNER_LINER = 'WU';

   /**
    * Intermediate bulk container, textile, coated
    */
   const INTERMEDIATE_BULK_CONTAINER_TEXTILE_COATED = 'WV';

   /**
    * Intermediate bulk container, textile, with liner
    */
   const INTERMEDIATE_BULK_CONTAINER_TEXTILE_WITH_LINER = 'WW';

   /**
    * Intermediate bulk container, textile, coated and liner
    */
   const INTERMEDIATE_BULK_CONTAINER_TEXTILE_COATED_AND_LINER = 'WX';

   /**
    * Intermediate bulk container, plywood, with inner liner
    */
   const INTERMEDIATE_BULK_CONTAINER_PLYWOOD_WITH_INNER_LINER = 'WY';

   /**
    * Intermediate bulk container, reconstituted wood, with inner liner
    */
   const INTERMEDIATE_BULK_CONTAINER_RECONSTITUTED_WOOD_WITH_INNER_LINER = 'WZ';

   /**
    * Bag, woven plastic, without inner coat/liner
    */
   const BAG_WOVEN_PLASTIC_WITHOUT_INNER_COAT = 'XA';

   /**
    * Bag, woven plastic, sift proof
    */
   const BAG_WOVEN_PLASTIC_SIFT_PROOF = 'XB';

   /**
    * Bag, woven plastic, water resistant
    */
   const BAG_WOVEN_PLASTIC_WATER_RESISTANT = 'XC';

   /**
    * Bag, plastics film
    */
   const BAG_PLASTICS_FILM = 'XD';

   /**
    * Bag, textile, without inner coat/liner
    */
   const BAG_TEXTILE_WITHOUT_INNER_COAT = 'XF';

   /**
    * Bag, textile, sift proof
    */
   const BAG_TEXTILE_SIFT_PROOF = 'XG';

   /**
    * Bag, textile, water resistant
    */
   const BAG_TEXTILE_WATER_RESISTANT = 'XH';

   /**
    * Bag, paper, multi-wall
    */
   const BAG_PAPER_MULTI_WALL = 'XJ';

   /**
    * Bag, paper, multi-wall, water resistant
    */
   const BAG_PAPER_MULTI_WALL_WATER_RESISTANT = 'XK';

   /**
    * Composite packaging, plastic receptacle in steel drum
    */
   const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_STEEL_DRUM = 'YA';

   /**
    * Composite packaging, plastic receptacle in steel crate box
    */
   const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_STEEL_CRATE_BOX = 'YB';

   /**
    * Composite packaging, plastic receptacle in aluminium drum
    */
   const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_ALUMINIUM_DRUM = 'YC';

   /**
    * Composite packaging, plastic receptacle in aluminium crate
    */
   const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_ALUMINIUM_CRATE = 'YD';

   /**
    * Composite packaging, plastic receptacle in wooden box
    */
   const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_WOODEN_BOX = 'YF';

   /**
    * Composite packaging, plastic receptacle in plywood drum
    */
   const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_PLYWOOD_DRUM = 'YG';

   /**
    * Composite packaging, plastic receptacle in plywood box
    */
   const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_PLYWOOD_BOX = 'YH';

   /**
    * Composite packaging, plastic receptacle in fibre drum
    */
   const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_FIBRE_DRUM = 'YJ';

   /**
    * Composite packaging, plastic receptacle in fibreboard box
    */
   const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_FIBREBOARD_BOX = 'YK';

   /**
    * Composite packaging, plastic receptacle in plastic drum
    */
   const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_PLASTIC_DRUM = 'YL';

   /**
    * Composite packaging, plastic receptacle in solid plastic box
    */
   const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_SOLID_PLASTIC_BOX = 'YM';

   /**
    * Composite packaging, glass receptacle in steel drum
    */
   const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_STEEL_DRUM = 'YN';

   /**
    * Composite packaging, glass receptacle in steel crate box
    */
   const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_STEEL_CRATE_BOX = 'YP';

   /**
    * Composite packaging, glass receptacle in aluminium drum
    */
   const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_ALUMINIUM_DRUM = 'YQ';

   /**
    * Composite packaging, glass receptacle in aluminium crate
    */
   const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_ALUMINIUM_CRATE = 'YR';

   /**
    * Composite packaging, glass receptacle in wooden box
    */
   const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_WOODEN_BOX = 'YS';

   /**
    * Composite packaging, glass receptacle in plywood drum
    */
   const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_PLYWOOD_DRUM = 'YT';

   /**
    * Composite packaging, glass receptacle in wickerwork hamper
    */
   const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_WICKERWORK_HAMPER = 'YV';

   /**
    * Composite packaging, glass receptacle in fibre drum
    */
   const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_FIBRE_DRUM = 'YW';

   /**
    * Composite packaging, glass receptacle in fibreboard box
    */
   const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_FIBREBOARD_BOX = 'YX';

   /**
    * Composite packaging, glass receptacle in expandable plastic pack
    */
   const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_EXPANDABLE_PLASTIC_PACK = 'YY';

   /**
    * Composite packaging, glass receptacle in solid plastic pack
    */
   const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_SOLID_PLASTIC_PACK = 'YZ';

   /**
    * Intermediate bulk container, paper, multi-wall
    */
   const INTERMEDIATE_BULK_CONTAINER_PAPER_MULTI_WALL = 'ZA';

   /**
    * Bag, large
    */
   const BAG_LARGE = 'ZB';

   /**
    * Intermediate bulk container, paper, multi-wall, water resistant
    */
   const INTERMEDIATE_BULK_CONTAINER_PAPER_MULTI_WALL_WATER_RESISTANT = 'ZC';

   /**
    * Intermediate bulk container, rigid plastic, with structural equipment, solids
    */
   const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC_WITH_STRUCTURAL_EQUIPMENT_SOLIDS = 'ZD';

   /**
    * Intermediate bulk container, rigid plastic, freestanding, solids
    */
   const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC_FREESTANDING_SOLIDS = 'ZF';

   /**
    * Intermediate bulk container, rigid plastic, with structural equipment, pressurised
    */
   const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC_WITH_STRUCTURAL_EQUIPMENT_PRESSURISED = 'ZG';

   /**
    * Intermediate bulk container, rigid plastic, freestanding, pressurised
    */
   const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC_FREESTANDING_PRESSURISED = 'ZH';

   /**
    * Intermediate bulk container, rigid plastic, with structural equipment, liquids
    */
   const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC_WITH_STRUCTURAL_EQUIPMENT_LIQUIDS = 'ZJ';

   /**
    * Intermediate bulk container, rigid plastic, freestanding, liquids
    */
   const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC_FREESTANDING_LIQUIDS = 'ZK';

   /**
    * Intermediate bulk container, composite, rigid plastic, solids
    */
   const INTERMEDIATE_BULK_CONTAINER_COMPOSITE_RIGID_PLASTIC_SOLIDS = 'ZL';

   /**
    * Intermediate bulk container, composite, flexible plastic, solids
    */
   const INTERMEDIATE_BULK_CONTAINER_COMPOSITE_FLEXIBLE_PLASTIC_SOLIDS = 'ZM';

   /**
    * Intermediate bulk container, composite, rigid plastic, pressurised
    */
   const INTERMEDIATE_BULK_CONTAINER_COMPOSITE_RIGID_PLASTIC_PRESSURISED = 'ZN';

   /**
    * Intermediate bulk container, composite, flexible plastic, pressurised
    */
   const INTERMEDIATE_BULK_CONTAINER_COMPOSITE_FLEXIBLE_PLASTIC_PRESSURISED = 'ZP';

   /**
    * Intermediate bulk container, composite, rigid plastic, liquids
    */
   const INTERMEDIATE_BULK_CONTAINER_COMPOSITE_RIGID_PLASTIC_LIQUIDS = 'ZQ';

   /**
    * Intermediate bulk container, composite, flexible plastic, liquids
    */
   const INTERMEDIATE_BULK_CONTAINER_COMPOSITE_FLEXIBLE_PLASTIC_LIQUIDS = 'ZR';

   /**
    * Intermediate bulk container, composite
    */
   const INTERMEDIATE_BULK_CONTAINER_COMPOSITE = 'ZS';

   /**
    * Intermediate bulk container, fibreboard
    */
   const INTERMEDIATE_BULK_CONTAINER_FIBREBOARD = 'ZT';

   /**
    * Intermediate bulk container, flexible
    */
   const INTERMEDIATE_BULK_CONTAINER_FLEXIBLE = 'ZU';

   /**
    * Intermediate bulk container, metal, other than steel
    */
   const INTERMEDIATE_BULK_CONTAINER_METAL_OTHER_THAN_STEEL = 'ZV';

   /**
    * Intermediate bulk container, natural wood
    */
   const INTERMEDIATE_BULK_CONTAINER_NATURAL_WOOD = 'ZW';

   /**
    * Intermediate bulk container, plywood
    */
   const INTERMEDIATE_BULK_CONTAINER_PLYWOOD = 'ZX';

   /**
    * Intermediate bulk container, reconstituted wood
    */
   const INTERMEDIATE_BULK_CONTAINER_RECONSTITUTED_WOOD = 'ZY';

   /**
    * Mutually defined
    */
   const MUTUALLY_DEFINED = 'ZZ';



}
