<?php

/**
 * Kod typu zamówienia (UNCL1001 subset)
 */
return [
    '220'  => 'Zamówienie',
    '227'  => 'Zamówienie konsygnacyjne',
];