<?php

/**
 * Kod typu noty kredytowej (UNCL1001 subset)
 * Usage
 * /ubl:CreditNote/cbc:CreditNoteTypeCode
 */
return [
    '81'  => 'Nota kredytowa związana z towarami lub usługami',
    '83'  => 'Nota kredytowa związana z korektami finansowymi',
    '381' => 'Nota kredytowa',
    '396' => 'Faktoringowa nota kredytowa',
    '532' => 'Nota kredytowa spedytora',
];
