<?php

/**
 * Przyczyny obciążeń (UNCL7161)
 * Usage
 * /ubl:Invoice/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
 * /ubl:Invoice/cac:InvoiceLine/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
 * /ubl:CreditNote/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
 * /ubl:CreditNote/cac:CreditNoteLine/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
 */
return [
    'AA'  => 'Reklama',
    'AAA' => 'Telekomunikacja',
    'AAC' => 'Modyfikacje techniczne',
    'AAD' => 'Produkcja na zamówienie',
    'AAE' => 'Nakłady',
    'AAF' => 'Poza biurem/lokalem',
    'AAH' => 'Dodatkowe przetwarzanie',
    'AAI' => 'Atestowanie',
    'AAS' => 'Akceptacja',
    'AAT' => 'Przyspieszona dostawa',
    'AAV' => 'Kkonstrukcja specjalna',
    'AAY' => 'Obiekty lotniska',
    'AAZ' => 'Koncesja',
    'ABA' => 'Obowiązkowe składowanie',
    'ABB' => 'Usunięcie paliwa',
    'ABC' => 'Ładowanie samolotu',
    'ABD' => 'Nadgodziny',
    'ABF' => 'Tooling', //
    'ABK' => 'Różne',
    'ABL' => 'Dodatkowe opakowanie',
    'ABN' => 'Dunnage',
    'ABR' => 'Konteneryzacja',
    'ABS' => 'Opakowanie kartonowe',
    'ABT' => 'Owinięty Jutą',
    'ABU' => 'Opakowanie z folii polietylenowej',
    'ACF' => 'Różne usługi',
    'ACG' => 'Zabieg emaliowania',
    'ACH' => 'Heat treatment',
    'ACI' => 'Obróbka cieplna',
    'ACJ' => 'Malowanie',
    'ACK' => 'Polerowanie',
    'ACL' => 'Podściółka',
    'ACM' => 'Konserwacja',
    'ACS' => 'Dopasowywanie',
    'ADC' => 'Konsolidacja',
    'ADE' => 'Bill of lading', //
    'ADJ' => 'Poduszka powietrzna',
    'ADK' => 'Transfer', //
    'ADL' => 'Slipsheet', //
    'ADM' => 'Binding', //
    'ADN' => 'Naprawa lub wymiana uszkodzonego opakowania zwrotnego',
    'ADO' => 'Skuteczna logistyka',
    'ADP' => 'Merchandising', //
    'ADQ' => 'Product mix', //
    'ADR' => 'Inne usługi',
    'ADT' => 'Odbiór',
    'ADW' => 'Choroba przewlekła',
    'ADY' => 'Wprowadzenie nowego produktu',
    'ADZ' => 'Bezpośrednia dostawa',
    'AEA' => 'Objazd',
    'AEB' => 'Odłączenie',
    'AEC' => 'Dystrybucja',
    'AED' => 'Obsługa ładunków niebezpiecznych',
    'AEF' => 'Czynsze i dzierżawy',
    'AEH' => 'Odmienna lokalizacja',
    'AEI' => 'Tankowanie samolotów',
    'AEJ' => 'Paliwo wysłane do magazynu',
    'AEK' => 'Cash on delivery', //
    'AEL' => 'Mała usługa przetwarzania zamówień',
    'AEM' => 'Usługi biurowe lub administracyjne',
    'AEN' => 'Gwarancja',
    'AEO' => 'Zbieranie i recykling',
    'AEP' => 'Pobieranie opłat za prawa autorskie',
    'AES' => 'Inspeckja weterynaryjna',
    'AET' => 'Usługa emerytalna',
    'AEU' => 'Przepustka dla posiadacza karty medycznej',
    'AEV' => 'Usługa ochrony środowiska',
    'AEW' => 'Usługa oczyszczania środowiska',
    'AEX' => 'Krajowa usługa kontroli czeków poza obszarem konta',
    'AEY' => 'Krajowa usługa płatnicza poza obszarem konta',
    'AEZ' => 'Krajowa usługa płatnicza w obszarze konta',
    'AJ'  => 'Dostosowanie',
    'AU'  => 'Poświadczenie',
    'CA'  => 'Katalogowanie',
    'CAB' => 'Przewóz',
    'CAD' => 'Certyfikacja',
    'CAE' => 'Świadectwo zgodności',
    'CAF' => 'Świadectwo pochodzenia',
    'CAI' => 'Cięcie',
    'CAJ' => 'Usługa konsularna',
    'CAK' => 'Kolekcja klienta',
    'CAL' => 'Payroll payment service', //
    'CAM' => 'Transport gotówki',
    'CAN' => 'Usługa bankowości domowej',
    'CAO' => 'Obsługa umów dwustronnych',
    'CAP' => 'Usługi pośrednictwa ubezpieczeniowego',
    'CAQ' => 'Cheque generation', //
    'CAR' => 'Preferencyjne miejsce handlu',
    'CAS' => 'Żuraw',
    'CAT' => 'Specjalna usługa kolorystyczna',
    'CAU' => 'Sortowanie',
    'CAV' => 'Odbiór i recykling baterii',
    'CAW' => 'Opłata za zwrot produktu',
    'CD'  => 'Ładowanie samochodu',
    'CG'  => 'Czyszczenie',
    'CS'  => 'Tłoczenie papierosów',
    'CT'  => 'Liczenie i przeliczanie',
    'DAB' => 'Układ/projekt',
    'DAD' => 'Rozładunek przydzielony kierowcy',
    'DL'  => 'Dostawa',
    'EG'  => 'Grawerowanie',
    'EP'  => 'Przyspieszenie',
    'ER'  => 'Gwarancja kursu wymiany',
    'FAA' => 'Produkcja',
    'FAB' => 'Wyrównanie frachtu',
    'FAC' => 'Nadzwyczajna obsługa frachtu',
    'FC'  => 'Usługa frachtowa',
    'FH'  => 'Napełnianie/obsługa',
    'FI'  => 'Finansowanie',
    'GAA' => 'Szlifowanie',
    'HAA' => 'Wąż gumowy',
    'HD'  => 'Obsługa',
    'HH'  => 'Hoisting and hauling', //
    'IAA' => 'Instalacja',
    'IAB' => 'Instalacja i gwarancja',
    'ID'  => 'Dostawa wewnętrzna',
    'IF'  => 'Kontrola',
    'IR'  => 'Instalacja i szkolenie',
    'IS'  => 'Fakturowanie',
    'KO'  => 'Przygotowanie żywności koszernej',
    'L1'  => 'Liczba przewoźników',
    'LA'  => 'Etykietowanie',
    'LAA' => 'Robocizna',
    'LAB' => 'Naprawa i zwrot',
    'LF'  => 'Legalizacja',
    'MAE' => 'Montaz',
    'MI'  => 'Faktura pocztowa',
    'ML'  => 'Faktura pocztowa do każdej lokalizacji',
    'NAA' => 'Opakowania bezzwrotne',
    'OA'  => 'Zewnętrzne złącza kablowe',
    'PA'  => 'Faktura z wysyłką',
    'PAA' => 'Fosforanowanie (obróbka stali)',
    'PC'  => 'Pakowanie',
    'PL'  => 'Paletowanie',
    'RAB' => 'Przepakowanie',
    'RAC' => 'Naprawa',
    'RAD' => 'Opakowanie zwrotne',
    'RAF' => 'Restocking',
    'RE'  => 'Ponowna dostawa',
    'RF'  => 'Odnawianie',
    'RH'  => 'Wynajem wagonów kolejowych',
    'RV'  => 'Załadunek',
    'SA'  => 'Odzyskiwanie',
    'SAA' => 'Shipping and handling', //
    'SAD' => 'Specjalne opakowanie',
    'SAE' => 'Stamping', //
    'SAI' => 'Consignee unload', //
    'SG'  => 'Folia termokurczliwa',
    'SH'  => 'Specjalna obsługa',
    'SM'  => 'Specjalne wykończenie',
    'SU'  => 'Ustawienia',
    'TAB' => 'Wynajem cystern',
    'TAC' => 'Testowanie',
    'TT'  => 'Transport - płatność przez stronę trzecią',
    'TV'  => 'Transport przez dostawcę',
    'V1'  => 'Drop yard', //
    'V2'  => 'Drop dock', //
    'WH'  => 'Magazynowanie',
    'XAA' => 'Połączenie przesyłek z tego samego dnia',
    'YY'  => 'Split pick-up', //
    'ZZZ' => 'Wzajemnie zdefiniowane',

];
