<?php

/**
 * VAT date code (UNCL2005 subset)
 * Usage
 * /ubl:Invoice/cac:InvoicePeriod/cbc:DescriptionCode
 * /ubl:CreditNote/cac:InvoicePeriod/cbc:DescriptionCode
 */
return [
    3 => 'Data wystawienia',
    35 => 'Data dostawy',
    432 => 'Data płatności',
];

