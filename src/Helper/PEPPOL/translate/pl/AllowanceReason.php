<?php


/**
 * Przyczyny upustu (UNCL5189 subset)
 * Usage
 * /ubl:Invoice/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
 * /ubl:Invoice/cac:InvoiceLine/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
 * /ubl:CreditNote/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
 * /ubl:CreditNote/cac:CreditNoteLine/cac:AllowanceCharge/cbc:AllowanceChargeReasonCode
 */
return [
    41  => 'Premia za prace przed terminem',
    42  => 'Inne premie',
    60  => 'Zniżka producenta dla odbiorcy',
    62  => 'Ze względu na status wojskowy',
    63  => 'Z powodu wypadku przy pracy',
    64  => 'Umowa specjalna',
    65  => 'Rabat na towar niepełnowartościowy',
    66  => 'Rabat wyprzedażowy',
    67  => 'Rabat na próbki',
    68  => 'Rabat na wyczerpanie asortymentu',
    70  => 'Rabat w Incoterm',
    71  => 'Limit progowy w punkcie sprzedaży',
    88  => 'Dopłata/potrącenie',
    95  => 'Rabat',
    100 => 'Rabat specjalny',
    102 => 'Stałe długookresowe',
    103 => 'Tymczasowy',
    104 => 'Standardowy',
    105 => 'Roczny obrót',
];

