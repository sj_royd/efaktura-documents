<?php

/**
 * Kod opłaty lub podatku lub kategorii opłat (Subset of UNCL5305)
 * Usage
 * /ubl:Invoice/cac:AllowanceCharge/cac:TaxCategory/cbc:ID
 * /ubl:Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cbc:ID
 * /ubl:Invoice/cac:InvoiceLine/cac:Item/cac:ClassifiedTaxCategory/cbc:ID
 * /ubl:CreditNote/cac:AllowanceCharge/cac:TaxCategory/cbc:ID
 * /ubl:CreditNote/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cbc:ID
 * /ubl:CreditNote/cac:CreditNoteLine/cac:Item/cac:ClassifiedTaxCategory/cbc:ID
 */
return [
    'AE' => 'Odwrotne obciążenie',
    'E'  => 'Zwolniony z podatku',
    'S'  => 'Stawka standardowa',
    'Z'  => 'Stawka zerowa',
    'G'  => 'Swobodny wywóz towarów, podatek nie jest naliczany',
    'O'  => 'Usługi nie podlegające opodatkowaniu',
    'K'  => 'Wewnątrzwspólnotowa dostawa towarów',
    'L'  => 'Podatek ogólny pośredni na Wyspach Kanaryjskich',
    'M'  => 'Podatek od produkcji, usług i importu w Ceucie i Malilii',
];

