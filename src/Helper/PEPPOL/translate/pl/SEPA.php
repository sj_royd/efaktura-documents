<?php

/**
 * SEPA indicator
 * Usage
 */
return [
    'SEPA' => 'SEPA unique bank assigned creditor identifier',
];