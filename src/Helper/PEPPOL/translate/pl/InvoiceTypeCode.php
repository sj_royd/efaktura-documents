<?php

/**
 * Kod typu faktury (UNCL1001 subset)
 * Usage
 * /ubl:Invoice/cbc:InvoiceTypeCode
 */
return [
    '80'  => 'Nota debetowa dotycząca towarów lub usług',
    '82'  => 'Faktura za usługi liczone',
    '84'  => 'Nota debetowa dotycząca korekt finansowych',
    '380' => 'Faktura VAT',
    '383' => 'Nota debetowa',
    '386' => 'Faktura z przedpłatą',
    '393' => 'Faktura faktoringowa',
    '395' => 'Faktura konsygnacyjna',
    '575' => 'Faktura ubezpieczyciela',
    '623' => 'Faktura spedytora',
    '780' => 'Faktura frachtowa',
];