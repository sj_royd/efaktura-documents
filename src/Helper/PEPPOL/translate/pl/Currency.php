<?php

/**
 * ISO 4217 Kody walut
 * Usage
 * /ubl:Invoice/cbc:DocumentCurrencyCode
 * /ubl:Invoice/cbc:TaxCurrencyCode
 * /ubl:Invoice/cac:AllowanceCharge/cbc:Amount/@currencyID
 * /ubl:Invoice/cac:AllowanceCharge/cbc:BaseAmount/@currencyID
 * /ubl:Invoice/cac:TaxTotal/cbc:TaxAmount/@currencyID
 * /ubl:Invoice/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxableAmount/@currencyID
 * /ubl:Invoice/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxAmount/@currencyID
 * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount/@currencyID
 * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:TaxExclusiveAmount/@currencyID
 * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID
 * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount/@currencyID
 * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:ChargeTotalAmount/@currencyID
 * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:PrepaidAmount/@currencyID
 * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:PayableRoundingAmount/@currencyID
 * /ubl:Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID
 * /ubl:Invoice/cac:InvoiceLine/cbc:LineExtensionAmount/@currencyID
 * /ubl:Invoice/cac:InvoiceLine/cac:AllowanceCharge/cbc:Amount/@currencyID
 * /ubl:Invoice/cac:InvoiceLine/cac:AllowanceCharge/cbc:BaseAmount/@currencyID
 * /ubl:Invoice/cac:InvoiceLine/cac:Price/cbc:PriceAmount/@currencyID
 * /ubl:Invoice/cac:InvoiceLine/cac:Price/cac:AllowanceCharge/cbc:Amount/@currencyID
 * /ubl:Invoice/cac:InvoiceLine/cac:Price/cac:AllowanceCharge/cbc:BaseAmount/@currencyID
 * /ubl:CreditNote/cbc:DocumentCurrencyCode
 * /ubl:CreditNote/cbc:TaxCurrencyCode
 * /ubl:CreditNote/cac:AllowanceCharge/cbc:Amount/@currencyID
 * /ubl:CreditNote/cac:AllowanceCharge/cbc:BaseAmount/@currencyID
 * /ubl:CreditNote/cac:TaxTotal/cbc:TaxAmount/@currencyID
 * /ubl:CreditNote/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxableAmount/@currencyID
 * /ubl:CreditNote/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxAmount/@currencyID
 * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:LineExtensionAmount/@currencyID
 * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:TaxExclusiveAmount/@currencyID
 * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount/@currencyID
 * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:AllowanceTotalAmount/@currencyID
 * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:ChargeTotalAmount/@currencyID
 * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:PrepaidAmount/@currencyID
 * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:PayableRoundingAmount/@currencyID
 * /ubl:CreditNote/cac:LegalMonetaryTotal/cbc:PayableAmount/@currencyID
 * /ubl:CreditNote/cac:CreditNoteLine/cbc:LineExtensionAmount/@currencyID
 * /ubl:CreditNote/cac:CreditNoteLine/cac:AllowanceCharge/cbc:Amount/@currencyID
 * /ubl:CreditNote/cac:CreditNoteLine/cac:AllowanceCharge/cbc:BaseAmount/@currencyID
 * /ubl:CreditNote/cac:CreditNoteLine/cac:Price/cbc:PriceAmount/@currencyID
 * /ubl:CreditNote/cac:CreditNoteLine/cac:Price/cac:AllowanceCharge/cbc:Amount/@currencyID
 * /ubl:CreditNote/cac:CreditNoteLine/cac:Price/cac:AllowanceCharge/cbc:BaseAmount/@currencyID
 */
return [
    'AED' => 'UAE Dirham',
    'AFN' => 'Afghani',
    'ALL' => 'Lek',
    'AMD' => 'Armenian Dram',
    'ANG' => 'Netherlands Antillean Guilder',
    'AOA' => 'Kwanza',
    'ARS' => 'Argentine Peso',
    'AUD' => 'Australian Dollar',
    'AWG' => 'Aruban Florin',
    'AZN' => 'Azerbaijan Manat',
    'BAM' => 'Convertible Mark',
    'BBD' => 'Barbados Dollar',
    'BDT' => 'Taka',
    'BGN' => 'Bulgarian Lev',
    'BHD' => 'Bahraini Dinar',
    'BIF' => 'Burundi Franc',
    'BMD' => 'Bermudian Dollar',
    'BND' => 'Brunei Dollar',
    'BOB' => 'Boliviano',
    'BOV' => 'Mvdol',
    'BRL' => 'Brazilian Real',
    'BSD' => 'Bahamian Dollar',
    'BTN' => 'Ngultrum',
    'BWP' => 'Pula',
    'BYN' => 'Belarusian Ruble',
    'BZD' => 'Belize Dollar',
    'CAD' => 'Canadian Dollar',
    'CDF' => 'Congolese Franc',
    'CHE' => 'WIR Euro',
    'CHF' => 'Swiss Franc',
    'CHW' => 'WIR Franc',
    'CLF' => 'Unidad de Fomento',
    'CLP' => 'Chilean Peso',
    'CNY' => 'Yuan Renminbi',
    'COP' => 'Colombian Peso',
    'COU' => 'Unidad de Valor Real',
    'CRC' => 'Costa Rican Colon',
    'CUC' => 'Peso Convertible',
    'CUP' => 'Cuban Peso',
    'CVE' => 'Cabo Verde Escudo',
    'CZK' => 'Czech Koruna',
    'DJF' => 'Djibouti Franc',
    'DKK' => 'Danish Krone',
    'DOP' => 'Dominican Peso',
    'DZD' => 'Algerian Dinar',
    'EGP' => 'Egyptian Pound',
    'ERN' => 'Nakfa',
    'ETB' => 'Ethiopian Birr',
    'EUR' => 'Euro',
    'FJD' => 'Fiji Dollar',
    'FKP' => 'Falkland Islands Pound',
    'GBP' => 'Funt szterling',
    'GEL' => 'Lari',
    'GHS' => 'Ghana Cedi',
    'GIP' => 'Gibraltar Pound',
    'GMD' => 'Dalasi',
    'GNF' => 'Guinean Franc',
    'GTQ' => 'Quetzal',
    'GYD' => 'Guyana Dollar',
    'HKD' => 'Hong Kong Dollar',
    'HNL' => 'Lempira',
    'HRK' => 'Kuna',
    'HTG' => 'Gourde',
    'HUF' => 'Forint',
    'IDR' => 'Rupiah',
    'ILS' => 'New Israeli Sheqel',
    'INR' => 'Indian Rupee',
    'IQD' => 'Iraqi Dinar',
    'IRR' => 'Iranian Rial',
    'ISK' => 'Iceland Krona',
    'JMD' => 'Jamaican Dollar',
    'JOD' => 'Jordanian Dinar',
    'JPY' => 'Yen',
    'KES' => 'Kenyan Shilling',
    'KGS' => 'Som',
    'KHR' => 'Riel',
    'KMF' => 'Comorian Franc',
    'KPW' => 'North Korean Won',
    'KRW' => 'Won',
    'KWD' => 'Kuwaiti Dinar',
    'KYD' => 'Cayman Islands Dollar',
    'KZT' => 'Tenge',
    'LAK' => 'Lao Kip',
    'LBP' => 'Lebanese Pound',
    'LKR' => 'Sri Lanka Rupee',
    'LRD' => 'Liberian Dollar',
    'LSL' => 'Loti',
    'LYD' => 'Libyan Dinar',
    'MAD' => 'Moroccan Dirham',
    'MDL' => 'Moldovan Leu',
    'MGA' => 'Malagasy Ariary',
    'MKD' => 'Denar',
    'MMK' => 'Kyat',
    'MNT' => 'Tugrik',
    'MOP' => 'Pataca',
    'MRU' => 'Ouguiya',
    'MUR' => 'Mauritius Rupee',
    'MVR' => 'Rufiyaa',
    'MWK' => 'Malawi Kwacha',
    'MXN' => 'Mexican Peso',
    'MXV' => 'Mexican Unidad de Inversion (UDI)',
    'MYR' => 'Malaysian Ringgit',
    'MZN' => 'Mozambique Metical',
    'NAD' => 'Namibia Dollar',
    'NGN' => 'Naira',
    'NIO' => 'Cordoba Oro',
    'NOK' => 'Norwegian Krone',
    'NPR' => 'Nepalese Rupee',
    'NZD' => 'New Zealand Dollar',
    'OMR' => 'Rial Omani',
    'PAB' => 'Balboa',
    'PEN' => 'Sol',
    'PGK' => 'Kina',
    'PHP' => 'Philippine Piso',
    'PKR' => 'Pakistan Rupee',
    'PLN' => 'Złoty',
    'PYG' => 'Guarani',
    'QAR' => 'Qatari Rial',
    'RON' => 'Romanian Leu',
    'RSD' => 'Serbian Dinar',
    'RUB' => 'Russian Ruble',
    'RWF' => 'Rwanda Franc',
    'SAR' => 'Saudi Riyal',
    'SBD' => 'Solomon Islands Dollar',
    'SCR' => 'Seychelles Rupee',
    'SDG' => 'Sudanese Pound',
    'SEK' => 'Swedish Krona',
    'SGD' => 'Singapore Dollar',
    'SHP' => 'Saint Helena Pound',
    'SLL' => 'Leone',
    'SOS' => 'Somali Shilling',
    'SRD' => 'Surinam Dollar',
    'SSP' => 'South Sudanese Pound',
    'STN' => 'Dobra',
    'SVC' => 'El Salvador Colon',
    'SYP' => 'Syrian Pound',
    'SZL' => 'Lilangeni',
    'THB' => 'Baht',
    'TJS' => 'Somoni',
    'TMT' => 'Turkmenistan New Manat',
    'TND' => 'Tunisian Dinar',
    'TOP' => 'Pa’anga',
    'TRY' => 'Turkish Lira',
    'TTD' => 'Trinidad and Tobago Dollar',
    'TWD' => 'New Taiwan Dollar',
    'TZS' => 'Tanzanian Shilling',
    'UAH' => 'Hryvnia',
    'UGX' => 'Uganda Shilling',
    'USD' => 'US Dollar',
    'USN' => 'US Dollar (Next day)',
    'UYI' => 'Uruguay Peso en Unidades Indexadas (URUIURUI)',
    'UYU' => 'Peso Uruguayo',
    'UZS' => 'Uzbekistan Sum',
    'VEF' => 'Boliwar',
    'VND' => 'Dong',
    'VUV' => 'Vatu',
    'WST' => 'Tala',
    'XAF' => 'CFA Franc BEAC',
    'XAG' => 'Srebro',
    'XAU' => 'Złoto',
    'XBA' => 'Bond Markets Unit European Composite Unit (EURCO)',
    'XBB' => 'Bond Markets Unit European Monetary Unit (E.M.U.-6)',
    'XBC' => 'Bond Markets Unit European Unit of Account 9 (E.U.A.-9)',
    'XBD' => 'Bond Markets Unit European Unit of Account 17 (E.U.A.-17)',
    'XCD' => 'East Caribbean Dollar',
    'XDR' => 'SDR (Special Drawing Right)',
    'XOF' => 'CFA Franc BCEAO',
    'XPD' => 'Pallad',
    'XPF' => 'CFP Franc',
    'XPT' => 'Platyna',
    'XSU' => 'Sucre',
    'XTS' => 'Codes specifically reserved for testing purposes',
    'XUA' => 'ADB Unit of Account',
    'YER' => 'Yemeni Rial',
    'ZAR' => 'Rand',
    'ZMW' => 'Zambian Kwacha',
    'ZWL' => 'Zimbabwe Dollar',
];
