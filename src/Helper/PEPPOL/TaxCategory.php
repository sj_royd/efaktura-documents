<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * Duty or tax or fee category code (Subset of UNCL5305)
 * Usage
 * /ubl:Invoice/cac:AllowanceCharge/cac:TaxCategory/cbc:ID
 * /ubl:Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cbc:ID
 * /ubl:Invoice/cac:InvoiceLine/cac:Item/cac:ClassifiedTaxCategory/cbc:ID
 * /ubl:CreditNote/cac:AllowanceCharge/cac:TaxCategory/cbc:ID
 * /ubl:CreditNote/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cbc:ID
 * /ubl:CreditNote/cac:CreditNoteLine/cac:Item/cac:ClassifiedTaxCategory/cbc:ID
 */
class TaxCategory
{
    /**
     * Vat Reverse Charge
     * Code specifying that the standard VAT rate is levied from the invoicee.
     */
    const REVERSE_CHARGE = 'AE';

    /**
     * Exempt from Tax
     * Code specifying that taxes are not applicable.
     */
    const EXEMPT = 'E';

    /**
     * Standard rate
     * Code specifying the standard rate.
     */
    const STANDARD = 'S';

    /**
     * Zero rated goods
     * Code specifying that the goods are at a zero rate.
     */
    const ZERO_RATED = 'Z';

    /**
     * Free export item, VAT not charged
     * Code specifying that the item is free export and taxes are not charged.
     */
    const ITEM_FREE_EXPORT = 'G';

    /**
     * Services outside scope of tax
     * Code specifying that taxes are not applicable to the services.
     */
    const SERVICE_OUTSIDE_SCOPE = 'O';

    /**
     * VAT exempt for EEA intra-community supply of goods and services
     * A tax category code indicating the item is VAT exempt due to an
     * intra-community supply in the European Economic Area.
     */
    const VAT_EXEMPT_EEA = 'K';

    /**
     * Canary Islands general indirect tax
     * Impuesto General Indirecto Canario (IGIC) is an indirect tax levied
     * on goods and services supplied in the Canary Islands (Spain) by traders
     *  and professionals, as well as on import of goods.
     */
    const CANARY_ISLAND = 'L';

    /**
     * Tax for production, services and importation in Ceuta and Melilla
     * Impuesto sobre la Producción, los Servicios y la Importación (IPSI)
     * is an indirect municipal tax, levied on the production, processing
     * and import of all kinds of movable tangible property, the supply
     * of services and the transfer of immovable property located
     * in the cities of Ceuta and Melilla.
     */
    const CEUTA_MELILLA = 'M';
}
