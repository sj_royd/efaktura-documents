<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * Order type code (UNCL1001 subset)
 */
class OrderTypeCode
{

    /**
     * Order
     * Document/message by means of which a buyer initiates a transaction with
     * a seller involving the supply of goods or services as specified, according
     * to conditions set out in an offer, or otherwise known to the buyer.
     */
    const ORDER = 220;

    /**
     * Consignment order
     * Order to deliver goods into stock with agreement on payment when goods
     * are sold out of this stock.
     */
    const METERED_SERVICE_INVOICE = 227;

}
