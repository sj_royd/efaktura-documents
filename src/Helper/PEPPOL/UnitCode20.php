<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * UN/ECE Recommandation 20
 * Usage
 * /ubl:Invoice/cac:InvoiceLine/cbc:InvoicedQuantity/@unitCode
 * /ubl:Invoice/cac:InvoiceLine/cac:Price/cbc:BaseQuantity/@unitCode
 * /ubl:CreditNote/cac:CreditNoteLine/cbc:CreditedQuantity/@unitCode
 * /ubl:CreditNote/cac:CreditNoteLine/cac:Price/cbc:BaseQuantity/@unitCode
 */
class UnitCode20
{
    /**
     * group
     */
    const GROUP = '10';

    /**
     * outfit
     */
    const OUTFIT = '11';

    /**
     * ration
     */
    const RATION = '13';

    /**
     * shot
     */
    const SHOT = '14';

    /**
     * stick, military
     */
    const STICK_MILITARY = '15';

    /**
     * twenty foot container
     */
    const TWENTY_FOOT_CONTAINER = '20';

    /**
     * forty foot container
     */
    const FORTY_FOOT_CONTAINER = '21';

    /**
     * decilitre per gram
     */
    const DECILITRE_PER_GRAM = '22';

    /**
     * gram per cubic centimetre
     */
    const GRAM_PER_CUBIC_CENTIMETRE = '23';

    /**
     * theoretical pound
     */
    const THEORETICAL_POUND = '24';

    /**
     * gram per square centimetre
     */
    const GRAM_PER_SQUARE_CENTIMETRE = '25';

    /**
     * theoretical ton
     */
    const THEORETICAL_TON = '27';

    /**
     * kilogram per square metre
     */
    const KILOGRAM_PER_SQUARE_METRE = '28';

    /**
     * kilopascal square metre per gram
     */
    const KILOPASCAL_SQUARE_METRE_PER_GRAM = '33';

    /**
     * kilopascal per millimetre
     */
    const KILOPASCAL_PER_MILLIMETRE = '34';

    /**
     * millilitre per square centimetre second
     */
    const MILLILITRE_PER_SQUARE_CENTIMETRE_SECOND = '35';

    /**
     * ounce per square foot
     */
    const OUNCE_PER_SQUARE_FOOT = '37';

    /**
     * ounce per square foot per 0,01inch
     */
    const OUNCE_PER_SQUARE_FOOT_PER_001INCH = '38';

    /**
     * millilitre per second
     */
    const MILLILITRE_PER_SECOND = '40';

    /**
     * millilitre per minute
     */
    const MILLILITRE_PER_MINUTE = '41';

    /**
     * sitas
     */
    const SITAS = '56';

    /**
     * mesh
     */
    const MESH = '57';

    /**
     * net kilogram
     */
    const NET_KILOGRAM = '58';

    /**
     * part per million
     */
    const PART_PER_MILLION = '59';

    /**
     * percent weight
     */
    const PERCENT_WEIGHT = '60';

    /**
     * part per billion (US)
     */
    const PART_PER_BILLION_US = '61';

    /**
     * millipascal
     */
    const MILLIPASCAL = '74';

    /**
     * milli-inch
     */
    const MILLI_INCH = '77';

    /**
     * pound per square inch absolute
     */
    const POUND_PER_SQUARE_INCH_ABSOLUTE = '80';

    /**
     * henry
     */
    const HENRY = '81';

    /**
     * foot pound-force
     */
    const FOOT_POUND_FORCE = '85';

    /**
     * pound per cubic foot
     */
    const POUND_PER_CUBIC_FOOT = '87';

    /**
     * poise
     */
    const POISE = '89';

    /**
     * stokes
     */
    const STOKES = '91';

    /**
     * fixed rate
     */
    const FIXED_RATE = '1I';

    /**
     * radian per second
     */
    const RADIAN_PER_SECOND = '2A';

    /**
     * radian per second squared
     */
    const RADIAN_PER_SECOND_SQUARED = '2B';

    /**
     * roentgen
     */
    const ROENTGEN = '2C';

    /**
     * volt AC
     */
    const VOLT_AC = '2G';

    /**
     * volt DC
     */
    const VOLT_DC = '2H';

    /**
     * British thermal unit (international table) per hour
     */
    const BRITISH_THERMAL_UNIT_PER_HOUR = '2I';

    /**
     * cubic centimetre per second
     */
    const CUBIC_CENTIMETRE_PER_SECOND = '2J';

    /**
     * cubic foot per hour
     */
    const CUBIC_FOOT_PER_HOUR = '2K';

    /**
     * cubic foot per minute
     */
    const CUBIC_FOOT_PER_MINUTE = '2L';

    /**
     * centimetre per second
     */
    const CENTIMETRE_PER_SECOND = '2M';

    /**
     * decibel
     */
    const DECIBEL = '2N';

    /**
     * kilobyte
     */
    const KILOBYTE = '2P';

    /**
     * kilobecquerel
     */
    const KILOBECQUEREL = '2Q';

    /**
     * kilocurie
     */
    const KILOCURIE = '2R';

    /**
     * megagram
     */
    const MEGAGRAM = '2U';

    /**
     * metre per minute
     */
    const METRE_PER_MINUTE = '2X';

    /**
     * milliroentgen
     */
    const MILLIROENTGEN = '2Y';

    /**
     * millivolt
     */
    const MILLIVOLT = '2Z';

    /**
     * megajoule
     */
    const MEGAJOULE = '3B';

    /**
     * manmonth
     */
    const MANMONTH = '3C';

    /**
     * centistokes
     */
    const CENTISTOKES = '4C';

    /**
     * microlitre
     */
    const MICROLITRE = '4G';

    /**
     * micrometre (micron)
     */
    const MICROMETRE_MICRON = '4H';

    /**
     * milliampere
     */
    const MILLIAMPERE = '4K';

    /**
     * megabyte
     */
    const MEGABYTE = '4L';

    /**
     * milligram per hour
     */
    const MILLIGRAM_PER_HOUR = '4M';

    /**
     * megabecquerel
     */
    const MEGABECQUEREL = '4N';

    /**
     * microfarad
     */
    const MICROFARAD = '4O';

    /**
     * newton per metre
     */
    const NEWTON_PER_METRE = '4P';

    /**
     * ounce inch
     */
    const OUNCE_INCH = '4Q';

    /**
     * ounce foot
     */
    const OUNCE_FOOT = '4R';

    /**
     * picofarad
     */
    const PICOFARAD = '4T';

    /**
     * pound per hour
     */
    const POUND_PER_HOUR = '4U';

    /**
     * ton (US) per hour
     */
    const TON_US_PER_HOUR = '4W';

    /**
     * kilolitre per hour
     */
    const KILOLITRE_PER_HOUR = '4X';

    /**
     * barrel (US) per minute
     */
    const BARREL_US_PER_MINUTE = '5A';

    /**
     * batch
     */
    const BATCH = '5B';

    /**
     * MMSCF/day
     */
    const MMSCF_DAY = '5E';

    /**
     * hydraulic horse power
     */
    const HYDRAULIC_HORSE_POWER = '5J';

    /**
     * ampere square metre per joule second
     */
    const AMPERE_SQUARE_METRE_PER_JOULE_SECOND = 'A10';

    /**
     * angstrom
     */
    const ANGSTROM = 'A11';

    /**
     * astronomical unit
     */
    const ASTRONOMICAL_UNIT = 'A12';

    /**
     * attojoule
     */
    const ATTOJOULE = 'A13';

    /**
     * barn
     */
    const BARN = 'A14';

    /**
     * barn per electronvolt
     */
    const BARN_PER_ELECTRONVOLT = 'A15';

    /**
     * barn per steradian electronvolt
     */
    const BARN_PER_STERADIAN_ELECTRONVOLT = 'A16';

    /**
     * barn per steradian
     */
    const BARN_PER_STERADIAN = 'A17';

    /**
     * becquerel per kilogram
     */
    const BECQUEREL_PER_KILOGRAM = 'A18';

    /**
     * becquerel per cubic metre
     */
    const BECQUEREL_PER_CUBIC_METRE = 'A19';

    /**
     * ampere per centimetre
     */
    const AMPERE_PER_CENTIMETRE = 'A2';

    /**
     * British thermal unit (international table) per second square foot degree Rankine
     */
    const BRITISH_THERMAL_UNIT_PER_SECOND_SQUARE_FOOT_DEGREE = 'A20';

    /**
     * British thermal unit (international table) per pound degree Rankine
     */
    const BRITISH_THERMAL_UNIT_PER_POUND_DEGREE = 'A21';

    /**
     * British thermal unit (international table) per second foot degree Rankine
     */
    const BRITISH_THERMAL_UNIT_PER_SECOND_FOOT_DEGREE = 'A22';

    /**
     * British thermal unit (international table) per hour square foot degree Rankine
     */
    const BRITISH_THERMAL_UNIT_PER_HOUR_SQUARE_FOOT_DEGREE = 'A23';

    /**
     * candela per square metre
     */
    const CANDELA_PER_SQUARE_METRE = 'A24';

    /**
     * coulomb metre
     */
    const COULOMB_METRE = 'A26';

    /**
     * coulomb metre squared per volt
     */
    const COULOMB_METRE_SQUARED_PER_VOLT = 'A27';

    /**
     * coulomb per cubic centimetre
     */
    const COULOMB_PER_CUBIC_CENTIMETRE = 'A28';

    /**
     * coulomb per cubic metre
     */
    const COULOMB_PER_CUBIC_METRE = 'A29';

    /**
     * ampere per millimetre
     */
    const AMPERE_PER_MILLIMETRE = 'A3';

    /**
     * coulomb per cubic millimetre
     */
    const COULOMB_PER_CUBIC_MILLIMETRE = 'A30';

    /**
     * coulomb per kilogram second
     */
    const COULOMB_PER_KILOGRAM_SECOND = 'A31';

    /**
     * coulomb per mole
     */
    const COULOMB_PER_MOLE = 'A32';

    /**
     * coulomb per square centimetre
     */
    const COULOMB_PER_SQUARE_CENTIMETRE = 'A33';

    /**
     * coulomb per square metre
     */
    const COULOMB_PER_SQUARE_METRE = 'A34';

    /**
     * coulomb per square millimetre
     */
    const COULOMB_PER_SQUARE_MILLIMETRE = 'A35';

    /**
     * cubic centimetre per mole
     */
    const CUBIC_CENTIMETRE_PER_MOLE = 'A36';

    /**
     * cubic decimetre per mole
     */
    const CUBIC_DECIMETRE_PER_MOLE = 'A37';

    /**
     * cubic metre per coulomb
     */
    const CUBIC_METRE_PER_COULOMB = 'A38';

    /**
     * cubic metre per kilogram
     */
    const CUBIC_METRE_PER_KILOGRAM = 'A39';

    /**
     * ampere per square centimetre
     */
    const AMPERE_PER_SQUARE_CENTIMETRE = 'A4';

    /**
     * cubic metre per mole
     */
    const CUBIC_METRE_PER_MOLE = 'A40';

    /**
     * ampere per square metre
     */
    const AMPERE_PER_SQUARE_METRE = 'A41';

    /**
     * curie per kilogram
     */
    const CURIE_PER_KILOGRAM = 'A42';

    /**
     * deadweight tonnage
     */
    const DEADWEIGHT_TONNAGE = 'A43';

    /**
     * decalitre
     */
    const DECALITRE = 'A44';

    /**
     * decametre
     */
    const DECAMETRE = 'A45';

    /**
     * decitex
     */
    const DECITEX = 'A47';

    /**
     * degree Rankine
     */
    const DEGREE_RANKINE = 'A48';

    /**
     * denier
     * A unit of yarn density. One denier equals a mass of 1 gram per 9 kilometres of length
     */
    const DENIER_LENGTH = 'A49';

    /**
     * ampere square metre
     */
    const AMPERE_SQUARE_METRE = 'A5';

    /**
     * electronvolt
     */
    const ELECTRONVOLT = 'A53';

    /**
     * electronvolt per metre
     */
    const ELECTRONVOLT_PER_METRE = 'A54';

    /**
     * electronvolt square metre
     */
    const ELECTRONVOLT_SQUARE_METRE = 'A55';

    /**
     * electronvolt square metre per kilogram
     */
    const ELECTRONVOLT_SQUARE_METRE_PER_KILOGRAM = 'A56';

    /**
     * 8-part cloud cover
     */
    const CLOUD_COVER_8_PART = 'A59';

    /**
     * ampere per square metre kelvin squared
     */
    const AMPERE_PER_SQUARE_METRE_KELVIN_SQUARED = 'A6';

    /**
     * exajoule
     */
    const EXAJOULE = 'A68';

    /**
     * farad per metre
     */
    const FARAD_PER_METRE = 'A69';

    /**
     * ampere per square millimetre
     */
    const AMPERE_PER_SQUARE_MILLIMETRE = 'A7';

    /**
     * femtojoule
     */
    const FEMTOJOULE = 'A70';

    /**
     * femtometre
     */
    const FEMTOMETRE = 'A71';

    /**
     * foot per second squared
     */
    const FOOT_PER_SECOND_SQUARED = 'A73';

    /**
     * foot pound-force per second
     */
    const FOOT_POUND_FORCE_PER_SECOND = 'A74';

    /**
     * freight ton
     */
    const FREIGHT_TON = 'A75';

    /**
     * gal
     */
    const GAL = 'A76';

    /**
     * ampere second
     */
    const AMPERE_SECOND = 'A8';

    /**
     * gigacoulomb per cubic metre
     */
    const GIGACOULOMB_PER_CUBIC_METRE = 'A84';

    /**
     * gigaelectronvolt
     */
    const GIGAELECTRONVOLT = 'A85';

    /**
     * gigahertz
     */
    const GIGAHERTZ = 'A86';

    /**
     * gigaohm
     */
    const GIGAOHM = 'A87';

    /**
     * gigaohm metre
     */
    const GIGAOHM_METRE = 'A88';

    /**
     * gigapascal
     */
    const GIGAPASCAL = 'A89';

    /**
     * rate
     */
    const RATE = 'A9';

    /**
     * gigawatt
     */
    const GIGAWATT = 'A90';

    /**
     * gon
     */
    const GON = 'A91';

    /**
     * gram per cubic metre
     */
    const GRAM_PER_CUBIC_METRE = 'A93';

    /**
     * gram per mole
     */
    const GRAM_PER_MOLE = 'A94';

    /**
     * gray
     */
    const GRAY = 'A95';

    /**
     * gray per second
     */
    const GRAY_PER_SECOND = 'A96';

    /**
     * hectopascal
     */
    const HECTOPASCAL = 'A97';

    /**
     * henry per metre
     */
    const HENRY_PER_METRE = 'A98';

    /**
     * bit
     */
    const BIT = 'A99';

    /**
     * ball
     */
    const BALL = 'AA';

    /**
     * bulk pack
     */
    const BULK_PACK = 'AB';

    /**
     * acre
     */
    const ACRE = 'ACR';

    /**
     * activity
     */
    const ACTIVITY = 'ACT';

    /**
     * byte
     */
    const BYTE = 'AD';

    /**
     * ampere per metre
     */
    const AMPERE_PER_METRE = 'AE';

    /**
     * additional minute
     */
    const ADDITIONAL_MINUTE = 'AH';

    /**
     * average minute per call
     */
    const AVERAGE_MINUTE_PER_CALL = 'AI';

    /**
     * fathom
     */
    const FATHOM = 'AK';

    /**
     * access line
     */
    const ACCESS_LINE = 'AL';

    /**
     * ampere hour
     */
    const AMPERE_HOUR = 'AMH';

    /**
     * ampere
     */
    const AMPERE = 'AMP';

    /**
     * year
     */
    const YEAR = 'ANN';

    /**
     * troy ounce or apothecary ounce
     */
    const TROY_OUNCE_OR_APOTHECARY_OUNCE = 'APZ';

    /**
     * anti-hemophilic factor (AHF) unit
     */
    const AHF_UNIT = 'AQ';

    /**
     * assortment
     */
    const ASSORTMENT = 'AS';

    /**
     * alcoholic strength by mass
     */
    const ALCOHOLIC_STRENGTH_BY_MASS = 'ASM';

    /**
     * alcoholic strength by volume
     */
    const ALCOHOLIC_STRENGTH_BY_VOLUME = 'ASU';

    /**
     * standard atmosphere
     */
    const STANDARD_ATMOSPHERE = 'ATM';

    /**
     * american wire gauge
     */
    const AMERICAN_WIRE_GAUGE = 'AWG';

    /**
     * assembly
     */
    const ASSEMBLY = 'AY';

    /**
     * British thermal unit (international table) per pound
     */
    const BRITISH_THERMAL_UNIT_PER_POUND = 'AZ';

    /**
     * barrel (US) per day
     */
    const BARREL_US_PER_DAY = 'B1';

    /**
     * bit per second
     */
    const BIT_PER_SECOND = 'B10';

    /**
     * joule per kilogram kelvin
     */
    const JOULE_PER_KILOGRAM_KELVIN = 'B11';

    /**
     * joule per metre
     */
    const JOULE_PER_METRE = 'B12';

    /**
     * joule per square metre
     */
    const JOULE_PER_SQUARE_METRE = 'B13';

    /**
     * joule per metre to the fourth power
     */
    const JOULE_PER_METRE_TO_FOURTH_POWER = 'B14';

    /**
     * joule per mole
     */
    const JOULE_PER_MOLE = 'B15';

    /**
     * joule per mole kelvin
     */
    const JOULE_PER_MOLE_KELVIN = 'B16';

    /**
     * credit
     */
    const CREDIT = 'B17';

    /**
     * joule second
     */
    const JOULE_SECOND = 'B18';

    /**
     * digit
     */
    const DIGIT = 'B19';

    /**
     * joule square metre per kilogram
     */
    const JOULE_SQUARE_METRE_PER_KILOGRAM = 'B20';

    /**
     * kelvin per watt
     */
    const KELVIN_PER_WATT = 'B21';

    /**
     * kiloampere
     */
    const KILOAMPERE = 'B22';

    /**
     * kiloampere per square metre
     */
    const KILOAMPERE_PER_SQUARE_METRE = 'B23';

    /**
     * kiloampere per metre
     */
    const KILOAMPERE_PER_METRE = 'B24';

    /**
     * kilobecquerel per kilogram
     */
    const KILOBECQUEREL_PER_KILOGRAM = 'B25';

    /**
     * kilocoulomb
     */
    const KILOCOULOMB = 'B26';

    /**
     * kilocoulomb per cubic metre
     */
    const KILOCOULOMB_PER_CUBIC_METRE = 'B27';

    /**
     * kilocoulomb per square metre
     */
    const KILOCOULOMB_PER_SQUARE_METRE = 'B28';

    /**
     * kiloelectronvolt
     */
    const KILOELECTRONVOLT = 'B29';

    /**
     * batting pound
     */
    const BATTING_POUND = 'B3';

    /**
     * gibibit
     */
    const GIBIBIT = 'B30';

    /**
     * kilogram metre per second
     */
    const KILOGRAM_METRE_PER_SECOND = 'B31';

    /**
     * kilogram metre squared
     */
    const KILOGRAM_METRE_SQUARED = 'B32';

    /**
     * kilogram metre squared per second
     */
    const KILOGRAM_METRE_SQUARED_PER_SECOND = 'B33';

    /**
     * kilogram per cubic decimetre
     */
    const KILOGRAM_PER_CUBIC_DECIMETRE = 'B34';

    /**
     * kilogram per litre
     */
    const KILOGRAM_PER_LITRE = 'B35';

    /**
     * barrel, imperial
     */
    const BARREL_IMPERIAL = 'B4';

    /**
     * kilojoule per kelvin
     */
    const KILOJOULE_PER_KELVIN = 'B41';

    /**
     * kilojoule per kilogram
     */
    const KILOJOULE_PER_KILOGRAM = 'B42';

    /**
     * kilojoule per kilogram kelvin
     */
    const KILOJOULE_PER_KILOGRAM_KELVIN = 'B43';

    /**
     * kilojoule per mole
     */
    const KILOJOULE_PER_MOLE = 'B44';

    /**
     * kilomole
     */
    const KILOMOLE = 'B45';

    /**
     * kilomole per cubic metre
     */
    const KILOMOLE_PER_CUBIC_METRE = 'B46';

    /**
     * kilonewton
     */
    const KILONEWTON = 'B47';

    /**
     * kilonewton metre
     */
    const KILONEWTON_METRE = 'B48';

    /**
     * kiloohm
     */
    const KILOOHM = 'B49';

    /**
     * kiloohm metre
     */
    const KILOOHM_METRE = 'B50';

    /**
     * kilosecond
     */
    const KILOSECOND = 'B52';

    /**
     * kilosiemens
     */
    const KILOSIEMENS = 'B53';

    /**
     * kilosiemens per metre
     */
    const KILOSIEMENS_PER_METRE = 'B54';

    /**
     * kilovolt per metre
     */
    const KILOVOLT_PER_METRE = 'B55';

    /**
     * kiloweber per metre
     */
    const KILOWEBER_PER_METRE = 'B56';

    /**
     * light year
     */
    const LIGHT_YEAR = 'B57';

    /**
     * litre per mole
     */
    const LITRE_PER_MOLE = 'B58';

    /**
     * lumen hour
     */
    const LUMEN_HOUR = 'B59';

    /**
     * lumen per square metre
     */
    const LUMEN_PER_SQUARE_METRE = 'B60';

    /**
     * lumen per watt
     */
    const LUMEN_PER_WATT = 'B61';

    /**
     * lumen second
     */
    const LUMEN_SECOND = 'B62';

    /**
     * lux hour
     */
    const LUX_HOUR = 'B63';

    /**
     * lux second
     */
    const LUX_SECOND = 'B64';

    /**
     * megaampere per square metre
     */
    const MEGAAMPERE_PER_SQUARE_METRE = 'B66';

    /**
     * megabecquerel per kilogram
     */
    const MEGABECQUEREL_PER_KILOGRAM = 'B67';

    /**
     * gigabit
     */
    const GIGABIT = 'B68';

    /**
     * megacoulomb per cubic metre
     */
    const MEGACOULOMB_PER_CUBIC_METRE = 'B69';

    /**
     * cycle
     */
    const CYCLE = 'B7';

    /**
     * megacoulomb per square metre
     */
    const MEGACOULOMB_PER_SQUARE_METRE = 'B70';

    /**
     * megaelectronvolt
     */
    const MEGAELECTRONVOLT = 'B71';

    /**
     * megagram per cubic metre
     */
    const MEGAGRAM_PER_CUBIC_METRE = 'B72';

    /**
     * meganewton
     */
    const MEGANEWTON = 'B73';

    /**
     * meganewton metre
     */
    const MEGANEWTON_METRE = 'B74';

    /**
     * megaohm
     */
    const MEGAOHM = 'B75';

    /**
     * megaohm metre
     */
    const MEGAOHM_METRE = 'B76';

    /**
     * megasiemens per metre
     */
    const MEGASIEMENS_PER_METRE = 'B77';

    /**
     * megavolt
     */
    const MEGAVOLT = 'B78';

    /**
     * megavolt per metre
     */
    const MEGAVOLT_PER_METRE = 'B79';

    /**
     * joule per cubic metre
     */
    const JOULE_PER_CUBIC_METRE = 'B8';

    /**
     * gigabit per second
     */
    const GIGABIT_PER_SECOND = 'B80';

    /**
     * reciprocal metre squared reciprocal second
     */
    const RECIPROCAL_METRE_SQUARED_RECIPROCAL_SECOND = 'B81';

    /**
     * inch per linear foot
     */
    const INCH_PER_LINEAR_FOOT = 'B82';

    /**
     * metre to the fourth power
     */
    const METRE_TO_FOURTH_POWER = 'B83';

    /**
     * microampere
     */
    const MICROAMPERE = 'B84';

    /**
     * microbar
     */
    const MICROBAR = 'B85';

    /**
     * microcoulomb
     */
    const MICROCOULOMB = 'B86';

    /**
     * microcoulomb per cubic metre
     */
    const MICROCOULOMB_PER_CUBIC_METRE = 'B87';

    /**
     * microcoulomb per square metre
     */
    const MICROCOULOMB_PER_SQUARE_METRE = 'B88';

    /**
     * microfarad per metre
     */
    const MICROFARAD_PER_METRE = 'B89';

    /**
     * microhenry
     */
    const MICROHENRY = 'B90';

    /**
     * microhenry per metre
     */
    const MICROHENRY_PER_METRE = 'B91';

    /**
     * micronewton
     */
    const MICRONEWTON = 'B92';

    /**
     * micronewton metre
     */
    const MICRONEWTON_METRE = 'B93';

    /**
     * microohm
     */
    const MICROOHM = 'B94';

    /**
     * microohm metre
     */
    const MICROOHM_METRE = 'B95';

    /**
     * micropascal
     */
    const MICROPASCAL = 'B96';

    /**
     * microradian
     */
    const MICRORADIAN = 'B97';

    /**
     * microsecond
     */
    const MICROSECOND = 'B98';

    /**
     * microsiemens
     */
    const MICROSIEMENS = 'B99';

    /**
     * bar [unit of pressure]
     */
    const BAR_PRESSURE = 'BAR';

    /**
     * base box
     */
    const BASE_BOX = 'BB';

    /**
     * board foot
     */
    const BOARD_FOOT = 'BFT';

    /**
     * brake horse power
     */
    const BRAKE_HORSE_POWER = 'BHP';

    /**
     * billion (EUR)
     */
    const BILLION_EUR = 'BIL';

    /**
     * dry barrel (US)
     */
    const DRY_BARREL_US = 'BLD';

    /**
     * barrel (US)
     */
    const BARREL_US = 'BLL';

    /**
     * hundred board foot
     */
    const HUNDRED_BOARD_FOOT = 'BP';

    /**
     * beats per minute
     */
    const BEATS_PER_MINUTE = 'BPM';

    /**
     * becquerel
     */
    const BECQUEREL = 'BQL';

    /**
     * British thermal unit (international table)
     */
    const BRITISH_THERMAL_UNIT = 'BTU';

    /**
     * bushel (US)
     */
    const BUSHEL_US = 'BUA';

    /**
     * bushel (UK)
     */
    const BUSHEL_UK = 'BUI';

    /**
     * call
     */
    const CALL = 'C0';

    /**
     * millifarad
     */
    const MILLIFARAD = 'C10';

    /**
     * milligal
     */
    const MILLIGAL = 'C11';

    /**
     * milligram per metre
     */
    const MILLIGRAM_PER_METRE = 'C12';

    /**
     * milligray
     */
    const MILLIGRAY = 'C13';

    /**
     * millihenry
     */
    const MILLIHENRY = 'C14';

    /**
     * millijoule
     */
    const MILLIJOULE = 'C15';

    /**
     * millimetre per second
     */
    const MILLIMETRE_PER_SECOND = 'C16';

    /**
     * millimetre squared per second
     */
    const MILLIMETRE_SQUARED_PER_SECOND = 'C17';

    /**
     * millimole
     */
    const MILLIMOLE = 'C18';

    /**
     * mole per kilogram
     */
    const MOLE_PER_KILOGRAM = 'C19';

    /**
     * millinewton
     */
    const MILLINEWTON = 'C20';

    /**
     * kibibit
     */
    const KIBIBIT = 'C21';

    /**
     * millinewton per metre
     */
    const MILLINEWTON_PER_METRE = 'C22';

    /**
     * milliohm metre
     */
    const MILLIOHM_METRE = 'C23';

    /**
     * millipascal second
     */
    const MILLIPASCAL_SECOND = 'C24';

    /**
     * milliradian
     */
    const MILLIRADIAN = 'C25';

    /**
     * millisecond
     */
    const MILLISECOND = 'C26';

    /**
     * millisiemens
     */
    const MILLISIEMENS = 'C27';

    /**
     * millisievert
     */
    const MILLISIEVERT = 'C28';

    /**
     * millitesla
     */
    const MILLITESLA = 'C29';

    /**
     * microvolt per metre
     */
    const MICROVOLT_PER_METRE = 'C3';

    /**
     * millivolt per metre
     */
    const MILLIVOLT_PER_METRE = 'C30';

    /**
     * milliwatt
     */
    const MILLIWATT = 'C31';

    /**
     * milliwatt per square metre
     */
    const MILLIWATT_PER_SQUARE_METRE = 'C32';

    /**
     * milliweber
     */
    const MILLIWEBER = 'C33';

    /**
     * mole
     */
    const MOLE = 'C34';

    /**
     * mole per cubic decimetre
     */
    const MOLE_PER_CUBIC_DECIMETRE = 'C35';

    /**
     * mole per cubic metre
     */
    const MOLE_PER_CUBIC_METRE = 'C36';

    /**
     * kilobit
     */
    const KILOBIT = 'C37';

    /**
     * mole per litre
     */
    const MOLE_PER_LITRE = 'C38';

    /**
     * nanoampere
     */
    const NANOAMPERE = 'C39';

    /**
     * nanocoulomb
     */
    const NANOCOULOMB = 'C40';

    /**
     * nanofarad
     */
    const NANOFARAD = 'C41';

    /**
     * nanofarad per metre
     */
    const NANOFARAD_PER_METRE = 'C42';

    /**
     * nanohenry
     */
    const NANOHENRY = 'C43';

    /**
     * nanohenry per metre
     */
    const NANOHENRY_PER_METRE = 'C44';

    /**
     * nanometre
     */
    const NANOMETRE = 'C45';

    /**
     * nanoohm metre
     */
    const NANOOHM_METRE = 'C46';

    /**
     * nanosecond
     */
    const NANOSECOND = 'C47';

    /**
     * nanotesla
     */
    const NANOTESLA = 'C48';

    /**
     * nanowatt
     */
    const NANOWATT = 'C49';

    /**
     * neper
     */
    const NEPER = 'C50';

    /**
     * neper per second
     */
    const NEPER_PER_SECOND = 'C51';

    /**
     * picometre
     */
    const PICOMETRE = 'C52';

    /**
     * newton metre second
     */
    const NEWTON_METRE_SECOND = 'C53';

    /**
     * newton metre squared per kilogram squared
     */
    const NEWTON_METRE_SQUARED_PER_KILOGRAM_SQUARED = 'C54';

    /**
     * newton per square metre
     */
    const NEWTON_PER_SQUARE_METRE = 'C55';

    /**
     * newton per square millimetre
     */
    const NEWTON_PER_SQUARE_MILLIMETRE = 'C56';

    /**
     * newton second
     */
    const NEWTON_SECOND = 'C57';

    /**
     * newton second per metre
     */
    const NEWTON_SECOND_PER_METRE = 'C58';

    /**
     * octave
     */
    const OCTAVE = 'C59';

    /**
     * ohm centimetre
     */
    const OHM_CENTIMETRE = 'C60';

    /**
     * ohm metre
     */
    const OHM_METRE = 'C61';

    /**
     * one
     */
    const ONE = 'C62';

    /**
     * one
     */
    const UNIT = 'C62';

    /**
     * parsec
     */
    const PARSEC = 'C63';

    /**
     * pascal per kelvin
     */
    const PASCAL_PER_KELVIN = 'C64';

    /**
     * pascal second
     */
    const PASCAL_SECOND = 'C65';

    /**
     * pascal second per cubic metre
     */
    const PASCAL_SECOND_PER_CUBIC_METRE = 'C66';

    /**
     * pascal second per metre
     */
    const PASCAL_SECOND_PER_METRE = 'C67';

    /**
     * petajoule
     */
    const PETAJOULE = 'C68';

    /**
     * phon
     */
    const PHON = 'C69';

    /**
     * centipoise
     */
    const CENTIPOISE = 'C7';

    /**
     * picoampere
     */
    const PICOAMPERE = 'C70';

    /**
     * picocoulomb
     */
    const PICOCOULOMB = 'C71';

    /**
     * picofarad per metre
     */
    const PICOFARAD_PER_METRE = 'C72';

    /**
     * picohenry
     */
    const PICOHENRY = 'C73';

    /**
     * kilobit per second
     */
    const KILOBIT_PER_SECOND = 'C74';

    /**
     * picowatt
     */
    const PICOWATT = 'C75';

    /**
     * picowatt per square metre
     */
    const PICOWATT_PER_SQUARE_METRE = 'C76';

    /**
     * pound-force
     */
    const POUND_FORCE = 'C78';

    /**
     * kilovolt ampere hour
     */
    const KILOVOLT_AMPERE_HOUR = 'C79';

    /**
     * millicoulomb per kilogram
     */
    const MILLICOULOMB_PER_KILOGRAM = 'C8';

    /**
     * rad
     */
    const RAD = 'C80';

    /**
     * radian
     */
    const RADIAN = 'C81';

    /**
     * radian square metre per mole
     */
    const RADIAN_SQUARE_METRE_PER_MOLE = 'C82';

    /**
     * radian square metre per kilogram
     */
    const RADIAN_SQUARE_METRE_PER_KILOGRAM = 'C83';

    /**
     * radian per metre
     */
    const RADIAN_PER_METRE = 'C84';

    /**
     * reciprocal angstrom
     */
    const RECIPROCAL_ANGSTROM = 'C85';

    /**
     * reciprocal cubic metre
     */
    const RECIPROCAL_CUBIC_METRE = 'C86';

    /**
     * reciprocal cubic metre per second
     */
    const RECIPROCAL_CUBIC_METRE_PER_SECOND = 'C87';

    /**
     * reciprocal electron volt per cubic metre
     */
    const RECIPROCAL_ELECTRON_VOLT_PER_CUBIC_METRE = 'C88';

    /**
     * reciprocal henry
     */
    const RECIPROCAL_HENRY = 'C89';

    /**
     * coil group
     */
    const COIL_GROUP = 'C9';

    /**
     * reciprocal joule per cubic metre
     */
    const RECIPROCAL_JOULE_PER_CUBIC_METRE = 'C90';

    /**
     * reciprocal kelvin or kelvin to the power minus one
     */
    const RECIPROCAL_KELVIN_OR_KELVIN_TO_POWER_MINUS_ONE = 'C91';

    /**
     * reciprocal metre
     */
    const RECIPROCAL_METRE = 'C92';

    /**
     * reciprocal square metre
     */
    const RECIPROCAL_SQUARE_METRE = 'C93';

    /**
     * reciprocal minute
     */
    const RECIPROCAL_MINUTE = 'C94';

    /**
     * reciprocal mole
     */
    const RECIPROCAL_MOLE = 'C95';

    /**
     * reciprocal pascal or pascal to the power minus one
     */
    const RECIPROCAL_PASCAL_OR_PASCAL_TO_POWER_MINUS_ONE = 'C96';

    /**
     * reciprocal second
     */
    const RECIPROCAL_SECOND = 'C97';

    /**
     * reciprocal second per metre squared
     */
    const RECIPROCAL_SECOND_PER_METRE_SQUARED = 'C99';

    /**
     * carrying capacity in metric ton
     */
    const CARRYING_CAPACITY_IN_METRIC_TON = 'CCT';

    /**
     * candela
     */
    const CANDELA = 'CDL';

    /**
     * degree Celsius
     */
    const DEGREE_CELSIUS = 'CEL';

    /**
     * hundred
     */
    const HUNDRED = 'CEN';

    /**
     * card
     */
    const CARD = 'CG';

    /**
     * centigram
     */
    const CENTIGRAM = 'CGM';

    /**
     * coulomb per kilogram
     */
    const COULOMB_PER_KILOGRAM = 'CKG';

    /**
     * hundred leave
     */
    const HUNDRED_LEAVE = 'CLF';

    /**
     * centilitre
     */
    const CENTILITRE = 'CLT';

    /**
     * square centimetre
     */
    const SQUARE_CENTIMETRE = 'CMK';

    /**
     * cubic centimetre
     */
    const CUBIC_CENTIMETRE = 'CMQ';

    /**
     * centimetre
     */
    const CENTIMETRE = 'CMT';

    /**
     * hundred pack
     */
    const HUNDRED_PACK = 'CNP';

    /**
     * cental (UK)
     */
    const CENTAL_UK = 'CNT';

    /**
     * coulomb
     */
    const COULOMB = 'COU';

    /**
     * content gram
     */
    const CONTENT_GRAM = 'CTG';

    /**
     * metric carat
     */
    const METRIC_CARAT = 'CTM';

    /**
     * content ton (metric)
     */
    const CONTENT_TON_METRIC = 'CTN';

    /**
     * curie
     */
    const CURIE = 'CUR';

    /**
     * hundred pound (cwt) / hundred weight (US)
     */
    const HUNDRED_POUND = 'CWA';

    /**
     * hundred pound (cwt) / hundred weight (US)
     */
    const HUNDRED_WEIGHT_US = 'CWA';

    /**
     * hundred weight (UK)
     */
    const HUNDRED_WEIGHT_UK = 'CWI';

    /**
     * kilowatt hour per hour
     */
    const KILOWATT_HOUR_PER_HOUR = 'D03';

    /**
     * lot [unit of weight]
     */
    const LOT_WEIGHT = 'D04';

    /**
     * reciprocal second per steradian
     */
    const RECIPROCAL_SECOND_PER_STERADIAN = 'D1';

    /**
     * siemens per metre
     */
    const SIEMENS_PER_METRE = 'D10';

    /**
     * mebibit
     */
    const MEBIBIT = 'D11';

    /**
     * siemens square metre per mole
     */
    const SIEMENS_SQUARE_METRE_PER_MOLE = 'D12';

    /**
     * sievert
     */
    const SIEVERT = 'D13';

    /**
     * sone
     */
    const SONE = 'D15';

    /**
     * square centimetre per erg
     */
    const SQUARE_CENTIMETRE_PER_ERG = 'D16';

    /**
     * square centimetre per steradian erg
     */
    const SQUARE_CENTIMETRE_PER_STERADIAN_ERG = 'D17';

    /**
     * metre kelvin
     */
    const METRE_KELVIN = 'D18';

    /**
     * square metre kelvin per watt
     */
    const SQUARE_METRE_KELVIN_PER_WATT = 'D19';

    /**
     * reciprocal second per steradian metre squared
     */
    const RECIPROCAL_SECOND_PER_STERADIAN_METRE_SQUARED = 'D2';

    /**
     * square metre per joule
     */
    const SQUARE_METRE_PER_JOULE = 'D20';

    /**
     * square metre per kilogram
     */
    const SQUARE_METRE_PER_KILOGRAM = 'D21';

    /**
     * square metre per mole
     */
    const SQUARE_METRE_PER_MOLE = 'D22';

    /**
     * pen gram (protein)
     */
    const PEN_GRAM_PROTEIN = 'D23';

    /**
     * square metre per steradian
     */
    const SQUARE_METRE_PER_STERADIAN = 'D24';

    /**
     * square metre per steradian joule
     */
    const SQUARE_METRE_PER_STERADIAN_JOULE = 'D25';

    /**
     * square metre per volt second
     */
    const SQUARE_METRE_PER_VOLT_SECOND = 'D26';

    /**
     * steradian
     */
    const STERADIAN = 'D27';

    /**
     * terahertz
     */
    const TERAHERTZ = 'D29';

    /**
     * terajoule
     */
    const TERAJOULE = 'D30';

    /**
     * terawatt
     */
    const TERAWATT = 'D31';

    /**
     * terawatt hour
     */
    const TERAWATT_HOUR = 'D32';

    /**
     * tesla
     */
    const TESLA = 'D33';

    /**
     * tex
     */
    const TEX = 'D34';

    /**
     * megabit
     */
    const MEGABIT = 'D36';

    /**
     * tonne per cubic metre
     */
    const TONNE_PER_CUBIC_METRE = 'D41';

    /**
     * tropical year
     */
    const TROPICAL_YEAR = 'D42';

    /**
     * unified atomic mass unit
     */
    const UNIFIED_ATOMIC_MASS_UNIT = 'D43';

    /**
     * var
     * The name of the unit is an acronym for volt-ampere-reactive
     */
    const VAR_UNIT = 'D44';

    /**
     * volt squared per kelvin squared
     */
    const VOLT_SQUARED_PER_KELVIN_SQUARED = 'D45';

    /**
     * volt - ampere
     */
    const VOLT_AMPERE = 'D46';

    /**
     * volt per centimetre
     */
    const VOLT_PER_CENTIMETRE = 'D47';

    /**
     * volt per kelvin
     */
    const VOLT_PER_KELVIN = 'D48';

    /**
     * millivolt per kelvin
     */
    const MILLIVOLT_PER_KELVIN = 'D49';

    /**
     * kilogram per square centimetre
     */
    const KILOGRAM_PER_SQUARE_CENTIMETRE = 'D5';

    /**
     * volt per metre
     */
    const VOLT_PER_METRE = 'D50';

    /**
     * volt per millimetre
     */
    const VOLT_PER_MILLIMETRE = 'D51';

    /**
     * watt per kelvin
     */
    const WATT_PER_KELVIN = 'D52';

    /**
     * watt per metre kelvin
     */
    const WATT_PER_METRE_KELVIN = 'D53';

    /**
     * watt per square metre
     */
    const WATT_PER_SQUARE_METRE = 'D54';

    /**
     * watt per square metre kelvin
     */
    const WATT_PER_SQUARE_METRE_KELVIN = 'D55';

    /**
     * watt per square metre kelvin to the fourth power
     */
    const WATT_PER_SQUARE_METRE_KELVIN_TO_FOURTH_POWER = 'D56';

    /**
     * watt per steradian
     */
    const WATT_PER_STERADIAN = 'D57';

    /**
     * watt per steradian square metre
     */
    const WATT_PER_STERADIAN_SQUARE_METRE = 'D58';

    /**
     * weber per metre
     */
    const WEBER_PER_METRE = 'D59';

    /**
     * roentgen per second
     */
    const ROENTGEN_PER_SECOND = 'D6';

    /**
     * weber per millimetre
     */
    const WEBER_PER_MILLIMETRE = 'D60';

    /**
     * minute [unit of angle]
     */
    const MINUTE_ANGLE = 'D61';

    /**
     * second [unit of angle]
     */
    const SECOND_ANGLE = 'D62';

    /**
     * book
     */
    const BOOK = 'D63';

    /**
     * round
     */
    const ROUND = 'D65';

    /**
     * number of words
     */
    const NUMBER_OF_WORDS = 'D68';

    /**
     * inch to the fourth power
     */
    const INCH_TO_FOURTH_POWER = 'D69';

    /**
     * joule square metre
     */
    const JOULE_SQUARE_METRE = 'D73';

    /**
     * kilogram per mole
     */
    const KILOGRAM_PER_MOLE = 'D74';

    /**
     * megacoulomb
     */
    const MEGACOULOMB = 'D77';

    /**
     * megajoule per second
     */
    const MEGAJOULE_PER_SECOND = 'D78';

    /**
     * microwatt
     */
    const MICROWATT = 'D80';

    /**
     * microtesla
     */
    const MICROTESLA = 'D81';

    /**
     * microvolt
     */
    const MICROVOLT = 'D82';

    /**
     * millinewton metre
     */
    const MILLINEWTON_METRE = 'D83';

    /**
     * microwatt per square metre
     */
    const MICROWATT_PER_SQUARE_METRE = 'D85';

    /**
     * millicoulomb
     */
    const MILLICOULOMB = 'D86';

    /**
     * millimole per kilogram
     */
    const MILLIMOLE_PER_KILOGRAM = 'D87';

    /**
     * millicoulomb per cubic metre
     */
    const MILLICOULOMB_PER_CUBIC_METRE = 'D88';

    /**
     * millicoulomb per square metre
     */
    const MILLICOULOMB_PER_SQUARE_METRE = 'D89';

    /**
     * rem
     */
    const REM = 'D91';

    /**
     * second per cubic metre
     */
    const SECOND_PER_CUBIC_METRE = 'D93';

    /**
     * second per cubic metre radian
     */
    const SECOND_PER_CUBIC_METRE_RADIAN = 'D94';

    /**
     * joule per gram
     */
    const JOULE_PER_GRAM = 'D95';

    /**
     * decare
     */
    const DECARE = 'DAA';

    /**
     * ten day
     */
    const TEN_DAY = 'DAD';

    /**
     * day
     */
    const DAY = 'DAY';

    /**
     * dry pound
     */
    const DRY_POUND = 'DB';

    /**
     * degree [unit of angle]
     */
    const DEGREE_ANGLE = 'DD';

    /**
     * decade
     */
    const DECADE = 'DEC';

    /**
     * decigram
     */
    const DECIGRAM = 'DG';

    /**
     * decagram
     */
    const DECAGRAM = 'DJ';

    /**
     * decilitre
     */
    const DECILITRE = 'DLT';

    /**
     * cubic decametre
     */
    const CUBIC_DECAMETRE = 'DMA';

    /**
     * square decimetre
     */
    const SQUARE_DECIMETRE = 'DMK';

    /**
     * standard kilolitre
     */
    const STANDARD_KILOLITRE = 'DMO';

    /**
     * cubic decimetre
     */
    const CUBIC_DECIMETRE = 'DMQ';

    /**
     * decimetre
     */
    const DECIMETRE = 'DMT';

    /**
     * decinewton metre
     */
    const DECINEWTON_METRE = 'DN';

    /**
     * dozen piece
     */
    const DOZEN_PIECE = 'DPC';

    /**
     * dozen pair
     */
    const DOZEN_PAIR = 'DPR';

    /**
     * displacement tonnage
     */
    const DISPLACEMENT_TONNAGE = 'DPT';

    /**
     * dram (US)
     */
    const DRAM_US = 'DRA';

    /**
     * dram (UK)
     */
    const DRAM_UK = 'DRI';

    /**
     * dozen roll
     */
    const DOZEN_ROLL = 'DRL';

    /**
     * dry ton
     */
    const DRY_TON = 'DT';

    /**
     * decitonne
     */
    const DECITONNE = 'DTN';

    /**
     * pennyweight
     */
    const PENNYWEIGHT = 'DWT';

    /**
     * dozen
     */
    const DOZEN = 'DZN';

    /**
     * dozen pack
     */
    const DOZEN_PACK = 'DZP';

    /**
     * newton per square centimetre
     */
    const NEWTON_PER_SQUARE_CENTIMETRE = 'E01';

    /**
     * megawatt hour per hour
     */
    const MEGAWATT_HOUR_PER_HOUR = 'E07';

    /**
     * megawatt per hertz
     */
    const MEGAWATT_PER_HERTZ = 'E08';

    /**
     * milliampere hour
     */
    const MILLIAMPERE_HOUR = 'E09';

    /**
     * degree day
     */
    const DEGREE_DAY = 'E10';

    /**
     * mille
     */
    const MILLE = 'E12';

    /**
     * kilocalorie (international table)
     */
    const KILOCALORIE = 'E14';

    /**
     * kilocalorie (thermochemical) per hour
     */
    const KILOCALORIE_THERM_PER_HOUR = 'E15';

    /**
     * million Btu(IT) per hour
     */
    const MILLION_BTU_IT_PER_HOUR = 'E16';

    /**
     * cubic foot per second
     */
    const CUBIC_FOOT_PER_SECOND = 'E17';

    /**
     * tonne per hour
     */
    const TONNE_PER_HOUR = 'E18';

    /**
     * ping
     */
    const PING = 'E19';

    /**
     * megabit per second
     */
    const MEGABIT_PER_SECOND = 'E20';

    /**
     * shares
     */
    const SHARES = 'E21';

    /**
     * TEU
     */
    const TEU = 'E22';

    /**
     * tyre
     */
    const TYRE = 'E23';

    /**
     * active unit
     */
    const ACTIVE_UNIT = 'E25';

    /**
     * dose
     */
    const DOSE = 'E27';

    /**
     * air dry ton
     */
    const AIR_DRY_TON = 'E28';

    /**
     * strand
     */
    const STRAND = 'E30';

    /**
     * square metre per litre
     */
    const SQUARE_METRE_PER_LITRE = 'E31';

    /**
     * litre per hour
     */
    const LITRE_PER_HOUR = 'E32';

    /**
     * foot per thousand
     */
    const FOOT_PER_THOUSAND = 'E33';

    /**
     * gigabyte
     */
    const GIGABYTE = 'E34';

    /**
     * terabyte
     */
    const TERABYTE = 'E35';

    /**
     * petabyte
     */
    const PETABYTE = 'E36';

    /**
     * pixel
     */
    const PIXEL = 'E37';

    /**
     * megapixel
     */
    const MEGAPIXEL = 'E38';

    /**
     * dots per inch
     */
    const DOTS_PER_INCH = 'E39';

    /**
     * gross kilogram
     */
    const GROSS_KILOGRAM = 'E4';

    /**
     * part per hundred thousand
     */
    const PART_PER_HUNDRED_THOUSAND = 'E40';

    /**
     * kilogram-force per square millimetre
     */
    const KILOGRAM_FORCE_PER_SQUARE_MILLIMETRE = 'E41';

    /**
     * kilogram-force per square centimetre
     */
    const KILOGRAM_FORCE_PER_SQUARE_CENTIMETRE = 'E42';

    /**
     * joule per square centimetre
     */
    const JOULE_PER_SQUARE_CENTIMETRE = 'E43';

    /**
     * kilogram-force metre per square centimetre
     */
    const KILOGRAM_FORCE_METRE_PER_SQUARE_CENTIMETRE = 'E44';

    /**
     * milliohm
     */
    const MILLIOHM = 'E45';

    /**
     * kilowatt hour per cubic metre
     */
    const KILOWATT_HOUR_PER_CUBIC_METRE = 'E46';

    /**
     * kilowatt hour per kelvin
     */
    const KILOWATT_HOUR_PER_KELVIN = 'E47';

    /**
     * service unit
     */
    const SERVICE_UNIT = 'E48';

    /**
     * working day
     */
    const WORKING_DAY = 'E49';

    /**
     * accounting unit
     */
    const ACCOUNTING_UNIT = 'E50';

    /**
     * job
     */
    const JOB = 'E51';

    /**
     * run foot
     */
    const RUN_FOOT = 'E52';

    /**
     * test
     */
    const TEST = 'E53';

    /**
     * trip
     */
    const TRIP = 'E54';

    /**
     * use
     * A unit of count defining the number of times an object is used
     */
    const USE_TIMES = 'E55';

    /**
     * well
     */
    const WELL = 'E56';

    /**
     * zone
     */
    const ZONE = 'E57';

    /**
     * exabit per second
     */
    const EXABIT_PER_SECOND = 'E58';

    /**
     * exbibyte
     */
    const EXBIBYTE = 'E59';

    /**
     * pebibyte
     */
    const PEBIBYTE = 'E60';

    /**
     * tebibyte
     */
    const TEBIBYTE = 'E61';

    /**
     * gibibyte
     */
    const GIBIBYTE = 'E62';

    /**
     * mebibyte
     */
    const MEBIBYTE = 'E63';

    /**
     * kibibyte
     */
    const KIBIBYTE = 'E64';

    /**
     * exbibit per metre
     */
    const EXBIBIT_PER_METRE = 'E65';

    /**
     * exbibit per square metre
     */
    const EXBIBIT_PER_SQUARE_METRE = 'E66';

    /**
     * exbibit per cubic metre
     */
    const EXBIBIT_PER_CUBIC_METRE = 'E67';

    /**
     * gigabyte per second
     */
    const GIGABYTE_PER_SECOND = 'E68';

    /**
     * gibibit per metre
     */
    const GIBIBIT_PER_METRE = 'E69';

    /**
     * gibibit per square metre
     */
    const GIBIBIT_PER_SQUARE_METRE = 'E70';

    /**
     * gibibit per cubic metre
     */
    const GIBIBIT_PER_CUBIC_METRE = 'E71';

    /**
     * kibibit per metre
     */
    const KIBIBIT_PER_METRE = 'E72';

    /**
     * kibibit per square metre
     */
    const KIBIBIT_PER_SQUARE_METRE = 'E73';

    /**
     * kibibit per cubic metre
     */
    const KIBIBIT_PER_CUBIC_METRE = 'E74';

    /**
     * mebibit per metre
     */
    const MEBIBIT_PER_METRE = 'E75';

    /**
     * mebibit per square metre
     */
    const MEBIBIT_PER_SQUARE_METRE = 'E76';

    /**
     * mebibit per cubic metre
     */
    const MEBIBIT_PER_CUBIC_METRE = 'E77';

    /**
     * petabit
     */
    const PETABIT = 'E78';

    /**
     * petabit per second
     */
    const PETABIT_PER_SECOND = 'E79';

    /**
     * pebibit per metre
     */
    const PEBIBIT_PER_METRE = 'E80';

    /**
     * pebibit per square metre
     */
    const PEBIBIT_PER_SQUARE_METRE = 'E81';

    /**
     * pebibit per cubic metre
     */
    const PEBIBIT_PER_CUBIC_METRE = 'E82';

    /**
     * terabit
     */
    const TERABIT = 'E83';

    /**
     * terabit per second
     */
    const TERABIT_PER_SECOND = 'E84';

    /**
     * tebibit per metre
     */
    const TEBIBIT_PER_METRE = 'E85';

    /**
     * tebibit per cubic metre
     */
    const TEBIBIT_PER_CUBIC_METRE = 'E86';

    /**
     * tebibit per square metre
     */
    const TEBIBIT_PER_SQUARE_METRE = 'E87';

    /**
     * bit per metre
     */
    const BIT_PER_METRE = 'E88';

    /**
     * bit per square metre
     */
    const BIT_PER_SQUARE_METRE = 'E89';

    /**
     * reciprocal centimetre
     */
    const RECIPROCAL_CENTIMETRE = 'E90';

    /**
     * reciprocal day
     */
    const RECIPROCAL_DAY = 'E91';

    /**
     * cubic decimetre per hour
     */
    const CUBIC_DECIMETRE_PER_HOUR = 'E92';

    /**
     * kilogram per hour
     */
    const KILOGRAM_PER_HOUR = 'E93';

    /**
     * kilomole per second
     */
    const KILOMOLE_PER_SECOND = 'E94';

    /**
     * mole per second
     */
    const MOLE_PER_SECOND = 'E95';

    /**
     * degree per second
     */
    const DEGREE_PER_SECOND = 'E96';

    /**
     * millimetre per degree Celcius metre
     */
    const MILLIMETRE_PER_DEGREE_CELCIUS_METRE = 'E97';

    /**
     * degree Celsius per kelvin
     */
    const DEGREE_CELSIUS_PER_KELVIN = 'E98';

    /**
     * hectopascal per bar
     */
    const HECTOPASCAL_PER_BAR = 'E99';

    /**
     * each
     */
    const EACH = 'EA';

    /**
     * electronic mail box
     */
    const ELECTRONIC_MAIL_BOX = 'EB';

    /**
     * equivalent gallon
     */
    const EQUIVALENT_GALLON = 'EQ';

    /**
     * bit per cubic metre
     */
    const BIT_PER_CUBIC_METRE = 'F01';

    /**
     * kelvin per kelvin
     */
    const KELVIN_PER_KELVIN = 'F02';

    /**
     * kilopascal per bar
     */
    const KILOPASCAL_PER_BAR = 'F03';

    /**
     * millibar per bar
     */
    const MILLIBAR_PER_BAR = 'F04';

    /**
     * megapascal per bar
     */
    const MEGAPASCAL_PER_BAR = 'F05';

    /**
     * poise per bar
     */
    const POISE_PER_BAR = 'F06';

    /**
     * pascal per bar
     */
    const PASCAL_PER_BAR = 'F07';

    /**
     * milliampere per inch
     */
    const MILLIAMPERE_PER_INCH = 'F08';

    /**
     * kelvin per hour
     */
    const KELVIN_PER_HOUR = 'F10';

    /**
     * kelvin per minute
     */
    const KELVIN_PER_MINUTE = 'F11';

    /**
     * kelvin per second
     */
    const KELVIN_PER_SECOND = 'F12';

    /**
     * slug
     */
    const SLUG = 'F13';

    /**
     * gram per kelvin
     */
    const GRAM_PER_KELVIN = 'F14';

    /**
     * kilogram per kelvin
     */
    const KILOGRAM_PER_KELVIN = 'F15';

    /**
     * milligram per kelvin
     */
    const MILLIGRAM_PER_KELVIN = 'F16';

    /**
     * pound-force per foot
     */
    const POUND_FORCE_PER_FOOT = 'F17';

    /**
     * kilogram square centimetre
     */
    const KILOGRAM_SQUARE_CENTIMETRE = 'F18';

    /**
     * kilogram square millimetre
     */
    const KILOGRAM_SQUARE_MILLIMETRE = 'F19';

    /**
     * pound inch squared
     */
    const POUND_INCH_SQUARED = 'F20';

    /**
     * pound-force inch
     */
    const POUND_FORCE_INCH = 'F21';

    /**
     * pound-force foot per ampere
     */
    const POUND_FORCE_FOOT_PER_AMPERE = 'F22';

    /**
     * gram per cubic decimetre
     */
    const GRAM_PER_CUBIC_DECIMETRE = 'F23';

    /**
     * kilogram per kilomol
     */
    const KILOGRAM_PER_KILOMOL = 'F24';

    /**
     * gram per hertz
     */
    const GRAM_PER_HERTZ = 'F25';

    /**
     * gram per day
     */
    const GRAM_PER_DAY = 'F26';

    /**
     * gram per hour
     */
    const GRAM_PER_HOUR = 'F27';

    /**
     * gram per minute
     */
    const GRAM_PER_MINUTE = 'F28';

    /**
     * gram per second
     */
    const GRAM_PER_SECOND = 'F29';

    /**
     * kilogram per day
     */
    const KILOGRAM_PER_DAY = 'F30';

    /**
     * kilogram per minute
     */
    const KILOGRAM_PER_MINUTE = 'F31';

    /**
     * milligram per day
     */
    const MILLIGRAM_PER_DAY = 'F32';

    /**
     * milligram per minute
     */
    const MILLIGRAM_PER_MINUTE = 'F33';

    /**
     * milligram per second
     */
    const MILLIGRAM_PER_SECOND = 'F34';

    /**
     * gram per day kelvin
     */
    const GRAM_PER_DAY_KELVIN = 'F35';

    /**
     * gram per hour kelvin
     */
    const GRAM_PER_HOUR_KELVIN = 'F36';

    /**
     * gram per minute kelvin
     */
    const GRAM_PER_MINUTE_KELVIN = 'F37';

    /**
     * gram per second kelvin
     */
    const GRAM_PER_SECOND_KELVIN = 'F38';

    /**
     * kilogram per day kelvin
     */
    const KILOGRAM_PER_DAY_KELVIN = 'F39';

    /**
     * kilogram per hour kelvin
     */
    const KILOGRAM_PER_HOUR_KELVIN = 'F40';

    /**
     * kilogram per minute kelvin
     */
    const KILOGRAM_PER_MINUTE_KELVIN = 'F41';

    /**
     * kilogram per second kelvin
     */
    const KILOGRAM_PER_SECOND_KELVIN = 'F42';

    /**
     * milligram per day kelvin
     */
    const MILLIGRAM_PER_DAY_KELVIN = 'F43';

    /**
     * milligram per hour kelvin
     */
    const MILLIGRAM_PER_HOUR_KELVIN = 'F44';

    /**
     * milligram per minute kelvin
     */
    const MILLIGRAM_PER_MINUTE_KELVIN = 'F45';

    /**
     * milligram per second kelvin
     */
    const MILLIGRAM_PER_SECOND_KELVIN = 'F46';

    /**
     * newton per millimetre
     */
    const NEWTON_PER_MILLIMETRE = 'F47';

    /**
     * pound-force per inch
     */
    const POUND_FORCE_PER_INCH = 'F48';

    /**
     * rod [unit of distance]
     */
    const ROD_DISTANCE = 'F49';

    /**
     * micrometre per kelvin
     */
    const MICROMETRE_PER_KELVIN = 'F50';

    /**
     * centimetre per kelvin
     */
    const CENTIMETRE_PER_KELVIN = 'F51';

    /**
     * metre per kelvin
     */
    const METRE_PER_KELVIN = 'F52';

    /**
     * millimetre per kelvin
     */
    const MILLIMETRE_PER_KELVIN = 'F53';

    /**
     * milliohm per metre
     */
    const MILLIOHM_PER_METRE = 'F54';

    /**
     * ohm per mile (statute mile)
     */
    const OHM_PER_MILE_STATUTE_MILE = 'F55';

    /**
     * ohm per kilometre
     */
    const OHM_PER_KILOMETRE = 'F56';

    /**
     * milliampere per pound-force per square inch
     */
    const MILLIAMPERE_PER_POUND_FORCE_PER_SQUARE_INCH = 'F57';

    /**
     * reciprocal bar
     */
    const RECIPROCAL_BAR = 'F58';

    /**
     * milliampere per bar
     */
    const MILLIAMPERE_PER_BAR = 'F59';

    /**
     * degree Celsius per bar
     */
    const DEGREE_CELSIUS_PER_BAR = 'F60';

    /**
     * kelvin per bar
     */
    const KELVIN_PER_BAR = 'F61';

    /**
     * gram per day bar
     */
    const GRAM_PER_DAY_BAR = 'F62';

    /**
     * gram per hour bar
     */
    const GRAM_PER_HOUR_BAR = 'F63';

    /**
     * gram per minute bar
     */
    const GRAM_PER_MINUTE_BAR = 'F64';

    /**
     * gram per second bar
     */
    const GRAM_PER_SECOND_BAR = 'F65';

    /**
     * kilogram per day bar
     */
    const KILOGRAM_PER_DAY_BAR = 'F66';

    /**
     * kilogram per hour bar
     */
    const KILOGRAM_PER_HOUR_BAR = 'F67';

    /**
     * kilogram per minute bar
     */
    const KILOGRAM_PER_MINUTE_BAR = 'F68';

    /**
     * kilogram per second bar
     */
    const KILOGRAM_PER_SECOND_BAR = 'F69';

    /**
     * milligram per day bar
     */
    const MILLIGRAM_PER_DAY_BAR = 'F70';

    /**
     * milligram per hour bar
     */
    const MILLIGRAM_PER_HOUR_BAR = 'F71';

    /**
     * milligram per minute bar
     */
    const MILLIGRAM_PER_MINUTE_BAR = 'F72';

    /**
     * milligram per second bar
     */
    const MILLIGRAM_PER_SECOND_BAR = 'F73';

    /**
     * gram per bar
     */
    const GRAM_PER_BAR = 'F74';

    /**
     * milligram per bar
     */
    const MILLIGRAM_PER_BAR = 'F75';

    /**
     * milliampere per millimetre
     */
    const MILLIAMPERE_PER_MILLIMETRE = 'F76';

    /**
     * pascal second per kelvin
     */
    const PASCAL_SECOND_PER_KELVIN = 'F77';

    /**
     * inch of water
     */
    const INCH_OF_WATER = 'F78';

    /**
     * inch of mercury
     */
    const INCH_OF_MERCURY = 'F79';

    /**
     * water horse power
     */
    const WATER_HORSE_POWER = 'F80';

    /**
     * bar per kelvin
     */
    const BAR_PER_KELVIN = 'F81';

    /**
     * hectopascal per kelvin
     */
    const HECTOPASCAL_PER_KELVIN = 'F82';

    /**
     * kilopascal per kelvin
     */
    const KILOPASCAL_PER_KELVIN = 'F83';

    /**
     * millibar per kelvin
     */
    const MILLIBAR_PER_KELVIN = 'F84';

    /**
     * megapascal per kelvin
     */
    const MEGAPASCAL_PER_KELVIN = 'F85';

    /**
     * poise per kelvin
     */
    const POISE_PER_KELVIN = 'F86';

    /**
     * volt per litre minute
     */
    const VOLT_PER_LITRE_MINUTE = 'F87';

    /**
     * newton centimetre
     */
    const NEWTON_CENTIMETRE = 'F88';

    /**
     * newton metre per degree
     */
    const NEWTON_METRE_PER_DEGREE = 'F89';

    /**
     * newton metre per ampere
     */
    const NEWTON_METRE_PER_AMPERE = 'F90';

    /**
     * bar litre per second
     */
    const BAR_LITRE_PER_SECOND = 'F91';

    /**
     * bar cubic metre per second
     */
    const BAR_CUBIC_METRE_PER_SECOND = 'F92';

    /**
     * hectopascal litre per second
     */
    const HECTOPASCAL_LITRE_PER_SECOND = 'F93';

    /**
     * hectopascal cubic metre per second
     */
    const HECTOPASCAL_CUBIC_METRE_PER_SECOND = 'F94';

    /**
     * millibar litre per second
     */
    const MILLIBAR_LITRE_PER_SECOND = 'F95';

    /**
     * millibar cubic metre per second
     */
    const MILLIBAR_CUBIC_METRE_PER_SECOND = 'F96';

    /**
     * megapascal litre per second
     */
    const MEGAPASCAL_LITRE_PER_SECOND = 'F97';

    /**
     * megapascal cubic metre per second
     */
    const MEGAPASCAL_CUBIC_METRE_PER_SECOND = 'F98';

    /**
     * pascal litre per second
     */
    const PASCAL_LITRE_PER_SECOND = 'F99';

    /**
     * degree Fahrenheit
     */
    const DEGREE_FAHRENHEIT = 'FAH';

    /**
     * farad
     */
    const FARAD = 'FAR';

    /**
     * fibre metre
     */
    const FIBRE_METRE = 'FBM';

    /**
     * thousand cubic foot
     */
    const THOUSAND_CUBIC_FOOT = 'FC';

    /**
     * hundred cubic metre
     */
    const HUNDRED_CUBIC_METRE = 'FF';

    /**
     * micromole
     */
    const MICROMOLE = 'FH';

    /**
     * failures in time
     */
    const FAILURES_IN_TIME = 'FIT';

    /**
     * flake ton
     */
    const FLAKE_TON = 'FL';

    /**
     * foot
     */
    const FOOT = 'FOT';

    /**
     * pound per square foot
     */
    const POUND_PER_SQUARE_FOOT = 'FP';

    /**
     * foot per minute
     */
    const FOOT_PER_MINUTE = 'FR';

    /**
     * foot per second
     */
    const FOOT_PER_SECOND = 'FS';

    /**
     * square foot
     */
    const SQUARE_FOOT = 'FTK';

    /**
     * cubic foot
     */
    const CUBIC_FOOT = 'FTQ';

    /**
     * pascal cubic metre per second
     */
    const PASCAL_CUBIC_METRE_PER_SECOND = 'G01';

    /**
     * centimetre per bar
     */
    const CENTIMETRE_PER_BAR = 'G04';

    /**
     * metre per bar
     */
    const METRE_PER_BAR = 'G05';

    /**
     * millimetre per bar
     */
    const MILLIMETRE_PER_BAR = 'G06';

    /**
     * square inch per second
     */
    const SQUARE_INCH_PER_SECOND = 'G08';

    /**
     * square metre per second kelvin
     */
    const SQUARE_METRE_PER_SECOND_KELVIN = 'G09';

    /**
     * stokes per kelvin
     */
    const STOKES_PER_KELVIN = 'G10';

    /**
     * gram per cubic centimetre bar
     */
    const GRAM_PER_CUBIC_CENTIMETRE_BAR = 'G11';

    /**
     * gram per cubic decimetre bar
     */
    const GRAM_PER_CUBIC_DECIMETRE_BAR = 'G12';

    /**
     * gram per litre bar
     */
    const GRAM_PER_LITRE_BAR = 'G13';

    /**
     * gram per cubic metre bar
     */
    const GRAM_PER_CUBIC_METRE_BAR = 'G14';

    /**
     * gram per millilitre bar
     */
    const GRAM_PER_MILLILITRE_BAR = 'G15';

    /**
     * kilogram per cubic centimetre bar
     */
    const KILOGRAM_PER_CUBIC_CENTIMETRE_BAR = 'G16';

    /**
     * kilogram per litre bar
     */
    const KILOGRAM_PER_LITRE_BAR = 'G17';

    /**
     * kilogram per cubic metre bar
     */
    const KILOGRAM_PER_CUBIC_METRE_BAR = 'G18';

    /**
     * newton metre per kilogram
     */
    const NEWTON_METRE_PER_KILOGRAM = 'G19';

    /**
     * US gallon per minute
     */
    const US_GALLON_PER_MINUTE = 'G2';

    /**
     * pound-force foot per pound
     */
    const POUND_FORCE_FOOT_PER_POUND = 'G20';

    /**
     * cup [unit of volume]
     */
    const CUP_VOLUME = 'G21';

    /**
     * peck
     */
    const PECK = 'G23';

    /**
     * tablespoon (US)
     */
    const TABLESPOON_US = 'G24';

    /**
     * teaspoon (US)
     */
    const TEASPOON_US = 'G25';

    /**
     * stere
     */
    const STERE = 'G26';

    /**
     * cubic centimetre per kelvin
     */
    const CUBIC_CENTIMETRE_PER_KELVIN = 'G27';

    /**
     * litre per kelvin
     */
    const LITRE_PER_KELVIN = 'G28';

    /**
     * cubic metre per kelvin
     */
    const CUBIC_METRE_PER_KELVIN = 'G29';

    /**
     * Imperial gallon per minute
     */
    const IMPERIAL_GALLON_PER_MINUTE = 'G3';

    /**
     * millilitre per kelvin
     */
    const MILLILITRE_PER_KELVIN = 'G30';

    /**
     * kilogram per cubic centimetre
     */
    const KILOGRAM_PER_CUBIC_CENTIMETRE = 'G31';

    /**
     * ounce (avoirdupois) per cubic yard
     */
    const OUNCE_PER_CUBIC_YARD = 'G32';

    /**
     * gram per cubic centimetre kelvin
     */
    const GRAM_PER_CUBIC_CENTIMETRE_KELVIN = 'G33';

    /**
     * gram per cubic decimetre kelvin
     */
    const GRAM_PER_CUBIC_DECIMETRE_KELVIN = 'G34';

    /**
     * gram per litre kelvin
     */
    const GRAM_PER_LITRE_KELVIN = 'G35';

    /**
     * gram per cubic metre kelvin
     */
    const GRAM_PER_CUBIC_METRE_KELVIN = 'G36';

    /**
     * gram per millilitre kelvin
     */
    const GRAM_PER_MILLILITRE_KELVIN = 'G37';

    /**
     * kilogram per cubic centimetre kelvin
     */
    const KILOGRAM_PER_CUBIC_CENTIMETRE_KELVIN = 'G38';

    /**
     * kilogram per litre kelvin
     */
    const KILOGRAM_PER_LITRE_KELVIN = 'G39';

    /**
     * kilogram per cubic metre kelvin
     */
    const KILOGRAM_PER_CUBIC_METRE_KELVIN = 'G40';

    /**
     * square metre per second bar
     */
    const SQUARE_METRE_PER_SECOND_BAR = 'G41';

    /**
     * microsiemens per centimetre
     */
    const MICROSIEMENS_PER_CENTIMETRE = 'G42';

    /**
     * microsiemens per metre
     */
    const MICROSIEMENS_PER_METRE = 'G43';

    /**
     * nanosiemens per centimetre
     */
    const NANOSIEMENS_PER_CENTIMETRE = 'G44';

    /**
     * nanosiemens per metre
     */
    const NANOSIEMENS_PER_METRE = 'G45';

    /**
     * stokes per bar
     */
    const STOKES_PER_BAR = 'G46';

    /**
     * cubic centimetre per day
     */
    const CUBIC_CENTIMETRE_PER_DAY = 'G47';

    /**
     * cubic centimetre per hour
     */
    const CUBIC_CENTIMETRE_PER_HOUR = 'G48';

    /**
     * cubic centimetre per minute
     */
    const CUBIC_CENTIMETRE_PER_MINUTE = 'G49';

    /**
     * gallon (US) per hour
     */
    const GALLON_US_PER_HOUR = 'G50';

    /**
     * litre per second
     */
    const LITRE_PER_SECOND = 'G51';

    /**
     * cubic metre per day
     */
    const CUBIC_METRE_PER_DAY = 'G52';

    /**
     * cubic metre per minute
     */
    const CUBIC_METRE_PER_MINUTE = 'G53';

    /**
     * millilitre per day
     */
    const MILLILITRE_PER_DAY = 'G54';

    /**
     * millilitre per hour
     */
    const MILLILITRE_PER_HOUR = 'G55';

    /**
     * cubic inch per hour
     */
    const CUBIC_INCH_PER_HOUR = 'G56';

    /**
     * cubic inch per minute
     */
    const CUBIC_INCH_PER_MINUTE = 'G57';

    /**
     * cubic inch per second
     */
    const CUBIC_INCH_PER_SECOND = 'G58';

    /**
     * milliampere per litre minute
     */
    const MILLIAMPERE_PER_LITRE_MINUTE = 'G59';

    /**
     * volt per bar
     */
    const VOLT_PER_BAR = 'G60';

    /**
     * cubic centimetre per day kelvin
     */
    const CUBIC_CENTIMETRE_PER_DAY_KELVIN = 'G61';

    /**
     * cubic centimetre per hour kelvin
     */
    const CUBIC_CENTIMETRE_PER_HOUR_KELVIN = 'G62';

    /**
     * cubic centimetre per minute kelvin
     */
    const CUBIC_CENTIMETRE_PER_MINUTE_KELVIN = 'G63';

    /**
     * cubic centimetre per second kelvin
     */
    const CUBIC_CENTIMETRE_PER_SECOND_KELVIN = 'G64';

    /**
     * litre per day kelvin
     */
    const LITRE_PER_DAY_KELVIN = 'G65';

    /**
     * litre per hour kelvin
     */
    const LITRE_PER_HOUR_KELVIN = 'G66';

    /**
     * litre per minute kelvin
     */
    const LITRE_PER_MINUTE_KELVIN = 'G67';

    /**
     * litre per second kelvin
     */
    const LITRE_PER_SECOND_KELVIN = 'G68';

    /**
     * cubic metre per day kelvin
     */
    const CUBIC_METRE_PER_DAY_KELVIN = 'G69';

    /**
     * cubic metre per hour kelvin
     */
    const CUBIC_METRE_PER_HOUR_KELVIN = 'G70';

    /**
     * cubic metre per minute kelvin
     */
    const CUBIC_METRE_PER_MINUTE_KELVIN = 'G71';

    /**
     * cubic metre per second kelvin
     */
    const CUBIC_METRE_PER_SECOND_KELVIN = 'G72';

    /**
     * millilitre per day kelvin
     */
    const MILLILITRE_PER_DAY_KELVIN = 'G73';

    /**
     * millilitre per hour kelvin
     */
    const MILLILITRE_PER_HOUR_KELVIN = 'G74';

    /**
     * millilitre per minute kelvin
     */
    const MILLILITRE_PER_MINUTE_KELVIN = 'G75';

    /**
     * millilitre per second kelvin
     */
    const MILLILITRE_PER_SECOND_KELVIN = 'G76';

    /**
     * millimetre to the fourth power
     */
    const MILLIMETRE_TO_FOURTH_POWER = 'G77';

    /**
     * cubic centimetre per day bar
     */
    const CUBIC_CENTIMETRE_PER_DAY_BAR = 'G78';

    /**
     * cubic centimetre per hour bar
     */
    const CUBIC_CENTIMETRE_PER_HOUR_BAR = 'G79';

    /**
     * cubic centimetre per minute bar
     */
    const CUBIC_CENTIMETRE_PER_MINUTE_BAR = 'G80';

    /**
     * cubic centimetre per second bar
     */
    const CUBIC_CENTIMETRE_PER_SECOND_BAR = 'G81';

    /**
     * litre per day bar
     */
    const LITRE_PER_DAY_BAR = 'G82';

    /**
     * litre per hour bar
     */
    const LITRE_PER_HOUR_BAR = 'G83';

    /**
     * litre per minute bar
     */
    const LITRE_PER_MINUTE_BAR = 'G84';

    /**
     * litre per second bar
     */
    const LITRE_PER_SECOND_BAR = 'G85';

    /**
     * cubic metre per day bar
     */
    const CUBIC_METRE_PER_DAY_BAR = 'G86';

    /**
     * cubic metre per hour bar
     */
    const CUBIC_METRE_PER_HOUR_BAR = 'G87';

    /**
     * cubic metre per minute bar
     */
    const CUBIC_METRE_PER_MINUTE_BAR = 'G88';

    /**
     * cubic metre per second bar
     */
    const CUBIC_METRE_PER_SECOND_BAR = 'G89';

    /**
     * millilitre per day bar
     */
    const MILLILITRE_PER_DAY_BAR = 'G90';

    /**
     * millilitre per hour bar
     */
    const MILLILITRE_PER_HOUR_BAR = 'G91';

    /**
     * millilitre per minute bar
     */
    const MILLILITRE_PER_MINUTE_BAR = 'G92';

    /**
     * millilitre per second bar
     */
    const MILLILITRE_PER_SECOND_BAR = 'G93';

    /**
     * cubic centimetre per bar
     */
    const CUBIC_CENTIMETRE_PER_BAR = 'G94';

    /**
     * litre per bar
     */
    const LITRE_PER_BAR = 'G95';

    /**
     * cubic metre per bar
     */
    const CUBIC_METRE_PER_BAR = 'G96';

    /**
     * millilitre per bar
     */
    const MILLILITRE_PER_BAR = 'G97';

    /**
     * microhenry per kiloohm
     */
    const MICROHENRY_PER_KILOOHM = 'G98';

    /**
     * microhenry per ohm
     */
    const MICROHENRY_PER_OHM = 'G99';

    /**
     * gallon (US) per day
     */
    const GALLON_US_PER_DAY = 'GB';

    /**
     * gigabecquerel
     */
    const GIGABECQUEREL = 'GBQ';

    /**
     * gram, dry weight
     */
    const GRAM_DRY_WEIGHT = 'GDW';

    /**
     * pound per gallon (US)
     */
    const POUND_PER_GALLON_US = 'GE';

    /**
     * gram per metre (gram per 100 centimetres)
     */
    const GRAM_PER_METRE = 'GF';

    /**
     * gram of fissile isotope
     */
    const GRAM_OF_FISSILE_ISOTOPE = 'GFI';

    /**
     * great gross
     */
    const GREAT_GROSS = 'GGR';

    /**
     * gill (US)
     */
    const GILL_US = 'GIA';

    /**
     * gram, including container
     */
    const GRAM_INCL_CONTAINER = 'GIC';

    /**
     * gill (UK)
     */
    const GILL_UK = 'GII';

    /**
     * gram, including inner packaging
     */
    const GRAM_INCL_INNER_PACKAGING = 'GIP';

    /**
     * gram per millilitre
     */
    const GRAM_PER_MILLILITRE = 'GJ';

    /**
     * gram per litre
     */
    const GRAM_PER_LITRE = 'GL';

    /**
     * dry gallon (US)
     */
    const DRY_GALLON_US = 'GLD';

    /**
     * gallon (UK)
     */
    const GALLON_UK = 'GLI';

    /**
     * gallon (US)
     */
    const GALLON_US = 'GLL';

    /**
     * gram per square metre
     */
    const GRAM_PER_SQUARE_METRE = 'GM';

    /**
     * milligram per square metre
     */
    const MILLIGRAM_PER_SQUARE_METRE = 'GO';

    /**
     * milligram per cubic metre
     */
    const MILLIGRAM_PER_CUBIC_METRE = 'GP';

    /**
     * microgram per cubic metre
     */
    const MICROGRAM_PER_CUBIC_METRE = 'GQ';

    /**
     * gram
     */
    const GRAM = 'GRM';

    /**
     * grain
     */
    const GRAIN = 'GRN';

    /**
     * gross
     */
    const GROSS = 'GRO';

    /**
     * gigajoule
     */
    const GIGAJOULE = 'GV';

    /**
     * gigawatt hour
     */
    const GIGAWATT_HOUR = 'GWH';

    /**
     * henry per kiloohm
     */
    const HENRY_PER_KILOOHM = 'H03';

    /**
     * henry per ohm
     */
    const HENRY_PER_OHM = 'H04';

    /**
     * millihenry per kiloohm
     */
    const MILLIHENRY_PER_KILOOHM = 'H05';

    /**
     * millihenry per ohm
     */
    const MILLIHENRY_PER_OHM = 'H06';

    /**
     * pascal second per bar
     */
    const PASCAL_SECOND_PER_BAR = 'H07';

    /**
     * microbecquerel
     */
    const MICROBECQUEREL = 'H08';

    /**
     * reciprocal year
     */
    const RECIPROCAL_YEAR = 'H09';

    /**
     * reciprocal hour
     */
    const RECIPROCAL_HOUR = 'H10';

    /**
     * reciprocal month
     */
    const RECIPROCAL_MONTH = 'H11';

    /**
     * degree Celsius per hour
     */
    const DEGREE_CELSIUS_PER_HOUR = 'H12';

    /**
     * degree Celsius per minute
     */
    const DEGREE_CELSIUS_PER_MINUTE = 'H13';

    /**
     * degree Celsius per second
     */
    const DEGREE_CELSIUS_PER_SECOND = 'H14';

    /**
     * square centimetre per gram
     */
    const SQUARE_CENTIMETRE_PER_GRAM = 'H15';

    /**
     * square decametre
     */
    const SQUARE_DECAMETRE = 'H16';

    /**
     * square hectometre
     */
    const SQUARE_HECTOMETRE = 'H18';

    /**
     * cubic hectometre
     */
    const CUBIC_HECTOMETRE = 'H19';

    /**
     * cubic kilometre
     */
    const CUBIC_KILOMETRE = 'H20';

    /**
     * blank
     */
    const BLANK = 'H21';

    /**
     * volt square inch per pound-force
     */
    const VOLT_SQUARE_INCH_PER_POUND_FORCE = 'H22';

    /**
     * volt per inch
     */
    const VOLT_PER_INCH = 'H23';

    /**
     * volt per microsecond
     */
    const VOLT_PER_MICROSECOND = 'H24';

    /**
     * percent per kelvin
     */
    const PERCENT_PER_KELVIN = 'H25';

    /**
     * ohm per metre
     */
    const OHM_PER_METRE = 'H26';

    /**
     * degree per metre
     */
    const DEGREE_PER_METRE = 'H27';

    /**
     * microfarad per kilometre
     */
    const MICROFARAD_PER_KILOMETRE = 'H28';

    /**
     * microgram per litre
     */
    const MICROGRAM_PER_LITRE = 'H29';

    /**
     * square micrometre (square micron)
     */
    const SQUARE_MICROMETRE_SQUARE_MICRON = 'H30';

    /**
     * ampere per kilogram
     */
    const AMPERE_PER_KILOGRAM = 'H31';

    /**
     * ampere squared second
     */
    const AMPERE_SQUARED_SECOND = 'H32';

    /**
     * farad per kilometre
     */
    const FARAD_PER_KILOMETRE = 'H33';

    /**
     * hertz metre
     */
    const HERTZ_METRE = 'H34';

    /**
     * kelvin metre per watt
     */
    const KELVIN_METRE_PER_WATT = 'H35';

    /**
     * megaohm per kilometre
     */
    const MEGAOHM_PER_KILOMETRE = 'H36';

    /**
     * megaohm per metre
     */
    const MEGAOHM_PER_METRE = 'H37';

    /**
     * megaampere
     */
    const MEGAAMPERE = 'H38';

    /**
     * megahertz kilometre
     */
    const MEGAHERTZ_KILOMETRE = 'H39';

    /**
     * newton per ampere
     */
    const NEWTON_PER_AMPERE = 'H40';

    /**
     * newton metre watt to the power minus 0,5
     */
    const NEWTON_METRE_WATT_TO_POWER_MINUS_05 = 'H41';

    /**
     * pascal per metre
     */
    const PASCAL_PER_METRE = 'H42';

    /**
     * siemens per centimetre
     */
    const SIEMENS_PER_CENTIMETRE = 'H43';

    /**
     * teraohm
     */
    const TERAOHM = 'H44';

    /**
     * volt second per metre
     */
    const VOLT_SECOND_PER_METRE = 'H45';

    /**
     * volt per second
     */
    const VOLT_PER_SECOND = 'H46';

    /**
     * watt per cubic metre
     */
    const WATT_PER_CUBIC_METRE = 'H47';

    /**
     * attofarad
     */
    const ATTOFARAD = 'H48';

    /**
     * centimetre per hour
     */
    const CENTIMETRE_PER_HOUR = 'H49';

    /**
     * reciprocal cubic centimetre
     */
    const RECIPROCAL_CUBIC_CENTIMETRE = 'H50';

    /**
     * decibel per kilometre
     */
    const DECIBEL_PER_KILOMETRE = 'H51';

    /**
     * decibel per metre
     */
    const DECIBEL_PER_METRE = 'H52';

    /**
     * kilogram per bar
     */
    const KILOGRAM_PER_BAR = 'H53';

    /**
     * kilogram per cubic decimetre kelvin
     */
    const KILOGRAM_PER_CUBIC_DECIMETRE_KELVIN = 'H54';

    /**
     * kilogram per cubic decimetre bar
     */
    const KILOGRAM_PER_CUBIC_DECIMETRE_BAR = 'H55';

    /**
     * kilogram per square metre second
     */
    const KILOGRAM_PER_SQUARE_METRE_SECOND = 'H56';

    /**
     * inch per two pi radiant
     */
    const INCH_PER_TWO_PI_RADIANT = 'H57';

    /**
     * metre per volt second
     */
    const METRE_PER_VOLT_SECOND = 'H58';

    /**
     * square metre per newton
     */
    const SQUARE_METRE_PER_NEWTON = 'H59';

    /**
     * cubic metre per cubic metre
     */
    const CUBIC_METRE_PER_CUBIC_METRE = 'H60';

    /**
     * millisiemens per centimetre
     */
    const MILLISIEMENS_PER_CENTIMETRE = 'H61';

    /**
     * millivolt per minute
     */
    const MILLIVOLT_PER_MINUTE = 'H62';

    /**
     * milligram per square centimetre
     */
    const MILLIGRAM_PER_SQUARE_CENTIMETRE = 'H63';

    /**
     * milligram per gram
     */
    const MILLIGRAM_PER_GRAM = 'H64';

    /**
     * millilitre per cubic metre
     */
    const MILLILITRE_PER_CUBIC_METRE = 'H65';

    /**
     * millimetre per year
     */
    const MILLIMETRE_PER_YEAR = 'H66';

    /**
     * millimetre per hour
     */
    const MILLIMETRE_PER_HOUR = 'H67';

    /**
     * millimole per gram
     */
    const MILLIMOLE_PER_GRAM = 'H68';

    /**
     * picopascal per kilometre
     */
    const PICOPASCAL_PER_KILOMETRE = 'H69';

    /**
     * picosecond
     */
    const PICOSECOND = 'H70';

    /**
     * percent per month
     */
    const PERCENT_PER_MONTH = 'H71';

    /**
     * percent per hectobar
     */
    const PERCENT_PER_HECTOBAR = 'H72';

    /**
     * percent per decakelvin
     */
    const PERCENT_PER_DECAKELVIN = 'H73';

    /**
     * watt per metre
     */
    const WATT_PER_METRE = 'H74';

    /**
     * decapascal
     */
    const DECAPASCAL = 'H75';

    /**
     * gram per millimetre
     */
    const GRAM_PER_MILLIMETRE = 'H76';

    /**
     * module width
     */
    const MODULE_WIDTH = 'H77';

    /**
     * French gauge
     */
    const FRENCH_GAUGE = 'H79';

    /**
     * rack unit
     */
    const RACK_UNIT = 'H80';

    /**
     * millimetre per minute
     */
    const MILLIMETRE_PER_MINUTE = 'H81';

    /**
     * big point
     */
    const BIG_POINT = 'H82';

    /**
     * litre per kilogram
     */
    const LITRE_PER_KILOGRAM = 'H83';

    /**
     * gram millimetre
     */
    const GRAM_MILLIMETRE = 'H84';

    /**
     * reciprocal week
     */
    const RECIPROCAL_WEEK = 'H85';

    /**
     * piece
     */
    const PIECE = 'H87';

    /**
     * megaohm kilometre
     */
    const MEGAOHM_KILOMETRE = 'H88';

    /**
     * percent per ohm
     */
    const PERCENT_PER_OHM = 'H89';

    /**
     * percent per degree
     */
    const PERCENT_PER_DEGREE = 'H90';

    /**
     * percent per ten thousand
     */
    const PERCENT_PER_TEN_THOUSAND = 'H91';

    /**
     * percent per one hundred thousand
     */
    const PERCENT_PER_ONE_HUNDRED_THOUSAND = 'H92';

    /**
     * percent per hundred
     */
    const PERCENT_PER_HUNDRED = 'H93';

    /**
     * percent per thousand
     */
    const PERCENT_PER_THOUSAND = 'H94';

    /**
     * percent per volt
     */
    const PERCENT_PER_VOLT = 'H95';

    /**
     * percent per bar
     */
    const PERCENT_PER_BAR = 'H96';

    /**
     * percent per inch
     */
    const PERCENT_PER_INCH = 'H98';

    /**
     * percent per metre
     */
    const PERCENT_PER_METRE = 'H99';

    /**
     * hank
     */
    const HANK = 'HA';

    /**
     * hectobar
     */
    const HECTOBAR = 'HBA';

    /**
     * hundred boxes
     */
    const HUNDRED_BOXES = 'HBX';

    /**
     * hundred count
     */
    const HUNDRED_COUNT = 'HC';

    /**
     * hundred kilogram, dry weight
     */
    const HUNDRED_KILOGRAM_DRY_WEIGHT = 'HDW';

    /**
     * head
     */
    const HEAD = 'HEA';

    /**
     * hectogram
     */
    const HECTOGRAM = 'HGM';

    /**
     * hundred cubic foot
     */
    const HUNDRED_CUBIC_FOOT = 'HH';

    /**
     * hundred international unit
     */
    const HUNDRED_INTERNATIONAL_UNIT = 'HIU';

    /**
     * hundred kilogram, net mass
     */
    const HUNDRED_KILOGRAM_NET_MASS = 'HKM';

    /**
     * hectolitre
     */
    const HECTOLITRE = 'HLT';

    /**
     * mile per hour (statute mile)
     */
    const MILE_PER_HOUR_STATUTE_MILE = 'HM';

    /**
     * million cubic metre
     */
    const MILLION_CUBIC_METRE = 'HMQ';

    /**
     * hectometre
     */
    const HECTOMETRE = 'HMT';

    /**
     * hectolitre of pure alcohol
     */
    const HECTOLITRE_OF_PURE_ALCOHOL = 'HPA';

    /**
     * hertz
     */
    const HERTZ = 'HTZ';

    /**
     * hour
     */
    const HOUR = 'HUR';

    /**
     * inch pound (pound inch)
     */
    const INCH_POUND_POUND_INCH = 'IA';

    /**
     * person
     */
    const PERSON = 'IE';

    /**
     * inch
     */
    const INCH = 'INH';

    /**
     * square inch
     */
    const SQUARE_INCH = 'INK';

    /**
     * cubic inch
     */
    const CUBIC_INCH = 'INQ';

    /**
     * international sugar degree
     */
    const INTERNATIONAL_SUGAR_DEGREE = 'ISD';

    /**
     * inch per second
     */
    const INCH_PER_SECOND = 'IU';

    /**
     * inch per second squared
     */
    const INCH_PER_SECOND_SQUARED = 'IV';

    /**
     * percent per millimetre
     */
    const PERCENT_PER_MILLIMETRE = 'J10';

    /**
     * per mille per psi
     */
    const PER_MILLE_PER_PSI = 'J12';

    /**
     * degree API
     */
    const DEGREE_API = 'J13';

    /**
     * degree Baume (origin scale)
     */
    const DEGREE_BAUME_ORIGIN_SCALE = 'J14';

    /**
     * degree Baume (US heavy)
     */
    const DEGREE_BAUME_US_HEAVY = 'J15';

    /**
     * degree Baume (US light)
     */
    const DEGREE_BAUME_US_LIGHT = 'J16';

    /**
     * degree Balling
     */
    const DEGREE_BALLING = 'J17';

    /**
     * degree Brix
     */
    const DEGREE_BRIX = 'J18';

    /**
     * degree Fahrenheit hour square foot per British thermal unit (thermochemical)
     */
    const DEGREE_FAHRENHEIT_HOUR_SQUARE_FOOT_PER_BRITISH_THERMAL_UNIT_THERM = 'J19';

    /**
     * joule per kilogram
     */
    const JOULE_PER_KILOGRAM = 'J2';

    /**
     * degree Fahrenheit per kelvin
     */
    const DEGREE_FAHRENHEIT_PER_KELVIN = 'J20';

    /**
     * degree Fahrenheit per bar
     */
    const DEGREE_FAHRENHEIT_PER_BAR = 'J21';

    /**
     * degree Fahrenheit hour square foot per British thermal unit (international table)
     */
    const DEGREE_FAHRENHEIT_HOUR_SQUARE_FOOT_PER_BRITISH_THERMAL_UNIT = 'J22';

    /**
     * degree Fahrenheit per hour
     */
    const DEGREE_FAHRENHEIT_PER_HOUR = 'J23';

    /**
     * degree Fahrenheit per minute
     */
    const DEGREE_FAHRENHEIT_PER_MINUTE = 'J24';

    /**
     * degree Fahrenheit per second
     */
    const DEGREE_FAHRENHEIT_PER_SECOND = 'J25';

    /**
     * reciprocal degree Fahrenheit
     */
    const RECIPROCAL_DEGREE_FAHRENHEIT = 'J26';

    /**
     * degree Oechsle
     */
    const DEGREE_OECHSLE = 'J27';

    /**
     * degree Rankine per hour
     */
    const DEGREE_RANKINE_PER_HOUR = 'J28';

    /**
     * degree Rankine per minute
     */
    const DEGREE_RANKINE_PER_MINUTE = 'J29';

    /**
     * degree Rankine per second
     */
    const DEGREE_RANKINE_PER_SECOND = 'J30';

    /**
     * degree Twaddell
     */
    const DEGREE_TWADDELL = 'J31';

    /**
     * micropoise
     */
    const MICROPOISE = 'J32';

    /**
     * microgram per kilogram
     */
    const MICROGRAM_PER_KILOGRAM = 'J33';

    /**
     * microgram per cubic metre kelvin
     */
    const MICROGRAM_PER_CUBIC_METRE_KELVIN = 'J34';

    /**
     * microgram per cubic metre bar
     */
    const MICROGRAM_PER_CUBIC_METRE_BAR = 'J35';

    /**
     * microlitre per litre
     */
    const MICROLITRE_PER_LITRE = 'J36';

    /**
     * baud
     */
    const BAUD = 'J38';

    /**
     * British thermal unit (mean)
     */
    const BRITISH_THERMAL_UNIT_MEAN = 'J39';

    /**
     * British thermal unit (international table) foot per hour square foot degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_FOOT_PER_HOUR SQUARE_FOOT_DEGREE_FAHRENHEIT = 'J40';

    /**
     * British thermal unit (international table) inch per hour square foot degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_INCH_PER_HOUR_SQUARE FOOT_DEGREE_FAHRENHEIT = 'J41';

    /**
     * British thermal unit (international table) inch per second square foot degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_INCH_PER_SECOND_SQUARE FOOT_DEGREE_FAHRENHEIT = 'J42';

    /**
     * British thermal unit (international table) per pound degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_PER_POUND_DEGREE_FAHRENHEIT = 'J43';

    /**
     * British thermal unit (international table) per minute
     */
    const BRITISH_THERMAL_UNIT_PER_MINUTE = 'J44';

    /**
     * British thermal unit (international table) per second
     */
    const BRITISH_THERMAL_UNIT_PER_SECOND = 'J45';

    /**
     * British thermal unit (thermochemical) foot per hour square foot degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_THERM_FOOT_PER_HOUR_SQUARE FOOT_DEGREE_FAHRENHEIT = 'J46';

    /**
     * British thermal unit (thermochemical) per hour
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_HOUR = 'J47';

    /**
     * British thermal unit (thermochemical) inch per hour square foot degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_THERM_INCH_PER_HOUR_SQUARE FOOT_DEGREE_FAHRENHEIT = 'J48';

    /**
     * British thermal unit (thermochemical) inch per second square foot degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_THERM_INCH_PER_SECOND SQUARE_FOOT_DEGREE_FAHRENHEIT = 'J49';

    /**
     * British thermal unit (thermochemical) per pound degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_POUND_DEGREE_FAHRENHEIT = 'J50';

    /**
     * British thermal unit (thermochemical) per minute
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_MINUTE = 'J51';

    /**
     * British thermal unit (thermochemical) per second
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_SECOND = 'J52';

    /**
     * coulomb square metre per kilogram
     */
    const COULOMB_SQUARE_METRE_PER_KILOGRAM = 'J53';

    /**
     * megabaud
     */
    const MEGABAUD = 'J54';

    /**
     * watt second
     */
    const WATT_SECOND = 'J55';

    /**
     * bar per bar
     */
    const BAR_PER_BAR = 'J56';

    /**
     * barrel (UK petroleum)
     */
    const BARREL_UK = 'J57';

    /**
     * barrel (UK petroleum) per minute
     */
    const BARREL_UK_PER_MINUTE = 'J58';

    /**
     * barrel (UK petroleum) per day
     */
    const BARREL_UK_PER_DAY = 'J59';

    /**
     * barrel (UK petroleum) per hour
     */
    const BARREL_UK_PER_HOUR = 'J60';

    /**
     * barrel (UK petroleum) per second
     */
    const BARREL_UK_PER_SECOND = 'J61';

    /**
     * barrel (US petroleum) per hour
     */
    const BARREL_US_PER_HOUR = 'J62';

    /**
     * barrel (US petroleum) per second
     */
    const BARREL_US_PER_SECOND = 'J63';

    /**
     * bushel (UK) per day
     */
    const BUSHEL_UK_PER_DAY = 'J64';

    /**
     * bushel (UK) per hour
     */
    const BUSHEL_UK_PER_HOUR = 'J65';

    /**
     * bushel (UK) per minute
     */
    const BUSHEL_UK_PER_MINUTE = 'J66';

    /**
     * bushel (UK) per second
     */
    const BUSHEL_UK_PER_SECOND = 'J67';

    /**
     * bushel (US dry) per day
     */
    const BUSHEL_US_DRY_PER_DAY = 'J68';

    /**
     * bushel (US dry) per hour
     */
    const BUSHEL_US_DRY_PER_HOUR = 'J69';

    /**
     * bushel (US dry) per minute
     */
    const BUSHEL_US_DRY_PER_MINUTE = 'J70';

    /**
     * bushel (US dry) per second
     */
    const BUSHEL_US_DRY_PER_SECOND = 'J71';

    /**
     * centinewton metre
     */
    const CENTINEWTON_METRE = 'J72';

    /**
     * centipoise per kelvin
     */
    const CENTIPOISE_PER_KELVIN = 'J73';

    /**
     * centipoise per bar
     */
    const CENTIPOISE_PER_BAR = 'J74';

    /**
     * calorie (mean)
     */
    const CALORIE_MEAN = 'J75';

    /**
     * calorie (international table) per gram degree Celsius
     */
    const CALORIE_PER_GRAM_DEGREE_CELSIUS = 'J76';

    /**
     * calorie (thermochemical) per centimetre second degree Celsius
     */
    const CALORIE_THERM_PER_CENTIMETRE_SECOND_DEGREE_CELSIUS = 'J78';

    /**
     * calorie (thermochemical) per gram degree Celsius
     */
    const CALORIE_THERM_PER_GRAM_DEGREE_CELSIUS = 'J79';

    /**
     * calorie (thermochemical) per minute
     */
    const CALORIE_THERM_PER_MINUTE = 'J81';

    /**
     * calorie (thermochemical) per second
     */
    const CALORIE_THERM_PER_SECOND = 'J82';

    /**
     * clo
     */
    const CLO = 'J83';

    /**
     * centimetre per second kelvin
     */
    const CENTIMETRE_PER_SECOND_KELVIN = 'J84';

    /**
     * centimetre per second bar
     */
    const CENTIMETRE_PER_SECOND_BAR = 'J85';

    /**
     * cubic centimetre per cubic metre
     */
    const CUBIC_CENTIMETRE_PER_CUBIC_METRE = 'J87';

    /**
     * cubic decimetre per day
     */
    const CUBIC_DECIMETRE_PER_DAY = 'J90';

    /**
     * cubic decimetre per cubic metre
     */
    const CUBIC_DECIMETRE_PER_CUBIC_METRE = 'J91';

    /**
     * cubic decimetre per minute
     */
    const CUBIC_DECIMETRE_PER_MINUTE = 'J92';

    /**
     * cubic decimetre per second
     */
    const CUBIC_DECIMETRE_PER_SECOND = 'J93';

    /**
     * ounce (UK fluid) per day
     */
    const OUNCE_UK_FLUID_PER_DAY = 'J95';

    /**
     * ounce (UK fluid) per hour
     */
    const OUNCE_UK_FLUID_PER_HOUR = 'J96';

    /**
     * ounce (UK fluid) per minute
     */
    const OUNCE_UK_FLUID_PER_MINUTE = 'J97';

    /**
     * ounce (UK fluid) per second
     */
    const OUNCE_UK_FLUID_PER_SECOND = 'J98';

    /**
     * ounce (US fluid) per day
     */
    const OUNCE_US_FLUID_PER_DAY = 'J99';

    /**
     * joule per kelvin
     */
    const JOULE_PER_KELVIN = 'JE';

    /**
     * megajoule per kilogram
     */
    const MEGAJOULE_PER_KILOGRAM = 'JK';

    /**
     * megajoule per cubic metre
     */
    const MEGAJOULE_PER_CUBIC_METRE = 'JM';

    /**
     * pipeline joint
     */
    const PIPELINE_JOINT = 'JNT';

    /**
     * joule
     */
    const JOULE = 'JOU';

    /**
     * hundred metre
     */
    const HUNDRED_METRE = 'JPS';

    /**
     * number of jewels
     */
    const NUMBER_OF_JEWELS = 'JWL';

    /**
     * kilowatt demand
     */
    const KILOWATT_DEMAND = 'K1';

    /**
     * ounce (US fluid) per hour
     */
    const OUNCE_US_FLUID_PER_HOUR = 'K10';

    /**
     * ounce (US fluid) per minute
     */
    const OUNCE_US_FLUID_PER_MINUTE = 'K11';

    /**
     * ounce (US fluid) per second
     */
    const OUNCE_US_FLUID_PER_SECOND = 'K12';

    /**
     * foot per degree Fahrenheit
     */
    const FOOT_PER_DEGREE_FAHRENHEIT = 'K13';

    /**
     * foot per hour
     */
    const FOOT_PER_HOUR = 'K14';

    /**
     * foot pound-force per hour
     */
    const FOOT_POUND_FORCE_PER_HOUR = 'K15';

    /**
     * foot pound-force per minute
     */
    const FOOT_POUND_FORCE_PER_MINUTE = 'K16';

    /**
     * foot per psi
     */
    const FOOT_PER_PSI = 'K17';

    /**
     * foot per second degree Fahrenheit
     */
    const FOOT_PER_SECOND_DEGREE_FAHRENHEIT = 'K18';

    /**
     * foot per second psi
     */
    const FOOT_PER_SECOND_PSI = 'K19';

    /**
     * kilovolt ampere reactive demand
     */
    const KILOVOLT_AMPERE_REACTIVE_DEMAND = 'K2';

    /**
     * reciprocal cubic foot
     */
    const RECIPROCAL_CUBIC_FOOT = 'K20';

    /**
     * cubic foot per degree Fahrenheit
     */
    const CUBIC_FOOT_PER_DEGREE_FAHRENHEIT = 'K21';

    /**
     * cubic foot per day
     */
    const CUBIC_FOOT_PER_DAY = 'K22';

    /**
     * cubic foot per psi
     */
    const CUBIC_FOOT_PER_PSI = 'K23';

    /**
     * gallon (UK) per day
     */
    const GALLON_UK_PER_DAY = 'K26';

    /**
     * gallon (UK) per hour
     */
    const GALLON_UK_PER_HOUR = 'K27';

    /**
     * gallon (UK) per second
     */
    const GALLON_UK_PER_SECOND = 'K28';

    /**
     * kilovolt ampere reactive hour
     */
    const KILOVOLT_AMPERE_REACTIVE_HOUR = 'K3';

    /**
     * gallon (US liquid) per second
     */
    const GALLON_US_LIQUID_PER_SECOND = 'K30';

    /**
     * gram-force per square centimetre
     */
    const GRAM_FORCE_PER_SQUARE_CENTIMETRE = 'K31';

    /**
     * gill (UK) per day
     */
    const GILL_UK_PER_DAY = 'K32';

    /**
     * gill (UK) per hour
     */
    const GILL_UK_PER_HOUR = 'K33';

    /**
     * gill (UK) per minute
     */
    const GILL_UK_PER_MINUTE = 'K34';

    /**
     * gill (UK) per second
     */
    const GILL_UK_PER_SECOND = 'K35';

    /**
     * gill (US) per day
     */
    const GILL_US_PER_DAY = 'K36';

    /**
     * gill (US) per hour
     */
    const GILL_US_PER_HOUR = 'K37';

    /**
     * gill (US) per minute
     */
    const GILL_US_PER_MINUTE = 'K38';

    /**
     * gill (US) per second
     */
    const GILL_US_PER_SECOND = 'K39';

    /**
     * standard acceleration of free fall
     */
    const STANDARD_ACCELERATION_OF_FREE_FALL = 'K40';

    /**
     * grain per gallon (US)
     */
    const GRAIN_PER_GALLON_US = 'K41';

    /**
     * horsepower (boiler)
     */
    const HORSEPOWER_BOILER = 'K42';

    /**
     * horsepower (electric)
     */
    const HORSEPOWER_ELECTRIC = 'K43';

    /**
     * inch per degree Fahrenheit
     */
    const INCH_PER_DEGREE_FAHRENHEIT = 'K45';

    /**
     * inch per psi
     */
    const INCH_PER_PSI = 'K46';

    /**
     * inch per second degree Fahrenheit
     */
    const INCH_PER_SECOND_DEGREE_FAHRENHEIT = 'K47';

    /**
     * inch per second psi
     */
    const INCH_PER_SECOND_PSI = 'K48';

    /**
     * reciprocal cubic inch
     */
    const RECIPROCAL_CUBIC_INCH = 'K49';

    /**
     * kilobaud
     */
    const KILOBAUD = 'K50';

    /**
     * kilocalorie (mean)
     */
    const KILOCALORIE_MEAN = 'K51';

    /**
     * kilocalorie (international table) per hour metre degree Celsius
     */
    const KILOCALORIE_PER_HOUR_METRE_DEGREE_CELSIUS = 'K52';

    /**
     * kilocalorie (thermochemical)
     */
    const KILOCALORIE_THERM = 'K53';

    /**
     * kilocalorie (thermochemical) per minute
     */
    const KILOCALORIE_THERM_PER_MINUTE = 'K54';

    /**
     * kilocalorie (thermochemical) per second
     */
    const KILOCALORIE_THERM_PER_SECOND = 'K55';

    /**
     * kilomole per hour
     */
    const KILOMOLE_PER_HOUR = 'K58';

    /**
     * kilomole per cubic metre kelvin
     */
    const KILOMOLE_PER_CUBIC_METRE_KELVIN = 'K59';

    /**
     * kilolitre
     */
    const KILOLITRE = 'K6';

    /**
     * kilomole per cubic metre bar
     */
    const KILOMOLE_PER_CUBIC_METRE_BAR = 'K60';

    /**
     * kilomole per minute
     */
    const KILOMOLE_PER_MINUTE = 'K61';

    /**
     * litre per litre
     */
    const LITRE_PER_LITRE = 'K62';

    /**
     * reciprocal litre
     */
    const RECIPROCAL_LITRE = 'K63';

    /**
     * pound (avoirdupois) per degree Fahrenheit
     */
    const POUND_PER_DEGREE_FAHRENHEIT = 'K64';

    /**
     * pound (avoirdupois) square foot
     */
    const POUND_SQUARE_FOOT = 'K65';

    /**
     * pound (avoirdupois) per day
     */
    const POUND_PER_DAY = 'K66';

    /**
     * pound per foot hour
     */
    const POUND_PER_FOOT_HOUR = 'K67';

    /**
     * pound per foot second
     */
    const POUND_PER_FOOT_SECOND = 'K68';

    /**
     * pound (avoirdupois) per cubic foot degree Fahrenheit
     */
    const POUND_PER_CUBIC_FOOT_DEGREE_FAHRENHEIT = 'K69';

    /**
     * pound (avoirdupois) per cubic foot psi
     */
    const POUND_PER_CUBIC_FOOT_PSI = 'K70';

    /**
     * pound (avoirdupois) per gallon (UK)
     */
    const POUND_PER_GALLON_UK = 'K71';

    /**
     * pound (avoirdupois) per hour degree Fahrenheit
     */
    const POUND_PER_HOUR_DEGREE_FAHRENHEIT = 'K73';

    /**
     * pound (avoirdupois) per hour psi
     */
    const POUND_PER_HOUR_PSI = 'K74';

    /**
     * pound (avoirdupois) per cubic inch degree Fahrenheit
     */
    const POUND_PER_CUBIC_INCH_DEGREE_FAHRENHEIT = 'K75';

    /**
     * pound (avoirdupois) per cubic inch psi
     */
    const POUND_PER_CUBIC_INCH_PSI = 'K76';

    /**
     * pound (avoirdupois) per psi
     */
    const POUND_PER_PSI = 'K77';

    /**
     * pound (avoirdupois) per minute
     */
    const POUND_PER_MINUTE = 'K78';

    /**
     * pound (avoirdupois) per minute degree Fahrenheit
     */
    const POUND_PER_MINUTE_DEGREE_FAHRENHEIT = 'K79';

    /**
     * pound (avoirdupois) per minute psi
     */
    const POUND_PER_MINUTE_PSI = 'K80';

    /**
     * pound (avoirdupois) per second
     */
    const POUND_PER_SECOND = 'K81';

    /**
     * pound (avoirdupois) per second degree Fahrenheit
     */
    const POUND_PER_SECOND_DEGREE_FAHRENHEIT = 'K82';

    /**
     * pound (avoirdupois) per second psi
     */
    const POUND_PER_SECOND_PSI = 'K83';

    /**
     * pound per cubic yard
     */
    const POUND_PER_CUBIC_YARD = 'K84';

    /**
     * pound-force per square foot
     */
    const POUND_FORCE_PER_SQUARE_FOOT = 'K85';

    /**
     * pound-force per square inch degree Fahrenheit
     */
    const POUND_FORCE_PER_SQUARE_INCH_DEGREE_FAHRENHEIT = 'K86';

    /**
     * psi cubic inch per second
     */
    const PSI_CUBIC_INCH_PER_SECOND = 'K87';

    /**
     * psi litre per second
     */
    const PSI_LITRE_PER_SECOND = 'K88';

    /**
     * psi cubic metre per second
     */
    const PSI_CUBIC_METRE_PER_SECOND = 'K89';

    /**
     * psi cubic yard per second
     */
    const PSI_CUBIC_YARD_PER_SECOND = 'K90';

    /**
     * pound-force second per square foot
     */
    const POUND_FORCE_SECOND_PER_SQUARE_FOOT = 'K91';

    /**
     * pound-force second per square inch
     */
    const POUND_FORCE_SECOND_PER_SQUARE_INCH = 'K92';

    /**
     * reciprocal psi
     */
    const RECIPROCAL_PSI = 'K93';

    /**
     * quart (UK liquid) per day
     */
    const QUART_UK_LIQUID_PER_DAY = 'K94';

    /**
     * quart (UK liquid) per hour
     */
    const QUART_UK_LIQUID_PER_HOUR = 'K95';

    /**
     * quart (UK liquid) per minute
     */
    const QUART_UK_LIQUID_PER_MINUTE = 'K96';

    /**
     * quart (UK liquid) per second
     */
    const QUART_UK_LIQUID_PER_SECOND = 'K97';

    /**
     * quart (US liquid) per day
     */
    const QUART_US_LIQUID_PER_DAY = 'K98';

    /**
     * quart (US liquid) per hour
     */
    const QUART_US_LIQUID_PER_HOUR = 'K99';

    /**
     * cake
     */
    const CAKE = 'KA';

    /**
     * katal
     */
    const KATAL = 'KAT';

    /**
     * kilocharacter
     */
    const KILOCHARACTER = 'KB';

    /**
     * kilobar
     */
    const KILOBAR = 'KBA';

    /**
     * kilogram of choline chloride
     */
    const KILOGRAM_OF_CHOLINE_CHLORIDE = 'KCC';

    /**
     * kilogram drained net weight
     */
    const KILOGRAM_DRAINED_NET_WEIGHT = 'KDW';

    /**
     * kelvin
     */
    const KELVIN = 'KEL';

    /**
     * kilogram
     */
    const KILOGRAM = 'KGM';

    /**
     * kilogram per second
     */
    const KILOGRAM_PER_SECOND = 'KGS';

    /**
     * kilogram of hydrogen peroxide
     */
    const KILOGRAM_OF_HYDROGEN_PEROXIDE = 'KHY';

    /**
     * kilohertz
     */
    const KILOHERTZ = 'KHZ';

    /**
     * kilogram per millimetre width
     */
    const KILOGRAM_PER_MILLIMETRE_WIDTH = 'KI';

    /**
     * kilogram, including container
     */
    const KILOGRAM_INCL_CONTAINER = 'KIC';

    /**
     * kilogram, including inner packaging
     */
    const KILOGRAM_INCL_INNER_PACKAGING = 'KIP';

    /**
     * kilosegment
     */
    const KILOSEGMENT = 'KJ';

    /**
     * kilojoule
     */
    const KILOJOULE = 'KJO';

    /**
     * kilogram per metre
     */
    const KILOGRAM_PER_METRE = 'KL';

    /**
     * lactic dry material percentage
     */
    const LACTIC_DRY_MATERIAL_PERCENTAGE = 'KLK';

    /**
     * kilolux
     */
    const KILOLUX = 'KLX';

    /**
     * kilogram of methylamine
     */
    const KILOGRAM_OF_METHYLAMINE = 'KMA';

    /**
     * kilometre per hour
     */
    const KILOMETRE_PER_HOUR = 'KMH';

    /**
     * square kilometre
     */
    const SQUARE_KILOMETRE = 'KMK';

    /**
     * kilogram per cubic metre
     */
    const KILOGRAM_PER_CUBIC_METRE = 'KMQ';

    /**
     * kilometre
     */
    const KILOMETRE = 'KMT';

    /**
     * kilogram of nitrogen
     */
    const KILOGRAM_OF_NITROGEN = 'KNI';

    /**
     * kilonewton per square metre
     */
    const KILONEWTON_PER_SQUARE_METRE = 'KNM';

    /**
     * kilogram named substance
     */
    const KILOGRAM_NAMED_SUBSTANCE = 'KNS';

    /**
     * knot
     */
    const KNOT = 'KNT';

    /**
     * milliequivalence caustic potash per gram of product
     */
    const MILLIEQUIVALENCE_CAUSTIC_POTASH_PER_GRAM_OF_PRODUCT = 'KO';

    /**
     * kilopascal
     */
    const KILOPASCAL = 'KPA';

    /**
     * kilogram of potassium hydroxide (caustic potash)
     */
    const KILOGRAM_OF_POTASSIUM_HYDROXIDE_CAUSTIC_POTASH = 'KPH';

    /**
     * kilogram of potassium oxide
     */
    const KILOGRAM_OF_POTASSIUM_OXIDE = 'KPO';

    /**
     * kilogram of phosphorus pentoxide (phosphoric anhydride)
     */
    const KILOGRAM_OF_PHOSPHORUS_PENTOXIDE_PHOSPHORIC_ANHYDRIDE = 'KPP';

    /**
     * kiloroentgen
     */
    const KILOROENTGEN = 'KR';

    /**
     * kilogram of substance 90 % dry
     */
    const KILOGRAM_OF_SUBSTANCE_90_DRY = 'KSD';

    /**
     * kilogram of sodium hydroxide (caustic soda)
     */
    const KILOGRAM_OF_SODIUM_HYDROXIDE_CAUSTIC_SODA = 'KSH';

    /**
     * kit
     */
    const KIT = 'KT';

    /**
     * kilotonne
     */
    const KILOTONNE = 'KTN';

    /**
     * kilogram of uranium
     */
    const KILOGRAM_OF_URANIUM = 'KUR';

    /**
     * kilovolt - ampere
     */
    const KILOVOLT_AMPERE = 'KVA';

    /**
     * kilovar
     */
    const KILOVAR = 'KVR';

    /**
     * kilovolt
     */
    const KILOVOLT = 'KVT';

    /**
     * kilogram per millimetre
     */
    const KILOGRAM_PER_MILLIMETRE = 'KW';

    /**
     * kilowatt hour
     */
    const KILOWATT_HOUR = 'KWH';

    /**
     * kilogram of tungsten trioxide
     */
    const KILOGRAM_OF_TUNGSTEN_TRIOXIDE = 'KWO';

    /**
     * kilowatt
     */
    const KILOWATT = 'KWT';

    /**
     * millilitre per kilogram
     */
    const MILLILITRE_PER_KILOGRAM = 'KX';

    /**
     * quart (US liquid) per minute
     */
    const QUART_US_LIQUID_PER_MINUTE = 'L10';

    /**
     * quart (US liquid) per second
     */
    const QUART_US_LIQUID_PER_SECOND = 'L11';

    /**
     * metre per second kelvin
     */
    const METRE_PER_SECOND_KELVIN = 'L12';

    /**
     * metre per second bar
     */
    const METRE_PER_SECOND_BAR = 'L13';

    /**
     * square metre hour degree Celsius per kilocalorie (international table)
     */
    const SQUARE_METRE_HOUR_DEGREE_CELSIUS_PER_KILOCALORIE = 'L14';

    /**
     * millipascal second per kelvin
     */
    const MILLIPASCAL_SECOND_PER_KELVIN = 'L15';

    /**
     * millipascal second per bar
     */
    const MILLIPASCAL_SECOND_PER_BAR = 'L16';

    /**
     * milligram per cubic metre kelvin
     */
    const MILLIGRAM_PER_CUBIC_METRE_KELVIN = 'L17';

    /**
     * milligram per cubic metre bar
     */
    const MILLIGRAM_PER_CUBIC_METRE_BAR = 'L18';

    /**
     * millilitre per litre
     */
    const MILLILITRE_PER_LITRE = 'L19';

    /**
     * litre per minute
     */
    const LITRE_PER_MINUTE = 'L2';

    /**
     * reciprocal cubic millimetre
     */
    const RECIPROCAL_CUBIC_MILLIMETRE = 'L20';

    /**
     * cubic millimetre per cubic metre
     */
    const CUBIC_MILLIMETRE_PER_CUBIC_METRE = 'L21';

    /**
     * mole per hour
     */
    const MOLE_PER_HOUR = 'L23';

    /**
     * mole per kilogram kelvin
     */
    const MOLE_PER_KILOGRAM_KELVIN = 'L24';

    /**
     * mole per kilogram bar
     */
    const MOLE_PER_KILOGRAM_BAR = 'L25';

    /**
     * mole per litre kelvin
     */
    const MOLE_PER_LITRE_KELVIN = 'L26';

    /**
     * mole per litre bar
     */
    const MOLE_PER_LITRE_BAR = 'L27';

    /**
     * mole per cubic metre kelvin
     */
    const MOLE_PER_CUBIC_METRE_KELVIN = 'L28';

    /**
     * mole per cubic metre bar
     */
    const MOLE_PER_CUBIC_METRE_BAR = 'L29';

    /**
     * mole per minute
     */
    const MOLE_PER_MINUTE = 'L30';

    /**
     * milliroentgen aequivalent men
     */
    const MILLIROENTGEN_AEQUIVALENT_MEN = 'L31';

    /**
     * nanogram per kilogram
     */
    const NANOGRAM_PER_KILOGRAM = 'L32';

    /**
     * ounce (avoirdupois) per day
     */
    const OUNCE_PER_DAY = 'L33';

    /**
     * ounce (avoirdupois) per hour
     */
    const OUNCE_PER_HOUR = 'L34';

    /**
     * ounce (avoirdupois) per minute
     */
    const OUNCE_PER_MINUTE = 'L35';

    /**
     * ounce (avoirdupois) per second
     */
    const OUNCE_PER_SECOND = 'L36';

    /**
     * ounce (avoirdupois) per gallon (UK)
     */
    const OUNCE_PER_GALLON_UK = 'L37';

    /**
     * ounce (avoirdupois) per gallon (US)
     */
    const OUNCE_PER_GALLON_US = 'L38';

    /**
     * ounce (avoirdupois) per cubic inch
     */
    const OUNCE_PER_CUBIC_INCH = 'L39';

    /**
     * ounce (avoirdupois)-force
     */
    const OUNCE_FORCE = 'L40';

    /**
     * ounce (avoirdupois)-force inch
     */
    const OUNCE_FORCE_INCH = 'L41';

    /**
     * picosiemens per metre
     */
    const PICOSIEMENS_PER_METRE = 'L42';

    /**
     * peck (UK)
     */
    const PECK_UK = 'L43';

    /**
     * peck (UK) per day
     */
    const PECK_UK_PER_DAY = 'L44';

    /**
     * peck (UK) per hour
     */
    const PECK_UK_PER_HOUR = 'L45';

    /**
     * peck (UK) per minute
     */
    const PECK_UK_PER_MINUTE = 'L46';

    /**
     * peck (UK) per second
     */
    const PECK_UK_PER_SECOND = 'L47';

    /**
     * peck (US dry) per day
     */
    const PECK_US_DRY_PER_DAY = 'L48';

    /**
     * peck (US dry) per hour
     */
    const PECK_US_DRY_PER_HOUR = 'L49';

    /**
     * peck (US dry) per minute
     */
    const PECK_US_DRY_PER_MINUTE = 'L50';

    /**
     * peck (US dry) per second
     */
    const PECK_US_DRY_PER_SECOND = 'L51';

    /**
     * psi per psi
     */
    const PSI_PER_PSI = 'L52';

    /**
     * pint (UK) per day
     */
    const PINT_UK_PER_DAY = 'L53';

    /**
     * pint (UK) per hour
     */
    const PINT_UK_PER_HOUR = 'L54';

    /**
     * pint (UK) per minute
     */
    const PINT_UK_PER_MINUTE = 'L55';

    /**
     * pint (UK) per second
     */
    const PINT_UK_PER_SECOND = 'L56';

    /**
     * pint (US liquid) per day
     */
    const PINT_US_LIQUID_PER_DAY = 'L57';

    /**
     * pint (US liquid) per hour
     */
    const PINT_US_LIQUID_PER_HOUR = 'L58';

    /**
     * pint (US liquid) per minute
     */
    const PINT_US_LIQUID_PER_MINUTE = 'L59';

    /**
     * pint (US liquid) per second
     */
    const PINT_US_LIQUID_PER_SECOND = 'L60';

    /**
     * slug per day
     */
    const SLUG_PER_DAY = 'L63';

    /**
     * slug per foot second
     */
    const SLUG_PER_FOOT_SECOND = 'L64';

    /**
     * slug per cubic foot
     */
    const SLUG_PER_CUBIC_FOOT = 'L65';

    /**
     * slug per hour
     */
    const SLUG_PER_HOUR = 'L66';

    /**
     * slug per minute
     */
    const SLUG_PER_MINUTE = 'L67';

    /**
     * slug per second
     */
    const SLUG_PER_SECOND = 'L68';

    /**
     * tonne per kelvin
     */
    const TONNE_PER_KELVIN = 'L69';

    /**
     * tonne per bar
     */
    const TONNE_PER_BAR = 'L70';

    /**
     * tonne per day
     */
    const TONNE_PER_DAY = 'L71';

    /**
     * tonne per day kelvin
     */
    const TONNE_PER_DAY_KELVIN = 'L72';

    /**
     * tonne per day bar
     */
    const TONNE_PER_DAY_BAR = 'L73';

    /**
     * tonne per hour kelvin
     */
    const TONNE_PER_HOUR_KELVIN = 'L74';

    /**
     * tonne per hour bar
     */
    const TONNE_PER_HOUR_BAR = 'L75';

    /**
     * tonne per cubic metre kelvin
     */
    const TONNE_PER_CUBIC_METRE_KELVIN = 'L76';

    /**
     * tonne per cubic metre bar
     */
    const TONNE_PER_CUBIC_METRE_BAR = 'L77';

    /**
     * tonne per minute
     */
    const TONNE_PER_MINUTE = 'L78';

    /**
     * tonne per minute kelvin
     */
    const TONNE_PER_MINUTE_KELVIN = 'L79';

    /**
     * tonne per minute bar
     */
    const TONNE_PER_MINUTE_BAR = 'L80';

    /**
     * tonne per second
     */
    const TONNE_PER_SECOND = 'L81';

    /**
     * tonne per second kelvin
     */
    const TONNE_PER_SECOND_KELVIN = 'L82';

    /**
     * tonne per second bar
     */
    const TONNE_PER_SECOND_BAR = 'L83';

    /**
     * ton (UK shipping)
     */
    const TON_UK_SHIPPING = 'L84';

    /**
     * ton long per day
     */
    const TON_LONG_PER_DAY = 'L85';

    /**
     * ton (US shipping)
     */
    const TON_US_SHIPPING = 'L86';

    /**
     * ton short per degree Fahrenheit
     */
    const TON_SHORT_PER_DEGREE_FAHRENHEIT = 'L87';

    /**
     * ton short per day
     */
    const TON_SHORT_PER_DAY = 'L88';

    /**
     * ton short per hour degree Fahrenheit
     */
    const TON_SHORT_PER_HOUR_DEGREE_FAHRENHEIT = 'L89';

    /**
     * ton short per hour psi
     */
    const TON_SHORT_PER_HOUR_PSI = 'L90';

    /**
     * ton short per psi
     */
    const TON_SHORT_PER_PSI = 'L91';

    /**
     * ton (UK long) per cubic yard
     */
    const TON_UK_LONG_PER_CUBIC_YARD = 'L92';

    /**
     * ton (US short) per cubic yard
     */
    const TON_US_SHORT_PER_CUBIC_YARD = 'L93';

    /**
     * ton-force (US short)
     */
    const TON_FORCE_US_SHORT = 'L94';

    /**
     * common year
     */
    const COMMON_YEAR = 'L95';

    /**
     * sidereal year
     */
    const SIDEREAL_YEAR = 'L96';

    /**
     * yard per degree Fahrenheit
     */
    const YARD_PER_DEGREE_FAHRENHEIT = 'L98';

    /**
     * yard per psi
     */
    const YARD_PER_PSI = 'L99';

    /**
     * pound per cubic inch
     */
    const POUND_PER_CUBIC_INCH = 'LA';

    /**
     * lactose excess percentage
     */
    const LACTOSE_EXCESS_PERCENTAGE = 'LAC';

    /**
     * pound
     */
    const POUND = 'LBR';

    /**
     * troy pound (US)
     */
    const TROY_POUND_US = 'LBT';

    /**
     * litre per day
     */
    const LITRE_PER_DAY = 'LD';

    /**
     * leaf
     */
    const LEAF = 'LEF';

    /**
     * linear foot
     */
    const LINEAR_FOOT = 'LF';

    /**
     * labour hour
     */
    const LABOUR_HOUR = 'LH';

    /**
     * link
     */
    const LINK = 'LK';

    /**
     * linear metre
     */
    const LINEAR_METRE = 'LM';

    /**
     * length
     */
    const LENGTH = 'LN';

    /**
     * lot [unit of procurement]
     */
    const LOT_PROCUREMENT = 'LO';

    /**
     * liquid pound
     */
    const LIQUID_POUND = 'LP';

    /**
     * litre of pure alcohol
     */
    const LITRE_OF_PURE_ALCOHOL = 'LPA';

    /**
     * layer
     */
    const LAYER = 'LR';

    /**
     * lump sum
     */
    const LUMP_SUM = 'LS';

    /**
     * ton (UK) or long ton (US)
     */
    const TON_UK_OR_LONG_TON_US = 'LTN';

    /**
     * litre
     */
    const LITRE = 'LTR';

    /**
     * metric ton, lubricating oil
     */
    const METRIC_TON_LUBRICATING_OIL = 'LUB';

    /**
     * lumen
     */
    const LUMEN = 'LUM';

    /**
     * lux
     */
    const LUX = 'LUX';

    /**
     * linear yard
     */
    const LINEAR_YARD = 'LY';

    /**
     * milligram per litre
     */
    const MILLIGRAM_PER_LITRE = 'M1';

    /**
     * reciprocal cubic yard
     */
    const RECIPROCAL_CUBIC_YARD = 'M10';

    /**
     * cubic yard per degree Fahrenheit
     */
    const CUBIC_YARD_PER_DEGREE_FAHRENHEIT = 'M11';

    /**
     * cubic yard per day
     */
    const CUBIC_YARD_PER_DAY = 'M12';

    /**
     * cubic yard per hour
     */
    const CUBIC_YARD_PER_HOUR = 'M13';

    /**
     * cubic yard per psi
     */
    const CUBIC_YARD_PER_PSI = 'M14';

    /**
     * cubic yard per minute
     */
    const CUBIC_YARD_PER_MINUTE = 'M15';

    /**
     * cubic yard per second
     */
    const CUBIC_YARD_PER_SECOND = 'M16';

    /**
     * kilohertz metre
     */
    const KILOHERTZ_METRE = 'M17';

    /**
     * gigahertz metre
     */
    const GIGAHERTZ_METRE = 'M18';

    /**
     * Beaufort
     */
    const BEAUFORT = 'M19';

    /**
     * reciprocal megakelvin or megakelvin to the power minus one
     */
    const RECIPROCAL_MEGAKELVIN_OR_MEGAKELVIN_TO_POWER_MINUS_ONE = 'M20';

    /**
     * reciprocal kilovolt - ampere reciprocal hour
     */
    const RECIPROCAL_KILOVOLT_AMPERE_RECIPROCAL_HOUR = 'M21';

    /**
     * millilitre per square centimetre minute
     */
    const MILLILITRE_PER_SQUARE_CENTIMETRE_MINUTE = 'M22';

    /**
     * newton per centimetre
     */
    const NEWTON_PER_CENTIMETRE = 'M23';

    /**
     * ohm kilometre
     */
    const OHM_KILOMETRE = 'M24';

    /**
     * percent per degree Celsius
     */
    const PERCENT_PER_DEGREE_CELSIUS = 'M25';

    /**
     * gigaohm per metre
     */
    const GIGAOHM_PER_METRE = 'M26';

    /**
     * megahertz metre
     */
    const MEGAHERTZ_METRE = 'M27';

    /**
     * kilogram per kilogram
     */
    const KILOGRAM_PER_KILOGRAM = 'M29';

    /**
     * reciprocal volt - ampere reciprocal second
     */
    const RECIPROCAL_VOLT_AMPERE_RECIPROCAL_SECOND = 'M30';

    /**
     * kilogram per kilometre
     */
    const KILOGRAM_PER_KILOMETRE = 'M31';

    /**
     * pascal second per litre
     */
    const PASCAL_SECOND_PER_LITRE = 'M32';

    /**
     * millimole per litre
     */
    const MILLIMOLE_PER_LITRE = 'M33';

    /**
     * newton metre per square metre
     */
    const NEWTON_METRE_PER_SQUARE_METRE = 'M34';

    /**
     * millivolt - ampere
     */
    const MILLIVOLT_AMPERE = 'M35';

    /**
     * 30-day month
     */
    const MONTH_30_DAY = 'M36';

    /**
     * actual/360
     */
    const ACTUAL_360 = 'M37';

    /**
     * kilometre per second squared
     */
    const KILOMETRE_PER_SECOND_SQUARED = 'M38';

    /**
     * centimetre per second squared
     */
    const CENTIMETRE_PER_SECOND_SQUARED = 'M39';

    /**
     * monetary value
     */
    const MONETARY_VALUE = 'M4';

    /**
     * yard per second squared
     */
    const YARD_PER_SECOND_SQUARED = 'M40';

    /**
     * millimetre per second squared
     */
    const MILLIMETRE_PER_SECOND_SQUARED = 'M41';

    /**
     * mile (statute mile) per second squared
     */
    const MILE_STATUTE_MILE_PER_SECOND_SQUARED = 'M42';

    /**
     * mil
     */
    const MIL = 'M43';

    /**
     * revolution
     */
    const REVOLUTION = 'M44';

    /**
     * degree [unit of angle] per second squared
     */
    const DEGREE_ANGLE_PER_SECOND_SQUARED = 'M45';

    /**
     * revolution per minute
     */
    const REVOLUTION_PER_MINUTE = 'M46';

    /**
     * circular mil
     */
    const CIRCULAR_MIL = 'M47';

    /**
     * square mile (based on U.S. survey foot)
     */
    const SQUARE_MILE_US_SURVEY_FOOT = 'M48';

    /**
     * chain (based on U.S. survey foot)
     */
    const CHAIN_US_SURVEY_FOOT = 'M49';

    /**
     * microcurie
     */
    const MICROCURIE = 'M5';

    /**
     * furlong
     */
    const FURLONG = 'M50';

    /**
     * foot (U.S. survey)
     */
    const FOOT_US_SURVEY = 'M51';

    /**
     * mile (based on U.S. survey foot)
     */
    const MILE_US_SURVEY_FOOT = 'M52';

    /**
     * metre per pascal
     */
    const METRE_PER_PASCAL = 'M53';

    /**
     * metre per radiant
     */
    const METRE_PER_RADIANT = 'M55';

    /**
     * shake
     */
    const SHAKE = 'M56';

    /**
     * mile per minute
     */
    const MILE_PER_MINUTE = 'M57';

    /**
     * mile per second
     */
    const MILE_PER_SECOND = 'M58';

    /**
     * metre per second pascal
     */
    const METRE_PER_SECOND_PASCAL = 'M59';

    /**
     * metre per hour
     */
    const METRE_PER_HOUR = 'M60';

    /**
     * inch per year
     */
    const INCH_PER_YEAR = 'M61';

    /**
     * kilometre per second
     */
    const KILOMETRE_PER_SECOND = 'M62';

    /**
     * inch per minute
     */
    const INCH_PER_MINUTE = 'M63';

    /**
     * yard per second
     */
    const YARD_PER_SECOND = 'M64';

    /**
     * yard per minute
     */
    const YARD_PER_MINUTE = 'M65';

    /**
     * yard per hour
     */
    const YARD_PER_HOUR = 'M66';

    /**
     * acre-foot (based on U.S. survey foot)
     */
    const ACRE_FOOT_US_SURVEY_FOOT = 'M67';

    /**
     * cord (128 ft3)
     */
    const CORD_128_FT3 = 'M68';

    /**
     * cubic mile (UK statute)
     */
    const CUBIC_MILE_UK_STATUTE = 'M69';

    /**
     * micro-inch
     */
    const MICRO_INCH = 'M7';

    /**
     * ton, register
     */
    const TON_REGISTER = 'M70';

    /**
     * cubic metre per pascal
     */
    const CUBIC_METRE_PER_PASCAL = 'M71';

    /**
     * bel
     */
    const BEL = 'M72';

    /**
     * kilogram per cubic metre pascal
     */
    const KILOGRAM_PER_CUBIC_METRE_PASCAL = 'M73';

    /**
     * kilogram per pascal
     */
    const KILOGRAM_PER_PASCAL = 'M74';

    /**
     * kilopound-force
     */
    const KILOPOUND_FORCE = 'M75';

    /**
     * poundal
     */
    const POUNDAL = 'M76';

    /**
     * kilogram metre per second squared
     */
    const KILOGRAM_METRE_PER_SECOND_SQUARED = 'M77';

    /**
     * pond
     */
    const POND = 'M78';

    /**
     * square foot per hour
     */
    const SQUARE_FOOT_PER_HOUR = 'M79';

    /**
     * stokes per pascal
     */
    const STOKES_PER_PASCAL = 'M80';

    /**
     * square centimetre per second
     */
    const SQUARE_CENTIMETRE_PER_SECOND = 'M81';

    /**
     * square metre per second pascal
     */
    const SQUARE_METRE_PER_SECOND_PASCAL = 'M82';

    /**
     * denier
     * Traditional unit for the indication of the linear mass of textile fibers and yarns.
     */
    const DENIER_MASS = 'M83';

    /**
     * pound per yard
     */
    const POUND_PER_YARD = 'M84';

    /**
     * ton, assay
     */
    const TON_ASSAY = 'M85';

    /**
     * pfund
     */
    const PFUND = 'M86';

    /**
     * kilogram per second pascal
     */
    const KILOGRAM_PER_SECOND_PASCAL = 'M87';

    /**
     * tonne per month
     */
    const TONNE_PER_MONTH = 'M88';

    /**
     * tonne per year
     */
    const TONNE_PER_YEAR = 'M89';

    /**
     * million Btu per 1000 cubic foot
     */
    const MILLION_BTU_PER_1000_CUBIC_FOOT = 'M9';

    /**
     * kilopound per hour
     */
    const KILOPOUND_PER_HOUR = 'M90';

    /**
     * pound per pound
     */
    const POUND_PER_POUND = 'M91';

    /**
     * pound-force foot
     */
    const POUND_FORCE_FOOT = 'M92';

    /**
     * newton metre per radian
     */
    const NEWTON_METRE_PER_RADIAN = 'M93';

    /**
     * kilogram metre
     */
    const KILOGRAM_METRE = 'M94';

    /**
     * poundal foot
     */
    const POUNDAL_FOOT = 'M95';

    /**
     * poundal inch
     */
    const POUNDAL_INCH = 'M96';

    /**
     * dyne metre
     */
    const DYNE_METRE = 'M97';

    /**
     * kilogram centimetre per second
     */
    const KILOGRAM_CENTIMETRE_PER_SECOND = 'M98';

    /**
     * gram centimetre per second
     */
    const GRAM_CENTIMETRE_PER_SECOND = 'M99';

    /**
     * megavolt ampere reactive hour
     */
    const MEGAVOLT_AMPERE_REACTIVE_HOUR = 'MAH';

    /**
     * megalitre
     */
    const MEGALITRE = 'MAL';

    /**
     * megametre
     */
    const MEGAMETRE = 'MAM';

    /**
     * megavar
     */
    const MEGAVAR = 'MAR';

    /**
     * megawatt
     */
    const MEGAWATT = 'MAW';

    /**
     * thousand standard brick equivalent
     */
    const THOUSAND_STANDARD_BRICK_EQUIVALENT = 'MBE';

    /**
     * thousand board foot
     */
    const THOUSAND_BOARD_FOOT = 'MBF';

    /**
     * millibar
     */
    const MILLIBAR = 'MBR';

    /**
     * microgram
     */
    const MICROGRAM = 'MC';

    /**
     * millicurie
     */
    const MILLICURIE = 'MCU';

    /**
     * air dry metric ton
     */
    const AIR_DRY_METRIC_TON = 'MD';

    /**
     * milligram
     */
    const MILLIGRAM = 'MGM';

    /**
     * megahertz
     */
    const MEGAHERTZ = 'MHZ';

    /**
     * square mile (statute mile)
     */
    const SQUARE_MILE_STATUTE_MILE = 'MIK';

    /**
     * thousand
     */
    const THOUSAND = 'MIL';

    /**
     * minute [unit of time]
     */
    const MINUTE_TIME = 'MIN';

    /**
     * million
     */
    const MILLION = 'MIO';

    /**
     * million international unit
     */
    const MILLION_INTERNATIONAL_UNIT = 'MIU';

    /**
     * milliard
     */
    const MILLIARD = 'MLD';

    /**
     * millilitre
     */
    const MILLILITRE = 'MLT';

    /**
     * square millimetre
     */
    const SQUARE_MILLIMETRE = 'MMK';

    /**
     * cubic millimetre
     */
    const CUBIC_MILLIMETRE = 'MMQ';

    /**
     * millimetre
     */
    const MILLIMETRE = 'MMT';

    /**
     * kilogram, dry weight
     */
    const KILOGRAM_DRY_WEIGHT = 'MND';

    /**
     * month
     */
    const MONTH = 'MON';

    /**
     * megapascal
     */
    const MEGAPASCAL = 'MPA';

    /**
     * cubic metre per hour
     */
    const CUBIC_METRE_PER_HOUR = 'MQH';

    /**
     * cubic metre per second
     */
    const CUBIC_METRE_PER_SECOND = 'MQS';

    /**
     * metre per second squared
     */
    const METRE_PER_SECOND_SQUARED = 'MSK';

    /**
     * square metre
     */
    const SQUARE_METRE = 'MTK';

    /**
     * cubic metre
     */
    const CUBIC_METRE = 'MTQ';

    /**
     * metre
     */
    const METRE = 'MTR';

    /**
     * metre per second
     */
    const METRE_PER_SECOND = 'MTS';

    /**
     * megavolt - ampere
     */
    const MEGAVOL_AMPERE = 'MVA';

    /**
     * megawatt hour (1000 kW.h)
     */
    const MEGAWATT_HOUR = 'MWH';

    /**
     * pen calorie
     */
    const PEN_CALORIE = 'N1';

    /**
     * pound foot per second
     */
    const POUND_FOOT_PER_SECOND = 'N10';

    /**
     * pound inch per second
     */
    const POUND_INCH_PER_SECOND = 'N11';

    /**
     * Pferdestaerke
     */
    const PFERDESTAERKE = 'N12';

    /**
     * centimetre of mercury (0 ºC)
     */
    const CENTIMETRE_OF_MERCURY = 'N13';

    /**
     * centimetre of water (4 ºC)
     */
    const CENTIMETRE_OF_WATER = 'N14';

    /**
     * foot of water (39.2 ºF)
     */
    const FOOT_OF_WATER = 'N15';

    /**
     * inch of mercury (32 ºF)
     */
    const INCH_OF_MERCURY_32F = 'N16';

    /**
     * inch of mercury (60 ºF)
     */
    const INCH_OF_MERCURY_60F = 'N17';

    /**
     * inch of water (39.2 ºF)
     */
    const INCH_OF_WATER_39F = 'N18';

    /**
     * inch of water (60 ºF)
     */
    const INCH_OF_WATER_60F = 'N19';

    /**
     * kip per square inch
     */
    const KIP_PER_SQUARE_INCH = 'N20';

    /**
     * poundal per square foot
     */
    const POUNDAL_PER_SQUARE_FOOT = 'N21';

    /**
     * ounce (avoirdupois) per square inch
     */
    const OUNCE_PER_SQUARE_INCH = 'N22';

    /**
     * conventional metre of water
     */
    const CONVENTIONAL_METRE_OF_WATER = 'N23';

    /**
     * gram per square millimetre
     */
    const GRAM_PER_SQUARE_MILLIMETRE = 'N24';

    /**
     * pound per square yard
     */
    const POUND_PER_SQUARE_YARD = 'N25';

    /**
     * poundal per square inch
     */
    const POUNDAL_PER_SQUARE_INCH = 'N26';

    /**
     * foot to the fourth power
     */
    const FOOT_TO_FOURTH_POWER = 'N27';

    /**
     * cubic decimetre per kilogram
     */
    const CUBIC_DECIMETRE_PER_KILOGRAM = 'N28';

    /**
     * cubic foot per pound
     */
    const CUBIC_FOOT_PER_POUND = 'N29';

    /**
     * print point
     */
    const PRINT_POINT = 'N3';

    /**
     * cubic inch per pound
     */
    const CUBIC_INCH_PER_POUND = 'N30';

    /**
     * kilonewton per metre
     */
    const KILONEWTON_PER_METRE = 'N31';

    /**
     * poundal per inch
     */
    const POUNDAL_PER_INCH = 'N32';

    /**
     * pound-force per yard
     */
    const POUND_FORCE_PER_YARD = 'N33';

    /**
     * poundal second per square foot
     */
    const POUNDAL_SECOND_PER_SQUARE_FOOT = 'N34';

    /**
     * poise per pascal
     */
    const POISE_PER_PASCAL = 'N35';

    /**
     * newton second per square metre
     */
    const NEWTON_SECOND_PER_SQUARE_METRE = 'N36';

    /**
     * kilogram per metre second
     */
    const KILOGRAM_PER_METRE_SECOND = 'N37';

    /**
     * kilogram per metre minute
     */
    const KILOGRAM_PER_METRE_MINUTE = 'N38';

    /**
     * kilogram per metre day
     */
    const KILOGRAM_PER_METRE_DAY = 'N39';

    /**
     * kilogram per metre hour
     */
    const KILOGRAM_PER_METRE_HOUR = 'N40';

    /**
     * gram per centimetre second
     */
    const GRAM_PER_CENTIMETRE_SECOND = 'N41';

    /**
     * poundal second per square inch
     */
    const POUNDAL_SECOND_PER_SQUARE_INCH = 'N42';

    /**
     * pound per foot minute
     */
    const POUND_PER_FOOT_MINUTE = 'N43';

    /**
     * pound per foot day
     */
    const POUND_PER_FOOT_DAY = 'N44';

    /**
     * cubic metre per second pascal
     */
    const CUBIC_METRE_PER_SECOND_PASCAL = 'N45';

    /**
     * foot poundal
     */
    const FOOT_POUNDAL = 'N46';

    /**
     * inch poundal
     */
    const INCH_POUNDAL = 'N47';

    /**
     * watt per square centimetre
     */
    const WATT_PER_SQUARE_CENTIMETRE = 'N48';

    /**
     * watt per square inch
     */
    const WATT_PER_SQUARE_INCH = 'N49';

    /**
     * British thermal unit (international table) per square foot hour
     */
    const BRITISH_THERMAL_UNIT_PER_SQUARE_FOOT_HOUR = 'N50';

    /**
     * British thermal unit (thermochemical) per square foot hour
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_SQUARE_FOOT_HOUR = 'N51';

    /**
     * British thermal unit (thermochemical) per square foot minute
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_SQUARE_FOOT_MINUTE = 'N52';

    /**
     * British thermal unit (international table) per square foot second
     */
    const BRITISH_THERMAL_UNIT_PER_SQUARE_FOOT_SECOND = 'N53';

    /**
     * British thermal unit (thermochemical) per square foot second
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_SQUARE_FOOT_SECOND = 'N54';

    /**
     * British thermal unit (international table) per square inch second
     */
    const BRITISH_THERMAL_UNIT_PER_SQUARE_INCH_SECOND = 'N55';

    /**
     * calorie (thermochemical) per square centimetre minute
     */
    const CALORIE_THERM_PER_SQUARE_CENTIMETRE_MINUTE = 'N56';

    /**
     * calorie (thermochemical) per square centimetre second
     */
    const CALORIE_THERM_PER_SQUARE_CENTIMETRE_SECOND = 'N57';

    /**
     * British thermal unit (international table) per cubic foot
     */
    const BRITISH_THERMAL_UNIT_PER_CUBIC_FOOT = 'N58';

    /**
     * British thermal unit (thermochemical) per cubic foot
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_CUBIC_FOOT = 'N59';

    /**
     * British thermal unit (international table) per degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_PER_DEGREE_FAHRENHEIT = 'N60';

    /**
     * British thermal unit (thermochemical) per degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_DEGREE_FAHRENHEIT = 'N61';

    /**
     * British thermal unit (international table) per degree Rankine
     */
    const BRITISH_THERMAL_UNIT_PER_DEGREE_RANKINE = 'N62';

    /**
     * British thermal unit (thermochemical) per degree Rankine
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_DEGREE_RANKINE = 'N63';

    /**
     * British thermal unit (thermochemical) per pound degree Rankine
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_POUND_DEGREE_RANKINE = 'N64';

    /**
     * kilocalorie (international table) per gram kelvin
     */
    const KILOCALORIE_PER_GRAM_KELVIN = 'N65';

    /**
     * British thermal unit (39 ºF)
     */
    const BRITISH_THERMAL_UNIT_39F = 'N66';

    /**
     * British thermal unit (59 ºF)
     */
    const BRITISH_THERMAL_UNIT_59F = 'N67';

    /**
     * British thermal unit (60 ºF)
     */
    const BRITISH_THERMAL_UNIT_60F = 'N68';

    /**
     * calorie (20 ºC)
     */
    const CALORIE_20 = 'N69';

    /**
     * quad (1015 BtuIT)
     */
    const QUAD_1015_BTUIT = 'N70';

    /**
     * therm (EC)
     */
    const THERM_EC = 'N71';

    /**
     * therm (U.S.)
     */
    const THERM_US = 'N72';

    /**
     * British thermal unit (thermochemical) per pound
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_POUND = 'N73';

    /**
     * British thermal unit (international table) per hour square foot degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_PER_HOUR_SQUARE_FOOT_DEGREE_FAHRENHEIT = 'N74';

    /**
     * British thermal unit (thermochemical) per hour square foot degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_HOUR_SQUARE_FOOT_DEGREE_FAHRENHEIT = 'N75';

    /**
     * British thermal unit (international table) per second square foot degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_PER_SECOND_SQUARE_FOOT_DEGREE_FAHRENHEIT = 'N76';

    /**
     * British thermal unit (thermochemical) per second square foot degree Fahrenheit
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_SECOND_SQUARE_FOOT_DEGREE_FAHRENHEIT = 'N77';

    /**
     * kilowatt per square metre kelvin
     */
    const KILOWATT_PER_SQUARE_METRE_KELVIN = 'N78';

    /**
     * kelvin per pascal
     */
    const KELVIN_PER_PASCAL = 'N79';

    /**
     * watt per metre degree Celsius
     */
    const WATT_PER_METRE_DEGREE_CELSIUS = 'N80';

    /**
     * kilowatt per metre kelvin
     */
    const KILOWATT_PER_METRE_KELVIN = 'N81';

    /**
     * kilowatt per metre degree Celsius
     */
    const KILOWATT_PER_METRE_DEGREE_CELSIUS = 'N82';

    /**
     * metre per degree Celcius metre
     */
    const METRE_PER_DEGREE_CELCIUS_METRE = 'N83';

    /**
     * degree Fahrenheit hour per British thermal unit (international table)
     */
    const DEGREE_FAHRENHEIT_HOUR_PER_BRITISH_THERMAL_UNIT = 'N84';

    /**
     * degree Fahrenheit hour per British thermal unit (thermochemical)
     */
    const DEGREE_FAHRENHEIT_HOUR_PER_BRITISH_THERMAL_UNIT_THERM = 'N85';

    /**
     * degree Fahrenheit second per British thermal unit (international table)
     */
    const DEGREE_FAHRENHEIT_SECOND_PER_BRITISH_THERMAL_UNIT = 'N86';

    /**
     * degree Fahrenheit second per British thermal unit (thermochemical)
     */
    const DEGREE_FAHRENHEIT_SECOND_PER_BRITISH_THERMAL_UNIT_THERM = 'N87';

    /**
     * degree Fahrenheit hour square foot per British thermal unit (international table) inch
     */
    const DEGREE_FAHRENHEIT_HOUR_SQUARE_FOOT_PER_BRITISH_THERMAL_UNIT_INCH = 'N88';

    /**
     * degree Fahrenheit hour square foot per British thermal unit (thermochemical) inch
     */
    const DEGREE_FAHRENHEIT_HOUR_SQUARE_FOOT_PER_BRITISH_THERMAL_UNIT_THERM_INCH = 'N89';

    /**
     * kilofarad
     */
    const KILOFARAD = 'N90';

    /**
     * reciprocal joule
     */
    const RECIPROCAL_JOULE = 'N91';

    /**
     * picosiemens
     */
    const PICOSIEMENS = 'N92';

    /**
     * ampere per pascal
     */
    const AMPERE_PER_PASCAL = 'N93';

    /**
     * franklin
     */
    const FRANKLIN = 'N94';

    /**
     * ampere minute
     */
    const AMPERE_MINUTE = 'N95';

    /**
     * biot
     */
    const BIOT = 'N96';

    /**
     * gilbert
     */
    const GILBERT = 'N97';

    /**
     * volt per pascal
     */
    const VOLT_PER_PASCAL = 'N98';

    /**
     * picovolt
     */
    const PICOVOLT = 'N99';

    /**
     * milligram per kilogram
     */
    const MILLIGRAM_PER_KILOGRAM = 'NA';

    /**
     * number of articles
     */
    const NUMBER_OF_ARTICLES = 'NAR';

    /**
     * number of cells
     */
    const NUMBER_OF_CELLS = 'NCL';

    /**
     * newton
     */
    const NEWTON = 'NEW';

    /**
     * message
     */
    const MESSAGE = 'NF';

    /**
     * nil
     */
    const NIL = 'NIL';

    /**
     * number of international units
     */
    const NUMBER_OF_INTERNATIONAL_UNITS = 'NIU';

    /**
     * load
     */
    const LOAD = 'NL';

    /**
     * Normalised cubic metre
     */
    const NORMALISED_CUBIC_METRE = 'NM3';

    /**
     * nautical mile
     */
    const NAUTICAL_MILE = 'NMI';

    /**
     * number of packs
     */
    const NUMBER_OF_PACKS = 'NMP';

    /**
     * number of parts
     */
    const NUMBER_OF_PARTS = 'NPT';

    /**
     * net ton
     */
    const NET_TON = 'NT';

    /**
     * newton metre
     */
    const NEWTON_METRE = 'NU';

    /**
     * part per thousand
     */
    const PART_PER_THOUSAND = 'NX';

    /**
     * panel
     */
    const PANEL = 'OA';

    /**
     * ozone depletion equivalent
     */
    const OZONE_DEPLETION_EQUIVALENT = 'ODE';

    /**
     * ohm
     */
    const OHM = 'OHM';

    /**
     * ounce per square yard
     */
    const OUNCE_PER_SQUARE_YARD = 'ON';

    /**
     * ounce (avoirdupois)
     */
    const OUNCE_AVOIRDUPOIS = 'ONZ';

    /**
     * oscillations per minute
     */
    const OSCILLATIONS_PER_MINUTE = 'OPM';

    /**
     * overtime hour
     */
    const OVERTIME_HOUR = 'OT';

    /**
     * fluid ounce (US)
     */
    const FLUID_OUNCE_US = 'OZA';

    /**
     * fluid ounce (UK)
     */
    const FLUID_OUNCE_UK = 'OZI';

    /**
     * percent
     */
    const PERCENT = 'P1';

    /**
     * coulomb per metre
     */
    const COULOMB_PER_METRE = 'P10';

    /**
     * kiloweber
     */
    const KILOWEBER = 'P11';

    /**
     * gamma
     */
    const GAMMA = 'P12';

    /**
     * kilotesla
     */
    const KILOTESLA = 'P13';

    /**
     * joule per second
     */
    const JOULE_PER_SECOND = 'P14';

    /**
     * joule per minute
     */
    const JOULE_PER_MINUTE = 'P15';

    /**
     * joule per hour
     */
    const JOULE_PER_HOUR = 'P16';

    /**
     * joule per day
     */
    const JOULE_PER_DAY = 'P17';

    /**
     * kilojoule per second
     */
    const KILOJOULE_PER_SECOND = 'P18';

    /**
     * kilojoule per minute
     */
    const KILOJOULE_PER_MINUTE = 'P19';

    /**
     * pound per foot
     */
    const POUND_PER_FOOT = 'P2';

    /**
     * kilojoule per hour
     */
    const KILOJOULE_PER_HOUR = 'P20';

    /**
     * kilojoule per day
     */
    const KILOJOULE_PER_DAY = 'P21';

    /**
     * nanoohm
     */
    const NANOOHM = 'P22';

    /**
     * ohm circular-mil per foot
     */
    const OHM_CIRCULAR_MIL_PER_FOOT = 'P23';

    /**
     * kilohenry
     */
    const KILOHENRY = 'P24';

    /**
     * lumen per square foot
     */
    const LUMEN_PER_SQUARE_FOOT = 'P25';

    /**
     * phot
     */
    const PHOT = 'P26';

    /**
     * footcandle
     */
    const FOOTCANDLE = 'P27';

    /**
     * candela per square inch
     */
    const CANDELA_PER_SQUARE_INCH = 'P28';

    /**
     * footlambert
     */
    const FOOTLAMBERT = 'P29';

    /**
     * lambert
     */
    const LAMBERT = 'P30';

    /**
     * stilb
     */
    const STILB = 'P31';

    /**
     * candela per square foot
     */
    const CANDELA_PER_SQUARE_FOOT = 'P32';

    /**
     * kilocandela
     */
    const KILOCANDELA = 'P33';

    /**
     * millicandela
     */
    const MILLICANDELA = 'P34';

    /**
     * Hefner-Kerze
     */
    const HEFNER_KERZE = 'P35';

    /**
     * international candle
     */
    const INTERNATIONAL_CANDLE = 'P36';

    /**
     * British thermal unit (international table) per square foot
     */
    const BRITISH_THERMAL_UNIT_PER_SQUARE_FOOT = 'P37';

    /**
     * British thermal unit (thermochemical) per square foot
     */
    const BRITISH_THERMAL_UNIT_THERM_PER_SQUARE_FOOT = 'P38';

    /**
     * calorie (thermochemical) per square centimetre
     */
    const CALORIE_THERM_PER_SQUARE_CENTIMETRE = 'P39';

    /**
     * langley
     */
    const LANGLEY = 'P40';

    /**
     * decade (logarithmic)
     */
    const DECADE_LOGARITHMIC = 'P41';

    /**
     * pascal squared second
     */
    const PASCAL_SQUARED_SECOND = 'P42';

    /**
     * bel per metre
     */
    const BEL_PER_METRE = 'P43';

    /**
     * pound mole
     */
    const POUND_MOLE = 'P44';

    /**
     * pound mole per second
     */
    const POUND_MOLE_PER_SECOND = 'P45';

    /**
     * pound mole per minute
     */
    const POUND_MOLE_PER_MINUTE = 'P46';

    /**
     * kilomole per kilogram
     */
    const KILOMOLE_PER_KILOGRAM = 'P47';

    /**
     * pound mole per pound
     */
    const POUND_MOLE_PER_POUND = 'P48';

    /**
     * newton square metre per ampere
     */
    const NEWTON_SQUARE_METRE_PER_AMPERE = 'P49';

    /**
     * five pack
     */
    const FIVE_PACK = 'P5';

    /**
     * weber metre
     */
    const WEBER_METRE = 'P50';

    /**
     * mol per kilogram pascal
     */
    const MOL_PER_KILOGRAM_PASCAL = 'P51';

    /**
     * mol per cubic metre pascal
     */
    const MOL_PER_CUBIC_METRE_PASCAL = 'P52';

    /**
     * unit pole
     */
    const UNIT_POLE = 'P53';

    /**
     * milligray per second
     */
    const MILLIGRAY_PER_SECOND = 'P54';

    /**
     * microgray per second
     */
    const MICROGRAY_PER_SECOND = 'P55';

    /**
     * nanogray per second
     */
    const NANOGRAY_PER_SECOND = 'P56';

    /**
     * gray per minute
     */
    const GRAY_PER_MINUTE = 'P57';

    /**
     * milligray per minute
     */
    const MILLIGRAY_PER_MINUTE = 'P58';

    /**
     * microgray per minute
     */
    const MICROGRAY_PER_MINUTE = 'P59';

    /**
     * nanogray per minute
     */
    const NANOGRAY_PER_MINUTE = 'P60';

    /**
     * gray per hour
     */
    const GRAY_PER_HOUR = 'P61';

    /**
     * milligray per hour
     */
    const MILLIGRAY_PER_HOUR = 'P62';

    /**
     * microgray per hour
     */
    const MICROGRAY_PER_HOUR = 'P63';

    /**
     * nanogray per hour
     */
    const NANOGRAY_PER_HOUR = 'P64';

    /**
     * sievert per second
     */
    const SIEVERT_PER_SECOND = 'P65';

    /**
     * millisievert per second
     */
    const MILLISIEVERT_PER_SECOND = 'P66';

    /**
     * microsievert per second
     */
    const MICROSIEVERT_PER_SECOND = 'P67';

    /**
     * nanosievert per second
     */
    const NANOSIEVERT_PER_SECOND = 'P68';

    /**
     * rem per second
     */
    const REM_PER_SECOND = 'P69';

    /**
     * sievert per hour
     */
    const SIEVERT_PER_HOUR = 'P70';

    /**
     * millisievert per hour
     */
    const MILLISIEVERT_PER_HOUR = 'P71';

    /**
     * microsievert per hour
     */
    const MICROSIEVERT_PER_HOUR = 'P72';

    /**
     * nanosievert per hour
     */
    const NANOSIEVERT_PER_HOUR = 'P73';

    /**
     * sievert per minute
     */
    const SIEVERT_PER_MINUTE = 'P74';

    /**
     * millisievert per minute
     */
    const MILLISIEVERT_PER_MINUTE = 'P75';

    /**
     * microsievert per minute
     */
    const MICROSIEVERT_PER_MINUTE = 'P76';

    /**
     * nanosievert per minute
     */
    const NANOSIEVERT_PER_MINUTE = 'P77';

    /**
     * reciprocal square inch
     */
    const RECIPROCAL_SQUARE_INCH = 'P78';

    /**
     * pascal square metre per kilogram
     */
    const PASCAL_SQUARE_METRE_PER_KILOGRAM = 'P79';

    /**
     * millipascal per metre
     */
    const MILLIPASCAL_PER_METRE = 'P80';

    /**
     * kilopascal per metre
     */
    const KILOPASCAL_PER_METRE = 'P81';

    /**
     * hectopascal per metre
     */
    const HECTOPASCAL_PER_METRE = 'P82';

    /**
     * standard atmosphere per metre
     */
    const STANDARD_ATMOSPHERE_PER_METRE = 'P83';

    /**
     * technical atmosphere per metre
     */
    const TECHNICAL_ATMOSPHERE_PER_METRE = 'P84';

    /**
     * torr per metre
     */
    const TORR_PER_METRE = 'P85';

    /**
     * psi per inch
     */
    const PSI_PER_INCH = 'P86';

    /**
     * cubic metre per second square metre
     */
    const CUBIC_METRE_PER_SECOND_SQUARE_METRE = 'P87';

    /**
     * rhe
     */
    const RHE = 'P88';

    /**
     * pound-force foot per inch
     */
    const POUND_FORCE_FOOT_PER_INCH = 'P89';

    /**
     * pound-force inch per inch
     */
    const POUND_FORCE_INCH_PER_INCH = 'P90';

    /**
     * perm (0 ºC)
     */
    const PERM_0 = 'P91';

    /**
     * perm (23 ºC)
     */
    const PERM_23 = 'P92';

    /**
     * byte per second
     */
    const BYTE_PER_SECOND = 'P93';

    /**
     * kilobyte per second
     */
    const KILOBYTE_PER_SECOND = 'P94';

    /**
     * megabyte per second
     */
    const MEGABYTE_PER_SECOND = 'P95';

    /**
     * reciprocal volt
     */
    const RECIPROCAL_VOLT = 'P96';

    /**
     * reciprocal radian
     */
    const RECIPROCAL_RADIAN = 'P97';

    /**
     * pascal to the power sum of stoichiometric numbers
     */
    const PASCAL_TO_POWER_SUM_OF_STOICHIOMETRIC_NUMBERS = 'P98';

    /**
     * mole per cubic metre to the power sum of stoichiometric numbers
     */
    const MOLE_PER_CUBIC_METRE_TO_POWER_SUM_OF_STOICHIOMETRIC_NUMBERS = 'P99';

    /**
     * pascal
     */
    const PASCAL = 'PAL';

    /**
     * pad
     */
    const PAD = 'PD';

    /**
     * proof litre
     */
    const PROOF_LITRE = 'PFL';

    /**
     * proof gallon
     */
    const PROOF_GALLON = 'PGL';

    /**
     * pitch
     */
    const PITCH = 'PI';

    /**
     * degree Plato
     */
    const DEGREE_PLATO = 'PLA';

    /**
     * pound per inch of length
     */
    const POUND_PER_INCH_OF_LENGTH = 'PO';

    /**
     * page per inch
     */
    const PAGE_PER_INCH = 'PQ';

    /**
     * pair
     */
    const PAIR = 'PR';

    /**
     * pound-force per square inch
     */
    const POUND_FORCE_PER_SQUARE_INCH = 'PS';

    /**
     * dry pint (US)
     */
    const DRY_PINT_US = 'PTD';

    /**
     * pint (UK)
     */
    const PINT_UK = 'PTI';

    /**
     * liquid pint (US)
     */
    const LIQUID_PINT_US = 'PTL';

    /**
     * portion
     */
    const PORTION = 'PTN';

    /**
     * joule per tesla
     */
    const JOULE_PER_TESLA = 'Q10';

    /**
     * erlang
     */
    const ERLANG = 'Q11';

    /**
     * octet
     */
    const OCTET = 'Q12';

    /**
     * octet per second
     */
    const OCTET_PER_SECOND = 'Q13';

    /**
     * shannon
     */
    const SHANNON = 'Q14';

    /**
     * hartley
     */
    const HARTLEY = 'Q15';

    /**
     * natural unit of information
     */
    const NATURAL_UNIT_OF_INFORMATION = 'Q16';

    /**
     * shannon per second
     */
    const SHANNON_PER_SECOND = 'Q17';

    /**
     * hartley per second
     */
    const HARTLEY_PER_SECOND = 'Q18';

    /**
     * natural unit of information per second
     */
    const NATURAL_UNIT_OF_INFORMATION_PER_SECOND = 'Q19';

    /**
     * second per kilogramm
     */
    const SECOND_PER_KILOGRAMM = 'Q20';

    /**
     * watt square metre
     */
    const WATT_SQUARE_METRE = 'Q21';

    /**
     * second per radian cubic metre
     */
    const SECOND_PER_RADIAN_CUBIC_METRE = 'Q22';

    /**
     * weber to the power minus one
     */
    const WEBER_TO_POWER_MINUS_ONE = 'Q23';

    /**
     * reciprocal inch
     */
    const RECIPROCAL_INCH = 'Q24';

    /**
     * dioptre
     */
    const DIOPTRE = 'Q25';

    /**
     * one per one
     */
    const ONE_PER_ONE = 'Q26';

    /**
     * newton metre per metre
     */
    const NEWTON_METRE_PER_METRE = 'Q27';

    /**
     * kilogram per square metre pascal second
     */
    const KILOGRAM_PER_SQUARE_METRE_PASCAL_SECOND = 'Q28';

    /**
     * microgram per hectogram
     */
    const MICROGRAM_PER_HECTOGRAM = 'Q29';

    /**
     * pH (potential of Hydrogen)
     */
    const PH_POTENTIAL_OF_HYDROGEN = 'Q30';

    /**
     * kilojoule per gram
     */
    const KILOJOULE_PER_GRAM = 'Q31';

    /**
     * femtolitre
     */
    const FEMTOLITRE = 'Q32';

    /**
     * picolitre
     */
    const PICOLITRE = 'Q33';

    /**
     * nanolitre
     */
    const NANOLITRE = 'Q34';

    /**
     * megawatts per minute
     */
    const MEGAWATTS_PER_MINUTE = 'Q35';

    /**
     * square metre per cubic metre
     */
    const SQUARE_METRE_PER_CUBIC_METRE = 'Q36';

    /**
     * Standard cubic metre per day
     */
    const STANDARD_CUBIC_METRE_PER_DAY = 'Q37';

    /**
     * Standard cubic metre per hour
     */
    const STANDARD_CUBIC_METRE_PER_HOUR = 'Q38';

    /**
     * Normalized cubic metre per day
     */
    const NORMALIZED_CUBIC_METRE_PER_DAY = 'Q39';

    /**
     * Normalized cubic metre per hour
     */
    const NORMALIZED_CUBIC_METRE_PER_HOUR = 'Q40';

    /**
     * meal
     */
    const MEAL = 'Q3';

    /**
     * page - facsimile
     */
    const PAGE_FACSIMILE = 'QA';

    /**
     * quarter (of a year)
     */
    const QUARTER_OF_A_YEAR = 'QAN';

    /**
     * page - hardcopy
     */
    const PAGE_HARDCOPY = 'QB';

    /**
     * quire
     */
    const QUIRE = 'QR';

    /**
     * dry quart (US)
     */
    const DRY_QUART_US = 'QTD';

    /**
     * quart (UK)
     */
    const QUART_UK = 'QTI';

    /**
     * liquid quart (US)
     */
    const LIQUID_QUART_US = 'QTL';

    /**
     * quarter (UK)
     */
    const QUARTER_UK = 'QTR';

    /**
     * pica
     */
    const PICA = 'R1';

    /**
     * thousand cubic metre
     */
    const THOUSAND_CUBIC_METRE = 'R9';

    /**
     * running or operating hour
     */
    const RUNNING_OR_OPERATING_HOUR = 'RH';

    /**
     * ream
     */
    const REAM = 'RM';

    /**
     * room
     */
    const ROOM = 'ROM';

    /**
     * pound per ream
     */
    const POUND_PER_REAM = 'RP';

    /**
     * revolutions per minute
     */
    const REVOLUTIONS_PER_MINUTE = 'RPM';

    /**
     * revolutions per second
     */
    const REVOLUTIONS_PER_SECOND = 'RPS';

    /**
     * revenue ton mile
     */
    const REVENUE_TON_MILE = 'RT';

    /**
     * square foot per second
     */
    const SQUARE_FOOT_PER_SECOND = 'S3';

    /**
     * square metre per second
     */
    const SQUARE_METRE_PER_SECOND = 'S4';

    /**
     * half year (6 months)
     */
    const HALF_YEAR_6_MONTHS = 'SAN';

    /**
     * score
     */
    const SCORE = 'SCO';

    /**
     * scruple
     */
    const SCRUPLE = 'SCR';

    /**
     * second [unit of time]
     */
    const SECOND_TIME = 'SEC';

    /**
     * set
     */
    const SET = 'SET';

    /**
     * segment
     */
    const SEGMENT = 'SG';

    /**
     * siemens
     */
    const SIEMENS = 'SIE';

    /**
     * Standard cubic metre
     */
    const STANDARD_CUBIC_METRE = 'SM3';

    /**
     * mile (statute mile)
     */
    const MILE_STATUTE = 'SMI';

    /**
     * square
     */
    const SQUARE = 'SQ';

    /**
     * square, roofing
     */
    const SQUARE_ROOFING = 'SQR';

    /**
     * strip
     */
    const STRIP = 'SR';

    /**
     * stick
     */
    const STICK = 'STC';

    /**
     * stone (UK)
     */
    const STONE_UK = 'STI';

    /**
     * stick, cigarette
     */
    const STICK_CIGARETTE = 'STK';

    /**
     * standard litre
     */
    const STANDARD_LITRE = 'STL';

    /**
     * ton (US) or short ton (UK/US)
     */
    const TON_US = 'STN';

    /**
     * ton (US) or short ton (UK/US)
     */
    const SHORT_TON_UK = 'STN';

    /**
     * ton (US) or short ton (UK/US)
     */
    const SHORT_TON_US = 'STN';

    /**
     * straw
     */
    const STRAW = 'STW';

    /**
     * skein
     */
    const SKEIN = 'SW';

    /**
     * shipment
     */
    const SHIPMENT = 'SX';

    /**
     * syringe
     */
    const SYRINGE = 'SYR';

    /**
     * telecommunication line in service
     */
    const TELECOMMUNICATION_LINE_IN_SERVICE = 'T0';

    /**
     * thousand piece
     */
    const THOUSAND_PIECE = 'T3';

    /**
     * kiloampere hour (thousand ampere hour)
     */
    const KILOAMPERE_HOUR_THOUSAND_AMPERE_HOUR = 'TAH';

    /**
     * total acid number
     */
    const TOTAL_ACID_NUMBER = 'TAN';

    /**
     * thousand square inch
     */
    const THOUSAND_SQUARE_INCH = 'TI';

    /**
     * metric ton, including container
     */
    const METRIC_TON_INCL_CONTAINER = 'TIC';

    /**
     * metric ton, including inner packaging
     */
    const METRIC_TON_INCL_INNER_PACKAGING = 'TIP';

    /**
     * tonne kilometre
     */
    const TONNE_KILOMETRE = 'TKM';

    /**
     * kilogram of imported meat, less offal
     */
    const KILOGRAM_OF_IMPORTED_MEAT_LESS_OFFAL = 'TMS';

    /**
     * tonne (metric ton)
     */
    const TONNE_METRIC_TON = 'TNE';

    /**
     * ten pack
     */
    const TEN_PACK = 'TP';

    /**
     * teeth per inch
     */
    const TEETH_PER_INCH = 'TPI';

    /**
     * ten pair
     */
    const TEN_PAIR = 'TPR';

    /**
     * thousand cubic metre per day
     */
    const THOUSAND_CUBIC_METRE_PER_DAY = 'TQD';

    /**
     * trillion (EUR)
     */
    const TRILLION_EUR = 'TRL';

    /**
     * ten set
     */
    const TEN_SET = 'TST';

    /**
     * ten thousand sticks
     */
    const TEN_THOUSAND_STICKS = 'TTS';

    /**
     * treatment
     */
    const TREATMENT = 'U1';

    /**
     * tablet
     */
    const TABLET = 'U2';

    /**
     * telecommunication line in service average
     */
    const TELECOMMUNICATION_LINE_IN_SERVICE_AVERAGE = 'UB';

    /**
     * telecommunication port
     */
    const TELECOMMUNICATION_PORT = 'UC';

    /**
     * volt - ampere per kilogram
     */
    const VOLT_AMPERE_PER_KILOGRAM = 'VA';

    /**
     * volt
     */
    const VOLT = 'VLT';

    /**
     * percent volume
     */
    const PERCENT_VOLUME = 'VP';

    /**
     * wet kilo
     */
    const WET_KILO = 'W2';

    /**
     * watt per kilogram
     */
    const WATT_PER_KILOGRAM = 'WA';

    /**
     * wet pound
     */
    const WET_POUND = 'WB';

    /**
     * cord
     */
    const CORD = 'WCD';

    /**
     * wet ton
     */
    const WET_TON = 'WE';

    /**
     * weber
     */
    const WEBER = 'WEB';

    /**
     * week
     */
    const WEEK = 'WEE';

    /**
     * wine gallon
     */
    const WINE_GALLON = 'WG';

    /**
     * watt hour
     */
    const WATT_HOUR = 'WHR';

    /**
     * working month
     */
    const WORKING_MONTH = 'WM';

    /**
     * standard
     */
    const STANDARD = 'WSD';

    /**
     * watt
     */
    const WATT = 'WTT';

    /**
     * Gunter's chain
     */
    const GUNTERS_CHAIN = 'X1';

    /**
     * square yard
     */
    const SQUARE_YARD = 'YDK';

    /**
     * cubic yard
     */
    const CUBIC_YARD = 'YDQ';

    /**
     * yard
     */
    const YARD = 'YRD';

    /**
     * hanging container
     */
    const HANGING_CONTAINER = 'Z11';

    /**
     * page
     */
    const PAGE = 'ZP';

    /**
     * mutually defined
     */
    const MUTUALLY_DEFINED = 'ZZ';

    /**
     * Drum, steel
     */
    const DRUM_STEEL = 'X1A';

    /**
     * Drum, aluminium
     */
    const DRUM_ALUMINIUM = 'X1B';

    /**
     * Drum, plywood
     */
    const DRUM_PLYWOOD = 'X1D';

    /**
     * Container, flexible
     */
    const CONTAINER_FLEXIBLE = 'X1F';

    /**
     * Drum, fibre
     */
    const DRUM_FIBRE = 'X1G';

    /**
     * Drum, wooden
     */
    const DRUM_WOODEN = 'X1W';

    /**
     * Barrel, wooden
     */
    const BARREL_WOODEN = 'X2C';

    /**
     * Jerrican, steel
     */
    const JERRICAN_STEEL = 'X3A';

    /**
     * Jerrican, plastic
     */
    const JERRICAN_PLASTIC = 'X3H';

    /**
     * Bag, super bulk
     */
    const BAG_SUPER_BULK = 'X43';

    /**
     * Bag, polybag
     */
    const BAG_POLYBAG = 'X44';

    /**
     * Box, steel
     */
    const BOX_STEEL = 'X4A';

    /**
     * Box, aluminium
     */
    const BOX_ALUMINIUM = 'X4B';

    /**
     * Box, natural wood
     */
    const BOX_NATURAL_WOOD = 'X4C';

    /**
     * Box, plywood
     */
    const BOX_PLYWOOD = 'X4D';

    /**
     * Box, reconstituted wood
     */
    const BOX_RECONSTITUTED_WOOD = 'X4F';

    /**
     * Box, fibreboard
     */
    const BOX_FIBREBOARD = 'X4G';

    /**
     * Box, plastic
     */
    const BOX_PLASTIC = 'X4H';

    /**
     * Bag, woven plastic
     */
    const BAG_WOVEN_PLASTIC = 'X5H';

    /**
     * Bag, textile
     */
    const BAG_TEXTILE = 'X5L';

    /**
     * Bag, paper
     */
    const BAG_PAPER = 'X5M';

    /**
     * Composite packaging, plastic receptacle
     */
    const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE = 'X6H';

    /**
     * Composite packaging, glass receptacle
     */
    const COMPOSITE_PACKAGING_GLASS_RECEPTACLE = 'X6P';

    /**
     * Case, car
     */
    const CASE_CAR = 'X7A';

    /**
     * Case, wooden
     */
    const CASE_WOODEN = 'X7B';

    /**
     * Pallet, wooden
     */
    const PALLET_WOODEN = 'X8A';

    /**
     * Crate, wooden
     */
    const CRATE_WOODEN = 'X8B';

    /**
     * Bundle, wooden
     */
    const BUNDLE_WOODEN = 'X8C';

    /**
     * Intermediate bulk container, rigid plastic
     */
    const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC = 'XAA';

    /**
     * Receptacle, fibre
     */
    const RECEPTACLE_FIBRE = 'XAB';

    /**
     * Receptacle, paper
     */
    const RECEPTACLE_PAPER = 'XAC';

    /**
     * Receptacle, wooden
     */
    const RECEPTACLE_WOODEN = 'XAD';

    /**
     * Aerosol
     */
    const AEROSOL = 'XAE';

    /**
     * Pallet, modular, collars 80cms * 60cms
     */
    const PALLET_MODULAR_COLLARS_80CM_60CM = 'XAF';

    /**
     * Pallet, shrinkwrapped
     */
    const PALLET_SHRINKWRAPPED = 'XAG';

    /**
     * Pallet, 100cms * 110cms
     */
    const PALLET_100CM_110CM = 'XAH';

    /**
     * Clamshell
     */
    const CLAMSHELL = 'XAI';

    /**
     * Cone
     */
    const CONE = 'XAJ';

    /**
     * Ball
     */
    const BALL_X = 'XAL';

    /**
     * Ampoule, non-protected
     */
    const AMPOULE_NON_PROTECTED = 'XAM';

    /**
     * Ampoule, protected
     */
    const AMPOULE_PROTECTED = 'XAP';

    /**
     * Atomizer
     */
    const ATOMIZER = 'XAT';

    /**
     * Capsule
     */
    const CAPSULE = 'XAV';

    /**
     * Belt
     */
    const BELT = 'XB4';

    /**
     * Barrel
     */
    const BARREL = 'XBA';

    /**
     * Bobbin
     */
    const BOBBIN = 'XBB';

    /**
     * Bottlecrate / bottlerack
     */
    const BOTTLECRATE_BOTTLERACK = 'XBC';

    /**
     * Board
     */
    const BOARD = 'XBD';

    /**
     * Bundle
     */
    const BUNDLE = 'XBE';

    /**
     * Balloon, non-protected
     */
    const BALLOON_NON_PROTECTED = 'XBF';

    /**
     * Bag
     */
    const BAG = 'XBG';

    /**
     * Bunch
     */
    const BUNCH = 'XBH';

    /**
     * Bin
     */
    const BIN = 'XBI';

    /**
     * Bucket
     */
    const BUCKET = 'XBJ';

    /**
     * Basket
     */
    const BASKET = 'XBK';

    /**
     * Bale, compressed
     */
    const BALE_COMPRESSED = 'XBL';

    /**
     * Basin
     */
    const BASIN = 'XBM';

    /**
     * Bale, non-compressed
     */
    const BALE_NON_COMPRESSED = 'XBN';

    /**
     * Bottle, non-protected, cylindrical
     */
    const BOTTLE_NON_PROTECTED_CYLINDRICAL = 'XBO';

    /**
     * Balloon, protected
     */
    const BALLOON_PROTECTED = 'XBP';

    /**
     * Bottle, protected cylindrical
     */
    const BOTTLE_PROTECTED_CYLINDRICAL = 'XBQ';

    /**
     * Bar
     */
    const BAR = 'XBR';

    /**
     * Bottle, non-protected, bulbous
     */
    const BOTTLE_NON_PROTECTED_BULBOUS = 'XBS';

    /**
     * Bolt
     */
    const BOLT = 'XBT';

    /**
     * Butt
     */
    const BUTT = 'XBU';

    /**
     * Bottle, protected bulbous
     */
    const BOTTLE_PROTECTED_BULBOUS = 'XBV';

    /**
     * Box, for liquids
     */
    const BOX_FOR_LIQUIDS = 'XBW';

    /**
     * Box
     */
    const BOX = 'XBX';

    /**
     * Board, in bundle/bunch/truss
     */
    const BOARD_IN_BUNDLE_BUNCH_TRUSS = 'XBY';

    /**
     * Bars, in bundle/bunch/truss
     */
    const BARS_IN_BUNDLE_BUNCH_TRUSS = 'XBZ';

    /**
     * Can, rectangular
     */
    const CAN_RECTANGULAR = 'XCA';

    /**
     * Crate, beer
     */
    const CRATE_BEER = 'XCB';

    /**
     * Churn
     */
    const CHURN = 'XCC';

    /**
     * Can, with handle and spout
     */
    const CAN_WITH_HANDLE_AND_SPOUT = 'XCD';

    /**
     * Creel
     */
    const CREEL = 'XCE';

    /**
     * Coffer
     */
    const COFFER = 'XCF';

    /**
     * Cage
     */
    const CAGE = 'XCG';

    /**
     * Chest
     */
    const CHEST = 'XCH';

    /**
     * Canister
     */
    const CANISTER = 'XCI';

    /**
     * Coffin
     */
    const COFFIN = 'XCJ';

    /**
     * Cask
     */
    const CASK = 'XCK';

    /**
     * Coil
     */
    const COIL = 'XCL';

    /**
     * Card
     */
    const CARD_X = 'XCM';

    /**
     * Container, not otherwise specified as transport equipment
     */
    const CONTAINER_NOT_OTHERWISE_SPECIFIED_AS_TRANSPORT_EQUIPMENT = 'XCN';

    /**
     * Carboy, non-protected
     */
    const CARBOY_NON_PROTECTED = 'XCO';

    /**
     * Carboy, protected
     */
    const CARBOY_PROTECTED = 'XCP';

    /**
     * Cartridge
     */
    const CARTRIDGE = 'XCQ';

    /**
     * Crate
     */
    const CRATE = 'XCR';

    /**
     * Case
     */
    const CASE = 'XCS';

    /**
     * Carton
     */
    const CARTON = 'XCT';

    /**
     * Cup
     */
    const CUP = 'XCU';

    /**
     * Cover
     */
    const COVER = 'XCV';

    /**
     * Cage, roll
     */
    const CAGE_ROLL = 'XCW';

    /**
     * Can, cylindrical
     */
    const CAN_CYLINDRICAL = 'XCX';

    /**
     * Cylinder
     */
    const CYLINDER = 'XCY';

    /**
     * Canvas
     */
    const CANVAS = 'XCZ';

    /**
     * Crate, multiple layer, plastic
     */
    const CRATE_MULTIPLE_LAYER_PLASTIC = 'XDA';

    /**
     * Crate, multiple layer, wooden
     */
    const CRATE_MULTIPLE_LAYER_WOODEN = 'XDB';

    /**
     * Crate, multiple layer, cardboard
     */
    const CRATE_MULTIPLE_LAYER_CARDBOARD = 'XDC';

    /**
     * Cage, Commonwealth Handling Equipment Pool (CHEP)
     */
    const CAGE_COMMONWEALTH_HANDLING_EQUIPMENT_POOL_CHEP = 'XDG';

    /**
     * Box, Commonwealth Handling Equipment Pool (CHEP), Eurobox
     */
    const BOX_COMMONWEALTH_HANDLING_EQUIPMENT_POOL_CHEP_EUROBOX = 'XDH';

    /**
     * Drum, iron
     */
    const DRUM_IRON = 'XDI';

    /**
     * Demijohn, non-protected
     */
    const DEMIJOHN_NON_PROTECTED = 'XDJ';

    /**
     * Crate, bulk, cardboard
     */
    const CRATE_BULK_CARDBOARD = 'XDK';

    /**
     * Crate, bulk, plastic
     */
    const CRATE_BULK_PLASTIC = 'XDL';

    /**
     * Crate, bulk, wooden
     */
    const CRATE_BULK_WOODEN = 'XDM';

    /**
     * Dispenser
     */
    const DISPENSER = 'XDN';

    /**
     * Demijohn, protected
     */
    const DEMIJOHN_PROTECTED = 'XDP';

    /**
     * Drum
     */
    const DRUM = 'XDR';

    /**
     * Tray, one layer no cover, plastic
     */
    const TRAY_ONE_LAYER_NO_COVER_PLASTIC = 'XDS';

    /**
     * Tray, one layer no cover, wooden
     */
    const TRAY_ONE_LAYER_NO_COVER_WOODEN = 'XDT';

    /**
     * Tray, one layer no cover, polystyrene
     */
    const TRAY_ONE_LAYER_NO_COVER_POLYSTYRENE = 'XDU';

    /**
     * Tray, one layer no cover, cardboard
     */
    const TRAY_ONE_LAYER_NO_COVER_CARDBOARD = 'XDV';

    /**
     * Tray, two layers no cover, plastic tray
     */
    const TRAY_TWO_LAYERS_NO_COVER_PLASTIC_TRAY = 'XDW';

    /**
     * Tray, two layers no cover, wooden
     */
    const TRAY_TWO_LAYERS_NO_COVER_WOODEN = 'XDX';

    /**
     * Tray, two layers no cover, cardboard
     */
    const TRAY_TWO_LAYERS_NO_COVER_CARDBOARD = 'XDY';

    /**
     * Bag, plastic
     */
    const BAG_PLASTIC = 'XEC';

    /**
     * Case, with pallet base
     */
    const CASE_WITH_PALLET_BASE = 'XED';

    /**
     * Case, with pallet base, wooden
     */
    const CASE_WITH_PALLET_BASE_WOODEN = 'XEE';

    /**
     * Case, with pallet base, cardboard
     */
    const CASE_WITH_PALLET_BASE_CARDBOARD = 'XEF';

    /**
     * Case, with pallet base, plastic
     */
    const CASE_WITH_PALLET_BASE_PLASTIC = 'XEG';

    /**
     * Case, with pallet base, metal
     */
    const CASE_WITH_PALLET_BASE_METAL = 'XEH';

    /**
     * Case, isothermic
     */
    const CASE_ISOTHERMIC = 'XEI';

    /**
     * Envelope
     */
    const ENVELOPE = 'XEN';

    /**
     * Flexibag
     */
    const FLEXIBAG = 'XFB';

    /**
     * Crate, fruit
     */
    const CRATE_FRUIT = 'XFC';

    /**
     * Crate, framed
     */
    const CRATE_FRAMED = 'XFD';

    /**
     * Flexitank
     */
    const FLEXITANK = 'XFE';

    /**
     * Firkin
     */
    const FIRKIN = 'XFI';

    /**
     * Flask
     */
    const FLASK = 'XFL';

    /**
     * Footlocker
     */
    const FOOTLOCKER = 'XFO';

    /**
     * Filmpack
     */
    const FILMPACK = 'XFP';

    /**
     * Frame
     */
    const FRAME = 'XFR';

    /**
     * Foodtainer
     */
    const FOODTAINER = 'XFT';

    /**
     * Cart, flatbed
     */
    const CART_FLATBED = 'XFW';

    /**
     * Bag, flexible container
     */
    const BAG_FLEXIBLE_CONTAINER = 'XFX';

    /**
     * Bottle, gas
     */
    const BOTTLE_GAS = 'XGB';

    /**
     * Girder
     */
    const GIRDER = 'XGI';

    /**
     * Container, gallon
     */
    const CONTAINER_GALLON = 'XGL';

    /**
     * Receptacle, glass
     */
    const RECEPTACLE_GLASS = 'XGR';

    /**
     * Tray, containing horizontally stacked flat items
     */
    const TRAY_CONTAINING_HORIZONTALLY_STACKED_FLAT_ITEMS = 'XGU';

    /**
     * Bag, gunny
     */
    const BAG_GUNNY = 'XGY';

    /**
     * Girders, in bundle/bunch/truss
     */
    const GIRDERS_IN_BUNDLE_BUNCH_TRUSS = 'XGZ';

    /**
     * Basket, with handle, plastic
     */
    const BASKET_WITH_HANDLE_PLASTIC = 'XHA';

    /**
     * Basket, with handle, wooden
     */
    const BASKET_WITH_HANDLE_WOODEN = 'XHB';

    /**
     * Basket, with handle, cardboard
     */
    const BASKET_WITH_HANDLE_CARDBOARD = 'XHC';

    /**
     * Hogshead
     */
    const HOGSHEAD = 'XHG';

    /**
     * Hanger
     */
    const HANGER = 'XHN';

    /**
     * Hamper
     */
    const HAMPER = 'XHR';

    /**
     * Package, display, wooden
     */
    const PACKAGE_DISPLAY_WOODEN = 'XIA';

    /**
     * Package, display, cardboard
     */
    const PACKAGE_DISPLAY_CARDBOARD = 'XIB';

    /**
     * Package, display, plastic
     */
    const PACKAGE_DISPLAY_PLASTIC = 'XIC';

    /**
     * Package, display, metal
     */
    const PACKAGE_DISPLAY_METAL = 'XID';

    /**
     * Package, show
     */
    const PACKAGE_SHOW = 'XIE';

    /**
     * Package, flow
     */
    const PACKAGE_FLOW = 'XIF';

    /**
     * Package, paper wrapped
     */
    const PACKAGE_PAPER_WRAPPED = 'XIG';

    /**
     * Drum, plastic
     */
    const DRUM_PLASTIC = 'XIH';

    /**
     * Package, cardboard, with bottle grip-holes
     */
    const PACKAGE_CARDBOARD_WITH_BOTTLE_GRIP_HOLES = 'XIK';

    /**
     * Tray, rigid, lidded stackable (CEN TS 14482:2002)
     */
    const TRAY_RIGID_LIDDED_STACKABLE = 'XIL';

    /**
     * Ingot
     */
    const INGOT = 'XIN';

    /**
     * Ingots, in bundle/bunch/truss
     */
    const INGOTS_IN_BUNDLE_BUNCH_TRUSS = 'XIZ';

    /**
     * Bag, jumbo
     */
    const BAG_JUMBO = 'XJB';

    /**
     * Jerrican, rectangular
     */
    const JERRICAN_RECTANGULAR = 'XJC';

    /**
     * Jug
     */
    const JUG = 'XJG';

    /**
     * Jar
     */
    const JAR = 'XJR';

    /**
     * Jutebag
     */
    const JUTEBAG = 'XJT';

    /**
     * Jerrican, cylindrical
     */
    const JERRICAN_CYLINDRICAL = 'XJY';

    /**
     * Keg
     */
    const KEG = 'XKG';

    /**
     * Kit
     */
    const KIT_X = 'XKI';

    /**
     * Luggage
     */
    const LUGGAGE = 'XLE';

    /**
     * Log
     */
    const LOG = 'XLG';

    /**
     * Lot
     */
    const LOT = 'XLT';

    /**
     * Lug
     */
    const LUG = 'XLU';

    /**
     * Liftvan
     */
    const LIFTVAN = 'XLV';

    /**
     * Logs, in bundle/bunch/truss
     */
    const LOGS_IN_BUNDLE_BUNCH_TRUSS = 'XLZ';

    /**
     * Crate, metal
     */
    const CRATE_METAL = 'XMA';

    /**
     * Bag, multiply
     */
    const BAG_MULTIPLY = 'XMB';

    /**
     * Crate, milk
     */
    const CRATE_MILK = 'XMC';

    /**
     * Container, metal
     */
    const CONTAINER_METAL = 'XME';

    /**
     * Receptacle, metal
     */
    const RECEPTACLE_METAL = 'XMR';

    /**
     * Sack, multi-wall
     */
    const SACK_MULTI_WALL = 'XMS';

    /**
     * Mat
     */
    const MAT = 'XMT';

    /**
     * Receptacle, plastic wrapped
     */
    const RECEPTACLE_PLASTIC_WRAPPED = 'XMW';

    /**
     * Matchbox
     */
    const MATCHBOX = 'XMX';

    /**
     * Not available
     */
    const NOT_AVAILABLE = 'XNA';

    /**
     * Unpacked or unpackaged
     */
    const UNPACKED_OR_UNPACKAGED = 'XNE';

    /**
     * Unpacked or unpackaged, single unit
     */
    const UNPACKED_OR_UNPACKAGED_SINGLE_UNIT = 'XNF';

    /**
     * Unpacked or unpackaged, multiple units
     */
    const UNPACKED_OR_UNPACKAGED_MULTIPLE_UNITS = 'XNG';

    /**
     * Nest
     */
    const NEST = 'XNS';

    /**
     * Net
     */
    const NET = 'XNT';

    /**
     * Net, tube, plastic
     */
    const NET_TUBE_PLASTIC = 'XNU';

    /**
     * Net, tube, textile
     */
    const NET_TUBE_TEXTILE = 'XNV';

    /**
     * Pallet, CHEP 40 cm x 60 cm
     */
    const PALLET_CHEP_40CM_60CM = 'XOA';

    /**
     * Pallet, CHEP 80 cm x 120 cm
     */
    const PALLET_CHEP_80CM_120CM = 'XOB';

    /**
     * Pallet, CHEP 100 cm x 120 cm
     */
    const PALLET_CHEP_100CM_120CM = 'XOC';

    /**
     * Pallet, AS 4068-1993
     */
    const PALLET_AS_4068_1993 = 'XOD';

    /**
     * Pallet, ISO T11
     */
    const PALLET_ISO_T11 = 'XOE';

    /**
     * Platform, unspecified weight or dimension
     */
    const PLATFORM_UNSPECIFIED_WEIGHT_OR_DIMENSION = 'XOF';

    /**
     * Block
     */
    const BLOCK = 'XOK';

    /**
     * Octabin
     */
    const OCTABIN = 'XOT';

    /**
     * Container, outer
     */
    const CONTAINER_OUTER = 'XOU';

    /**
     * Pan
     */
    const PAN = 'XP2';

    /**
     * Packet
     */
    const PACKET = 'XPA';

    /**
     * Pallet, box Combined open-ended box and pallet
     */
    const PALLET_BOX_COMBINED_OPEN_ENDED_BOX_AND_PALLET = 'XPB';

    /**
     * Parcel
     */
    const PARCEL = 'XPC';

    /**
     * Pallet, modular, collars 80cms * 100cms
     */
    const PALLET_MODULAR_COLLARS_80CM_100CM = 'XPD';

    /**
     * Pallet, modular, collars 80cms * 120cms
     */
    const PALLET_MODULAR_COLLARS_80CM_120CM = 'XPE';

    /**
     * Pen
     */
    const PEN = 'XPF';

    /**
     * Plate
     */
    const PLATE = 'XPG';

    /**
     * Pitcher
     */
    const PITCHER = 'XPH';

    /**
     * Pipe
     */
    const PIPE = 'XPI';

    /**
     * Punnet
     */
    const PUNNET = 'XPJ';

    /**
     * Package
     */
    const PACKAGE = 'XPK';

    /**
     * Pail
     */
    const PAIL = 'XPL';

    /**
     * Plank
     */
    const PLANK = 'XPN';

    /**
     * Pouch
     */
    const POUCH = 'XPO';

    /**
     * Piece
     */
    const PIECE_X = 'XPP';

    /**
     * Receptacle, plastic
     */
    const RECEPTACLE_PLASTIC = 'XPR';

    /**
     * Pot
     */
    const POT = 'XPT';

    /**
     * Tray
     */
    const TRAY = 'XPU';

    /**
     * Pipes, in bundle/bunch/truss
     */
    const PIPES_IN_BUNDLE_BUNCH_TRUSS = 'XPV';

    /**
     * Pallet
     */
    const PALLET = 'XPX';

    /**
     * Plates, in bundle/bunch/truss
     */
    const PLATES_IN_BUNDLE_BUNCH_TRUSS = 'XPY';

    /**
     * Planks, in bundle/bunch/truss
     */
    const PLANKS_IN_BUNDLE_BUNCH_TRUSS = 'XPZ';

    /**
     * Drum, steel, non-removable head
     */
    const DRUM_STEEL_NON_REMOVABLE_HEAD = 'XQA';

    /**
     * Drum, steel, removable head
     */
    const DRUM_STEEL_REMOVABLE_HEAD = 'XQB';

    /**
     * Drum, aluminium, non-removable head
     */
    const DRUM_ALUMINIUM_NON_REMOVABLE_HEAD = 'XQC';

    /**
     * Drum, aluminium, removable head
     */
    const DRUM_ALUMINIUM_REMOVABLE_HEAD = 'XQD';

    /**
     * Drum, plastic, non-removable head
     */
    const DRUM_PLASTIC_NON_REMOVABLE_HEAD = 'XQF';

    /**
     * Drum, plastic, removable head
     */
    const DRUM_PLASTIC_REMOVABLE_HEAD = 'XQG';

    /**
     * Barrel, wooden, bung type
     */
    const BARREL_WOODEN_BUNG_TYPE = 'XQH';

    /**
     * Barrel, wooden, removable head
     */
    const BARREL_WOODEN_REMOVABLE_HEAD = 'XQJ';

    /**
     * Jerrican, steel, non-removable head
     */
    const JERRICAN_STEEL_NON_REMOVABLE_HEAD = 'XQK';

    /**
     * Jerrican, steel, removable head
     */
    const JERRICAN_STEEL_REMOVABLE_HEAD = 'XQL';

    /**
     * Jerrican, plastic, non-removable head
     */
    const JERRICAN_PLASTIC_NON_REMOVABLE_HEAD = 'XQM';

    /**
     * Jerrican, plastic, removable head
     */
    const JERRICAN_PLASTIC_REMOVABLE_HEAD = 'XQN';

    /**
     * Box, wooden, natural wood, ordinary
     */
    const BOX_WOODEN_NATURAL_WOOD_ORDINARY = 'XQP';

    /**
     * Box, wooden, natural wood, with sift proof walls
     */
    const BOX_WOODEN_NATURAL_WOOD_WITH_SIFT_PROOF_WALLS = 'XQQ';

    /**
     * Box, plastic, expanded
     */
    const BOX_PLASTIC_EXPANDED = 'XQR';

    /**
     * Box, plastic, solid
     */
    const BOX_PLASTIC_SOLID = 'XQS';

    /**
     * Rod
     */
    const ROD = 'XRD';

    /**
     * Ring
     */
    const RING = 'XRG';

    /**
     * Rack, clothing hanger
     */
    const RACK_CLOTHING_HANGER = 'XRJ';

    /**
     * Rack
     */
    const RACK = 'XRK';

    /**
     * Reel
     */
    const REEL = 'XRL';

    /**
     * Roll
     */
    const ROLL = 'XRO';

    /**
     * Rednet
     */
    const REDNET = 'XRT';

    /**
     * Rods, in bundle/bunch/truss
     */
    const RODS_IN_BUNDLE_BUNCH_TRUSS = 'XRZ';

    /**
     * Sack
     */
    const SACK = 'XSA';

    /**
     * Slab
     */
    const SLAB = 'XSB';

    /**
     * Crate, shallow
     */
    const CRATE_SHALLOW = 'XSC';

    /**
     * Spindle
     */
    const SPINDLE = 'XSD';

    /**
     * Sea-chest
     */
    const SEA_CHEST = 'XSE';

    /**
     * Sachet
     */
    const SACHET = 'XSH';

    /**
     * Skid
     */
    const SKID = 'XSI';

    /**
     * Case, skeleton
     */
    const CASE_SKELETON = 'XSK';

    /**
     * Slipsheet
     */
    const SLIPSHEET = 'XSL';

    /**
     * Sheetmetal
     */
    const SHEETMETAL = 'XSM';

    /**
     * Spool
     */
    const SPOOL = 'XSO';

    /**
     * Sheet, plastic wrapping
     */
    const SHEET_PLASTIC_WRAPPING = 'XSP';

    /**
     * Case, steel
     */
    const CASE_STEEL = 'XSS';

    /**
     * Sheet
     */
    const SHEET = 'XST';

    /**
     * Suitcase
     */
    const SUITCASE = 'XSU';

    /**
     * Envelope, steel
     */
    const ENVELOPE_STEEL = 'XSV';

    /**
     * Shrinkwrapped
     */
    const SHRINKWRAPPED = 'XSW';

    /**
     * Sleeve
     */
    const SLEEVE = 'XSY';

    /**
     * Sheets, in bundle/bunch/truss
     */
    const SHEETS_IN_BUNDLE_BUNCH_TRUSS = 'XSZ';

    /**
     * Tablet
     */
    const TABLET_X = 'XT1';

    /**
     * Tub
     */
    const TUB = 'XTB';

    /**
     * Tea-chest
     */
    const TEA_CHEST = 'XTC';

    /**
     * Tube, collapsible
     */
    const TUBE_COLLAPSIBLE = 'XTD';

    /**
     * Tyre
     */
    const TYRE_X = 'XTE';

    /**
     * Tank container, generic
     */
    const TANK_CONTAINER_GENERIC = 'XTG';

    /**
     * Tierce
     */
    const TIERCE = 'XTI';

    /**
     * Tank, rectangular
     */
    const TANK_RECTANGULAR = 'XTK';

    /**
     * Tub, with lid
     */
    const TUB_WITH_LID = 'XTL';

    /**
     * Tin
     */
    const TIN = 'XTN';

    /**
     * Tun
     */
    const TUN = 'XTO';

    /**
     * Trunk
     */
    const TRUNK = 'XTR';

    /**
     * Truss
     */
    const TRUSS = 'XTS';

    /**
     * Bag, tote
     */
    const BAG_TOTE = 'XTT';

    /**
     * Tube
     */
    const TUBE = 'XTU';

    /**
     * Tube, with nozzle
     */
    const TUBE_WITH_NOZZLE = 'XTV';

    /**
     * Pallet, triwall
     */
    const PALLET_TRIWALL = 'XTW';

    /**
     * Tank, cylindrical
     */
    const TANK_CYLINDRICAL = 'XTY';

    /**
     * Tubes, in bundle/bunch/truss
     */
    const TUBES_IN_BUNDLE_BUNCH_TRUSS = 'XTZ';

    /**
     * Uncaged
     */
    const UNCAGED = 'XUC';

    /**
     * Unit
     */
    const UNIT_X = 'XUN';

    /**
     * Vat
     */
    const VAT = 'XVA';

    /**
     * Bulk, gas (at 1031 mbar and 15°C)
     */
    const BULK_GAS_1031_MBAR_15C = 'XVG';

    /**
     * Vial
     */
    const VIAL = 'XVI';

    /**
     * Vanpack
     */
    const VANPACK = 'XVK';

    /**
     * Bulk, liquid
     */
    const BULK_LIQUID = 'XVL';

    /**
     * Bulk, solid, large particles (“nodules”)
     */
    const BULK_SOLID_LARGE_PARTICLES = 'XVO';

    /**
     * Vacuum-packed
     */
    const VACUUM_PACKED = 'XVP';

    /**
     * Bulk, liquefied gas (at abnormal temperature/pressure)
     */
    const BULK_LIQUEFIED_GAS_AT_ABNORMAL_TEMPERATURE_PRESSURE = 'XVQ';

    /**
     * Vehicle
     */
    const VEHICLE = 'XVN';

    /**
     * Bulk, solid, granular particles (“grains”)
     */
    const BULK_SOLID_GRANULAR_PARTICLES = 'XVR';

    /**
     * Bulk, scrap metal
     */
    const BULK_SCRAP_METAL = 'XVS';

    /**
     * Bulk, solid, fine particles (“powders”)
     */
    const BULK_SOLID_FINE_PARTICLES = 'XVY';

    /**
     * Intermediate bulk container
     */
    const INTERMEDIATE_BULK_CONTAINER = 'XWA';

    /**
     * Wickerbottle
     */
    const WICKERBOTTLE = 'XWB';

    /**
     * Intermediate bulk container, steel
     */
    const INTERMEDIATE_BULK_CONTAINER_STEEL = 'XWC';

    /**
     * Intermediate bulk container, aluminium
     */
    const INTERMEDIATE_BULK_CONTAINER_ALUMINIUM = 'XWD';

    /**
     * Intermediate bulk container, metal
     */
    const INTERMEDIATE_BULK_CONTAINER_METAL = 'XWF';

    /**
     * Intermediate bulk container, steel, pressurised > 10 kpa
     */
    const INTERMEDIATE_BULK_CONTAINER_STEEL_PRESSURISED_UP_10_KPA = 'XWG';

    /**
     * Intermediate bulk container, aluminium, pressurised > 10 kpa
     */
    const INTERMEDIATE_BULK_CONTAINER_ALUMINIUM_PRESSURISED_UP_10_KPA = 'XWH';

    /**
     * Intermediate bulk container, metal, pressure 10 kpa
     */
    const INTERMEDIATE_BULK_CONTAINER_METAL_PRESSURE_10_KPA = 'XWJ';

    /**
     * Intermediate bulk container, steel, liquid
     */
    const INTERMEDIATE_BULK_CONTAINER_STEEL_LIQUID = 'XWK';

    /**
     * Intermediate bulk container, aluminium, liquid
     */
    const INTERMEDIATE_BULK_CONTAINER_ALUMINIUM_LIQUID = 'XWL';

    /**
     * Intermediate bulk container, metal, liquid
     */
    const INTERMEDIATE_BULK_CONTAINER_METAL_LIQUID = 'XWM';

    /**
     * Intermediate bulk container, woven plastic, without coat/liner
     */
    const INTERMEDIATE_BULK_CONTAINER_WOVEN_PLASTIC_WITHOUT_COAT_LINER = 'XWN';

    /**
     * Intermediate bulk container, woven plastic, coated
     */
    const INTERMEDIATE_BULK_CONTAINER_WOVEN_PLASTIC_COATED = 'XWP';

    /**
     * Intermediate bulk container, woven plastic, with liner
     */
    const INTERMEDIATE_BULK_CONTAINER_WOVEN_PLASTIC_WITH_LINER = 'XWQ';

    /**
     * Intermediate bulk container, woven plastic, coated and liner
     */
    const INTERMEDIATE_BULK_CONTAINER_WOVEN_PLASTIC_COATED_AND_LINER = 'XWR';

    /**
     * Intermediate bulk container, plastic film
     */
    const INTERMEDIATE_BULK_CONTAINER_PLASTIC_FILM = 'XWS';

    /**
     * Intermediate bulk container, textile with out coat/liner
     */
    const INTERMEDIATE_BULK_CONTAINER_TEXTILE_WITH_OUT_COAT_LINER = 'XWT';

    /**
     * Intermediate bulk container, natural wood, with inner liner
     */
    const INTERMEDIATE_BULK_CONTAINER_NATURAL_WOOD_WITH_INNER_LINER = 'XWU';

    /**
     * Intermediate bulk container, textile, coated
     */
    const INTERMEDIATE_BULK_CONTAINER_TEXTILE_COATED = 'XWV';

    /**
     * Intermediate bulk container, textile, with liner
     */
    const INTERMEDIATE_BULK_CONTAINER_TEXTILE_WITH_LINER = 'XWW';

    /**
     * Intermediate bulk container, textile, coated and liner
     */
    const INTERMEDIATE_BULK_CONTAINER_TEXTILE_COATED_AND_LINER = 'XWX';

    /**
     * Intermediate bulk container, plywood, with inner liner
     */
    const INTERMEDIATE_BULK_CONTAINER_PLYWOOD_WITH_INNER_LINER = 'XWY';

    /**
     * Intermediate bulk container, reconstituted wood, with inner liner
     */
    const INTERMEDIATE_BULK_CONTAINER_RECONSTITUTED_WOOD_WITH_INNER_LINER = 'XWZ';

    /**
     * Bag, woven plastic, without inner coat/liner
     */
    const BAG_WOVEN_PLASTIC_WITHOUT_INNER_COAT_LINER = 'XXA';

    /**
     * Bag, woven plastic, sift proof
     */
    const BAG_WOVEN_PLASTIC_SIFT_PROOF = 'XXB';

    /**
     * Bag, woven plastic, water resistant
     */
    const BAG_WOVEN_PLASTIC_WATER_RESISTANT = 'XXC';

    /**
     * Bag, plastics film
     */
    const BAG_PLASTICS_FILM = 'XXD';

    /**
     * Bag, textile, without inner coat/liner
     */
    const BAG_TEXTILE_WITHOUT_INNER_COAT_LINER = 'XXF';

    /**
     * Bag, textile, sift proof
     */
    const BAG_TEXTILE_SIFT_PROOF = 'XXG';

    /**
     * Bag, textile, water resistant
     */
    const BAG_TEXTILE_WATER_RESISTANT = 'XXH';

    /**
     * Bag, paper, multi-wall
     */
    const BAG_PAPER_MULTI_WALL = 'XXJ';

    /**
     * Bag, paper, multi-wall, water resistant
     */
    const BAG_PAPER_MULTI_WALL_WATER_RESISTANT = 'XXK';

    /**
     * Composite packaging, plastic receptacle in steel drum
     */
    const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_STEEL_DRUM = 'XYA';

    /**
     * Composite packaging, plastic receptacle in steel crate box
     */
    const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_STEEL_CRATE_BOX = 'XYB';

    /**
     * Composite packaging, plastic receptacle in aluminium drum
     */
    const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_ALUMINIUM_DRUM = 'XYC';

    /**
     * Composite packaging, plastic receptacle in aluminium crate
     */
    const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_ALUMINIUM_CRATE = 'XYD';

    /**
     * Composite packaging, plastic receptacle in wooden box
     */
    const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_WOODEN_BOX = 'XYF';

    /**
     * Composite packaging, plastic receptacle in plywood drum
     */
    const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_PLYWOOD_DRUM = 'XYG';

    /**
     * Composite packaging, plastic receptacle in plywood box
     */
    const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_PLYWOOD_BOX = 'XYH';

    /**
     * Composite packaging, plastic receptacle in fibre drum
     */
    const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_FIBRE_DRUM = 'XYJ';

    /**
     * Composite packaging, plastic receptacle in fibreboard box
     */
    const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_FIBREBOARD_BOX = 'XYK';

    /**
     * Composite packaging, plastic receptacle in plastic drum
     */
    const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_PLASTIC_DRUM = 'XYL';

    /**
     * Composite packaging, plastic receptacle in solid plastic box
     */
    const COMPOSITE_PACKAGING_PLASTIC_RECEPTACLE_IN_SOLID_PLASTIC_BOX = 'XYM';

    /**
     * Composite packaging, glass receptacle in steel drum
     */
    const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_STEEL_DRUM = 'XYN';

    /**
     * Composite packaging, glass receptacle in steel crate box
     */
    const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_STEEL_CRATE_BOX = 'XYP';

    /**
     * Composite packaging, glass receptacle in aluminium drum
     */
    const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_ALUMINIUM_DRUM = 'XYQ';

    /**
     * Composite packaging, glass receptacle in aluminium crate
     */
    const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_ALUMINIUM_CRATE = 'XYR';

    /**
     * Composite packaging, glass receptacle in wooden box
     */
    const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_WOODEN_BOX = 'XYS';

    /**
     * Composite packaging, glass receptacle in plywood drum
     */
    const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_PLYWOOD_DRUM = 'XYT';

    /**
     * Composite packaging, glass receptacle in wickerwork hamper
     */
    const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_WICKERWORK_HAMPER = 'XYV';

    /**
     * Composite packaging, glass receptacle in fibre drum
     */
    const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_FIBRE_DRUM = 'XYW';

    /**
     * Composite packaging, glass receptacle in fibreboard box
     */
    const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_FIBREBOARD_BOX = 'XYX';

    /**
     * Composite packaging, glass receptacle in expandable plastic pack
     */
    const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_EXPANDABLE_PLASTIC_PACK = 'XYY';

    /**
     * Composite packaging, glass receptacle in solid plastic pack
     */
    const COMPOSITE_PACKAGING_GLASS_RECEPTACLE_IN_SOLID_PLASTIC_PACK = 'XYZ';

    /**
     * Intermediate bulk container, paper, multi-wall
     */
    const INTERMEDIATE_BULK_CONTAINER_PAPER_MULTI_WALL = 'XZA';

    /**
     * Bag, large
     */
    const BAG_LARGE = 'XZB';

    /**
     * Intermediate bulk container, paper, multi-wall, water resistant
     */
    const INTERMEDIATE_BULK_CONTAINER_PAPER_MULTI_WALL_WATER_RESISTANT = 'XZC';

    /**
     * Intermediate bulk container, rigid plastic, with structural equipment, solids
     */
    const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC_WITH_STRUCTURAL_EQUIPMENT_SOLIDS = 'XZD';

    /**
     * Intermediate bulk container, rigid plastic, freestanding, solids
     */
    const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC_FREESTANDING_SOLIDS = 'XZF';

    /**
     * Intermediate bulk container, rigid plastic, with structural equipment, pressurised
     */
    const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC_WITH_STRUCTURAL_EQUIPMENT_PRESSURISED = 'XZG';

    /**
     * Intermediate bulk container, rigid plastic, freestanding, pressurised
     */
    const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC_FREESTANDING_PRESSURISED = 'XZH';

    /**
     * Intermediate bulk container, rigid plastic, with structural equipment, liquids
     */
    const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC_WITH_STRUCTURAL_EQUIPMENT_LIQUIDS = 'XZJ';

    /**
     * Intermediate bulk container, rigid plastic, freestanding, liquids
     */
    const INTERMEDIATE_BULK_CONTAINER_RIGID_PLASTIC_FREESTANDING_LIQUIDS = 'XZK';

    /**
     * Intermediate bulk container, composite, rigid plastic, solids
     */
    const INTERMEDIATE_BULK_CONTAINER_COMPOSITE_RIGID_PLASTIC_SOLIDS = 'XZL';

    /**
     * Intermediate bulk container, composite, flexible plastic, solids
     */
    const INTERMEDIATE_BULK_CONTAINER_COMPOSITE_FLEXIBLE_PLASTIC_SOLIDS = 'XZM';

    /**
     * Intermediate bulk container, composite, rigid plastic, pressurised
     */
    const INTERMEDIATE_BULK_CONTAINER_COMPOSITE_RIGID_PLASTIC_PRESSURISED = 'XZN';

    /**
     * Intermediate bulk container, composite, flexible plastic, pressurised
     */
    const INTERMEDIATE_BULK_CONTAINER_COMPOSITE_FLEXIBLE_PLASTIC_PRESSURISED = 'XZP';

    /**
     * Intermediate bulk container, composite, rigid plastic, liquids
     */
    const INTERMEDIATE_BULK_CONTAINER_COMPOSITE_RIGID_PLASTIC_LIQUIDS = 'XZQ';

    /**
     * Intermediate bulk container, composite, flexible plastic, liquids
     */
    const INTERMEDIATE_BULK_CONTAINER_COMPOSITE_FLEXIBLE_PLASTIC_LIQUIDS = 'XZR';

    /**
     * Intermediate bulk container, composite
     */
    const INTERMEDIATE_BULK_CONTAINER_COMPOSITE = 'XZS';

    /**
     * Intermediate bulk container, fibreboard
     */
    const INTERMEDIATE_BULK_CONTAINER_FIBREBOARD = 'XZT';

    /**
     * Intermediate bulk container, flexible
     */
    const INTERMEDIATE_BULK_CONTAINER_FLEXIBLE = 'XZU';

    /**
     * Intermediate bulk container, metal, other than steel
     */
    const INTERMEDIATE_BULK_CONTAINER_METAL_OTHER_THAN_STEEL = 'XZV';

    /**
     * Intermediate bulk container, natural wood
     */
    const INTERMEDIATE_BULK_CONTAINER_NATURAL_WOOD = 'XZW';

    /**
     * Intermediate bulk container, plywood
     */
    const INTERMEDIATE_BULK_CONTAINER_PLYWOOD = 'XZX';

    /**
     * Intermediate bulk container, reconstituted wood
     */
    const INTERMEDIATE_BULK_CONTAINER_RECONSTITUTED_WOOD = 'XZY';



}
