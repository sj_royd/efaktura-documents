<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * Payment means code (UNCL4461)
 * Usage
 * /ubl:Invoice/cac:PaymentMeans/cbc:PaymentMeansCode
 * /ubl:CreditNote/cac:PaymentMeans/cbc:PaymentMeansCode
 */
class PaymentMeans
{
    /**
    * Instrument not defined
    */
   const INSTRUMENT_NOT_DEFINED = 1;

   /**
    * Automated clearing house credit
    */
   const AUTOMATED_CLEARING_HOUSE_CREDIT = 2;

   /**
    * Automated clearing house debit
    */
   const AUTOMATED_CLEARING_HOUSE_DEBIT = 3;

   /**
    * ACH demand debit reversal
    */
   const ACH_DEMAND_DEBIT_REVERSAL = 4;

   /**
    * ACH demand credit reversal
    */
   const ACH_DEMAND_CREDIT_REVERSAL = 5;

   /**
    * ACH demand credit
    */
   const ACH_DEMAND_CREDIT = 6;

   /**
    * ACH demand debit
    */
   const ACH_DEMAND_DEBIT = 7;

   /**
    * Hold
    */
   const HOLD = 8;

   /**
    * regional clearing
    */
   const REGIONAL_CLEARING = 9;

   /**
    * National clearing
    */
   const NATIONAL_CLEARING = 9;

   /**
    * In cash
    */
   const IN_CASH = 10;

   /**
    * ACH savings credit reversal
    */
   const ACH_SAVINGS_CREDIT_REVERSAL = 11;

   /**
    * ACH savings debit reversal
    */
   const ACH_SAVINGS_DEBIT_REVERSAL = 12;

   /**
    * ACH savings credit
    */
   const ACH_SAVINGS_CREDIT = 13;

   /**
    * ACH savings debit
    */
   const ACH_SAVINGS_DEBIT = 14;

   /**
    * Bookentry credit
    */
   const BOOKENTRY_CREDIT = 15;

   /**
    * Bookentry debit
    */
   const BOOKENTRY_DEBIT = 16;

   /**
    * ACH demand cash concentration/disbursement (CCD) credit
    */
   const ACH_DEMAND_CCD_CREDIT = 17;

   /**
    * ACH demand cash concentration/disbursement (CCD) debit
    */
   const ACH_DEMAND_CCD_DEBIT = 18;

   /**
    * ACH demand corporate trade payment (CTP) credit
    */
   const ACH_DEMAND_CTP_CREDIT = 19;

   /**
    * Cheque
    */
   const CHEQUE = 20;

   /**
    * Banker's draft
    */
   const BANKERS_DRAFT = 21;

   /**
    * Certified banker's draft
    */
   const CERTIFIED_BANKERS_DRAFT = 22;

   /**
    * Bank cheque (issued by a banking or similar establishment)
    */
   const BANK_CHEQUE = 23;

   /**
    * Bill of exchange awaiting acceptance
    */
   const BILL_OF_EXCHANGE_AWAITING_ACCEPTANCE = 24;

   /**
    * Certified cheque
    */
   const CERTIFIED_CHEQUE = 25;

   /**
    * Local cheque
    */
   const LOCAL_CHEQUE = 26;

   /**
    * ACH demand corporate trade payment (CTP) debit
    */
   const ACH_DEMAND_CTP_DEBIT = 27;

   /**
    * ACH demand corporate trade exchange (CTX) credit
    */
   const ACH_DEMAND_CTX_CREDIT = 28;

   /**
    * ACH demand corporate trade exchange (CTX) debit
    */
   const ACH_DEMAND_CTX_DEBIT = 29;

   /**
    * Credit transfer
    */
   const CREDIT_TRANSFER = 30;

   /**
    * Debit transfer
    */
   const DEBIT_TRANSFER = 31;

   /**
    * ACH demand cash concentration/disbursement plus (CCD+) credit
    */
   const ACH_DEMAND_CCDP_CREDIT = 32;

   /**
    * ACH demand cash concentration/disbursement plus (CCD+) debit
    */
   const ACH_DEMAND_CCDP_DEBIT = 33;

   /**
    * ACH prearranged payment and deposit (PPD)
    */
   const ACH_PPD = 34;

   /**
    * ACH savings cash concentration/disbursement (CCD) credit
    */
   const ACH_SAVINGS_CCD_CREDIT = 35;

   /**
    * ACH savings cash concentration/disbursement (CCD) debit
    */
   const ACH_SAVINGS_CCD_DEBIT = 36;

   /**
    * ACH savings corporate trade payment (CTP) credit
    */
   const ACH_SAVINGS_CTP_CREDIT = 37;

   /**
    * ACH savings corporate trade payment (CTP) debit
    */
   const ACH_SAVINGS_CTP_DEBIT = 38;

   /**
    * ACH savings corporate trade exchange (CTX) credit
    */
   const ACH_SAVINGS_CTX_CREDIT = 39;

   /**
    * ACH savings corporate trade exchange (CTX) debit
    */
   const ACH_SAVINGS_CTX_DEBIT = 40;

   /**
    * ACH savings cash concentration/disbursement plus (CCD+) credit
    */
   const ACH_SAVINGS_CCDP_CREDIT = 41;

   /**
    * Payment to bank account
    */
   const PAYMENT_TO_BANK_ACCOUNT = 42;

   /**
    * ACH savings cash concentration/disbursement plus (CCD+) debit
    */
   const ACH_SAVINGS_CCDP_DEBIT = 43;

   /**
    * Accepted bill of exchange
    */
   const ACCEPTED_BILL_OF_EXCHANGE = 44;

   /**
    * Referenced home-banking credit transfer
    */
   const REFERENCED_HOME_BANKING_CREDIT_TRANSFER = 45;

   /**
    * Interbank debit transfer
    */
   const INTERBANK_DEBIT_TRANSFER = 46;

   /**
    * Home-banking debit transfer
    */
   const HOME_BANKING_DEBIT_TRANSFER = 47;

   /**
    * Bank card
    */
   const BANK_CARD = 48;

   /**
    * Direct debit
    */
   const DIRECT_DEBIT = 49;

   /**
    * Payment by postgiro
    */
   const PAYMENT_BY_POSTGIRO = 50;

   /**
    * FR, norme 6 97-Telereglement CFONB (French Organisation for
    */
   const CFONB = 51;

   /**
    * Urgent commercial payment
    */
   const URGENT_COMMERCIAL_PAYMENT = 52;

   /**
    * Urgent Treasury Payment
    */
   const URGENT_TREASURY_PAYMENT = 53;

   /**
    * Credit card
    */
   const CREDIT_CARD = 54;

   /**
    * Debit card
    */
   const DEBIT_CARD = 55;

   /**
    * Bankgiro
    */
   const BANKGIRO = 56;

   /**
    * Standing agreement
    */
   const STANDING_AGREEMENT = 57;

   /**
    * SEPA credit transfer
    */
   const SEPA_CREDIT_TRANSFER = 58;

   /**
    * SEPA direct debit
    */
   const SEPA_DIRECT_DEBIT = 59;

   /**
    * Promissory note
    */
   const PROMISSORY_NOTE = 60;

   /**
    * Promissory note signed by the debtor
    */
   const PROMISSORY_NOTE_SIGNED_BY_THE_DEBTOR = 61;

   /**
    * Promissory note signed by the debtor and endorsed by a bank
    */
   const PROMISSORY_NOTE_SIGNED_BY_THE_DEBTOR_AND_ENDORSED_BY_A_BANK = 62;

   /**
    * Promissory note signed by the debtor and endorsed by a third party
    */
   const PROMISSORY_NOTE_SIGNED_BY_THE_DEBTOR_AND_ENDORSED_BY_A_THIRD_PARTY = 63;

   /**
    * Promissory note signed by a bank
    */
   const PROMISSORY_NOTE_SIGNED_BY_A_BANK = 64;

   /**
    * Promissory note signed by a bank and endorsed by another bank
    */
   const PROMISSORY_NOTE_SIGNED_BY_A_BANK_AND_ENDORSED_BY_ANOTHER_BANK = 65;

   /**
    * Promissory note signed by a third party
    */
   const PROMISSORY_NOTE_SIGNED_BY_A_THIRD_PARTY = 66;

   /**
    * Promissory note signed by a third party and endorsed by a bank
    */
   const PROMISSORY_NOTE_SIGNED_BY_A_THIRD_PARTY_AND_ENDORSED_BY_A_BANK = 67;

   /**
    * Online payment service
    */
   const ONLINE_PAYMENT_SERVICE = 68;

   /**
    * Bill drawn by the creditor on the debtor
    */
   const BILL_DRAWN_BY_THE_CREDITOR_ON_THE_DEBTOR = 70;

   /**
    * Bill drawn by the creditor on a bank
    */
   const BILL_DRAWN_BY_THE_CREDITOR_ON_A_BANK = 74;

   /**
    * Bill drawn by the creditor, endorsed by another bank
    */
   const BILL_DRAWN_BY_THE_CREDITOR = 75;

   /**
    * Bill drawn by the creditor on a bank and endorsed by a third party
    */
   const BILL_DRAWN_BY_THE_CREDITOR_ON_A_BANK_AND_ENDORSED_BY_A_THIRD_PARTY = 76;

   /**
    * Bill drawn by the creditor on a third party
    */
   const BILL_DRAWN_BY_THE_CREDITOR_ON_A_THIRD_PARTY = 77;

   /**
    * Bill drawn by creditor on third party, accepted and endorsed by bank
    */
   const BILL_DRAWN_BY_CREDITOR_ON_THIRD_PARTY = 78;

   /**
    * Not transferable banker's draft
    */
   const NOT_TRANSFERABLE_BANKERS_DRAFT = 91;

   /**
    * Not transferable local cheque
    */
   const NOT_TRANSFERABLE_LOCAL_CHEQUE = 92;

   /**
    * Reference giro
    */
   const REFERENCE_GIRO = 93;

   /**
    * Urgent giro
    */
   const URGENT_GIRO = 94;

   /**
    * Free format giro
    */
   const FREE_FORMAT_GIRO = 95;

   /**
    * Requested method for payment was not used
    */
   const REQUESTED_METHOD_FOR_PAYMENT_WAS_NOT_USED = 96;

   /**
    * Clearing between partners
    */
   const CLEARING_BETWEEN_PARTNERS = 97;

   /**
    * Mutually defined
    */
   const MUTUALLY_DEFINED = 'ZZZ';


}
