<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * Electronic Address Scheme (EAS)
 * Usage
 * /ubl:Invoice/cac:AccountingSupplierParty/cac:Party/cbc:EndpointID/@schemeID
 * /ubl:Invoice/cac:AccountingCustomerParty/cac:Party/cbc:EndpointID/@schemeID
 * /ubl:CreditNote/cac:AccountingSupplierParty/cac:Party/cbc:EndpointID/@schemeID
 * /ubl:CreditNote/cac:AccountingCustomerParty/cac:Party/cbc:EndpointID/@schemeID
 */
class EAS
{

    /**
     * System Information et Repertoire des Entreprise et des Etablissements: SIRENE
     */
    const SIRENE = '0002';

    /**
     * Organisationsnumme
     */
    const ORGANISATIONSNUMME = '0007';

    /**
     * SIRET-CODE
     */
    const SIRET_CODE = '0009';

    /**
     * LY-tunnus
     */
    const LY_TUNNUS = '0037';

    /**
     * Data Universal Numbering System (D-U-N-S Number)
     */
    const DUNS = '0060';

    /**
     * EAN
     */
    const EAN = '0088';

    /**
     * EAN/GLN
     */
    const GLN = '0088';

    /**
     * DANISH CHAMBER OF COMMERCE Scheme (EDIRA compliant)
     */
    const DANISH = '0096';

    /**
     * FTI - Ediforum Italia, (EDIRA compliant)
     */
    const FTI = '0097';

    /**
     * Vereniging van Kamers van Koophandel en Fabrieken in Nederland
     * (Association of Chambers of Commerce and Industry in the Netherlands),
     * Scheme (EDIRA compliant)
     */
    const NETHERLANDS = '0106';

    /**
     * SIA Object Identifiers
     */
    const SIA = '0135';

    /**
     * SECETI Object Identifiers
     */
    const SECETI = '0142';

    /**
     * DIGSTORG
     */
    const DIGSTORG = '0184';

    /**
     * Dutch Originator's Identification Number
     */
    const DUTCH = '0190';

    /**
     * Centre of Registers and Information Systems of the Ministry of Justice
     */
    const MINISTRY_OF_JUSTICE = '0191';

    /**
     * Enhetsregisteret ved Bronnoysundregisterne
     */
    const BRONNOYSUND  = '0192';

    /**
     * UBL.BE party identifier
     */
    const UBL_BE = '0193';

    /**
     * Singapore UEN identifier
     */
    const SINGAPORE_UEN = '0195';

    /**
     * Kennitala - Iceland legal id for individuals and legal entities
     */
    const ICELAND_LEGAL_INDIVIDUALS = '0196';

    /**
     * ERSTORG
     */
    const ERSTORG = '0198';

    /**
     * Global legal entity identifier (GLEIF)
     */
    const GLEIF = '0199';

    /**
     * Legal entity code (Lithuania)
     */
    const LEGAL_LT = '0200';

    /**
     * Danish Ministry of the Interior and Health
     */
    const DANISH_HEALTH = '9901';

    /**
     * The Danish Commerce and Companies Agency
     */
    const DANISH_COMPANIES_AGENCY = '9902';

    /**
     * Danish Ministry of Taxation, Central Customs and Tax Administration
     */
    const DANISH_TAXATION = '9904';

    /**
     * Danish VANS providers
     */
    const DANISH_VANS = '9905';

    /**
     * Ufficio responsabile gestione partite IVA
     */
    const UFFICIO = '9906';

    /**
     * TAX Authority
     */
    const TAX_AUTHORITY = '9907';

    /**
     * Hungary VAT number
     */
    const VAT_HU = '9910';

    /**
     * Business Registers Network
     */
    const BRN = '9913';

    /**
     * Österreichische Umsatzsteuer-Identifikationsnummer
     */
    const OMI = '9914';

    /**
     * Österreichisches Verwaltungs bzw. Organisationskennzeichen
     */
    const OVO = '9915';

    /**
     * Kennitala - Iceland legal id for organizations and individuals
     */
    const ICELAND_LEGAL_ORGANIZATIONS = '9917';

    /**
     * SOCIETY FOR WORLDWIDE INTERBANK FINANCIAL, TELECOMMUNICATION S.W.I.F.T
     */
    const SWIFT = '9918';

    /**
     * Kennziffer des Unternehmensregisters
     */
    const KENNZIFFER = '9919';

    /**Agencia Española de Administración Tributaria
     *
     */
    const ESPANOLA_TRIBUTARIA = '9920';

    /**
     * Indice delle Pubbliche Amministrazioni
     */
    const INDICE_PUBBLICHE_AMMINISTRAZIONI = '9921';

    /**
     * Andorra VAT number
     */
    const VAT_AD = '9922';

    /**
     * Albania VAT number
     */
    const VAT_AL = '9923';

    /**
     * Bosnia and Herzegovina VAT number
     */
    const VAT_BA = '9924';

    /**
     * Belgium VAT number
     */
    const VAT_BE = '9925';

    /**
     * Bulgaria VAT number
     */
    const VAT_BG = '9926';

    /**
     * Switzerland VAT number
     */
    const VAT_CH = '9927';

    /**
     * Cyprus VAT number
     */
    const VAT_CY = '9928';

    /**
     * Czech Republic VAT number
     */
    const VAT_CZ = '9929';

    /**
     * Germany VAT number
     */
    const VAT_DE = '9930';

    /**
     * Estonia VAT number
     */
    const VAT_EE = '9931';

    /**
     * United Kingdom VAT number
     */
    const VAT_GB = '9932';

    /**
     * Greece VAT number
     */
    const VAT_GR = '9933';

    /**
     * Croatia VAT number
     */
    const VAT_HR = '9934';

    /**
     * Ireland VAT number
     */
    const VAT_IR = '9935';

    /**
     * Liechtenstein VAT number
     */
    const VAT_LI = '9936';

    /**
     * Lithuania VAT number
     */
    const VAT_LT = '9937';

    /**
     * Luxemburg VAT number
     */
    const VAT_LU = '9938';

    /**
     * Latvia VAT number
     */
    const VAT_LV = '9939';

    /**
     * Monaco VAT number
     */
    const VAT_MC = '9940';

    /**
     * Montenegro VAT number
     */
    const VAT_ME = '9941';

    /**
     * Macedonia, the former Yugoslav Republic of VAT number
     */
    const VAT_MK = '9942';

    /**
     * Malta VAT number
     */
    const VAT_MT = '9943';

    /**
     * Netherlands VAT number
     */
    const VAT_NL = '9944';

    /**
     * Poland VAT number
     */
    const VAT_PL = '9945';

    /**
     * Portugal VAT number
     */
    const VAT_PT = '9946';

    /**
     * Romania VAT number
     */
    const VAT_RO = '9947';

    /**
     * Serbia VAT number
     */
    const VAT_RS = '9948';

    /**
     * Slovenia VAT number
     */
    const VAT_SI = '9949';

    /**
     * Slovakia VAT number
     */
    const VAT_SK = '9950';

    /**
     * San Marino VAT number
     */
    const VAT_SM = '9951';

    /**
     * Turkey VAT number
     */
    const VAT_TR = '9952';

    /**
     * Holy See (Vatican City State) VAT number
     */
    const VAT_VA = '9953';

    /**
     * Swedish VAT number
     */
    const VAT_SE = '9955';

    /**
     * Belgian Crossroad Bank of Enterprises
     */
    const BELGIAN_CROSSROAD_BANK = '9956';

    /**
     * French VAT number
     */
    const VAT_FR = '9957';

    /**
     * German Leitweg ID
     */
    const GERMAN_LEITWEG = '9958';
}
