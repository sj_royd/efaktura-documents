<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * Invoice type code (UNCL1001 subset)
 * Usage /ubl:Invoice/cbc:InvoiceTypeCode
 */
class InvoiceTypeCode
{

    /**
     * Debit note related to goods or services
     * Debit information related to a transaction for goods or services
     * to the relevant party.
     */
    const DEBIT_NOTE_GOODS_SERVICES = 80;

    /**
     * Metered services invoice
     * Document/message claiming payment for the supply of metered services
     * (e.g., gas, electricity, etc.) supplied to a fixed meter whose
     * consumption is measured over a period of time.
     */
    const METERED_SERVICE_INVOICE = 82;

    /**
     * Debit note related to financial adjustments
     *
     */
    const DEBIT_NOTE_FINANCIAL = 84;

    /**
     * Commercial invoice
     * (1334) Document/message claiming payment for goods or services supplied
     * under conditions agreed between seller and buyer.
     */
    const INVOICE = 380;

    /**
     * Debit note
     * Document/message for providing debit information to the relevant party.
     */
    const DEBIT_NOTE = 383;

    /**
     * Prepayment invoice
     * An invoice to pay amounts for goods and services in advance; these amounts
     * will be subtracted from the final invoice.
     */
    const PREPAYMENT_INVOICE = 386;

    /**
     * Factored invoice
     * Invoice assigned to a third party for collection.
     */
    const FACTORED_INVOICE = 393;

    /**
     * Consignment invoice
     * Commercial invoice that covers a transaction other than one involving a sale.
     */
    const CONSIGNMENT_INVOICE = 395;

    /**
     * Insurer's invoice
     * Document/message issued by an insurer specifying the cost of an insurance
     * which has been effected and claiming payment therefore.
     */
    const INSURERS_INVOICE = 575;

    /**
     * Forwarder's invoice
     * Invoice issued by a freight forwarder specifying services rendered and
     * costs incurred and claiming payment therefore.
     */
    const FORWARDERS_INVOICE = 623;

    /**
     * Freight invoice
     * Document/message issued by a transport operation specifying freight costs
     * and charges incurred for a transport operation and stating conditions of payment.
     */
    const FREIGHT_INVOICE = 780;

}
