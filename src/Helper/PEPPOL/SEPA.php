<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * SEPA indicator
 * Usage
 */
class SEPA
{
    /**
     * SEPA unique bank assigned creditor identifier
     */
    const SEPA = 'SEPA';
}
