<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * VAT date code (UNCL2005 subset)
 * Usage
 * /ubl:Invoice/cac:InvoicePeriod/cbc:DescriptionCode
 * /ubl:CreditNote/cac:InvoicePeriod/cbc:DescriptionCode
 */
class VatDate
{
    /**
     * Invoice document issue date time
     */
    const ISSUE = 3;

    /**
     * Delivery date/time, actual
     */
    const DELIVERY = 35;

    /**
     * Paid to date
     */
    const PAID_TO = 423;
}
