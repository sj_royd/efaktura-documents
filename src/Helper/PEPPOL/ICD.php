<?php

namespace SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * ISO 6523 ICD list
 * Usage
 * /ubl:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification/cbc:ID/@schemeID
 * /ubl:Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cbc:CompanyID/@schemeID
 * /ubl:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID/@schemeID
 * /ubl:Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cbc:CompanyID/@schemeID
 * /ubl:Invoice/cac:PayeeParty/cac:PartyIdentification/cbc:ID/@schemeID
 * /ubl:Invoice/cac:PayeeParty/cac:PartyLegalEntity/cbc:CompanyID/@schemeID
 * /ubl:Invoice/cac:Delivery/cac:DeliveryLocation/cbc:ID/@schemeID
 * /ubl:Invoice/cac:InvoiceLine/cac:Item/cac:StandardItemIdentification/cbc:ID/@schemeID
 * /ubl:CreditNote/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification/cbc:ID/@schemeID
 * /ubl:CreditNote/cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cbc:CompanyID/@schemeID
 * /ubl:CreditNote/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID/@schemeID
 * /ubl:CreditNote/cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cbc:CompanyID/@schemeID
 * /ubl:CreditNote/cac:PayeeParty/cac:PartyIdentification/cbc:ID/@schemeID
 * /ubl:CreditNote/cac:PayeeParty/cac:PartyLegalEntity/cbc:CompanyID/@schemeID
 * /ubl:CreditNote/cac:Delivery/cac:DeliveryLocation/cbc:ID/@schemeID
 * /ubl:CreditNote/cac:CreditNoteLine/cac:Item/cac:StandardItemIdentification/cbc:ID/@schemeID
 */
class ICD
{
    /**
     * System Information et Repertoire des Entreprise et des Etablissements: SIRENE
     */
    const SIRENE = '0002';

    /**
     * Codification Numerique des Etablissments Financiers En Belgique
     */
    const CODIFICATION_NUMERIQUE_DES_ETABLISSMENTS_FINANCIERS_EN_BELGIQUE = '0003';

    /**
     * NBS/OSI NETWORK
     */
    const OSI_NETWORK = '0004';

    /**
     * NBS/OSI NETWORK
     */
    const NBS_NETWORK = '0004';

    /**
     * USA FED GOV OSI NETWORK
     */
    const USA_FED_GOV_OSI_NETWORK = '0005';

    /**
     * USA DOD OSI NETWORK
     */
    const USA_DOD_OSI_NETWORK = '0006';

    /**
     * Organisationsnummer
     */
    const ORGANISATIONSNUMMER = '0007';

    /**
     * LE NUMERO NATIONAL
     */
    const LE_NUMERO_NATIONAL = '0008';

    /**
     * SIRET-CODE
     */
    const SIRET = '0009';

    /**
     * Organizational Identifiers for Structured Names under ISO 9541 Part 2
     */
    const ORGANIZATIONAL_IDENTIFIERS_FOR_STRUCTURED_NAMES_UNDER_ISO_9541_PART_2 = '0010';

    /**
     * International Code Designator for the Identification of OSI-based, Amateur Radio
                Organizations, Network Objects and Application Services.
     */
    const OSI_BASED = '0011';

    /**
     * European Computer Manufacturers Association: ECMA
     */
    const ECMA = '0012';

    /**
     * VSA FTP CODE (FTP = File Transfer Protocol)
     */
    const VSA_FTP_CODE = '0013';

    /**
     * NIST/OSI Implememts' Workshop
     */
    const NIST_OSI_IMPLEMEMTS_WORKSHOP = '0014';

    /**
     * Electronic Data Interchange: EDI
     */
    const EDI = '0015';

    /**
     * EWOS Object Identifiers
     */
    const EWOS_OBJECT = '0016';

    /**
     * COMMON LANGUAGE
     */
    const COMMON_LANGUAGE = '0017';

    /**
     * SNA/OSI Network
     */
    const SNA_OSI_NETWORK = '0018';

    /**
     * Air Transport Industry Services Communications Network
     */
    const AIR_TRANSPORT_INDUSTRY_SERVICES_COMMUNICATIONS_NETWORK = '0019';

    /**
     * European Laboratory for Particle Physics: CERN
     */
    const CERN = '0020';

    /**
     * SOCIETY FOR WORLDWIDE INTERBANK FINANCIAL, TELECOMMUNICATION S.W.I.F.T.
     */
    const SWIFT = '0021';

    /**
     * OSF Distributed Computing Object Identification
     */
    const OSF = '0022';

    /**
     * Nordic University and Research Network: NORDUnet
     */
    const NORDUNET = '0023';

    /**
     * Digital Equipment Corporation: DEC
     */
    const DEC = '0024';

    /**
     * OSI ASIA-OCEANIA WORKSHOP
     */
    const OSI_ASIA = '0025';

    /**
     * NATO ISO 6523 ICD Encoding scheme
     */
    const NATO = '0026';

    /**
     * Aeronautical Telecommunications Network (ATN)
     */
    const ATN = '0027';

    /**
     * International Standard ISO 6523
     */
    const ISO_6523 = '0028';

    /**
     * The All-Union Classifier of Enterprises and Organisations
     */
    const ALL_UNION_CLASSIFIER_OF_ENTERPRISES_AND_ORGANISATIONS = '0029';

    /**
     * AT&T/OSI Network
     */
    const ATNT_OSI_NETWORK = '0030';

    /**
     * EDI Partner Identification Code
     */
    const EDI_PARTNER_IDENTIFICATION_CODE = '0031';

    /**
     * Telecom Australia
     */
    const TELECOM_AUSTRALIA = '0032';

    /**
     * S G W OSI Internetwork
     */
    const SGW_OSI_INTERNETWORK = '0033';

    /**
     * Reuter Open Address Standard
     */
    const REUTER_OPEN_ADDRESS_STANDARD = '0034';

    /**
     * ISO 6523 - ICD
     */
    const ISO_6523_ICD = '0035';

    /**
     * TeleTrust Object Identifiers
     */
    const TELETRUST_OBJECT = '0036';

    /**
     * LY-tunnus
     */
    const LY_TUNNUS = '0037';

    /**
     * The Australian GOSIP Network
     */
    const GOSIP_NETWORK = '0038';

    /**
     * The OZ DOD OSI Network
     */
    const OZ_DOD_OSI_NETWORK = '0039';

    /**
     * Unilever Group Companies
     */
    const UNILEVER_GROUP_COMPANIES = '0040';

    /**
     * Citicorp Global Information Network
     */
    const CITICORP_GLOBAL_INFORMATION_NETWORK = '0041';

    /**
     * DBP Telekom Object Identifiers
     */
    const DBP_TELEKOM_OBJECT = '0042';

    /**
     * HydroNETT
     */
    const HYDRONETT = '0043';

    /**
     * Thai Industrial Standards Institute (TISI)
     */
    const TISI = '0044';

    /**
     * ICI Company Identification System
     */
    const ICI = '0045';

    /**
     * FUNLOC
     */
    const FUNLOC = '0046';

    /**
     * BULL ODI/DSA/UNIX Network
     */
    const BULL_ODI_DSA_UNIX_NETWORK = '0047';

    /**
     * OSINZ
     */
    const OSINZ = '0048';

    /**
     * Auckland Area Health
     */
    const AUCKLAND_AREA_HEALTH = '0049';

    /**
     * Firmenich
     */
    const FIRMENICH = '0050';

    /**
     * AGFA-DIS
     */
    const AGFA_DIS = '0051';

    /**
     * Society of Motion Picture and Television Engineers (SMPTE)
     */
    const SMPTE = '0052';

    /**
     * Migros_Network M_NETOPZ
     */
    const M_NETOPZ = '0053';

    /**
     * ISO6523 - ICDPCR
     */
    const ISO_6523_ICDPCR = '0054';

    /**
     * Energy Net
     */
    const ENERGY_NET = '0055';

    /**
     * Nokia Object Identifiers (NOI)
     */
    const NOI = '0056';

    /**
     * Saint Gobain
     */
    const SAINT_GOBAIN = '0057';

    /**
     * Siemens Corporate Network
     */
    const SIEMENS_CORPORATE_NETWORK = '0058';

    /**
     * DANZNET
     */
    const DANZNET = '0059';

    /**
     * Data Universal Numbering System (D-U-N-S Number)
     */
    const DUNS = '0060';

    /**
     * SOFFEX OSI
     */
    const SOFFEX_OSI = '0061';

    /**
     * KPN OVN
     */
    const KPN_OVN = '0062';

    /**
     * ascomOSINet
     */
    const ASCOMOSINET = '0063';

    /**
     * UTC: Uniforme Transport Code
     */
    const UTC = '0064';

    /**
     * SOLVAY OSI CODING
     */
    const SOLVAY_OSI = '0065';

    /**
     * Roche Corporate Network
     */
    const ROCHE_CORPORATE_NETWORK = '0066';

    /**
     * ZellwegerOSINet
     */
    const ZELLWEGEROSINET = '0067';

    /**
     * Intel Corporation OSI
     */
    const INTEL_OSI = '0068';

    /**
     * SITA Object Identifier Tree
     */
    const SITA_OBJECT = '0069';

    /**
     * DaimlerChrysler Corporate Network
     */
    const DAIMLERCHRYSLER_CORPORATE_NETWORK = '0070';

    /**
     * LEGO /OSI NETWORK
     */
    const LEGO_OSI_NETWORK = '0071';

    /**
     * NAVISTAR/OSI Network
     */
    const NAVISTAR_OSI_NETWORK = '0072';

    /**
     * ICD Formatted ATM address
     */
    const ICD_ATM_ADDRESS = '0073';

    /**
     * ARINC
     */
    const ARINC = '0074';

    /**
     * Alcanet/Alcatel-Alsthom Corporate Network
     */
    const ALCANET_CORPORATE_NETWORK = '0075';

    /**
     * Sistema Italiano di Identificazione di ogetti gestito da UNINFO
     */
    const ITALIAN_OSI_UNINFO = '0076';

    /**
     * Sistema Italiano di Indirizzamento di Reti OSI Gestito da UNINFO
     */
    const ITALIAN_OSI_RETI_UNINFO = '0077';

    /**
     * Mitel terminal or switching equipment
     */
    const MITEL = '0078';

    /**
     * ATM Forum
     */
    const ATM_FORUM = '0079';

    /**
     * UK National Health Service Scheme, (EDIRA compliant)
     */
    const UK_NATIONAL_HEALTH_SERVICE = '0080';

    /**
     * International NSAP
     */
    const NSAP = '0081';

    /**
     * Norwegian Telecommunications Authority's, NTA'S, EDI, identifier scheme (EDIRA
                compliant)
     */
    const NTA_EDI = '0082';

    /**
     * Advanced Telecommunications Modules Limited, Corporate Network
     */
    const ADVANCED_TELECOMMUNICATIONS_MODULES_LIMITED = '0083';

    /**
     * Athens Chamber of Commerce & Industry Scheme (EDIRA compliant)
     */
    const ATHENS_CHAMBER_OF_COMMERCE_N_INDUSTRY_ = '0084';

    /**
     * Swiss Chambers of Commerce Scheme (EDIRA) compliant
     */
    const SWISS_CHAMBERS_OF_COMMERCE = '0085';

    /**
     * United States Council for International Business (USCIB) Scheme, (EDIRA
                compliant)
     */
    const USCIB = '0086';

    /**
     * National Federation of Chambers of Commerce & Industry of Belgium, Scheme (EDIRA
                compliant)
     */
    const NATIONAL_FEDERATION_OF_CHAMBERS_OF_COMMERCE_N_INDUSTRY_OF_BELGIUM = '0087';

    /**
     * EAN Location Code
     */
    const EAN_LOCATION = '0088';

    /**
     * The Association of British Chambers of Commerce Ltd. Scheme, (EDIRA compliant)
     */
    const ASSOCIATION_OF_BRITISH_CHAMBERS_OF_COMMERCE_LTD = '0089';

    /**
     * Internet IP addressing - ISO 6523 ICD encoding
     */
    const INTERNET_IP = '0090';

    /**
     * Cisco Systems / OSI Network
     */
    const CISCO_OSI_NETWORK = '0091';

    /**
     * Revenue Canada Business Number Registration (EDIRA compliant)
     */
    const REVENUE_CANADA_BUSINESS_NUMBER_REGISTRATION = '0093';

    /**
     * DEUTSCHER INDUSTRIE- UND HANDELSTAG (DIHT) Scheme (EDIRA compliant)
     */
    const DIHT = '0094';

    /**
     * Hewlett - Packard Company Internal AM Network
     */
    const HP_INTERNAL_AM_NETWORK = '0095';

    /**
     * DANISH CHAMBER OF COMMERCE Scheme (EDIRA compliant)
     */
    const DANISH_CHAMBER_OF_COMMERCE = '0096';

    /**
     * FTI - Ediforum Italia, (EDIRA compliant)
     */
    const FTI = '0097';

    /**
     * CHAMBER OF COMMERCE TEL AVIV-JAFFA Scheme (EDIRA compliant)
     */
    const CHAMBER_OF_COMMERCE_TEL_AVIV_JAFFA = '0098';

    /**
     * Siemens Supervisory Systems Network
     */
    const SIEMENS_SUPERVISORY_SYSTEMS_NETWORK = '0099';

    /**
     * PNG_ICD Scheme
     */
    const PNG_ICD = '0100';

    /**
     * South African Code Allocation
     */
    const SOUTH_AFRICAN_CODE_ALLOCATION = '0101';

    /**
     * HEAG
     */
    const HEAG = '0102';

    /**
     * BT - ICD Coding System
     */
    const BT_ICD = '0104';

    /**
     * Portuguese Chamber of Commerce and Industry Scheme (EDIRA compliant)
     */
    const PORTUGUESE_CHAMBER_OF_COMMERCE_AND_INDUSTRY = '0105';

    /**
     * Vereniging van Kamers van Koophandel en Fabrieken in Nederland (Association of
                Chambers of Commerce and Industry in the Netherlands), Scheme (EDIRA compliant)
     */
    const ASSOCIATION_OF_CHAMBERS_OF_COMMERCE_AND_INDUSTRY_IN_THE_NETHERLANDS = '0106';

    /**
     * Association of Swedish Chambers of Commerce and Industry Scheme (EDIRA
                compliant)
     */
    const ASSOCIATION_OF_SWEDISH_CHAMBERS_OF_COMMERCE_N_INDUSTRY_SCHEME = '0107';

    /**
     * Australian Chambers of Commerce and Industry Scheme (EDIRA compliant)
     */
    const AUSTRALIAN_CHAMBERS_OF_COMMERCE_N_INDUSTRY = '0108';

    /**
     * BellSouth ICD AESA (ATM End System Address)
     */
    const BELLSOUTH_ICD_AESA = '0109';

    /**
     * Bell Atlantic
     */
    const BELL_ATLANTIC = '0110';

    /**
     * Object Identifiers
     */
    const OBJECT_IDENTIFIERS = '0111';

    /**
     * ISO register for Standards producing Organizations
     */
    const ISO_REGISTER_FOR_STANDARDS_PRODUCING_ORGANIZATIONS = '0112';

    /**
     * OriginNet
     */
    const ORIGINNET = '0113';

    /**
     * Check Point Software Technologies
     */
    const CHECK_POINT_SOFTWARE_TECHNOLOGIES = '0114';

    /**
     * Pacific Bell Data Communications Network
     */
    const PACIFIC_BELL_DATA_COMMUNICATIONS_NETWORK = '0115';

    /**
     * PSS Object Identifiers
     */
    const PSS_OBJECT = '0116';

    /**
     * STENTOR-ICD CODING SYSTEM
     */
    const STENTOR_ICD = '0117';

    /**
     * ATM-Network ZN'96
     */
    const ATM_NETWORK_ZN96 = '0118';

    /**
     * MCI / OSI Network
     */
    const MCI_OSI_NETWORK = '0119';

    /**
     * Advantis
     */
    const ADVANTIS = '0120';

    /**
     * Affable Software Data Interchange Codes
     */
    const AFFABLE_SOFTWARE_DATA_INTERCHANGE = '0121';

    /**
     * BB-DATA GmbH
     */
    const BB_DATA = '0122';

    /**
     * BASF Company ATM-Network
     */
    const BASF_ATM_NETWORK = '0123';

    /**
     * IOTA Identifiers for Organizations for Telecommunications Addressing using the ICD
                system format defined in ISO/IEC 8348
     */
    const IOTA = '0124';

    /**
     * Henkel Corporate Network (H-Net)
     */
    const HENKEL_H_NET = '0125';

    /**
     * GTE/OSI Network
     */
    const GTE_OSI_NETWORK = '0126';

    /**
     * Dresdner Bank Corporate Network
     */
    const DRESDNER_BANK_CORPORATE_NETWORK = '0127';

    /**
     * BCNR (Swiss Clearing Bank Number)
     */
    const BCNR = '0128';

    /**
     * BPI (Swiss Business Partner Identification) code
     */
    const BPI = '0129';

    /**
     * Directorates of the European Commission
     */
    const DIRECTORATES_OF_THE_EUROPEAN_COMMISSION = '0130';

    /**
     * Code for the Identification of National Organizations
     */
    const NATIONAL_ORGANIZATIONS = '0131';

    /**
     * Certicom Object Identifiers
     */
    const CERTICOM_OBJECT = '0132';

    /**
     * TC68 OID
     */
    const TC68_OID = '0133';

    /**
     * Infonet Services Corporation
     */
    const INFONET_SERVICES_CORPORATION = '0134';

    /**
     * SIA Object Identifiers
     */
    const SIA_OBJECT = '0135';

    /**
     * Cable & Wireless Global ATM End-System Address Plan
     */
    const CABLE_N_WIRELESS_GLOBAL_ATM_END_SYSTEM_ADDRESS_PLAN = '0136';

    /**
     * Global AESA scheme
     */
    const GLOBAL_AESA = '0137';

    /**
     * France Telecom ATM End System Address Plan
     */
    const FRANCE_TELECOM_ATM_END_SYSTEM_ADDRESS_PLAN = '0138';

    /**
     * Savvis Communications AESA:.
     */
    const SAVVIS_AESA = '0139';

    /**
     * Toshiba Organizations, Partners, And Suppliers' (TOPAS) Code
     */
    const TOPAS = '0140';

    /**
     * NATO Commercial and Government Entity system
     */
    const NATO_GOV = '0141';

    /**
     * SECETI Object Identifiers
     */
    const SECETI_OBJECT = '0142';

    /**
     * EINESTEINet AG
     */
    const EINESTEINET_AG = '0143';

    /**
     * DoDAAC (Department of Defense Activity Address Code)
     */
    const DODAAC = '0144';

    /**
     * DGCP (Direction Générale de la Comptabilité Publique)administrative accounting
                identification scheme
     */
    const DGCP = '0145';

    /**
     * DGI (Direction Générale des Impots) code
     */
    const DGI = '0146';

    /**
     * Standard Company Code
     */
    const STANDARD_COMPANY = '0147';

    /**
     * ITU (International Telecommunications Union)Data Network Identification Codes
                (DNIC)
     */
    const ITU_DNIC = '0148';

    /**
     * Global Business Identifier
     */
    const GLOBAL_BUSINESS = '0149';

    /**
     * Madge Networks Ltd- ICD ATM Addressing Scheme
     */
    const MADGE_NETWORKS_LTD_ICD_ATM_ADDRESSING = '0150';

    /**
     * Australian Business Number (ABN) Scheme
     */
    const ABN = '0151';

    /**
     * Edira Scheme Identifier Code
     */
    const EDIRA = '0152';

    /**
     * Concert Global Network Services ICD AESA
     */
    const ICD_AESA = '0153';

    /**
     * Identification number of economic subjects: (ICO)
     */
    const ICO = '0154';

    /**
     * Global Crossing AESA (ATM End System Address)
     */
    const GLOBAL_CROSSING_AESA = '0155';

    /**
     * AUNA
     */
    const AUNA = '0156';

    /**
     * ATM interconnection with the Dutch KPN Telecom
     */
    const ATM_DUTCH_KPN_TELECOM = '0157';

    /**
     * Identification number of economic subject (ICO) Act on State Statistics of 29 November
                2001, § 27
     */
    const ICO_2001_11_29 = '0158';

    /**
     * ACTALIS Object Identifiers
     */
    const ACTALIS_OBJECT = '0159';

    /**
     * GTIN - Global Trade Item Number
     */
    const GTIN_GS1 = '0160';

    /**
     * GTIN - Global Trade Item Number
     */
    const GS1_GTIN = '0160';

    /**
     * ECCMA Open Technical Directory
     */
    const ECCMA_OPEN_TECHNICAL_DIRECTORY = '0161';

    /**
     * CEN/ISSS Object Identifier Scheme
     */
    const CEN_ISSS_OBJECT_IDENTIFIER = '0162';

    /**
     * US-EPA Facility Identifier
     */
    const US_EPA_FACILITY_IDENTIFIER = '0163';

    /**
     * TELUS Corporation
     */
    const TELUS_CORPORATION = '0164';

    /**
     * FIEIE Object identifiers
     */
    const FIEIE_OBJECT = '0165';

    /**
     * Swissguide Identifier Scheme
     */
    const SWISSGUIDE_IDENTIFIER = '0166';

    /**
     * Priority Telecom ATM End System Address Plan
     */
    const PRIORITY_TELECOM_ATM_END_SYSTEM_ADDRESS_PLAN = '0167';

    /**
     * Vodafone Ireland OSI Addressing
     */
    const VODAFONE_IRELAND_OSI_ADDRESSING = '0168';

    /**
     * Swiss Federal Business Identification Number. Central Business names Index (zefix)
                Identification Number
     */
    const ZEFIX = '0169';

    /**
     * Teikoku Company Code
     */
    const TEIKOKU_COMPANY = '0170';

    /**
     * Luxembourg CP & CPS (Certification Policy and Certification Practice Statement)
                Index
     */
    const LUXEMBOURG_CP_CPS = '0171';

    /**
     * Project Group “Lists of Properties” (PROLIST®)
     */
    const PROLISTA = '0172';

    /**
     * eCL@ss
     */
    const ECLASS = '0173';

    /**
     * StepNexus
     */
    const STEPNEXUS = '0174';

    /**
     * Siemens AG
     */
    const SIEMENS_AG = '0175';

    /**
     * Paradine GmbH
     */
    const PARADINE = '0176';

    /**
     * Odette International Limited
     */
    const ODETTE_INTERNATIONAL_LIMITED = '0177';

    /**
     * Route1 MobiNET
     */
    const ROUTE1_MOBINET = '0178';

    /**
     * Penango Object Identifiers
     */
    const PENANGO_OBJECT = '0179';

    /**
     * Lithuanian military PKI
     */
    const LITHUANIAN_MILITARY_PKI = '0180';

    /**
     * Numéro d'identification suisse des enterprises (IDE), Swiss Unique Business
                Identification Number (UIDB)
     */
    const UIDB = '0183';

    /**
     * DIGSTORG
     */
    const DIGSTORG = '0184';

    /**
     * Perceval Object Code
     */
    const PERCEVAL_OBJECT = '0185';

    /**
     * TrustPoint Object Identifiers
     */
    const TRUSTPOINT_OBJECT = '0186';

    /**
     * Amazon Unique Identification Scheme
     */
    const AMAZON_UNIQUE_IDENTIFICATION = '0187';

    /**
     * Corporate Number of The Social Security and Tax Number System
     */
    const CORPORATE_NUMBER_OF_THE_SOCIAL_SECURITY_AND_TAX_NUMBER_SYSTEM = '0188';

    /**
     * European Business Identifier (EBID)
     */
    const EBID = '0189';

    /**
     * Organisatie Indentificatie Nummer (OIN)
     */
    const OIN = '0190';

    /**
     * Company Code (Estonia)
     */
    const COMPANY_CODE_ESTONIA = '0191';

    /**
     * Organisasjonsnummer
     */
    const ORGANISASJONSNUMMER = '0192';

    /**
     * UBL.BE Party Identifier
     */
    const UBL_BE = '0193';

    /**
     * Singapore Nationwide E-Invoice Framework
     */
    const SINGAPORE_NATIONWIDE_EINVOICE_FRAMEWORK = '0195';

    /**
     * Icelandic identifier - Íslensk kennitala
     */
    const ICELANDIC_IDENTIFIER = '0196';


}
