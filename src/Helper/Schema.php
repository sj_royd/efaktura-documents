<?php

namespace SJRoyd\PEF\BIS\Helper;

class Schema
{
    const CAC    = '{urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2}';
    const CBC    = '{urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2}';
    const CAC_PL = '{urn:pl:extended:CommonAggregateComponents-2}';
    const CBC_PL = '{urn:pl:extended:CommonBasicComponents-2}';
    const EXT    = '{urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2}';
    const CCTS   = '{urn:un:unece:uncefact:documentation:2}';
    const QDT    = '{urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2}';
    const UDT    = '{urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2}';
    const XS     = '{http://www.w3.org/2001/XMLSchema}';

    public static function getNS($ns)
    {
        return preg_replace('~\{(.*)\}~', '$1', $ns);
    }
}
