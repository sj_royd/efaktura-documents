<?php

namespace SJRoyd\PEF\BIS\Helper;

interface Callback
{
    public function callback($item);
}
