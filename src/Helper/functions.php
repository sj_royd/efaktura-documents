<?php

namespace SJRoyd\PEF\BIS\Helper;

use SJRoyd\PEF\BIS\Document\Document;

/**
 *
 * @param type $array
 * @return type
 */
function cleanArray($array)
{
    foreach($array as $k => $v) {
        if(is_null($v)){
            unset($array[$k]);
        }
    }
    return $array;
}

/**
 *
 * @param type $date
 * @param type $format
 * @return type
 */
function dateFormat($date, $format='Y-m-d')
{
    return $date ? $date->format($format) : null;
}

function date($date)
{
    return ($date instanceof \DateTime || !$date) ? $date : new \DateTime($date);
}

/**
 *
 * @param float $value
 * @return string
 */
function amountFormat($value)
{
    $rounding = Document::$currencyRounding;
    return sprintf("%.{$rounding}f", $value);
}