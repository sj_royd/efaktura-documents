<?php

namespace SJRoyd\PEF\BIS\Helper;

trait Getter
{

    /**
     * @var \ReflectionClass
     */
    private $rc;

    public function __get($name)
    {
        return $this->isPropertyAccessible($name) ? $this->$name : null;
    }

    public function __isset($name)
    {
        return $this->isPropertyAccessible($name);
    }

    /**
     *
     * @param string $name
     * @return \ReflectionProperty
     */
    private function isPropertyAccessible($name)
    {
        $this->rc || $this->rc = new \ReflectionClass($this);
        $prop = $this->rc->getProperty($name);
        return $prop->isPublic() || $prop->isProtected();
    }
}
