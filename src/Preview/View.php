<?php

namespace SJRoyd\PEF\BIS\Preview;

class View
{
    /**
     * @var \Twig_Environment
     */
    private static $twig;

    /**
     *
     */
    private static function setTwig()
    {
        $loader = new \Twig\Loader\FilesystemLoader(__DIR__.'/templates');
        self::$twig = new \Twig\Environment($loader, [
            'auto_reload' => true,
            'debug' => true
        ]);
        self::$twig->addExtension(new \Twig\Extension\DebugExtension());
        $core = self::$twig->getExtension(\Twig\Extension\CoreExtension::class);
        $core->setNumberFormat(2, ',', ' ');
        $core->setDateFormat(\SJRoyd\PEF\BIS\Document\Document::$previewDateFormat);
        self::filters();
    }

    /**
     *
     * @param string $tplName
     * @param array $data
     * @return string
     */
    public static function render($tplName, $data)
    {
        !self::$twig && self::setTwig();

        return self::$twig->render("{$tplName}.twig", $data);
    }

    /**
     *
     */
    private static function filters()
    {
        $list = [
            new \Twig\TwigFilter('colsum', function($array, $column)
            {
                $result = 0;
                foreach($array as $obj) {
                    $result += $obj->$column;
                }
                return $result;
            }),
            new \Twig\TwigFilter('format_bytes', function($bytes, $precision = 2)
            {
                $units = array('B', 'kB', 'MB', 'GB', 'TB');
                $bytes = max($bytes, 0);
                $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
                $pow = min($pow, count($units) - 1);

                $bytes /= pow(1024, $pow);

                return round($bytes, $precision) . ' ' . $units[$pow];
            }),
        ];

        foreach($list as $filter){
            self::$twig->addFilter($filter);
        }
    }
}
