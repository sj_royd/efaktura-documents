<?php

namespace SJRoyd\PEF\BIS\Exception;

class EmptyDocumentException extends \Exception
{
    public function __construct($docName = "", $code = 0, \Throwable $previous = NULL)
    {
        $message = "{$docName} XML data is empty";
        parent::__construct($message, $code, $previous);
    }

}
