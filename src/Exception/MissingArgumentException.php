<?php

namespace SJRoyd\PEF\BIS\Exception;

class MissingArgumentException extends \Exception
{
    public function __construct($argName = "", $code = 0, \Throwable $previous = NULL)
    {
        $message = "Missing agrument {$argName}";
        parent::__construct($message, $code, $previous);
    }

}
