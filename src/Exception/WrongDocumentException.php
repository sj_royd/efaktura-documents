<?php

namespace SJRoyd\PEF\BIS\Exception;

class WrongDocumentException extends \Exception
{
    public function __construct($docName = "", $code = 0, \Throwable $previous = NULL)
    {
        $message = "XML data is not a {$docName} document";
        parent::__construct($message, $code, $previous);
    }

}
