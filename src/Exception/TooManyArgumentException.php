<?php

namespace SJRoyd\PEF\BIS\Exception;

class TooManyArgumentException extends \Exception
{
    public function __construct($argName, $count, \Throwable $previous = NULL)
    {
        $message = "Too many arguments {$argName}, max {$count} expected";
        parent::__construct($message, 0, $previous);
    }

}
