<?php

namespace SJRoyd\PEF\BIS\Exception;

class WrongArgumentException extends \Exception
{
    public function __construct($argName = "", $code = 0, \Throwable $previous = NULL)
    {
        $message = "Agrument {$argName} is wrong";
        parent::__construct($message, $code, $previous);
    }

}
