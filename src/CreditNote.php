<?php

namespace SJRoyd\PEF\BIS;

use Sabre\Xml\Reader;
use Sabre\Xml\Service;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Document as Doc;
use SJRoyd\PEF\BIS\Document\CreditNoteSchema as S;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * @property string $id numer faktury / invoice number
 * @property \DateTime $issueDate data wystawienia faktury / Invoice issue date
 * @property string $note uwagi tekstowe do faktury / Invoice note
 * @property string $accountingCost znak stanowiska kosztów u nabywcy / Buyer accounting reference
 * @property Doc\Reference\ContractorDocument $contractReference referencje do kontraktu
 * / contract reference
 * @property Doc\Party\AccountingSupplier $accountingSupplier dane sprzedawcy / supplier data
 * @property Doc\Party\AccountingCustomer $accountingCustomer dane klienta / customer data
 * @property Doc\MonetaryTotal $total Podsumowanie dla faktury / Invoice document totals
 * @property Doc\CreditNoteLine $lines
 */
class CreditNote extends Doc\Document implements XmlSerializable, XmlDeserializable, Helper\Callback
{
    /**
     * wersja komunikatu / message version
     * @var string
     */
    protected $customizationID = 'urn:fdc:www.efaktura.gov.pl:ver1.0:trns:account_corr:ver1.0';

    /**
     * identyfikator procesu biznesowego / business process identifier
     * @var string
     */
    protected $profileID = 'urn:fdc:www.efaktura.gov.pl:ver1.0:account_corr:ver1.0';

    /**
     *
     * @var CreditNoteLine[]
     */
    protected $lines = [];

    /**
     *
     * @param string $id                  Invoice number
     * @param \DateTime|string $issueDate Invoice issue date
     * @param string $currency            Currency code
     */
    public function __construct($id, $issueDate, $currency = PEPPOL\Currency::PLN)
    {
        $this->id = $id;
        $this->issueDate = Helper\date($issueDate);
        $this->currency = $currency;
        self::$currencyCode = $currency;
        Helper\CodeList::validate(
            Helper\CodeList::getCurrency($currency), $currency
        );
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @param string $accountingCost
     * @return $this
     */
    public function setAccountingCost($accountingCost)
    {
        $this->accountingCost = $accountingCost;
        return $this;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setContractReference($id)
    {
        $this->contractReference = new Doc\Reference\ContractorDocument($id);
        return $this;
    }

    /**
     * @param string $name
     * @return Doc\Party\AccountingSupplier
     */
    public function supplier($name)
    {
        $this->accountingSupplier = new Doc\Party\AccountingSupplier($name);
        return $this->accountingSupplier;
    }

    /**
     * @param string $name
     * @return Doc\Party\AccountingCustomer
     */
    public function customer($name)
    {
        $this->accountingCustomer = new Doc\Party\AccountingCustomer($name);
        return $this->accountingCustomer;
    }

    /**
     *
     * @return Doc\MonetaryTotal
     */
    public function total()
    {
        !$this->total && $this->total = new Doc\MonetaryTotal();
        $this->total->setCallback($this);
        return $this->total;
    }


    /**
     *
     * @param string $note
     * @param float $amount
     * @return Doc\CreditNoteLine
     */
    public function line($note, $amount)
    {
        static $id = 0;
        $id++;
        $line = new Doc\CreditNoteLine($id, $note, $amount);
        $this->lines[] = $line;
        $this->callback();
        return $line;
    }

    public function callback($item = null)
    {
        $this->total()->calculateAmounts($this->lines);
    }


    /**
     * The xmlSerialize method is called during xml writing.
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer, $cac = S::CAC, $cbc = S::CBC)
    {
//        $this->validate();
        $data = [
            $cbc.'CustomizationID'      => $this->customizationID,
            $cbc.'ProfileID'            => $this->profileID,
            $cbc.'ID'                   => $this->id,
            $cbc.'IssueDate'            => Helper\dateFormat($this->issueDate),
            $cbc.'Note'                 => $this->note,
            $cbc.'DocumentCurrencyCode' => self::$currencyCode,
            $cbc.'AccountingCost'       => $this->accountingCost,

            $cac.'ContractDocumentReference'   => $this->contractReference,
        ];
        $data += [
            $cac.'AccountingSupplierParty' => [
                $cac.'Party' => $this->accountingSupplier
            ],
            $cac.'AccountingCustomerParty' => [
                $cac.'Party' => $this->accountingCustomer
            ],
        ];
        $data[$cac.'LegalMonetaryTotal'] = $this->total;
        foreach($this->lines as $line){
            $data[] = [
                'name' => $cac.'CreditNoteLine',
                'value' => $line
            ];
        }

        $writer->write(Helper\cleanArray($data));
    }

    /**
     * The deserialize method is called during xml parsing.
     * @return mixed
     */
    public static function xmlDeserialize(Reader $reader, $cac = S::CAC, $cbc = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($cbc.'ID'),
                D::get($cbc.'IssueDate', '\DateTime'),
                D::get($cbc.'DocumentCurrencyCode')
            );
        $object->parse           = true;
        $object->taxCurrency =
        self::$taxCurrencyCode   = D::get($cbc.'TaxCurrencyCode');
        $object->note            = D::get($cbc.'Note');
        $object->accountingCost  = D::get($cbc.'AccountingCost');

        $object->contractReference  = D::get($cac.'ContractDocumentReference');

        $object->accountingSupplier = D::get($cac.'AccountingSupplierParty');
        $object->accountingCustomer = D::get($cac.'AccountingCustomerParty');

        $object->total = D::get($cac.'LegalMonetaryTotal');

        $object->lines = D::getArray($cac.'CreditNoteLine');

        return $object;
    }

    /**
     * Generate Invoice object to HTML
     * @return string
     */
    public function html()
    {
//        $this->validate();
        return Preview\View::render('credit_note', [
            'doc' => $this,
            'code' => new Helper\CodeList()
        ]);
    }


    /**
     * Generate Invoice object to XML
     * @return string
     */
    public function xml()
    {
        $xmlService = new Service();
        $xmlService->namespaceMap = [
            S::getNS(S::NS)   => '',
            S::getNS(S::CBC)  => 'cbc',
            S::getNS(S::CAC)  => 'cac',
            S::getNS(S::CCTS) => 'ccts',
            S::getNS(S::QDT)  => 'qdt',
            S::getNS(S::UDT)  => 'udt',
            S::getNS(S::XS)   => 'xs',
        ];
        return $xmlService->write('CreditNote', [
            $this
        ]);
    }

    /**
     * Parse XML to Invoice object
     * @param string $xml
     * @return Invoice
     */
    public static function parse($xml)
    {
        if(!$xml){
            throw new Exception\EmptyDocumentException('Invoice Correction');
        }
        $service = new Service();
        $service->elementMap = [
            S::NS. 'CreditNote'    => CreditNote::class,

            S::CAC.'ContractDocumentReference'   => Doc\Reference\ContractorDocument::class,

            S::CAC.'AccountingSupplierParty' => Doc\Party\Billing\AccountingSupplier::class,
            S::CAC.'AccountingCustomerParty' => Doc\Party\Billing\AccountingCustomer::class,

            S::CAC.'PostalAddress'    => Doc\Party\Address::class,
            S::CAC.'PartyTaxScheme'   => Doc\Party\TaxScheme::class,
            S::CAC.'PartyLegalEntity' => Doc\Party\LegalEntity::class,
            S::CAC.'Contact'          => Doc\Party\Contact::class,
            S::CAC.'Address'          => Doc\Party\Address::class,

            S::CAC.'LegalMonetaryTotal'     => Doc\MonetaryTotal::class,

            S::CAC.'CreditNoteLine'         => Doc\CreditNoteLine::class,
        ];

        $object = $service->parse($xml);
        if($object instanceof CreditNote){
            return $object;
        }

        throw new Exception\WrongDocumentException('Credit Note');
    }

}
