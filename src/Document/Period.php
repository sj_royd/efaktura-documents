<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;


/**
 * @property \DateTime $startDate
 * @property \DateTime $endDate
 * @property int $descriptionCode
 */
class Period implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * data początku okresu fakturowania / Invoicing period start date
     * @var \DateTime
     */
    protected $startDate;

    /**
     * data końca okresu fakturowania / Invoicing period end date
     * @var \DateTime
     */
    protected $endDate;

    /**
     * kod daty naliczania podatku (wg UNTDID 2005) / tax date type code (UNTDID 2005)
     * https://www.unece.org/trade/untdid/d00a/tred/tred2005.htm
     * @var int
     */
    protected $descriptionCode;

    public function __construct($startDate, $endDate)
    {
        $this->startDate = $startDate ? Helper\date($startDate) : null;
        $this->endDate = $endDate ? Helper\date($endDate) : null;
    }

    /**
     * kod daty naliczania podatku (wg UNTDID 2005) / tax date type code (UNTDID 2005)
     * https://www.unece.org/trade/untdid/d00a/tred/tred2005.htm
     * @param string $descriptionCode
     * @return $this
     */
    public function setDescriptionCode($descriptionCode)
    {
        $this->descriptionCode = $descriptionCode;
        Helper\CodeList::validate(
            Helper\CodeList::getVatDate($descriptionCode), $descriptionCode
        );
        return $this;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $data = [
            $b.'StartDate'       => Helper\dateFormat($this->startDate),
            $b.'EndDate'         => Helper\dateFormat($this->endDate),
            $b.'DescriptionCode' => $this->descriptionCode,
        ];
        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($b.'StartDate', '\DateTime'),
                D::get($b.'EndDate', '\DateTime')
            );
        $object->descriptionCode = D::get($b.'DescriptionCode');

        return $object;
    }

}
