<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * podsumowanie podatku / tax totals
 * @property float $amount waluta podatku i podatek ogółem / tax currency and tax tax total
 * @property string $currency
 * @property TaxSubtotal[] $subtotal dane dla grupy opodatkowania / tax subtotals
 */
class TaxTotal implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * waluta podatku i podatek ogółem / tax currency and tax tax total
     * @var float
     */
    protected $amount;

    /**
     *
     * @var string
     */
    protected $currency;

    /**
     * dane dla grupy opodatkowania / tax subtotals
     * @var TaxSubtotal[]
     */
    protected $subtotal = [];

    private $withSubtotal = true;

    /**
     * @param string $currency
     */
    public function __construct($currency, $withSubtotal = true)
    {
        $this->currency = $currency;
        $this->withSubtotal = $withSubtotal;
    }

    /**
     *
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     *
     * @param array $lines
     * @param array $allowancesCharges
     */
    public function calculateAmounts($lines = [], $allowancesCharges = [])
    {
        $this->amount = 0;
        $this->subtotal = [];

        /** @var InvoiceLine $line */
        foreach($lines as $line) {
            $this->amount += $line->taxAmount;

            if($this->withSubtotal){
                $tc = $line->item->taxCategory;
                $index = $tc->getIndex();
                if(!array_key_exists($index, $this->subtotal)){
                    $this->subtotal[$index] = new TaxSubtotal($tc->percent, $tc->id);
                    $this->subtotal[$index]->setCurrency(Document::$currencyCode);
                }
                $this->subtotal[$index]->addAmounts($line->netAmount, $line->taxAmount);
            }
        }

        /** @var AllowanceCharge $ac */
        foreach($allowancesCharges as $ac) {

            if($this->withSubtotal){
                $tc = $ac->taxCategory;
                $index = $tc->getIndex();
                if(!array_key_exists($index, $this->subtotal)){
                    $this->subtotal[$index] = new TaxSubtotal($tc->percent, $tc->id);
                    $this->subtotal[$index]->setCurrency(Document::$currencyCode);
                }
            }

            if($ac->taxCategory){

            $taxAmount = round($ac->amount * ($ac->taxCategory->percent / 100), Document::$currencyRounding);
                if($ac->isCharge()) {
                    $this->amount += $taxAmount;
                    $this->withSubtotal
                        && $this->subtotal[$index]->addAmounts($ac->amount, $taxAmount);
                } elseif($ac->isAllowance()) {
                    $this->amount -= $taxAmount;
                    $this->withSubtotal
                        && $this->subtotal[$index]->addAmounts(-$ac->amount, -$taxAmount);
                }
            }
        }

        ksort($this->subtotal);
    }

    protected function validate()
    {
        if(is_nan($this->amount)){
            throw new Ex\MissingArgumentException('Tax Total Amount');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $writer->write([
            [
                'name'       => $b.'TaxAmount',
                'value'      => Helper\amountFormat($this->amount),
                'attributes' => [
                    'currencyID' => $this->currency
                ]
            ]
        ]);
        foreach($this->subtotal as $subtotal){
            $writer->write([
                $a.'TaxSubtotal' => $subtotal
            ]);
        }
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($b.'TaxAmount/@currencyID')
            );
        $object->amount   = D::cast(D::get($b.'TaxAmount'), D::CAST_FLOAT);
        $object->subtotal = D::getArray($a.'TaxSubtotal');

        return $object;
    }

}
