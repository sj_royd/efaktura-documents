<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property int $id numer pozycji faktury / Line (position) number
 * @property string $note dodatkowa informacja lub uwaga / Invoice line note
 * @property float $quantity ilość fakturowana / Invoiced quantity
 * @property string $quantityUnitCode
 * @property float $netAmount wartość netto pozycji / Invoice line net amount
 * @property float $taxAmount wartość podatku pozycji / Invoice line tax amount
 * @property float $grossAmount wartość brutto pozycji / Invoice line gross amount
 * @property float $accountingCost pozycja kosztów u nabywcy
 * / Invoice line Buyer accounting reference
 * @property Period $period okres fakturowany
 * @property int $orderLineReference numer odnośnej pozycji zamówienia
 * / Referenced purchase order line reference
 * @property Reference\Additional $documentReference
 * Referencja do dokumentu powiązanego / Invoice line referenced object identifier
 * @property AllowanceCharge[] $allowancesCharges obciążenia lub upusty / charges or allowances
 * @property Line\InvoiceItem $item
 * @property Line\Price $price
 */
class InvoiceLine implements XmlSerializable, XmlDeserializable, Helper\Callback
{
    use Getter;

    /**
     * numer pozycji faktury / Line (position) number
     * @var int
     */
    protected $id;

    /**
     * dodatkowa informacja lub uwaga / Invoice line note
     * @var string
     */
    protected $note;

    /**
     * ilość fakturowana / Invoiced quantity
     * @var float
     */
    protected $quantity;

    /**
     *
     * @var string
     */
    protected $quantityUnitCode;

    /**
     * wartość netto pozycji / Invoice line net amount
     * @var float
     */
    protected $netAmount;

    /**
     * wartość podatku pozycji / Invoice line tax amount
     * @var float
     */
    protected $taxAmount;

    /**
     * wartość brutto pozycji / Invoice line gross amount
     * @var float
     */
    protected $grossAmount;

    /**
     * pozycja kosztów u nabywcy / Invoice line Buyer accounting reference
     * @var float
     */
    protected $accountingCost;

    /**
     * okres fakturowany
     * @var Period
     */
    protected $period;

    /**
     * numer odnośnej pozycji zamówienia / Referenced purchase order line reference
     * @var int
     */
    protected $orderLineReference;

    /**
     * Referencja do dokumentu powiązanego / Invoice line referenced object identifier
     * @var Reference\Additional
     */
    protected $documentReference;//

    /**
     * obciążenia lub upusty / charges or allowances
     * @var AllowanceCharge[]
     */
    protected $allowancesCharges = [];

    /**
     *
     * @var Line\InvoiceItem
     */
    protected $item;

    /**
     *
     * @var Line\Price
     */
    protected $price;

    private $callbackObj;

    /**
     *
     * @param int $id numer pozycji faktury / Line (position) number
     * @param float $quantity ilość fakturowana / Invoiced quantity
     * @param string $unitCode UN/ECE_20 lub UN/ECE_21
     */
    public function __construct($id, $quantity, $unitCode)
    {
        $this->id = $id;
        $this->quantity = $quantity;
        $this->quantityUnitCode = $unitCode;
        Helper\CodeList::validate(
            Helper\CodeList::getUnitCode($unitCode), $unitCode
        );
    }

    /**
     * dodatkowa informacja lub uwaga / Invoice line note
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    /**
     * wartość netto pozycji / Invoice line net amount
     * @param float $amount
     * @return $this
     */
    public function setNetAmount($amount)
    {
        $this->netAmount = $amount;
        return $this;
    }

    /**
     * pozycja kosztów u nabywcy / Invoice line Buyer accounting reference
     * @param string $accountingCost
     * @return $this
     */
    public function setAccountingCost($accountingCost)
    {
        $this->accountingCost = $accountingCost;
        return $this;
    }

    /**
     * okres fakturowany
     * @param \DateTime|string $startDate
     * @param \DateTime|string $endDate
     * @return $this
     */
    public function setPeriod($startDate, $endDate)
    {
        $this->period = new Period($startDate, $endDate);
        return $this;
    }

    /**
     * numer odnośnej pozycji zamówienia / Referenced purchase order line reference
     * @param int $orderLineReference
     * @return $this
     */
    public function setOrderLineReference($orderLineReference)
    {
        $this->orderLineReference = $orderLineReference;
        return $this;
    }

    /**
     * Referencja do dokumentu powiązanego / Invoice line referenced object identifier
     * @param string $id
     * @param string $schemeId
     * @param bool $isInvoice
     * @return $this
     */
    public function setDocumentReference($id, $schemeId, $isInvoice = true)
    {
        !$this->documentReference
                && $this->documentReference = new Reference\Additional($id, $schemeId);
        $isInvoice && $this->documentReference->setTypeCode(130);
        return $this;
    }

    /**
     * obciążenia lub upusty / charges or allowances
     * @param float $amount kwota obciążenia/upustu / charge amount
     * @param int $percent procent opodatkowania / tax percentage
     * @param string $taxCategoryId kod kategorii opodatkowania obciążenia / charge tax category code
     * @return AllowanceCharge
     */
    public function addAllowanceCharge($amount, $percent = null, $taxCategoryId = null)
    {
        if(!$this->price){
            throw new \InvalidArgumentException('Price not set');
        }
        $allowanceCharge = new AllowanceCharge($amount, $percent, $taxCategoryId);
        $allowanceCharge->setCallback($this);
        $this->allowancesCharges[] = $allowanceCharge;

        return $allowanceCharge;
    }

    /**
     *
     * @param string $name
     * @param int $taxPercent
     * @param string $taxCode
     * @return Line\Item
     */
    public function item($name, $taxPercent, $taxCode = Helper\PEPPOL\TaxCategory::STANDARD)
    {
        if(!$this->item){
            $this->item = new Line\InvoiceItem($name);
            $this->item->taxCategory($taxCode, $taxPercent);
        }
        $this->price && self::calculateAmounts($this);
        return $this->item;
    }

    /**
     *
     * @return Line\Price
     */
    public function price($unitPriceAmount)
    {
        !$this->price && $this->price
                = new Line\Price($unitPriceAmount, 1, $this->quantityUnitCode);

        $this->price->setCallback($this);

        $this->netAmount = $unitPriceAmount * $this->quantity;

        $this->item && self::calculateAmounts($this);
        return $this->price;
    }

    /**
     *
     * @param float $item
     */
    public function callback($item)
    {
        $amount = ($this->price->amount / $this->price->quantity) * $this->quantity;

        foreach($this->allowancesCharges as $ac){
            if($ac->isAllowance()){
                $amount -= $ac->amount;
            } else {
                $amount += $ac->amount;
            }
        }

        $this->netAmount = $amount;
        $this->item && self::calculateAmounts($this);
        $this->callbackObj->callback();
    }

    /**
     *
     * @param Helper\Callback $object
     */
    public function setCallback($object)
    {
        $this->callbackObj = $object;
    }

    /**
     * @param InvoiceLine $object
     */
    protected static function calculateAmounts($object)
    {
        $object->grossAmount = round(
                $object->netAmount * (1 + ($object->item->taxCategory->percent / 100)),
                Document::$currencyRounding
            );
        $object->taxAmount = $object->grossAmount - $object->netAmount;
    }

    protected function validate()
    {
        if(!is_numeric($this->id)){
            throw new Ex\WrongArgumentException('ID');
        }
        if(!is_numeric($this->quantity)){
            throw new Ex\WrongArgumentException('Invoiced Quantity');
        }
        if(!is_numeric($this->netAmount)){
            throw new Ex\WrongArgumentException('Line Extension Amount');
        }
        if(!$this->item){
            throw new Ex\MissingArgumentException('Item');
        }
        if(!$this->price){
            throw new Ex\MissingArgumentException('Price');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $b.'ID' => $this->id,
            $b.'Note' => $this->note,
            [
                'name' => $b.'InvoicedQuantity',
                'value' => $this->quantity,
                'attributes' => [
                    'unitCode' => $this->quantityUnitCode
                ]
            ],
            [
                'name' => $b.'LineExtensionAmount',
                'value' => Helper\amountFormat($this->netAmount),
                'attributes' => [
                    'currencyID' => Document::$currencyCode
                ]
            ],
            $b.'AccountingCost' => $this->accountingCost,
            $a.'InvoicePeriod' => $this->period,
            $a.'OrderLineReference' => $this->orderLineReference ? [
                $b.'LineID' => $this->orderLineReference
            ] : null,
            $a.'DocumentReference' => $this->documentReference,
        ];
        foreach($this->allowancesCharges as $ac){
            $data[] = [
                'name' => $a.'AllowanceCharge',
                'value' => $ac
            ];
        }
        $data[$a.'Item'] = $this->item;
        $data[$a.'Price'] = $this->price;

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::cast(D::get($b.'ID'), D::CAST_INT),
                D::cast(D::get($b.'InvoicedQuantity'), D::CAST_FLOAT),
                D::get($b.'InvoicedQuantity/@unitCode')
            );
        $object->note               = D::get($b.'Note');
        $object->accountingCost     = D::get($b.'AccountingCost');
        $object->netAmount          = D::cast(D::get($b.'LineExtensionAmount'), D::CAST_FLOAT);
        $object->period             = D::get($a.'InvoicePeriod');
        $object->orderLineReference = D::cast(D::get($a.'OrderLineReference/'.$b.'LineID'), D::CAST_INT);
        $object->documentReference  = D::get($a.'DocumentReference');
        $object->allowancesCharges  = D::getArray($a.'AllowanceCharge');
        $object->item               = D::get($a.'Item');
        $object->price              = D::get($a.'Price');

        self::calculateAmounts($object);

        return $object;
    }

}
