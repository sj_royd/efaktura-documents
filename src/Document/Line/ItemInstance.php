<?php

namespace SJRoyd\PEF\BIS\Document\Line;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $name
 * @property string $nameCode
 */
class ItemInstance implements XmlSerializable, XmlDeserializable
{
    use Getter;

    protected $serialId;

    protected $lotNumberId;


    public function setSerialId($serialId)
    {
        $this->serialId = $serialId;
        return $this;
    }

    public function setLotNumberId($lotNumberId)
    {
        $this->lotNumberId = $lotNumberId;
        return $this;
    }


    protected function validate()
    {
        if(!$this->serialId || $this->lotNumberId){
            throw new Ex\MissingArgumentException('Name or LotNumberID');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $b.'SerialID' => $this->serialId,
            $this->lotNumberId ? [
                'name' => $b.'LotIdentification',
                'value' => [
                    'name' => 'LotNumberID',
                    'value' => $this->lotNumberId
                ],
            ] : null,
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self();
        $object->serialId    = D::get($b.'SerialID');
        $object->lotNumberId = D::get($a.'LotIdentification/'.$b.'LotNumberID');
        return $object;
    }
}
