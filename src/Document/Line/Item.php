<?php

namespace SJRoyd\PEF\BIS\Document\Line;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Helper\Getter;

/**
 * @property string $description opis pozycji / Item description
 * @property string $name nazwa pozycji / Item name
 * @property string $buyerIdentification identyfikator wg kupującego / Item Buyer's identifier
 * @property string $sellerIdentification identyfikator wg sprzedającego / Item Seller's identifier
 * @property string $standardIdentification identyfikator standardowy np kod EAN
 * / Item standard identifier ex. EAN Code
 * @property string $standardIdentificationSchemeId 0160 - GS1 GTIN (ICD)
 * @property string $originCountry kod kraju pochodzenia pozycji (ISO 3166)
 * / Item country of origin code (ISO 3166)
 * @property array $classificationCode kod systemu klasyfikacji oraz kod klasyfikacji pozycji
     * / Item classification system code and item classification code (e.g. UNSPSC)
 * @property array $classificationCodeListId UNCL7143
 * @property array $classificationCodeListVersion
 * @property Document\TaxCategory $taxCategory
 * @property ItemProperty[] $property Dodatkowe parametry pozycji / Additional item attributes
 */
abstract class Item implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * opis pozycji / Item description
     * @var string
     */
    protected $description;

    /**
     * nazwa pozycji / Item name
     * @var string
     */
    protected $name;

    /**
     * identyfikator wg kupującego / Item Buyer's identifier
     * @var string
     */
    protected $buyerIdentification;

    /**
     * identyfikator wg sprzedającego / Item Seller's identifier
     * @var string
     */
    protected $sellerIdentification;

    /**
     * identyfikator standardowy np kod EAN / Item standard identifier ex. EAN Code
     * @var string
     */
    protected $standardIdentification;

    /**
     * 0160 - GS1 GTIN (ICD)
     * @var string
     */
    protected $standardIdentificationSchemeId;

    /**
     * kod kraju pochodzenia pozycji (ISO 3166) / Item country of origin code (ISO 3166)
     * @var string
     */
    protected $originCountry;

    /**
     * kod systemu klasyfikacji oraz kod klasyfikacji pozycji
     * / Item classification system code and item classification code (e.g. UNSPSC)
     * @var array
     */
    protected $classificationCode = [];

    /**
     * UNCL7143
     * @var array
     */
    protected $classificationCodeListId = [];

    /**
     *
     * @var array
     */
    protected $classificationCodeListVersion = [];

    /**
     *
     * @var Document\TaxCategory
     */
    protected $taxCategory;

    /**
     * Dodatkowe parametry pozycji / Additional item attributes
     * @var ItemProperty
     */
    protected $property = [];

    /**
     *
     * @var type
     */
    protected $specificationReference = [];

    /**
     *
     * @var ItemInstance[]
     */
    protected $instance = [];

    /**
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    protected abstract function validate();

    public abstract function xmlSerialize(Writer $writer);

    public abstract static function xmlDeserialize(Reader $reader);
}
