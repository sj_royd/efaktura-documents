<?php

namespace SJRoyd\PEF\BIS\Document\Line;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $name
 * @property string $nameCode
 * @property string $nameCodeListId
 * @property string $value
 * @property string $quantity
 * @property string $quantityUnitCode
 * @property string $qualifier
 */
class ItemProperty implements XmlSerializable, XmlDeserializable
{
    use Getter;

    protected $name;

    protected $nameCode;

    protected $nameCodeListId;

    protected $value;

    protected $quantity;

    protected $quantityUnitCode;

    protected $qualifier;

    /**
     *
     * @param string $name
     * @param string $value
     */
    public function __construct($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     *
     * @param string $nameCode
     * @param string $listId
     * @return $this
     */
    public function setNameCode($nameCode, $listId)
    {
        $this->nameCode = $nameCode;
        $this->nameCodeListId = $listId;
        return $this;
    }

    /**
     *
     * @param float $quantity
     * @param string $unitCode
     * @return $this
     */
    public function setQuantity($quantity, $unitCode)
    {
        $this->quantity = $quantity;
        $this->quantityUnitCode = $unitCode;
        Helper\CodeList::validate(
            Helper\CodeList::getUnitCode($unitCode), $unitCode
        );
        return $this;
    }

    /**
     *
     * @param string $qualifier
     * @return $this
     */
    public function setQualifier($qualifier)
    {
        $this->qualifier = $qualifier;
        return $this;
    }

    protected function validate()
    {
        if(!$this->name){
            throw new Ex\MissingArgumentException('Name');
        }
        if(!$this->value){
            throw new Ex\MissingArgumentException('Value');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $b.'Name' => $this->name,
            $this->nameCode ? [
                'name' => $b.'NameCode',
                'value' => $this->nameCode,
                'attributes' => [
                    'listID' => $this->nameCodeListId
                ]
            ] : null,
            $b.'Value' => $this->value,
            $this->quantity ? [
                'name' => $b.'ValueQuantity',
                'value' => $this->quantity,
                'attributes' => [
                    'unitCode' => $this->quantityUnitCode
                ]
            ] : null,
            $b.'ValueQualifier' => $this->qualifier
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($b.'Name'),
                D::get($b.'Value')
            );
        $object->nameCode           = D::get($b.'NameCode');
        $object->nameCodeListId     = D::get($b.'NameCode/@listID');
        $object->quantity           = D::get($b.'ValueQuantity');
        $object->quantityUnitCode   = D::get($b.'ValueQuantity/@unitCode');
        $object->qualifier          = D::get($b.'ValueQualifier');
        return $object;
    }
}
