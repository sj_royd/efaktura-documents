<?php

namespace SJRoyd\PEF\BIS\Document\Line;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Document\Line\Item as I;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $name nazwa pozycji / Item name
 * @property string $description opis pozycji / Item description
 * @property string $buyerIdentification identyfikator wg kupującego / Item Buyer's identifier
 * @property string $sellerIdentification identyfikator wg sprzedającego / Item Seller's identifier
 * @property string $standardIdentification identyfikator standardowy np kod EAN
 * / Item standard identifier ex. EAN Code
 * @property string $standardIdentificationSchemeId 0160 - GS1 GTIN (ICD)
 * @property string $specificationReference
 * @property array $classificationCode kod systemu klasyfikacji oraz kod klasyfikacji pozycji
     * / Item classification system code and item classification code (e.g. UNSPSC)
 * @property array $classificationCodeListId UNCL7143
 * @property array $classificationCodeListVersion
 * @property Document\TaxCategory $taxCategory
 * @property ItemProperty[] $property Dodatkowe parametry pozycji / Additional item attributes
 * @property ItemInstance[] $instance
 */
class OrderItem extends Item
{
    use I\Description;
    use I\BuyerIdentification;
    use I\SellerIdentification;
    use I\StandardIdentification;
    use I\SpecificationReference;
    use I\ClassificationCode;
    use I\TaxCategory;
    use I\Property;
    use I\Instance;


    protected function validate()
    {
        if(!$this->name){
            throw new Ex\MissingArgumentException('Name');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $b.'Description' => $this->description,
            $b.'Name' => $this->name,
            $a.'BuyersItemIdentification' => $this->buyerIdentification ? [
                $b.'ID' => $this->buyerIdentification
            ] : null,
            $a.'SellersItemIdentification' => $this->sellerIdentification ? [
                $b.'ID' => $this->sellerIdentification
            ] : null,
            $a.'StandardItemIdentification' => $this->standardIdentification ? [
                [
                    'name' => $b.'ID',
                    'value' => $this->standardIdentification,
                    'attributes' => [
                        'schemeID' => $this->standardIdentificationSchemeId
                    ]
                ],
            ] : null,
        ];
        foreach($this->specificationReference as $r){
            $data[] = [
                'name' => $a.'ItemSpecificationDocumentReference',
                'value' => [
                    'name' => $b.'ID',
                    'value' => $r
                ],
            ];
        }
        foreach($this->classificationCode as $i => $c){
            $data[] = [
                'name' => $a.'CommodityClassification',
                'value' => [
                    'name' => $b.'ItemClassificationCode',
                    'value' => $c,
                    'attributes' => call_user_func(function() use ($i){
                        $a =  [
                            'listID' => $this->classificationCodeListId[$i]
                        ];
                        $this->classificationCodeListVersion[$i]
                                && $a['listVersionID'] = $this->classificationCodeListVersion[$i];
                        return $a;
                    })
                ],
            ];
        }
        $data[$a.'ClassifiedTaxCategory'] = $this->taxCategory;
        foreach($this->property as $p){
            $data[] = [
                'name' => $a.'AdditionalItemProperty',
                'value' => $p
            ];
        }
        foreach($this->instance as $i){
            $data[] = [
                'name' => $a.'ItemInstance',
                'value' => $i
            ];
        }

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($b.'Name')
            );
        $object->description            = D::get($b.'Description');
        $object->buyerIdentification    = D::get($a.'BuyersItemIdentification/'.$b.'ID');
        $object->sellerIdentification   = D::get($a.'SellersItemIdentification/'.$b.'ID');
        $object->standardIdentification = D::get($a.'StandardItemIdentification/'.$b.'ID');
        $object->standardIdentificationSchemeId
                = D::get($a.'StandardItemIdentification/'.$b.'ID/@schemeID');

        $object->specificationReference = D::getArray(
                                            $a.'ItemSpecificationDocumentReference',
                                            $b.'ID'
                                        );
        $object->classificationCode     = D::getArray(
                                            $a.'CommodityClassification',
                                            $b.'ItemClassificationCode'
                                        );
        $object->classificationCodeListId = D::getArray(
                                            $a.'CommodityClassification',
                                            $b.'ItemClassificationCode/@listID'
                                        );
        $object->classificationCodeListVersion = D::getArray(
                                            $a.'CommodityClassification',
                                            $b.'ItemClassificationCode/@listVersionID'
                                        );
        $object->taxCategory            = D::get($a.'ClassifiedTaxCategory');
        $object->property               = D::getArray($a.'AdditionalItemProperty');
        $object->instance               = D::getArray($a.'ItemInstance');
        return $object;
    }

}
