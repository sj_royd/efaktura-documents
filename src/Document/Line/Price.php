<?php

namespace SJRoyd\PEF\BIS\Document\Line;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property float $amount cena netto dla pozycji / Item net price
 * @property float $quantity ilość podstawowa dla ceny jednostkowej / Item price base quantity
 * @property string $quantityCode UN/ECE_20 UN/ECE_21
 * @property Document\AllowanceCharge $allowance upust / allowance
 */
class Price implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * @var float
     */
    protected $baseAmount;

    /**
     * cena netto dla pozycji / Item net price
     * @var float
     */
    protected $amount;

    /**
     * ilość podstawowa dla ceny jednostkowej / Item price base quantity
     * @var float
     */
    protected $quantity;

    /**
     * UN/ECE_20 UN/ECE_21
     * @var string
     */
    protected $quantityCode;

    /**
     * upust / allowance
     * @var Document\AllowanceCharge
     */
    protected $allowance;

    /**
     *
     * @var Helper\Callback
     */
    private $callbackObj;

    /**
     *
     * @param float $amount
     * @param float $quantity
     * @param string $unitCode UN\ECE20 i 21
     */
    public function __construct($amount, $quantity, $unitCode)
    {
        $this->baseAmount = $amount / $quantity;
        $this->amount = $amount;
        $this->quantityCode = $unitCode;
        Helper\CodeList::validate(
            Helper\CodeList::getUnitCode($unitCode), $unitCode
        );
        $this->quantity = $quantity;
    }

    /**
     *
     * @param float $amount Cena jednostkowa
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->baseAmount = $amount;
        $this->amount = $amount * $this->quantity;
        $this->doCallback();
        return $this;
    }

    /**
     *
     * @param float $quantity
     * @return $this
     */
    public function setQuantity($quantity, $unitCode = null)
    {
        $this->quantity = $quantity;
        $this->amount = $this->baseAmount * $quantity;
        if($unitCode) {
            $this->quantityCode = $unitCode;
            Helper\CodeList::validate(
                Helper\CodeList::getUnitCode($unitCode), $unitCode
            );
        }
        $this->doCallback();
        return $this;
    }

    /**
     * @param float $amount
     * @return Document\AllowanceCharge
     */
    protected function newAllowance($amount)
    {
        return new Document\AllowanceCharge($amount);
    }

    /**
     * upust / allowance
     * @param float $amount
     * @return $this
     */
    public function setAllowance($amount)
    {
        $this->allowance = $this->newAllowance($amount);
        $this->allowance->setAllowance();
        $this->allowance->setBaseAmount($this->amount / $this->quantity);

        $this->amount = ($this->allowance->baseAmount - $amount) * $this->quantity;
        $this->doCallback();

        return $this;
    }

    /**
     *
     * @param Helper\Callback $object
     */
    public function setCallback($object)
    {
        $this->callbackObj = $object;
    }

    private function doCallback()
    {
        $this->callbackObj && $this->callbackObj->callback($this->amount / $this->quantity);
    }

    protected function validate()
    {
        if(!is_numeric($this->amount)){
            throw new Ex\WrongArgumentException('Price Amount');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        if($this->allowance){
            $this->allowance->setReason(null, null);
        }

        $data = [];
        $data[] = [
            'name' => $b.'PriceAmount',
            'value' => Helper\amountFormat($this->amount),
            'attributes' => [
                'currencyID' => Document\Document::$currencyCode
            ]
        ];
        $this->quantity && $data[] = [
            'name' => $b.'BaseQuantity',
            'value' => $this->quantity,
            'attributes' => call_user_func(function(){
                $a =  [];
                $this->quantityCode && $a['unitCode'] = $this->quantityCode;
                return $a;
            })
        ];
        $data[$a.'AllowanceCharge'] = $this->allowance;

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::cast(D::get($b.'PriceAmount'), D::CAST_FLOAT),
                D::cast(D::get($b.'BaseQuantity'), D::CAST_FLOAT) ?: 1,
                D::get($b.'BaseQuantity/@unitCode')
            );
        $object->allowance = D::get($a.'AllowanceCharge');
        return $object;
    }

}
