<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * Dane odbiorcy faktury (jeśli inny niż Kupujący) i dostawy
 * / Delicer to and invoice receiver data (if different from customer)
 * @property string $locationId
 * @property Party\Address $address
 * @property Party\Delivery $party
 */
class OrderDelivery implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * identyfikator odbiorcy dostawy (zgodny z listą PEPPOL)
     * / "Endpoint" of Deliver to identifier
     * @var string
     */
    protected $locationId;


    protected $locationSchemeId;

    /**
     *
     * @var Party\Address
     */
    protected $address;

    /**
     *
     * @var Period
     */
    protected $requestedPeriod;

    /**
     * nazwa odbiorcy / deliver to name
     * @var Party\Order\Delivery
     */
    protected $party;

    public function __construct($carrierName)
    {
        if($carrierName){
            $this->party = new Party\Order\Delivery($carrierName);
        } else {
            $this->party = false;
        }
    }

        /**
     * identyfikator odbiorcy dostawy (zgodny z listą PEPPOL)
     * / "Endpoint" of Deliver to identifier
     * @param string $locationId
     * @param string $schemeId ICD
     * @return $this
     */
    public function setLocationId($locationId, $schemeId = null)
    {
        $this->locationId = $locationId;
        $this->locationSchemeId = $schemeId;
        // ICD
        if($schemeId){
            Helper\CodeList::validate(
                Helper\CodeList::getIcd($schemeId), $schemeId
            );
        }
        return $this;
    }

    /**
     *
     * @param string $countryCode
     * @return Party\Address
     */
    public function address($countryCode)
    {
        !$this->address = $this->address = new Party\Address($countryCode);
        return $this->address;
    }

    /**
     *
     * @param \DateTime|string $endDate Data końcowa
     * @param \DateTime|string $startDate Data początkowa
     * @return $this
     */
    public function setRequestedPeriod($endDate, $startDate = null)
    {
        $this->requestedPeriod = new Period($startDate, $endDate);
        return $this;
    }


    protected function validate()
    {
        if(is_null($this->party)){
            throw new \InvalidArgumentException('Party data missing');
        }
        if($this->locationId && !$this->address){
            throw new Ex\MissingArgumentException('Address');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $a.'DeliveryLocation'   => $this->locationId || $this->address ? Helper\cleanArray([
                [
                    'name' => $b.'ID',
                    'value' => $this->locationId,
                    'attributes' => call_user_func(function(){
                        $a =  [];
                        $this->locationSchemeId && $a['schemeID'] = $this->locationSchemeId;

                        return $a;
                    })
                ],
                $a.'Address' => $this->address,
            ]) : null,
            $a.'RequestedDeliveryPeriod' => $this->requestedPeriod,
            $a.'DeliveryParty'           => $this->party?:null,
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(null);
        $object->locationId       = D::get($a.'DeliveryLocation/'.$b.'ID');
        $object->locationSchemeId = D::get($a.'DeliveryLocation/'.$b.'ID/@schemeID');
        $object->address          = D::get($a.'DeliveryLocation/'.$a.'Address');
        $object->requestedPeriod  = D::get($a.'RequestedDeliveryPeriod');
        $object->party            = D::get($a.'DeliveryParty');

        return $object;
    }

}
