<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;


/**
 *
 */
class DeliveryTerms implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $specialTerms;

    /**
     * @var string
     */
    protected $locationId;

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setSpecialTerms($specialTerms)
    {
        $this->specialTerms = $specialTerms;
        return $this;
    }

    public function setLocation($locationId)
    {
        $this->locationId = $locationId;
        return $this;
    }


    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $data = [
            $b.'ID'       => $this->id,
            $b.'SpecialTerms'         => $this->specialTerms,
            $a.'DeliveryLocation' => $this->locationId ? [
                $b.'ID' => $this->locationId
            ] : null,
        ];
        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self();
        $object->id             = D::get($b.'ID');
        $object->specialTerms   = D::get($b.'SpecialTerms');
        $object->locationId     = D::get($a.'DeliveryLocation/'.$b.'ID');

        return $object;
    }

}
