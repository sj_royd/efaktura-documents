<?php

namespace SJRoyd\PEF\BIS\Document;

use SJRoyd\PEF\BIS\Helper\Getter;

abstract class Document
{
    use Getter;

    /**
     * Liczba liczb w miejscu dziesiętnym w wartościach kwot
     * @var int
     */
    public static $currencyRounding = 2;

    /**
     *
     * @var string
     */
    public static $previewDateFormat = 'Y-m-d';


    protected $parse = false;

    /**
     * wersja komunikatu / message version
     * @var string
     */
    protected $customizationID;

    /**
     * identyfikator procesu biznesowego / business process identifier
     * @var string
     */
    protected $profileID;

    protected $salesOrderId;

    /**
     * numer faktury / invoice number
     * @var string
     */
    protected $id;

    /**
     * data wystawienia faktury / Invoice issue date
     * @var \DateTime
     */
    protected $issueDate;

    /**
     * @var boolean
     */
    protected $issueTime;

    /**
     * termin płatności faktury / Payment due date
     * @var \DateTime
     */
    protected $dueDate;

    /**
     * data powstania obowiązku podatkowego / Tax point date - the date when the VAT becomes accountable
     * @var \DateTime
     */
    protected $taxPointDate;

    /**
     * identyfikator typu faktury / Invoice type code
     * @var int
     */
    protected $typeCode;

    /**
     * uwagi tekstowe do faktury / Invoice note
     * @var string
     */
    protected $note;

    /**
     * kod waluty faktury / Invoice currency code
     * @var string
     */
    protected $currency;
    public static $currencyCode;

    /**
     * kod waluty podatku VAT / VAT accounting currency code
     * The currency used for VAT accounting and reporting purposes as accepted
     * or required in the country of the Seller. Shall be used in combination
     * with the Invoice total VAT amount in accounting currency (BT-111),
     * < when the VAT accounting currency code differs from the Invoice currency code. >
     * @var string
     */
    protected $taxCurrency;
    public static $taxCurrencyCode;

    /**
     * znak stanowiska kosztów u nabywcy / Buyer accounting reference
     * @var string
     */
    protected $accountingCost;

    /**
     * referencja kupującego / Buyer reference
     * @var string
     */
    protected $buyerReference;

    /**
     * referencja klienta / Customer reference
     * @var string
     */
    protected $customerReference;

    /**
     * okres fakturowany / invoice period
     * @var Period
     */
    protected $period;

    /**
     * referencja do promocji
     * @var Reference\QuotationDocument
     */
    protected $quotationReference;

    /**
     * referencje do zamówienia
     * @var Reference\Order
     */
    protected $orderReference;

    /**
     * referencje do faktury
     * @var Reference\Billing[]
     */
    protected $billingReference = [];

    /**
     * referencje do dokumentu awiza wysyłki / Despatch Advice reference
     * @var Reference\DespatchDocument
     */
    protected $despatchReference;

    /**
     * referencje do dokumentu potwierdzenia odbioru / Receiving advice reference
     * @var Reference\ReceiptDocument
     */
    protected $receiptReference;

    /**
     * referencje do dokumentu zamawiającego / buyer document reference
     * @var Reference\OriginatorDocument
     */
    protected $originatorReference;

    /**
     * referencje do kontraktu / contract reference
     * @var Reference\ContractDocument
     */
    protected $contractReference;

    /**
     *
     * @var Reference\Additional[]
     */
    protected $additionalReferences = [];
    /**
     * referencja do projektu / Project reference
     * @var Reference\Project
     */
    protected $projectReference;

    protected $buyerCustomer;

    protected $sellerSupplier;

    protected $originatorCustomer;

    /**
     * dane sprzedawcy / supplier data
     * @var Party\AccountingSupplier
     */
    protected $accountingSupplier;

    /**
     * dane klienta / customer data
     * @var Party\AccountingCustomer
     */
    protected $accountingCustomer;

    /**
     * dane odbiorcy płatności / Payee data
     * @var Party\Payee
     */
    protected $payee;

    /**
     * dane przedstawiciela podatkowego / Tax representative data
     * @var Party\TaxRepresentative
     */
    protected $taxRepresentative;

    /**
     * dane odbiorcy faktury (jeśli inny niż Kupujący) i dostawy
     * / Delicer to and invoice receiver data (if different from customer)
     * @var Delivery
     */
    protected $delivery;

    /**
     *
     * @var DeliveryTerms
     */
    protected $deliveryTerms;

    /**
     * sposób zapłaty / payment means
     * @var PaymentMeans[]
     */
    protected $paymentMeans = [];

    /**
     * warunki płatności / Payment terms
     * @var string
     */
    protected $paymentTerms;

    /**
     * obciążenia lub upusty / charges or allowances
     * @var AllowanceCharge[]
     */
    protected $allowancesCharges = [];

    /**
     * podsumowanie podatku / tax totals
     * @var TaxTotal[]
     */
    protected $taxTotal = [];

    /**
     * Podsumowanie dla faktury / Invoice document totals
     * @var MonetaryTotal
     */
    protected $total;

    /**
     *
     * @var InvoiceLine[]
     */
    protected $lines = [];

    public static abstract function parse($xml);
}
