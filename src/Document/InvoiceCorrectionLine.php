<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 *
 */
class InvoiceCorrectionLine extends InvoiceLine
{
    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $b.'ID' => $this->id,
            $b.'Note' => $this->note,
            [
                'name' => $b.'CreditedQuantity',
                'value' => $this->quantity,
                'attributes' => [
                    'unitCode' => $this->quantityUnitCode
                ]
            ],
            [
                'name' => $b.'LineExtensionAmount',
                'value' => Helper\amountFormat($this->netAmount),
                'attributes' => [
                    'currencyID' => Document::$currencyCode
                ]
            ],
            $b.'AccountingCost' => $this->accountingCost,
            $a.'InvoicePeriod' => $this->period,
            $a.'OrderLineReference' => $this->orderLineReference ? [
                $b.'LineID' => $this->orderLineReference
            ] : null,
            $a.'DocumentReference' => $this->documentReference,
        ];
        foreach($this->allowancesCharges as $ac){
            $data[] = [
                'name' => $a.'AllowanceCharge',
                'value' => $ac
            ];
        }
        $data[$a.'Item'] = $this->item;
        $data[$a.'Price'] = $this->price;

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::cast(D::get($b.'ID'), D::CAST_INT),
                D::cast(D::get($b.'CreditedQuantity'), D::CAST_FLOAT),
                D::get($b.'CreditedQuantity/@unitCode')
            );
        $object->note               = D::get($b.'Note');
        $object->accountingCost     = D::get($b.'AccountingCost');
        $object->netAmount          = D::cast(D::get($b.'LineExtensionAmount'), D::CAST_FLOAT);
        $object->period             = D::get($a.'InvoicePeriod');
        $object->orderLineReference = D::cast(D::get($a.'OrderLineReference/'.$b.'LineID'), D::CAST_INT);
        $object->documentReference  = D::get($a.'DocumentReference');
        $object->allowancesCharges  = D::getArray($a.'AllowanceCharge');
        $object->item               = D::get($a.'Item');
        $object->price              = D::get($a.'Price');

        self::calculateAmounts($object);

        return $object;
    }

}
