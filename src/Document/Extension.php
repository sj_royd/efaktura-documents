<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Document\CreditNoteSchema as S;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;

/**
 * @property string $name
 * @property Correction\Invoice $originalInvoiceData
 * @property Correction\Invoice $invoiceDifference
 */
class Extension implements XmlSerializable, XmlDeserializable
{
    use Getter;

    protected $name;

    /**
     *
     * @var Correction\Invoice
     */
    protected $originalInvoiceData;

    /**
     *
     * @var Correction\InvoiceDifference
     */
    protected $invoiceDifference;

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {

    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self();
        $object->name                = D::get(S::CBC.'Name');
        $object->originalInvoiceData = D::get(S::EXT.'ExtensionContent/'.S::CAC_PL.'OriginalInvoiceData');
        $object->invoiceDifference   = D::get(S::EXT.'ExtensionContent/'.S::CAC_PL.'InvoiceCorrection');

        return $object;
    }
}
