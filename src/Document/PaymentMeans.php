<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * Sposób zapłaty / payment means
 * @property string $code kod typu płatności / payment means type code
 * @property string $codeName
 * @property string $id identyfikator płatności / Remittance information
 * @property PaymentMeans\Card $card Dane karty płatniczej / Payment card information
 * @property PaymentMeans\Account $account Dane do przelewu / Payment bank transfer information
 * @property PaymentMeans\Mandate $mandate identyfikator płatności przypisany przez odbiorcę płatności
 * / identifier assigned by the Payee
 */
class PaymentMeans implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * kod typu płatności / payment means type code
     * @var string
     */
    protected $code;

    /**
     *
     * @var string
     */
    protected $codeName;

    /**
     * identyfikator płatności / Remittance information
     * @var string
     */
    protected $id;

    /**
     * Dane karty płatniczej / Payment card information
     * @var PaymentMeans\Card
     */
    protected $card;

    /**
     * Dane do przelewu / Payment bank transfer information
     * @var PaymentMeans\Account
     */
    protected $account;

    /**
     * identyfikator płatności przypisany przez odbiorcę płatności
     * / identifier assigned by the Payee
     * @var PaymentMeans\Mandate
     */
    protected $mandate;

    /**
     *
     * @param type $code kod typu płatności / payment means type code (UNCL4461)
     * @param type $codeName
     */
    public function __construct($code, $codeName = null)
    {
        $this->code = $code;
        // UNCL4461
        Helper\CodeList::validate(
            Helper\CodeList::getPaymentMeans($code), $code
        );
        $this->codeName = $codeName;
    }

    /**
     * identyfikator płatności / Remittance information
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Dane karty płatniczej / Payment card information
     * @param string $number numer karty / Payment card number
     * @param string $network typ karty / Card Network identifier
     * @return Card
     */
    public function card($number, $network)
    {
        !$this->card && $this->card = new PaymentMeans\Card($number, $network);
        return $this->card;
    }

    /**
     * Dane konta do wpłaty / Payment account identifier at a payment
     * @param string $number numer konta operatora / account identifier
     * @return Account
     */
    public function account($number)
    {
        !$this->account && $this->account = new PaymentMeans\Account($number);
        return $this->account;
    }

    /**
     * Polecenie zapłaty / Direct debit
     * @return Mandate
     */
    public function mandate()
    {
        !$this->mandate && $this->mandate = new PaymentMeans\Mandate();
        return $this->mandate;
    }



    protected function validate()
    {
        if(!$this->code){
            throw new Ex\MissingArgumentException('Payment Means Code');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            [
                'name' => $b.'PaymentMeansCode',
                'value' => $this->code,
                'attributes' => call_user_func(function(){
                    $a =  [];
                    $this->codeName && $a['name'] = $this->codeName;
                    return $a;
                })
            ],
            $b.'PaymentID' => $this->id,
            $a.'CardAccount'   => $this->card,
            $b.'PayeeFinancialAccount' => $this->account,
            $b.'PaymentMandate' => $this->mandate,
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($b.'PaymentMeansCode'),
                D::get($b.'PaymentMeansCode/@name')
            );
        $object->id = D::get($b.'PaymentID');
        $object->card = D::get($a.'CardAccount');
        $object->account = D::get($a.'PayeeFinancialAccount');
        $object->mandate = D::get($a.'PaymentMandate');

        return $object;
    }

}
