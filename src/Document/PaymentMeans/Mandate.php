<?php

namespace SJRoyd\PEF\BIS\Document\PaymentMeans;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * Polecenie zapłaty / Direct debit
 * @property string $number numer karty / Payment card number
 * @property string $network typ karty / Card Network identifier
 * @property string $holderName nazwa właściciela karty / Payment card holder name
 * / identifier assigned by the Payee
 */
class Mandate implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * Niepowtarzalny identyfikator płatności przypisany przez odbiorcę płatności
     *  do określenia szczegółów polecenia zapłaty.
     * / Unique identifier assigned by the Payee for referencing the direct debit mandate.
     * @var string
     */
    protected $id;

    /**
     * obciążane konta płatnika / the account to be debited by the direct debit
     * @var Account
     */
    protected $account;

    /**
     * identyfikator płatności / payment identifier
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * numer obciążanego konta płatnika / the account to be debited by the direct debit
     * @param string $number
     * @return $this
     */
    public function setAccountNumber($number)
    {
        !$this->account && $this->account = new Account($number);
        return $this;
    }


    protected function validate()
    {
        if(!$this->id || !$this->account){
            throw new Ex\MissingArgumentException('ID or Financial Account');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $b.'ID'                    => $this->id,
            $b.'PayerFinancialAccount' => $this->account,
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self();
        $object->id      = D::get($b.'ID');
        $object->account = D::get($a.'PayerFinancialAccount');

        return $object;
    }

}
