<?php

namespace SJRoyd\PEF\BIS\Document\PaymentMeans;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * Dane konta bankowego / Bank account information
 * @property string $number numer konta / account identifier
 * @property string $name nazwa konta / the name of the account
 * @property string $institutionName identyfikator operatora płatności elektronicznych
 * / An identifier for the service provider
 * / identifier assigned by the Payee
 */
class Account implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * numer konta operatora (np. IBAN) / account identifier (e.g. IBAN)
     * @var string
     */
    protected $number;

    /**
     * nazwa konta / the name of the account
     * @var string
     */
    protected $name;

    /**
     * identyfikator operatora płatności elektronicznych
     * / An identifier for the service provider
     * @var string
     */
    protected $institutionName;

    /**
     *
     * @param string $number numer konta operatora (np. IBAN) / account identifier (e.g. IBAN)
     */
    public function __construct($number)
    {
        $this->number = $number;
    }

    /**
     * nazwa konta / the name of the account
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * identyfikator operatora płatności elektronicznych
     * / An identifier for the service provider
     * @param string $institutionName
     * @return $this
     */
    public function setInstitutionName($institutionName)
    {
        $this->institutionName = $institutionName;
        return $this;
    }

    protected function validate()
    {
        if(!$this->number){
            throw new Ex\MissingArgumentException('Bank account number');
        }
    }


    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $b.'ID'                         => $this->number,
            $b.'Name'                       => $this->name,
            $a.'FinancialInstitutionBranch' => $this->institutionName ? [
                $b.'ID' => $this->institutionName,
            ] : null
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($b.'ID')
            );
        $object->name = D::get($b.'Name');
        $object->institutionName = D::get($a.'FinancialInstitutionBranch/'.$b.'ID');

        return $object;
    }

}
