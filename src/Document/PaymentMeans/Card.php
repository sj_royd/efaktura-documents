<?php

namespace SJRoyd\PEF\BIS\Document\PaymentMeans;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * Dane karty płatniczej / Payment card information
 * @property string $number numer karty / Payment card number
 * @property string $network typ karty / Card Network identifier
 * @property string $holderName nazwa właściciela karty / Payment card holder name
 * / identifier assigned by the Payee
 */
class Card implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * numer karty / Payment card number
     * @var string
     */
    protected $number;

    /**
     * typ karty / Card Network identifier
     * @var string
     */
    protected $network;

    /**
     * nazwa właściciela karty / Payment card holder name
     * @var string
     */
    protected $holderName;

    /**
     *
     * @param string $number numer karty / Payment card number
     * @param string $network typ karty / Card Network identifier
     */
    public function __construct($number, $network)
    {
        $this->number = $number;
        $this->network = $network;
    }

    /**
     *
     * @param string $holderName nazwa właściciela karty / Payment card holder name
     * / identifier assigned by the Payee
     * @return $this
     */
    public function setHolderName($holderName)
    {
        $this->holderName = $holderName;
        return $this;
    }


    protected function validate()
    {
        if(!$this->number){
            throw new Ex\MissingArgumentException('Card number missing');
        }
        if(!$this->network){
            throw new Ex\MissingArgumentException('Card Network ID missing');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $b.'PrimaryAccountNumberID' => $this->number,
            $b.'NetworkID'              => $this->network,
            $b.'HolderName'             => $this->holderName
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($b.'PrimaryAccountNumberID'),
                D::get($b.'NetworkID')
            );
        $object->holderName = D::get($b.'HolderName');

        return $object;
    }

}
