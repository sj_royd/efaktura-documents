<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * obciążenia lub upusty / charges or allowances
 * @property bool $indicator wskaźnik obciążenia / charge indicator
 * @property string $reasonCode kod powodu obciążenia / charge reason code
 * @property string $reason opis tekstowy powodu obciążenia / Charge reason description text
 * @property int $multiplierFactor procent obciążenia / charge percentage
 * @property float $amount kwota obciążenia / charge amount
 * @property float $baseAmount wartość bazowa do wyliczenia obciążenia / charge base amount
 * @property TaxCategory $taxCategory kategoria opodatkowania / tax category
 */
class AllowanceCharge implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * wskaźnik obciążenia/upustu / charge/allowance indicator
     * @var bool
     */
    protected $indicator;

    /**
     * kod powodu obciążenia/upustu / charge/allowance reason code
     * @var string
     */
    protected $reasonCode;

    /**
     * opis tekstowy powodu obciążenia/upustu / Charge/allowance reason description text
     * @var string
     */
    protected $reason;

    /**
     * procent obciążenia/upustu / charge/allowance percentage
     * @var int
     */
    protected $multiplierFactor;

    /**
     * kwota obciążenia/upustu / charge/allowance amount
     * @var float
     */
    protected $amount;

    /**
     * wartość bazowa / base amount
     * @var float
     */
    protected $baseAmount;

    /**
     * kategoria opodatkowania / tax category
     * @var TaxCategory
     */
    protected $taxCategory;

    /**
     *
     * @param float $amount kwota obciążenia/upustu / charge amount
     * @param int $percent procent opodatkowania / tax percentage
     * @param string $taxCategoryId kod kategorii opodatkowania obciążenia / charge tax category code
     */
    public function __construct($amount, $percent = null, $taxCategoryId = null)
    {
        $this->amount = $amount;
        $taxCategoryId && $this->taxCategory = $this->newTaxCategory($taxCategoryId, $percent);
    }

    /**
     * @param string $taxCategoryId
     * @param int $percent
     * @return TaxCategory
     */
    protected function newTaxCategory($taxCategoryId, $percent)
    {
        return new TaxCategory($taxCategoryId, $percent);
    }

    /**
     * Obciążenie / charge
     * @return $this
     */
    public function setCharge()
    {
        $this->indicator = true;
        return $this;
    }

    /**
     * Upust / allowance
     * @return $this
     */
    public function setAllowance()
    {
        $this->indicator = false;
        return $this;
    }

    /**
     * Obciążenie / charge
     */
    public function isCharge()
    {
        return $this->indicator === true;
    }

    /**
     * Upust / allowance
     */
    public function isAllowance()
    {
        return $this->indicator === false;
    }

    /**
     * powód obciążenia / charge reason
     * @param string $reason opis tekstowy / description text
     * @param string $code kod / code
     * @return $this
     */
    public function setReason($reason, $code = null)
    {
        $this->reason = $reason;
        $this->reasonCode = $code;
        if(is_null($this->indicator)){
            throw new \InvalidArgumentException('Set charge or allowance before reason');
        }
        if($this->indicator){
            // UNCL7161
            Helper\CodeList::validate(
                Helper\CodeList::getChargeReason($code), $code
            );
        } else {
            // UNCL5189
            Helper\CodeList::validate(
                Helper\CodeList::getAllowanceReason($code), $code
            );
        }
        return $this;
    }

    /**
     * procent obciążenia / charge percentage
     * @param float $multiplierFactor
     * @return $this
     */
    public function setMultiplierFactor($multiplierFactor)
    {
        $this->multiplierFactor = $multiplierFactor;
        $this->baseAmount = (100 / $multiplierFactor) * $this->amount;
        return $this;
    }

    /**
     * wartość bazowa do wyliczenia obciążenia / charge base amount
     * @param float $baseAmount
     * @return $this
     */
    public function setBaseAmount($baseAmount)
    {
        $this->baseAmount = $baseAmount;
        $this->multiplierFactor = ($this->amount / $baseAmount) * 100;
        return $this;
    }

    /**
     *
     * @param Helper\Callback $object
     */
    public function setCallback($object)
    {
        $object->callback($this->isCharge() ? $this->amount : -$this->amount);
    }


    protected function validate()
    {
        if(is_null($this->indicator)){
            throw new Ex\MissingArgumentException('Charge Indicator');
        }
        if(is_nan($this->amount)){
            throw new Ex\WrongArgumentException('Charge Amount');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $b.'ChargeIndicator'            => ($this->indicator ? 'true' : 'false'),
            $b.'AllowanceChargeReasonCode'  => $this->reasonCode,
            $b.'AllowanceChargeReason'      => $this->reason,
            $b.'MultiplierFactorNumeric'    => $this->multiplierFactor,
            [
                'name'       => $b.'Amount',
                'value'      => Helper\amountFormat($this->amount),
                'attributes' => [
                    'currencyID' => Document::$currencyCode
                ]
            ],
            $this->baseAmount ? [
                'name'   => $b.'BaseAmount',
                'value'      => Helper\amountFormat($this->baseAmount),
                'attributes' => [
                    'currencyID' => Document::$currencyCode
                ]
            ] : null,
            $a.'TaxCategory'                => $this->taxCategory
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::cast(D::get($b.'Amount'), D::CAST_FLOAT)
            );
        $object->indicator        = D::cast(D::get($b.'ChargeIndicator'), D::CAST_BOOL);
        $object->reasonCode       = D::get($b.'AllowanceChargeReasonCode');
        $object->reason           = D::get($b.'AllowanceChargeReason');
        $object->multiplierFactor = D::cast(D::get($b.'MultiplierFactorNumeric'), D::CAST_INT);
        $object->baseAmount       = D::cast(D::get($b.'BaseAmount'), D::CAST_FLOAT);
        $object->taxCategory      = D::get($a.'TaxCategory');

        return $object;
    }

}
