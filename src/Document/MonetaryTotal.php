<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * Podsumowanie dla faktury / Invoice document totals
 * @property float $netAmount suma wartości netto z pozycji faktury
 * / Sum of Invoice line net amount
 * @property float $taxExclusiveAmount suma faktury bez podatku
 * / Invoice total amount without VAT
 * @property float $taxInclusiveAmount suma faktury z podatkiem
 * / Invoice total amount with VAT
 * @property float $allowancesAmount suma upustów na poziomie dokumentu
 * / Sum of allowances on document level
 * @property float $chargesAmount suma obciążeń na poziomie dokumentu
 * / Sum of charges on document level
 * @property float $prepaidAmount suma przedpłat / Paid amount
 * @property float $toPayRoundingAmount suma zaokrągleń / Rounding amount
 * @property float $toPayAmount wartość do zapłaty / Amount due for payment
 */
class MonetaryTotal implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * suma wartości netto z pozycji faktury / Sum of Invoice line net amount
     * @var float
     */
    protected $netAmount;

    /**
     * suma faktury bez podatku /Invoice total amount without VAT
     * @var float
     */
    protected $taxExclusiveAmount;

    /**
     * suma faktury z podatkiem / Invoice total amount with VAT
     * @var float
     */
    protected $taxInclusiveAmount;

    /**
     * suma upustów na poziomie dokumentu / Sum of allowances on document level
     * @var float
     */
    protected $allowancesAmount;

    /**
     * suma obciążeń na poziomie dokumentu / Sum of charges on document level
     * @var float
     */
    protected $chargesAmount;

    /**
     * suma przedpłat / Paid amount
     * @var float
     */
    protected $prepaidAmount;

    /**
     * suma zaokrągleń / Rounding amount
     * @var float
     */
    protected $toPayRoundingAmount;

    /**
     * wartość do zapłaty / Amount due for payment
     * @var float
     */
    protected $toPayAmount;

    private $simply = false;

    /**
     * suma przedpłat / Paid amount
     * @param float $amount
     * @return $this
     */
    public function setPrepaidAmount($amount)
    {
        if($this->simply){
            return $this;
        }
        $this->prepaidAmount = $amount;
        return $this;
    }

    /**
     * suma zaokrągleń / Rounding amount
     * @param float $amount
     * @return $this
     */
    public function setToPayRoundingAmount($amount)
    {
        if($this->simply){
            return $this;
        }
        $this->toPayRoundingAmount = $amount;
        return $this;
    }

    /**
     *
     * @param array $lines
     * @param array $allowancesCharges
     */
    public function calculateAmounts($lines = [], $allowancesCharges = [])
    {
        $this->netAmount =
            $this->taxExclusiveAmount =
            $this->taxInclusiveAmount =
            $this->allowancesAmount =
            $this->chargesAmount =
            $this->toPayAmount = 0;

        /** @var InvoiceLine|CreditNoteLine $line */
        foreach($lines as $line) {
            $this->netAmount += $line->netAmount;
            if(!$this->simply){
                $this->taxExclusiveAmount += $line->netAmount;
                $this->taxInclusiveAmount += $line->grossAmount;
                $this->toPayAmount += $line->grossAmount;
            } else {
                $this->toPayAmount += $line->netAmount;
            }
        }

        /** @var AllowanceCharge $ac */
        foreach($allowancesCharges as $ac) {
            $net = $ac->amount;
            $gross = $ac->taxCategory
                ? round($net * (1 + ($ac->taxCategory->percent / 100)), Document::$currencyRounding)
                : $net;
            if($ac->isAllowance()) {
                $this->allowancesAmount += $net;
                $this->taxExclusiveAmount -= $net;
                $this->taxInclusiveAmount -= $gross;
                $this->toPayAmount -= $gross;
            } elseif($ac->isCharge()) {
                $this->chargesAmount += $net;
                $this->taxExclusiveAmount += $net;
                $this->taxInclusiveAmount += $gross;
                $this->toPayAmount += $gross;
            }
        }

        if($this->prepaidAmount) {
            $this->toPayAmount -= $this->prepaidAmount;
        }
    }

    public function setCallback($object)
    {
        if($object instanceof \SJRoyd\PEF\BIS\CreditNote){
            $this->simply = true;
        }
    }

    protected function validate()
    {
        if(is_nan($this->netAmount)){
            throw new \InvalidArgumentException('Line Extension Amount missing');
        }
        if(!$this->simply){
            if(is_nan($this->taxExclusiveAmount)){
                throw new Ex\WrongArgumentException('Tax Exclusive Amount');
            }
            if(is_nan($this->taxInclusiveAmount)){
                throw new Ex\WrongArgumentException('Tax Inclusive Amount');
            }
            if($this->prepaidAmount + $this->toPayAmount != $this->taxInclusiveAmount){
                throw new \InvalidArgumentException('Total calculating exception');
            }
        }
        if(is_nan($this->toPayAmount)){
            throw new Ex\MissingArgumentException('Payable Amount');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $src = [
            $b.'LineExtensionAmount'   => $this->netAmount,
            $b.'TaxExclusiveAmount'    => $this->taxExclusiveAmount,
            $b.'TaxInclusiveAmount'    => $this->taxInclusiveAmount,
            $b.'AllowanceTotalAmount'  => $this->allowancesAmount,
            $b.'ChargeTotalAmount'     => $this->chargesAmount,
            $b.'PrepaidAmount'         => $this->prepaidAmount,
            $b.'PayableRoundingAmount' => $this->toPayRoundingAmount,
            $b.'PayableAmount'         => $this->toPayAmount,
        ];

        $data = [];
        foreach($src as $k=>$v){
            !is_null($v) && $data[] = [
                'name' => $k,
                'value' => Helper\amountFormat($v),
                'attributes' => [
                    'currencyID' => Document::$currencyCode
                ]
            ];
        }
        $writer->write($data);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self();
        $object->netAmount           = D::cast(D::get($b.'LineExtensionAmount'), D::CAST_FLOAT);
        $object->taxExclusiveAmount  = D::cast(D::get($b.'TaxExclusiveAmount'), D::CAST_FLOAT);
        $object->taxInclusiveAmount  = D::cast(D::get($b.'TaxInclusiveAmount'), D::CAST_FLOAT);
        $object->allowancesAmount    = D::cast(D::get($b.'AllowanceTotalAmount'), D::CAST_FLOAT);
        $object->chargesAmount       = D::cast(D::get($b.'ChargeTotalAmount'), D::CAST_FLOAT);
        $object->prepaidAmount       = D::cast(D::get($b.'PrepaidAmount'), D::CAST_FLOAT);
        $object->toPayRoundingAmount = D::cast(D::get($b.'PayableRoundingAmount'), D::CAST_FLOAT);
        $object->toPayAmount         = D::cast(D::get($b.'PayableAmount'), D::CAST_FLOAT);

        return $object;
    }

}
