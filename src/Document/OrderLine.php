<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property int $id numer pozycji faktury / Line (position) number
 * @property string $note dodatkowa informacja lub uwaga / Invoice line note
 * @property float $quantity ilość fakturowana / Invoiced quantity
 * @property string $quantityUnitCode
 * @property float $netAmount wartość netto pozycji / Invoice line net amount
 * @property float $taxAmount wartość podatku pozycji / Invoice line tax amount
 * @property float $grossAmount wartość brutto pozycji / Invoice line gross amount
 * @property bool $partialDelivery
 * @property float $accountingCost pozycja kosztów u nabywcy
 * / Invoice line Buyer accounting reference
 * @property OrderDelivery $delivery
 * @property Reference\Additional $documentReference
 * Referencja do dokumentu powiązanego / Invoice line referenced object identifier
 * @property AllowanceCharge[] $allowancesCharges obciążenia lub upusty / charges or allowances
 * @property Line\OrderItem $item
 * @property Line\Price $price
 */
class OrderLine implements XmlSerializable, XmlDeserializable, Helper\Callback
{
    use Getter;

    /**
     * dodatkowa informacja lub uwaga / Invoice line note
     * @var string
     */
    protected $note;

    /**
     * numer pozycji faktury / Line (position) number
     * @var int
     */
    protected $id;

    /**
     * ilość fakturowana / Invoiced quantity
     * @var float
     */
    protected $quantity;

    /**
     *
     * @var string
     */
    protected $quantityUnitCode;

    /**
     * wartość netto pozycji / Invoice line net amount
     * @var float
     */
    protected $netAmount;

    /**
     * wartość podatku pozycji / Invoice line tax amount
     * @var float
     */
    protected $taxAmount;

    /**
     * wartość brutto pozycji / Invoice line gross amount
     * @var float
     */
    protected $grossAmount;

    /**
     * częściowa dostawa
     * @var bool
     */
    protected $partialDelivery;

    /**
     * pozycja kosztów u nabywcy / Invoice line Buyer accounting reference
     * @var float
     */
    protected $accountingCost;

    /**
     *
     * @var OrderDelivery
     */
    protected $delivery;

    /**
     *
     * @var Party\Order\LineOriginator
     */
    protected $originator;

    /**
     * obciążenia lub upusty / charges or allowances
     * @var AllowanceCharge[]
     */
    protected $allowancesCharges = [];

    /**
     *
     * @var Line\Price
     */
    protected $price;

    /**
     *
     * @var Line\OrderItem
     */
    protected $item;

    private $callbackObj;

    /**
     *
     * @param int $id numer pozycji faktury / Line (position) number
     * @param float $quantity ilość fakturowana / Invoiced quantity
     * @param string $unitCode UN/ECE_20 lub UN/ECE_21
     */
    public function __construct($id, $quantity, $unitCode)
    {
        $this->id = $id;
        $this->quantity = $quantity;
        $this->quantityUnitCode = $unitCode;
        Helper\CodeList::validate(
            Helper\CodeList::getUnitCode($unitCode), $unitCode
        );
    }

    /**
     * dodatkowa informacja lub uwaga / Invoice line note
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    /**
     * wartość netto pozycji / Invoice line net amount
     * @param float $amount
     * @return $this
     */
    public function setNetAmount($amount)
    {
        $this->netAmount = $amount;
        return $this;
    }

    /**
     *
     * @param bool $indicator
     * @return $this
     */
    public function setPartialDelivery($indicator)
    {
        $this->partialDelivery = $indicator;
        return $this;
    }

    /**
     * pozycja kosztów u nabywcy / Invoice line Buyer accounting reference
     * @param string $accountingCost
     * @return $this
     */
    public function setAccountingCost($accountingCost)
    {
        $this->accountingCost = $accountingCost;
        return $this;
    }

    /**
     * okres fakturowany
     * @param \DateTime|string $startDate
     * @param \DateTime|string $endDate
     * @return $this
     */
    public function setPeriod($startDate, $endDate)
    {
        $this->delivery = new OrderDelivery(null);
        $this->delivery->setRequestedPeriod($endDate, $startDate);
        return $this;
    }

    /**
     *
     * @param string $name
     * @param string $identification
     * @return $this
     */
    public function setOriginator($name, $identification = null)
    {
        $this->originator = new Party\Order\LineOriginator($name);
        if($identification){
            $this->originator->setIdentification($identification);
        }
        return $this;
    }

    /**
     * Referencja do dokumentu powiązanego / Invoice line referenced object identifier
     * @param string $id
     * @param string $schemeId
     * @param bool $isInvoice
     * @return $this
     */
    public function setDocumentReference($id, $schemeId, $isInvoice = true)
    {
        !$this->documentReference
                && $this->documentReference = new Reference\Additional($id, $schemeId);
        $isInvoice && $this->documentReference->setTypeCode(130);
        return $this;
    }

    /**
     * obciążenia lub upusty / charges or allowances
     * @param float $amount kwota obciążenia/upustu / charge amount
     * @param int $percent procent opodatkowania / tax percentage
     * @param string $taxCategoryId kod kategorii opodatkowania obciążenia / charge tax category code
     * @return AllowanceCharge
     */
    public function addAllowanceCharge($amount, $percent = null, $taxCategoryId = null)
    {
        if(!$this->price){
            throw new \InvalidArgumentException('Price not set');
        }
        $allowanceCharge = new AllowanceCharge($amount, $percent, $taxCategoryId);
        $allowanceCharge->setCallback($this);
        $this->allowancesCharges[] = $allowanceCharge;

        return $allowanceCharge;
    }

    /**
     *
     * @param string $name
     * @param int $taxPercent
     * @param string $taxCode
     * @return Line\OrderItem
     */
    public function item($name, $taxPercent, $taxCode = Helper\PEPPOL\TaxCategory::STANDARD)
    {
        if(!$this->item){
            $this->item = new Line\OrderItem($name);
            $this->item->taxCategory($taxCode, $taxPercent);
        }
        $this->price && self::calculateAmounts($this);
        return $this->item;
    }

    /**
     *
     * @return Line\Price
     */
    public function price($unitPriceAmount)
    {
        !$this->price && $this->price
                = new Line\Price($unitPriceAmount, 1, $this->quantityUnitCode);

        $this->price->setCallback($this);

        $this->netAmount = $unitPriceAmount * $this->quantity;

        $this->item && self::calculateAmounts($this);
        return $this->price;
    }

    /**
     *
     * @param float $item
     */
    public function callback($item)
    {
        $amount = ($this->price->amount / $this->price->quantity) * $this->quantity;

        foreach($this->allowancesCharges as $ac){
            if($ac->isAllowance()){
                $amount -= $ac->amount;
            } else {
                $amount += $ac->amount;
            }
        }

        $this->netAmount = $amount;
        $this->item && self::calculateAmounts($this);
        $this->callbackObj->callback();
    }

    /**
     *
     * @param Helper\Callback $object
     */
    public function setCallback($object)
    {
        $this->callbackObj = $object;
    }

    /**
     * @param InvoiceLine $object
     */
    protected static function calculateAmounts($object)
    {
        if(!$object->item->taxCategory){
            return;
        }
        $object->grossAmount = round(
                $object->netAmount * (1 + ($object->item->taxCategory->percent / 100)),
                Document::$currencyRounding
            );
        $object->taxAmount = $object->grossAmount - $object->netAmount;
    }

    protected function validate()
    {
        if(!is_numeric($this->id)){
            throw new Ex\WrongArgumentException('ID');
        }
        if(!is_numeric($this->quantity)){
            throw new Ex\WrongArgumentException('Invoiced Quantity');
        }
        if(!is_numeric($this->netAmount)){
            throw new Ex\WrongArgumentException('Line Extension Amount');
        }
        if(!$this->item){
            throw new Ex\MissingArgumentException('Item');
        }
        if(!$this->price){
            throw new Ex\MissingArgumentException('Price');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();
//var_dump($this);die;
        $data = [
            $b.'Note' => $this->note,
            $a.'LineItem' => Helper\cleanArray([

                $b.'ID' => $this->id,
                [
                    'name' => $b.'Quantity',
                    'value' => $this->quantity,
                    'attributes' => [
                        'unitCode' => $this->quantityUnitCode
                    ]
                ],
                [
                    'name' => $b.'LineExtensionAmount',
                    'value' => Helper\amountFormat($this->netAmount),
                    'attributes' => [
                        'currencyID' => Document::$currencyCode
                    ]
                ],
                $b.'PartialDeliveryIndicator' => $this->getIndicator($this->partialDelivery),
                $b.'AccountingCost' => $this->accountingCost,
                $a.'Delivery' => $this->delivery,
                $a.'OriginatorParty' => $this->originator,
            ])
        ];
        foreach($this->allowancesCharges as $ac){
            $data[$a.'LineItem'][] = [
                'name' => $a.'AllowanceCharge',
                'value' => $ac
            ];
        }
        $data[$a.'LineItem'][$a.'Price'] = $this->price;
        $data[$a.'LineItem'][$a.'Item'] = $this->item;

        $writer->write(Helper\cleanArray($data));
    }

    private function getIndicator($val)
    {
        if(is_null($val)){
            return null;
        }

        return $val ? 'true' : 'false';
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);
//print_r(D::debug());die;
        $i = $a.'LineItem/';

        $object = new self(
                D::cast(D::get($i.$b.'ID'), D::CAST_INT),
                D::cast(D::get($i.$b.'Quantity'), D::CAST_FLOAT),
                D::get($i.$b.'Quantity/@unitCode')
            );
        $object->note               = D::get($b.'Note');
        $object->netAmount          = D::cast(D::get($i.$b.'LineExtensionAmount'), D::CAST_FLOAT);
        $object->partialDelivery    = D::cast(D::get($i.$b.'PartialDeliveryIndicator'), D::CAST_BOOL);
        $object->accountingCost     = D::get($i.$b.'AccountingCost');
        $object->delivery           = D::get($i.$a.'Delivery');
        $object->originator         = D::get($i.$a.'OriginatorParty');
        $object->allowancesCharges  = D::getArray($i.$a.'AllowanceCharge');
        $object->price              = D::get($i.$a.'Price');
        $object->item               = D::get($i.$a.'Item');

        self::calculateAmounts($object);

        return $object;
    }

}
