<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * Dane odbiorcy faktury (jeśli inny niż Kupujący) i dostawy
 * / Delicer to and invoice receiver data (if different from customer)
 * @property \DateTime $date
 * @property string $locationId
 * @property Party\Address $address
 * @property Party\Billing\Delivery $party
 */
class BillingDelivery implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * faktyczna data dostawy / actual delivery date
     * @var \DateTime
     */
    protected $date;

    /**
     * identyfikator odbiorcy dostawy (zgodny z listą PEPPOL)
     * / "Endpoint" of Deliver to identifier
     * @var string
     */
    protected $locationId;


    protected $locationSchemeId;

    /**
     *
     * @var Party\Address
     */
    protected $address;

    /**
     * nazwa odbiorcy / deliver to name
     * @var Party\Billing\Delivery
     */
    protected $party;

    /**
     * faktyczna data dostawy / actual delivery date
     * @param \DateTime|string $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = Helper\date($date);
        return $this;
    }

    /**
     * identyfikator odbiorcy dostawy (zgodny z listą PEPPOL)
     * / "Endpoint" of Deliver to identifier
     * @param string $locationId
     * @param string $schemeId ICD
     * @return $this
     */
    public function setLocationId($locationId, $schemeId = null)
    {
        $this->locationId = $locationId;
        $this->locationSchemeId = $schemeId;
        // ICD
        if($schemeId){
            Helper\CodeList::validate(
                Helper\CodeList::getIcd($schemeId), $schemeId
            );
        }
        return $this;
    }

    public function address($countryCode)
    {
        !$this->address = $this->address = new Party\Address($countryCode);
        return $this->address;
    }

    /**
     * nazwa odbiorcy / deliver to name
     * @param string $name
     * @return $this
     */
    public function setReceipientName($name)
    {
        !$this->party = $this->party = new Party\Billing\Delivery($name);
        return $this;
    }


    protected function validate()
    {
        if(!$this->date || (!$this->locationId || !$this->address) || !$this->party){
            throw new \InvalidArgumentException('Delivery data missing');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $b.'ActualDeliveryDate' => Helper\dateFormat($this->date),
            $a.'DeliveryLocation'   => $this->locationId || $this->address ? Helper\cleanArray([
                [
                    'name' => $b.'ID',
                    'value' => $this->locationId,
                    'attributes' => call_user_func(function(){
                        $a =  [];
                        $this->locationSchemeId && $a['schemeID'] = $this->locationSchemeId;

                        return $a;
                    })
                ],
                $a.'Address' => $this->address,
            ]) : null,
            $a.'DeliveryParty' => $this->party,
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self();
        $object->date             = D::get($b.'ActualDeliveryDate', '\DateTime');
        $object->locationId       = D::get($a.'DeliveryLocation/'.$b.'ID');
        $object->locationSchemeId = D::get($a.'DeliveryLocation/'.$b.'ID/@schemeID');
        $object->address          = D::get($a.'DeliveryLocation/'.$a.'Address');
        $object->party            = D::get($a.'DeliveryParty');

        return $object;
    }

}
