<?php

namespace SJRoyd\PEF\BIS\Document;

use SJRoyd\PEF\BIS\Helper\Schema;

class CreditNoteSchema extends Schema
{
    const NS = '{urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2}';
}
