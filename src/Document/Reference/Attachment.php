<?php

namespace SJRoyd\PEF\BIS\Document\Reference;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $embeddedDocument Treść załącznika / attachment content
 * @property string $embeddedSize Wielkość załącznika / attachment size
 * @property string $embeddedMimeCode Mime code załącznika / attachment mime code
 * @property string $embeddedFileName Nazwa pliku załącznika / attachment file name
 * @property string $externalUri link do pliku / file location link
 */
class Attachment implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * Mime code załącznika / attachment mime code
     * @var string
     */
    protected $embeddedMimeCode;

    /**
     * Nazwa pliku załącznika / attachment file name
     * @var string
     */
    protected $embeddedFileName;

    /**
     * Treść załącznika / attachment content
     * @var string
     */
    protected $embeddedDocument;

    /**
     * Wielkość załącznika / attachment size
     * @var string
     */
    protected $embeddedSize;

    /**
     * link do pliku / file location link
     * @var string
     */
    protected $externalUri;

    /**
     * @param string $content
     * @param string $mimecode
     * @param string $filename
     */
    public function setEmbedded($content, $mimecode, $filename)
    {
        if(!is_string($content)){
            throw new \InvalidArgumentException('Embedded document content Must be a string value');
        }
        $this->embeddedDocument = $content;
        $this->embeddedSize = strlen($content);
        $this->embeddedMimeCode = $mimecode;
        $this->embeddedFileName = $filename;
    }

    /**
     * @param string $uri
     */
    public function setExternalUri($uri)
    {
        $this->externalUri = $uri;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        if($this->embeddedDocument){
            $writer->write([
                [
                    'name' => $b.'EmbeddedDocumentBinaryObject',
                    'value' => base64_encode($this->embeddedDocument),
                    'attributes' => [
                        'mimeCode' => $this->embeddedMimeCode,
                        'filename' => $this->embeddedFileName,
                    ]
                ]
            ]);
        }

        if($this->externalUri){
            $writer->write([
                $a.'ExternalReference' => [
                    $b.'URI' => $this->externalUri
                ]
            ]);
        }
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self();
        $object->externalUri = D::get($a.'ExternalReference/'.$b.'URI');
        $embedded = D::get($b.'EmbeddedDocumentBinaryObject');
        if($embedded){
            $object->setEmbedded(
                base64_decode($embedded),
                D::get($b.'EmbeddedDocumentBinaryObject/@mimeCode'),
                D::get($b.'EmbeddedDocumentBinaryObject/@filename')
            );
        }

        return $object;
    }

}
