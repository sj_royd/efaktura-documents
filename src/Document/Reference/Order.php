<?php

namespace SJRoyd\PEF\BIS\Document\Reference;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * ORDER AND SALES ORDER REFERENCE
 */
class Order extends Reference
{
    /**
     * An identifier of a referenced sales order, issued by the Seller.
     * @var string
     */
    protected $salesOrderId;

    /**
     * @param string $id
     * @param string $salesOrderId
     */
    public function __construct($id, $salesOrderId = null)
    {
        parent::__construct($id);
        $this->salesOrderId = $salesOrderId;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        parent::xmlSerialize($writer);
        $this->salesOrderId && $writer->write([
            $b.'SalesOrderID' => $this->salesOrderId
        ]);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($b.'ID')
            );
        $object->salesOrderId = D::get($b.'SalesOrderID');

        return $object;
    }

}
