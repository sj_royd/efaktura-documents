<?php

namespace SJRoyd\PEF\BIS\Document\Reference;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $schemeId
 * @property string $documentDescription
 * @property string $documentTypeCode
 * @property Attachment $attachment
 */
class Additional extends Reference
{

    /**
     * @var string
     */
    protected $schemeId;

    /**
     * @var string
     */
    protected $documentDescription;

    /**
     * @var string
     */
    protected $documentTypeCode;

    /**
     * @var Attachment
     */
    protected $attachment;

    /**
     *
     * @param string $id
     * @param string $schemeId
     */
    public function __construct($id, $schemeId)
    {
        parent::__construct($id);
        $this->schemeId = $schemeId;
        // UNCL1153
        Helper\CodeList::validate(
            Helper\CodeList::getInvoicedObjectIdentifier($schemeId), $schemeId
        );
    }

    /**
     *
     * @param string $documentDescription
     * @return $this
     */
    public function setDescription($documentDescription)
    {
        $this->documentDescription = $documentDescription;
        return $this;
    }

    /**
     *
     * @param string $documentTypeCode
     * @return $this
     */
    public function setTypeCode($documentTypeCode)
    {
        $this->documentTypeCode = $documentTypeCode;
        return $this;
    }


    /**
     * @return $this
     */
    public function attachment()
    {
        $this->attachment = new Attachment();
        return $this->attachment;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $data = [
            [
                'name' => $b.'ID',
                'value' => $this->id,
                'attributes' => [
                    'schemeID' => $this->schemeId
                ]
            ],
            $b.'DocumentTypeCode'    => $this->documentTypeCode,
            $b.'DocumentDescription' => $this->documentDescription,
            $a.'Attachment'          => $this->attachment,
        ];
        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($b.'ID'),
                D::get($b.'ID/@schemeID')
            );

        $object->documentDescription = D::get($b.'DocumentDescription');
        $object->documentTypeCode    = D::get($b.'DocumentTypeCode');
        $object->attachment          = D::get($a.'Attachment');

        return $object;
    }

}
