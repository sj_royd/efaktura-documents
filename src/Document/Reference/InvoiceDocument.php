<?php

namespace SJRoyd\PEF\BIS\Document\Reference;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * INVOICE DOCUMENT REFERENCE
 */
class InvoiceDocument extends Reference
{
    /**
     * Preceding Invoice issue date
     * @var string
     */
    protected $issueDate;

    /**
     * @param string $id
     * @param string|\DateTime $issueDate
     */
    public function __construct($id, $issueDate = null)
    {
        parent::__construct($id);
        $this->issueDate = Helper\date($issueDate);
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        parent::xmlSerialize($writer);
        $this->issueDate && $writer->write([
            $b.'IssueDate' => Helper\dateFormat($this->issueDate)
        ]);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($b.'ID')
            );
        $object->issueDate = D::get($b.'IssueDate', '\DateTime');

        return $object;
    }

}
