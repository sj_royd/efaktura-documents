<?php

namespace SJRoyd\PEF\BIS\Document\Reference;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property InvoiceDocument $invoiceReference
 */
class Billing implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * @var InvoiceDocument
     */
    protected $invoiceReference;

    /**
     * @param string $id
     * @param string|\DateTime $issueDate
     */
    public function documentReference($id, $issueDate = null)
    {
        $this->invoiceReference = new InvoiceDocument($id, $issueDate);
        return $this->invoiceReference;
    }

    protected function validate()
    {
        if(!$this->invoiceReference){
            throw new Ex\MissingArgumentException('Invoice Document Reference');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $writer->write([
            $a.'InvoiceDocumentReference' => $this->invoiceReference
        ]);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        $object = new self();

        $children = $reader->parseInnerTree();
        foreach($children as $child) {
            $child['value'] instanceof InvoiceDocument && $object->invoiceReference = $child['value'];
        }
        return $object;
    }

}
