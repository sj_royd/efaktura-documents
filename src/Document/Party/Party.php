<?php

namespace SJRoyd\PEF\BIS\Document\Party;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Helper\Getter;

/**
 * @property string $name
 */
abstract class Party implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * EndpointID
     * @var string
     */
    protected $endpoint;

    /**
     * EndpointID/@schemeID (EAS)
     * @var string
     */
    protected $endpointScheme;

    /**
     * PartyIdentification
     * @var mixed
     */
    protected $identification;

    /**
     * PartyIdentification/@schemeID (ICD,SEPA)
     * @var mixed
     */
    protected $identificationScheme;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     * @var Address
     */
    protected $address;

    /**
     * @var TaxScheme
     */
    protected $taxScheme;

    /**
     * @var LegalEntity
     */
    protected $legalEntity;

    /**
     * @var Contact
     */
    protected $contact;

    public function __construct($name = null)
    {
        $this->name = $name;
    }

    protected abstract function validate();

    public abstract function xmlSerialize(Writer $writer);

    public abstract static function xmlDeserialize(Reader $reader);

}
