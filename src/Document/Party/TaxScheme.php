<?php

namespace SJRoyd\PEF\BIS\Document\Party;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $companyId
 * @property string $taxSchemeId
 */
class TaxScheme implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * @var string
     */
    protected $companyId;

    /**
     * @var string
     */
    protected $taxSchemeId;

    public function __construct($companyId, $taxSchemeId = 'VAT')
    {
        $this->companyId = $companyId;
        $this->taxSchemeId = $taxSchemeId;
    }

    protected function validate()
    {
        if(!$this->companyId){
            throw new Ex\MissingArgumentException('Company ID');
        }
        if(!$this->taxSchemeId){
            throw new Ex\MissingArgumentException('Tax Scheme ID');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $writer->write([
            $b.'CompanyID' => $this->companyId,
            $a.'TaxScheme' => [
                $b.'ID' => $this->taxSchemeId
            ],
        ]);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($b.'CompanyID'),
                D::get($a.'TaxScheme/'.$b.'ID')
            );

        return $object;
    }

}
