<?php

namespace SJRoyd\PEF\BIS\Document\Party;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $name
 * @property string $companyId
 * @property string $companySchemeId
 * @property string $legalForm
 * @property Address $address
 */
class LegalEntity implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $companyId;

    /**
     * ICD
     * @var string
     */
    protected $companySchemeId;

    /**
     * @var string
     */
    protected $legalForm;

    /**
     * @var Address
     */
    protected $address;

    /**
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @param string $companyId
     * @param string $schemeId
     * @return $this
     */
    public function setCompanyId($companyId, $schemeId = null)
    {
        $this->companyId = $companyId;
        $this->companySchemeId = $schemeId;
        // ICD
        if($schemeId){
            Helper\CodeList::validate(
                Helper\CodeList::getIcd($schemeId), $schemeId
            );
        }
        return $this;
    }

    /**
     *
     * @param string $legalForm
     * @return $this
     */
    public function setLegalForm($legalForm)
    {
        $this->legalForm = $legalForm;
        return $this;
    }

    /**
     *
     * @param string $countryCode kod kraju / Country code
     * @param string $city City name
     * @return $this
     */
    public function setAddress($countryCode, $city = null)
    {
        $this->address = new Address($countryCode);
        $city && $this->address->setCity($city);

        return $this;
    }


    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $data = [
            $b.'RegistrationName'     => $this->name,
            [
                'name' => $b.'CompanyID',
                'value' => $this->companyId,
                'attributes' => call_user_func(function(){
                    $a =  [];
                    $this->companySchemeId && $a['schemeID'] = $this->companySchemeId;
                    return $a;
                })
            ],
            $b.'CompanyLegalForm'     => $this->legalForm,
            $a.'RegistrationAddress'  => $this->address,
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self();

        $object->name            = D::get($b.'RegistrationName');
        $object->companyId       = D::get($b.'CompanyID');
        $object->companySchemeId = D::get($b.'CompanyID/@schemeID');
        $object->legalForm       = D::get($b.'CompanyLegalForm');
        $object->address         = D::get($a.'RegistrationAddress'); // TODO

        return $object;
    }

}
