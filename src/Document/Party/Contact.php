<?php

namespace SJRoyd\PEF\BIS\Document\Party;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $street Street name
 * @property string $additionalStreet Additional Street name
 * @property string $city City name
 */
class Contact implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $email;


    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }


    public function validate()
    {
        if(!$this->name || !$this->phone || !$this->email){
            throw new Ex\MissingArgumentException('Contact Name, Phone or Email');
        }
    }


    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $data = [];
        $this->name  && $data[$b.'Name']           = $this->name;
        $this->phone && $data[$b.'Telephone']      = $this->phone;
        $this->email && $data[$b.'ElectronicMail'] = $this->email;

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self();
        $object->name  = D::get($b.'Name');
        $object->phone = D::get($b.'Telephone');
        $object->email = D::get($b.'ElectronicMail');

        return $object;
    }

}
