<?php

namespace SJRoyd\PEF\BIS\Document\Party\Billing;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document\Party as P;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 *
 */
class Delivery extends P\Party
{

    protected function validate()
    {
        if(!$this->name){
            throw new Ex\MissingArgumentException('Party name');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $writer->write([
            $a.'PartyName' => [
                $b.'Name' => $this->name
            ]
        ]);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($a.'PartyName/'.$b.'Name')
            );

        return $object;
    }

}
