<?php

namespace SJRoyd\PEF\BIS\Document\Party\Billing;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document\Party as P;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Document\Party as HP;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property Address $address Adres przedstawiciela podatkowego / Tax representative address
 * @property P\TaxScheme $taxScheme
 */
class TaxRepresentative extends P\Party
{
    use HP\Address;
    use HP\TaxScheme;

    protected function validate()
    {
        if(!$this->name){
            throw new Ex\MissingArgumentException('Party name');
        }
        if(!$this->address){
            throw new Ex\MissingArgumentException('Party Postal Address');
        }
        if(!$this->taxScheme){
            throw new Ex\MissingArgumentException('Party Tax Scheme');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $a.'PartyName' => [
                $b.'Name' => $this->name
            ],
            $a.'PostalAddress' => $this->address,
            $a.'PartyTaxScheme' => $this->taxScheme
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($a.'PartyName/'.$b.'Name')
            );
        $object->address   = D::get($a.'PostalAddress');
        $object->taxScheme = D::get($a.'PartyTaxScheme');

        return $object;
    }

}
