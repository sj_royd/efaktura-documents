<?php

namespace SJRoyd\PEF\BIS\Document\Party\Billing;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document\Party as P;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Document\Party as HP;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $endpoint EndpointID
 * @property string $endpointScheme EndpointID/@schemeID (EAS)
 * @property string $identification PartyIdentification
 * @property string $identificationScheme PartyIdentification/@schemeID (ICD,SEPA)
 * @property P\Address $address
 * @property P\TaxScheme $taxScheme
 * @property P\LegalEntity $legalEntity
 * @property P\Contact $contact
 */
class AccountingSupplier extends P\Party
{
    use HP\Address;
    use HP\Contact;
    use HP\EndPoint;
    use HP\Identification;

    /**
     * Dane rejestracyjne sprzedawcy / supplier legal registration data
     * @param string $name nazwa rejestrowa sprzedawcy / supplier registration name
     * @param string $companyId identyfikator rejestrowy sprzedawcy / supplier registration identifier
     * @param string $legalForm informacje: kapitał, ... / legal notes
     * @param string $companyIdScheme ICD Seller legal registration identifier identification scheme identifier
     * @return $this
     */
    public function setLegalEntity($name, $companyId = null, $legalForm = null, $companyIdScheme = null)
    {
        !$this->legalEntity && $this->legalEntity = new P\LegalEntity;

        $this->legalEntity->setName($name);
        $companyId && $this->legalEntity->setCompanyId($companyId, $companyIdScheme);
        $legalForm && $this->legalEntity->setLegalForm($legalForm);

        return $this;
    }

    /**
     *
     * @param string $companyId numer rejestracji podatkowej VAT (NIP) sprzedawcy
     * / the VAT registration number of the supplier
     * @param string $taxSchemeId typ podatku (VAT) / type of tax (VAT)
     * Mandatory element. For Seller VAT identifier (BT-31), use value “VAT”,
     * for the seller tax registration identifier (BT-32), use != "VAT"
     * @return $this
     */
    public function addTaxScheme($companyId, $taxSchemeId = 'VAT')
    {
        $this->taxScheme[] = new P\TaxScheme($companyId, $taxSchemeId);
        return $this;
    }

    protected function validate()
    {
        if(!$this->endpoint){
            throw new Ex\MissingArgumentException('Endpoint ID');
        }
        if(!$this->address){
            throw new Ex\MissingArgumentException('Party Postal Address missing');
        }
        if(count($this->taxScheme) > 2){
            throw new Ex\TooManyArgumentException('Party Tax Scheme', 2);
        }
        if(!$this->legalEntity){
            throw new Ex\MissingArgumentException('Party legal entity');
        }
        if(!$this->legalEntity->name){
            throw new Ex\MissingArgumentException('Party legal entity Registration name');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            [
                'name'       => $b.'EndpointID',
                'value'      => $this->endpoint,
                'attributes' => [
                    'schemeID'  => $this->endpointScheme
                ]
            ]
        ];
        foreach((array)$this->identification as $i => $scheme){
            $data[] = [
                'name'       => $a.'PartyIdentification',
                'value'      => [
                    [
                        'name' => $b.'ID',
                        'value' => $scheme,
                        'attributes' => call_user_func(function() use ($i){
                                $a =  [];
                                $this->identificationScheme[$i]
                                        && $a['schemeID'] = $this->identificationScheme[$i];

                                return $a;
                            })
                    ],
                ],
            ];
        }
        $this->name && $data[$a.'PartyName'] = [
            $b.'Name' => $this->name
        ];
        $data[$a.'PostalAddress'] = $this->address;
        foreach((array)$this->taxScheme as $i => $scheme){
            $data[] = [
                'name'       => $a.'PartyTaxScheme',
                'value'      => $scheme,
                'attributes' => []
            ];
        }
        $data[$a.'PartyLegalEntity'] = $this->legalEntity;
        $data[$a.'Contact'] = $this->contact;

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $p = $a.'Party/';

        $object = new self(
                D::get($p.$a.'PartyName/'.$b.'Name')
            );
        $object->endpoint             = D::get($p.$b.'EndpointID');
        $object->endpointScheme       = D::get($p.$b.'EndpointID/@schemeID');
        $object->identification       = D::getArray($p.$a.'PartyIdentification', $b.'ID');
        $object->identificationScheme = D::getArray($p.$a.'PartyIdentification', $b.'ID/@schemeID');
        $object->address              = D::get($p.$a.'PostalAddress');
        $object->taxScheme            = D::getArray($p.$a.'PartyTaxScheme');
        $object->legalEntity          = D::get($p.$a.'PartyLegalEntity');
        $object->contact              = D::get($p.$a.'Contact');

        return $object;
    }

}
