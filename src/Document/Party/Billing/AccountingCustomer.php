<?php

namespace SJRoyd\PEF\BIS\Document\Party\Billing;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document\Party as P;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Document\Party as HP;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $endpoint EndpointID
 * @property string $endpointScheme EndpointID/@schemeID (EAS)
 * @property string $identification PartyIdentification
 * @property string $identificationScheme PartyIdentification/@schemeID (ICD,SEPA)
 * @property P\Address $address
 * @property P\TaxScheme $taxScheme
 * @property P\LegalEntity $legalEntity
 * @property P\Contact $contact
 */
class AccountingCustomer extends P\Party
{
    use HP\Address;
    use HP\Contact;
    use HP\EndPoint;
    use HP\Identification;
    use HP\TaxScheme;

    /**
     * Dane rejestracyjne nabywcy / customer legal registration data
     * @param string $name nazwa rejestracyjna nabywcy / customer legal registration name
     * @param string $companyId identyfikator rejestracyjny nabywcy / customer legal registration identifier
     * @param string $companyIdScheme ICD Buyer legal registration identifier identification scheme identifier
     * @return $this
     */
    public function setLegalEntity($name, $companyId = null, $companyIdScheme = null)
    {
        !$this->legalEntity && $this->legalEntity = new P\LegalEntity;

        $this->legalEntity->setName($name);
        $companyId && $this->legalEntity->setCompanyId($companyId, $companyIdScheme);

        return $this;
    }

    protected function validate()
    {
        if(!$this->address){
            throw new Ex\MissingArgumentException('Party Postal Address');
        }
        if(!$this->legalEntity){
            throw new Ex\MissingArgumentException('Party legal entity');
        }
        if(!$this->legalEntity->name){
            throw new Ex\MissingArgumentException('Party legal entity Registration name');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            [
                'name'       => $b.'EndpointID',
                'value'      => $this->endpoint,
                'attributes' => [
                    'schemeID'  => $this->endpointScheme
                ]
            ]
        ];
        $this->identification && $data[$a.'PartyIdentification'] = [
            [
                'name'       => $b.'ID',
                'value'      => $this->identification,
                'attributes' => call_user_func(function(){
                        $a =  [];
                        $this->identificationScheme && $a['schemeID'] = $this->identificationScheme;

                        return $a;
                    })
            ]
        ];
        $this->name && $data[$a.'PartyName'] = [
            $b.'Name' => $this->name
        ];
        $data[$a.'PostalAddress'] = $this->address;
        $data[$a.'PartyTaxScheme'] = $this->taxScheme;
        $data[$a.'PartyLegalEntity'] = $this->legalEntity;
        $data[$a.'Contact'] = $this->contact;

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $p = $a.'Party/';

        $object = new self(
                D::get($p.$a.'PartyName/'.$b.'Name')
            );
        $object->endpoint             = D::get($p.$b.'EndpointID');
        $object->endpointScheme       = D::get($p.$b.'EndpointID/@schemeID');
        $object->identification       = D::get($p.$a.'PartyIdentification/'.$b.'ID');
        $object->identificationScheme = D::get($p.$a.'PartyIdentification/'.$b.'ID/@schemeID');
        $object->address              = D::get($p.$a.'PostalAddress');
        $object->taxScheme            = D::get($p.$a.'PartyTaxScheme');
        $object->legalEntity          = D::get($p.$a.'PartyLegalEntity');
        $object->contact              = D::get($p.$a.'Contact');

        return $object;
    }

}
