<?php

namespace SJRoyd\PEF\BIS\Document\Party\Billing;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document\Party as P;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Document\Party as HP;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $identification
 * @property string $identificationScheme ICD,SEPA
 * @property P\LegalEntity $legalEntity
 */
class Payee extends P\Party
{
    use HP\Identification;


    /**
     *
     * @param string $companyId Identyfikator odbiorcy płatności / Payee legal Id
     * @param string $companyIdScheme ICD Payee legal registration identifier identification scheme identifier
     * @return $this
     */
    public function setLegalEntity($companyId, $companyIdScheme = null)
    {
        $this->legalEntity = new P\LegalEntity;
        $this->legalEntity->setCompanyId($companyId, $companyIdScheme);
        return $this;
    }

    protected function validate()
    {
        if(!$this->name){
            throw new Ex\MissingArgumentException('Party name');
        }
        if(!$this->legalEntity){
            throw new Ex\MissingArgumentException('Party legal entity');
        }
        if(!$this->legalEntity->companyId){
            throw new Ex\MissingArgumentException('Party legal entity Company ID');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [];
        $this->identification && $data[$a.'PartyIdentification'] = [
            [
                'name'       => $b.'ID',
                'value'      => $this->identification,
                'attributes' => call_user_func(function(){
                        $a =  [];
                        $this->identificationScheme && $a['schemeID'] = $this->identificationScheme;

                        return $a;
                    })
            ]
        ];

        $data += [
            $a.'PartyName' => [
                $b.'Name' => $this->name
            ],
            $a.'PartyLegalEntity' => $this->legalEntity
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($a.'PartyName/'.$b.'Name')
            );
        $object->identification       = D::get($a.'PartyIdentification/'.$b.'ID');
        $object->identificationScheme = D::get($a.'PartyIdentification/'.$b.'ID/@schemeID');
        $object->legalEntity          = D::get($a.'PartyLegalEntity');

        return $object;
    }

}
