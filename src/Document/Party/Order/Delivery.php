<?php

namespace SJRoyd\PEF\BIS\Document\Party\Order;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document\Party as P;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Document\Party as HP;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $identification PartyIdentification
 * @property string $identificationScheme PartyIdentification/@schemeID (ICD,SEPA)
 * @property P\Contact $contact
 */
class Delivery extends P\Party
{
    use HP\Contact;
    use HP\Identification;


    protected function validate()
    {
        if(!$this->name){
            throw new Ex\MissingArgumentException('Party name');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [];
        $this->identification && $data[$a.'PartyIdentification'] = [
            [
                'name'       => $b.'ID',
                'value'      => $this->identification,
                'attributes' => call_user_func(function(){
                        $a =  [];
                        $this->identificationScheme && $a['schemeID'] = $this->identificationScheme;

                        return $a;
                    })
            ]
        ];
        $this->name && $data[$a.'PartyName'] = [
            $b.'Name' => $this->name
        ];
        $data[$a.'Contact']           = $this->contact;

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($a.'PartyName/'.$b.'Name')
            );
        $object->identification       = D::get($a.'PartyIdentification/'.$b.'ID');
        $object->identificationScheme = D::get($a.'PartyIdentification/'.$b.'ID/@schemeID');
        $object->contact              = D::get($a.'Contact');

        return $object;
    }

}
