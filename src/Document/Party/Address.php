<?php

namespace SJRoyd\PEF\BIS\Document\Party;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property string $street Street name
 * @property string $additionalStreet Additional Street name
 * @property string $city City name
 * @property string $postalCode Postal zone
 * @property string $countrySubentity Country subentity
 * @property string $addressLine Address line
 * @property string $countryCode Country Identification code (ISO 3166)
 */
class Address implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * linia adresu cz.1 (nazwa ulicy) / address line part 1 (street name)
     * @var string
     */
    protected $street;

    /**
     * linia adresu cz.2 (c.d. nazwa ulicy) / address line part 2 (street name)
     * @var string
     */
    protected $additionalStreet;

    /**
     * nazwa miejscowości / the name of the town
     * @var string
     */
    protected $city;

    /**
     * kod pocztowy / Zip (postal) code
     * @var string
     */
    protected $postalCode;

    /**
     * nazwa regionu / the region of the country name
     * @var string
     */
    protected $countrySubentity;

    /**
     * linia adresu cz.3 (dodatkowe informacje adresu)
     * / address line part 3 (additional address information)
     * @var string
     */
    protected $addressLine;

    /**
     * kod kraju / Country code
     * ISO 3166
     * @var string
     */
    protected $countryCode;

    public function __construct($countryCode)
    {
        $this->countryCode = $countryCode;
        // ISO3166
        Helper\CodeList::validate(
            Helper\CodeList::getCountryAlpha2($countryCode), $countryCode
        );
    }

    /**
     *
     * @param string $street
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     *
     * @param string $additionalStreet
     * @return $this
     */
    public function setAdditionalStreet($additionalStreet)
    {
        $this->additionalStreet = $additionalStreet;
        return $this;
    }

    /**
     *
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     *
     * @param string $postalCode
     * @return $this
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     *
     * @param string $countrySubentity
     * @return $this
     */
    public function setCountrySubentity($countrySubentity)
    {
        $this->countrySubentity = $countrySubentity;
        return $this;
    }

    /**
     *
     * @param string $addressLine
     * @return $this
     */
    public function setAddressLine($addressLine)
    {
        $this->addressLine = $addressLine;
        return $this;
    }


    public function validate()
    {
        if(!$this->countryCode){
            throw new Ex\MissingArgumentException('Country ID');
        }
    }


    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $data = [
            $b.'StreetName'           => $this->street,
            $b.'AdditionalStreetName' => $this->additionalStreet,
            $b.'CityName'             => $this->city,
            $b.'PostalZone'           => $this->postalCode,
            $b.'CountrySubentity'     => $this->countrySubentity,
            $a.'AddressLine'          => $this->addressLine ? [
                $b.'Line'                 => $this->addressLine
            ] : null,
            $a.'Country' => [
                $b.'IdentificationCode'   => $this->countryCode
            ],
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(D::get($a.'Country/'.$b.'IdentificationCode'));
        $object->street             = D::get($b.'StreetName');
        $object->additionalStreet   = D::get($b.'AdditionalStreetName');
        $object->city               = D::get($b.'CityName');
        $object->postalCode         = D::get($b.'PostalZone');
        $object->countrySubentity   = D::get($b.'CountrySubentity');
        $object->addressLine        = D::get($a.'AddressLine/'.$b.'Line');
        $object->countryCode        = D::get($a.'Country/'.$b.'IdentificationCode');

        return $object;
    }

}
