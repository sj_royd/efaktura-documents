<?php

namespace SJRoyd\PEF\BIS\Document;

use SJRoyd\PEF\BIS\Helper\Schema;

class InvoiceSchema extends Schema
{
    const NS = '{urn:oasis:names:specification:ubl:schema:xsd:Invoice-2}';
}
