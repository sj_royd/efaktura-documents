<?php

namespace SJRoyd\PEF\BIS\Document\Correction;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property TaxCategory $taxCategory kategoria opodatkowania / tax category
 */
class AllowanceCharge extends Document\AllowanceCharge
{
    /**
     * kategoria opodatkowania / tax category
     * @var TaxCategory
     */
    protected $taxCategory;

    /**
     * @param string $taxCategoryId
     * @param int $percent
     * @return TaxCategory
     */
    protected function newTaxCategory($taxCategoryId, $percent)
    {
        return new TaxCategory($taxCategoryId, $percent);
    }

    /**
     *
     * @param Document\AllowanceCharge $ac
     * @return self
     */
    public static function copy(Document\AllowanceCharge $ac)
    {
        $object = new self($ac->amount);
        $object->indicator        = $ac->indicator;
        $object->reasonCode       = $ac->reasonCode;
        $object->reason           = $ac->reason;
        $object->multiplierFactor = $ac->multiplierFactor;
        $object->baseAmount       = $ac->baseAmount;
        $ac->taxCategory &&
            $object->taxCategory  = TaxCategory::copy($ac->taxCategory);

        return $object;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        parent::xmlSerialize($writer, $a, $b);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        return parent::xmlDeserialize($reader, $a, $b);
    }

}
