<?php

namespace SJRoyd\PEF\BIS\Document\Correction;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property Period $period okres fakturowany
 * @property Reference\Additional $documentReference
 * Referencja do dokumentu powiązanego / Invoice line referenced object identifier
 * @property AllowanceCharge[] $allowancesCharges obciążenia lub upusty / charges or allowances
 * @property Line\Item $item
 * @property Line\Price $price
 */
class InvoiceLine extends Document\InvoiceLine
{

    /**
     * okres fakturowany
     * @var Period
     */
    protected $period;

    /**
     * Referencja do dokumentu powiązanego / Invoice line referenced object identifier
     * @var Reference\Additional
     */
    protected $documentReference;//

    /**
     * obciążenia lub upusty / charges or allowances
     * @var AllowanceCharge[]
     */
    protected $allowancesCharges = [];

    /**
     *
     * @var Line\Item
     */
    protected $item;

    /**
     *
     * @var Line\Price
     */
    protected $price;

    /**
     *
     * @param Document\InvoiceLine $line
     * @return self
     */
    public static function copy(Document\InvoiceLine $line)
    {
        $object = new self($line->id, $line->quantity, $line->quantityUnitCode);
        $object->note               = $line->note;
        $object->accountingCost     = $line->accountingCost;
        $object->netAmount          = $line->netAmount;
        $object->taxAmount          = $line->taxAmount;
        $object->grossAmount        = $line->grossAmount;
        $line->period &&
            $object->period             = Period::copy($line->period);
        $object->orderLineReference = $line->orderLineReference;
        $line->documentReference &&
            $object->documentReference  = Reference\Additional::copy($line->documentReference);
        $object->item               = Line\Item::copy($line->item);
        $object->price              = Line\Price::copy($line->price);
        foreach($line->allowancesCharges as $allowanceCharge){
            $object->allowancesCharges[] = AllowanceCharge::copy($allowanceCharge);
        }

        return $object;
    }

    /**
     * okres fakturowany
     * @param \DateTime|string $startDate
     * @param \DateTime|string $endDate
     * @return $this
     */
    public function setPeriod($startDate, $endDate)
    {
        $this->period = new Period($startDate, $endDate);
        return $this;
    }

    /**
     * Referencja do dokumentu powiązanego / Invoice line referenced object identifier
     * @param string $id
     * @param string $schemeId
     * @param bool $isInvoice
     * @return $this
     */
    public function setDocumentReference($id, $schemeId, $isInvoice = true)
    {
        !$this->documentReference
                && $this->documentReference = new Reference\Additional($id, $schemeId);
        $isInvoice && $this->documentReference->setTypeCode(130);
        return $this;
    }

    /**
     * obciążenia lub upusty / charges or allowances
     * @param float $amount kwota obciążenia/upustu / charge amount
     * @param int $percent procent opodatkowania / tax percentage
     * @param string $taxCategoryId kod kategorii opodatkowania obciążenia / charge tax category code
     * @return AllowanceCharge
     */
    public function addAllowanceCharge($amount, $percent = null, $taxCategoryId = null)
    {
        if(!$this->price){
            throw new \InvalidArgumentException('Price not set');
        }
        $allowanceCharge = new AllowanceCharge($amount, $percent, $taxCategoryId);
        $allowanceCharge->setCallback($this);
        $this->allowancesCharges[] = $allowanceCharge;

        return $allowanceCharge;
    }

    /**
     *
     * @param string $name
     * @param int $taxPercent
     * @param string $taxCode
     * @return Line\Item
     */
    public function item($name, $taxPercent, $taxCode = Helper\PEPPOL\TaxCategory::STANDARD)
    {
        if(!$this->item){
            $this->item = new Line\Item($name);
            $this->item->taxCategory($taxCode, $taxPercent);
        }
        $this->price && self::calculateAmounts($this);
        return $this->item;
    }

    /**
     *
     * @return Line\Price
     */
    public function price($unitPriceAmount)
    {
        !$this->price && $this->price
                = new Line\Price($unitPriceAmount, 1, $this->quantityUnitCode);

        $this->price->setCallback($this);

        $this->netAmount = $unitPriceAmount * $this->quantity;

        $this->item && self::calculateAmounts($this);
        return $this->price;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        parent::xmlSerialize($writer, $a, $b);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        return parent::xmlDeserialize($reader, $a, $b);
    }

}
