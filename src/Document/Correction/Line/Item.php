<?php

namespace SJRoyd\PEF\BIS\Document\Correction\Line;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property Document\Correction\TaxCategory $taxCategory
 */
class Item extends Document\Line\Item
{
    /**
     *
     * @var Document\Correction\TaxCategory
     */
    protected $taxCategory;

    /**
     *
     * @param Document\Line\Item $item
     * @return self
     */
    public static function copy(Document\Line\Item $item)
    {
        $object = new self($item->name);
        $object->description                    = $item->description;
        $object->buyerIdentification            = $item->buyerIdentification;
        $object->standardIdentification         = $item->standardIdentification;
        $object->standardIdentificationSchemeId = $item->standardIdentificationSchemeId;
        $object->originCountry                  = $item->originCountry;
        $object->classificationCode             = $item->classificationCode;
        $object->classificationCodeListId       = $item->classificationCodeListId;
        $object->classificationCodeListVersion  = $item->classificationCodeListVersion;
        $object->property                       = $item->property;
        $object->taxCategory = Document\Correction\TaxCategory::copy($item->taxCategory);

        return $object;
    }

    /**
     *
     * @param string $taxCode
     * @param int $percent
     * @return Document\Correction\TaxCategory
     */
    protected function newTaxCategory($taxCode, $percent)
    {
        return new Document\Correction\TaxCategory($taxCode, $percent);
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        parent::xmlSerialize($writer, $a, $b);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        return parent::xmlDeserialize($reader, $a, $b);
    }

}
