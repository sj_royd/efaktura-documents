<?php

namespace SJRoyd\PEF\BIS\Document\Correction\Line;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property Document\Correction\AllowanceCharge $allowance upust / allowance
 */
class Price extends Document\Line\Price 
{
    /**
     * upust / allowance
     * @var Document\Correction\AllowanceCharge
     */
    protected $allowance;

    /**
     * @param float $amount
     * @return Document\Correction\AllowanceCharge
     */
    protected function newAllowance($amount)
    {
        return new Document\Correction\AllowanceCharge($amount);
    }

    /**
     *
     * @param Document\Line\Price $price
     * @return self
     */
    public function copy(Document\Line\Price $price)
    {
        $object = new self(
                $price->amount,
                $price->quantity,
                $price->quantityCode
            );
        $price->allowance &&
                $object->allowance = Document\Correction\AllowanceCharge::copy($price->allowance);

        return $object;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        parent::xmlSerialize($writer, $a, $b);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        return parent::xmlDeserialize($reader, $a, $b);
    }

}
