<?php

namespace SJRoyd\PEF\BIS\Document\Correction\Difference;

use SJRoyd\PEF\BIS\Document\Correction;

class MonetaryTotal extends Correction\MonetaryTotal
{
    public function calculate(\SJRoyd\PEF\BIS\InvoiceCorrection $invoice)
    {
        $this->netAmount
                = $invoice->total->netAmount
                - $invoice->originInvoiceData->total->netAmount;
        $this->taxExclusiveAmount
                = $invoice->total->taxExclusiveAmount
                - $invoice->originInvoiceData->total->taxExclusiveAmount;
        $this->taxInclusiveAmount
                = $invoice->total->taxInclusiveAmount
                - $invoice->originInvoiceData->total->taxInclusiveAmount;
        $this->allowancesAmount
                = $invoice->total->allowancesAmount
                - $invoice->originInvoiceData->total->allowancesAmount;
        $this->chargesAmount
                = $invoice->total->chargesAmount
                - $invoice->originInvoiceData->total->chargesAmount;
        $this->prepaidAmount
                = $invoice->total->prepaidAmount
                - $invoice->originInvoiceData->total->prepaidAmount;
        $this->toPayRoundingAmount
                = $invoice->total->toPayRoundingAmount
                - $invoice->originInvoiceData->total->toPayRoundingAmount;
        $this->toPayAmount
                = $invoice->total->toPayAmount
                - $invoice->originInvoiceData->total->toPayAmount;
    }
}
