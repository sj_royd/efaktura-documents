<?php

namespace SJRoyd\PEF\BIS\Document\Correction\Difference;

use SJRoyd\PEF\BIS\Document\Correction;

class TaxTotal extends Correction\TaxTotal
{
    public function calculate(\SJRoyd\PEF\BIS\InvoiceCorrection $invoice)
    {
        $this->amount = $invoice->taxTotal()->amount - $invoice->originInvoiceData->taxTotal()->amount;

        $list = [];
        foreach($invoice->taxTotal()->subtotal as $st) {
            $tc = $st->category;
            $index = $tc->getIndex();
            $list[$index] = [
                $tc->id,
                $tc->percent,
                $st->netAmount,
                $st->amount
            ];
        }

        foreach($invoice->originInvoiceData->taxTotal()->subtotal as $st) {
            $tc = $st->category;
            $index = $tc->getIndex();

            if(array_key_exists($index, $list)) {
                $list[$index][2] -= $st->netAmount;
                $list[$index][3] -= $st->amount;
            } else {
                $list[$index] = [
                    $tc->id,
                    $tc->percent,
                    -$st->netAmount,
                    -$st->amount
                ];
            }
        }

        foreach($list as $i => $item) {
            $ts = new Correction\TaxSubtotal($item[1], $item[0]);
            $ts->setCurrency($invoice::$currencyCode);
            $ts->addAmounts($item[2], $item[3]);
            $this->subtotal[$i] = $ts;
        }
        ksort($this->subtotal);
    }
}
