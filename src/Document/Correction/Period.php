<?php

namespace SJRoyd\PEF\BIS\Document\Correction;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Schema as S;

class Period extends Document\Period 
{

    /**
     *
     * @param Document\Period $period
     * @return self
     */
    public static function copy(Document\Period $period)
    {
        $object = new self($period->startDate, $period->endDate);
        $object->descriptionCode = $period->descriptionCode;

        return $object;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        parent::xmlSerialize($writer, $a, $b);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        return parent::xmlDeserialize($reader, $a, $b);
    }
}
