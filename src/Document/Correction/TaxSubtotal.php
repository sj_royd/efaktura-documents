<?php

namespace SJRoyd\PEF\BIS\Document\Correction;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Schema as S;

class TaxSubtotal extends Document\TaxSubtotal
{

    /**
     * kategoria podatku / tax category
     * @var TaxCategory
     */
    protected $category;

    public function __construct($percent, $code)
    {
        $this->category = new TaxCategory($code, $percent);
    }

    /**
     *
     * @param Document\TaxSubtotal $subtotal
     * @return self
     */
    public static function copy(Document\TaxSubtotal $subtotal)
    {
        $object = new self(0,'S');
        $object->netAmount     = $subtotal->netAmount;
        $object->amount        = $subtotal->amount;
        $object->currency      = $subtotal->currency;
        $object->category      = TaxCategory::copy($subtotal->category);
        $object->grossAmount   = $subtotal->grossAmount;

        return $object;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        parent::xmlSerialize($writer, $a, $b);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        return parent::xmlDeserialize($reader, $a, $b);
    }

}
