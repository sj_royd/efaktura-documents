<?php

namespace SJRoyd\PEF\BIS\Document\Correction;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Schema as S;

class TaxCategory extends Document\TaxCategory 
{
    /**
     *
     * @param Document\TaxCategory $category
     * @return self
     */
    public static function copy(Document\TaxCategory $category)
    {
        $object = new self($category->id, $category->percent);
        $object->extemptionReasonCode = $category->extemptionReasonCode;
        $object->extemptionReason     = $category->extemptionReason;
        $object->schemeId             = $category->schemeId;

        return $object;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        parent::xmlSerialize($writer, $a, $b);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        return parent::xmlDeserialize($reader, $a, $b);
    }
}
