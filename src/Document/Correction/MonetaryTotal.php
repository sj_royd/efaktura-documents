<?php

namespace SJRoyd\PEF\BIS\Document\Correction;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Schema as S;

class MonetaryTotal extends Document\MonetaryTotal 
{

    /**
     *
     * @param Document\MonetaryTotal $total
     * @return self
     */
    public static function copy(Document\MonetaryTotal $total)
    {
        $object = new self();
        $object->netAmount           = $total->netAmount;
        $object->taxExclusiveAmount  = $total->taxExclusiveAmount;
        $object->taxInclusiveAmount  = $total->taxInclusiveAmount;
        $object->allowancesAmount    = $total->allowancesAmount;
        $object->chargesAmount       = $total->chargesAmount;
        $object->prepaidAmount       = $total->prepaidAmount;
        $object->toPayRoundingAmount = $total->toPayRoundingAmount;
        $object->toPayAmount         = $total->toPayAmount;

        return $object;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        parent::xmlSerialize($writer, $a, $b);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        return parent::xmlDeserialize($reader, $a, $b);
    }
}
