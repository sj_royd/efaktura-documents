<?php

namespace SJRoyd\PEF\BIS\Document\Correction;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Document as Doc;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property AllowanceCharge[] $allowancesCharges obciążenia lub upusty / charges or allowances
 * @property TaxTotal[] $taxTotal podsumowanie podatku / tax totals
 * @property MonetaryTotal $total Podsumowanie dla faktury / Invoice document totals
 */
class InvoiceDifference extends Doc\Document implements XmlSerializable, XmlDeserializable
{

    /**
     *
     * @return Difference\TaxTotal
     */
    public function taxTotal()
    {
        !$this->taxTotal && $this->taxTotal[] = new Difference\TaxTotal(self::$currencyCode);
        return $this->taxTotal[0];
    }

    /**
     *
     * @return Difference\MonetaryTotal
     */
    public function total()
    {
        !$this->total && $this->total = new Difference\MonetaryTotal();
        return $this->total;
    }

    public function calculate(\SJRoyd\PEF\BIS\InvoiceCorrection $invoice)
    {
        $this->taxTotal()->calculate($invoice);
        $this->total()->calculate($invoice);

        $list = [];
        foreach($invoice->allowancesCharges as $ac) {
            $tc = $ac->taxCategory;
            $index = $tc->getIndex().'-'.(int)$ac->indicator;
            if(array_key_exists($index, $list)) {
                $list[$index][2] += $ac->amount;
            } else {
                $list[$index] = [
                    $tc->id,
                    $tc->percent,
                    $ac->amount,
                    $ac->indicator
                ];
            }
        }

        foreach($invoice->originInvoiceData->allowancesCharges as $ac) {
            $tc = $ac->taxCategory;
            $index = $tc->getIndex().'-'.(int)$ac->indicator;

            if(array_key_exists($index, $list)) {
                $list[$index][2] -= $ac->amount;
            } else {
                $list[$index] = [
                    $tc->id,
                    $tc->percent,
                    -$ac->amount,
                    $ac->indicator
                ];
            }
        }

        foreach($list as $i => $item) {
            $ac = new Difference\AllowanceCharge($item[2], $item[1], $item[0]);
            $item[3] ? $ac->setCharge() : $ac->setAllowance();
            $this->allowancesCharges[$i] = $ac;
        }

    }

    /**
     * The xmlSerialize method is called during xml writing.
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        $data = [];
        foreach($this->allowancesCharges as $allowanceCharge){
            $data[] = [
                'name' => $a.'AllowanceCharge',
                'value' => $allowanceCharge
            ];
        }
        foreach($this->taxTotal as $tax){
            $data[] = [
                'name' => $a.'TaxTotal',
                'value' => $tax
            ];
        }
        $data[$a.'LegalMonetaryTotal'] = $this->total;

        $writer->write(Helper\cleanArray($data));
    }

    /**
     * The deserialize method is called during xml parsing.
     * @return mixed
     */
    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        D::prepare($reader);

        $object = new self();
        $object->allowancesCharges  = D::getArray($a.'AllowanceCharge');
        $object->taxTotal           = D::getArray($a.'TaxTotal');
        $object->total              = D::get($a.'LegalMonetaryTotal');

        return $object;
    }

    public static function parse($xml) {

    }

}
