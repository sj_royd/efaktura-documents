<?php

namespace SJRoyd\PEF\BIS\Document\Correction;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Schema as S;

class TaxTotal extends Document\TaxTotal 
{

    /**
     * dane dla grupy opodatkowania / tax subtotals
     * @var TaxSubtotal[]
     */
    protected $subtotal = [];

    /**
     *
     * @param Document\TaxTotal $taxTotal
     * @return self
     */
    public static function copy(Document\TaxTotal $taxTotal)
    {
        $object = new self($taxTotal->currency);
        $object->amount = $taxTotal->amount;
        foreach($taxTotal->subtotal as $subtotal){
            $object->subtotal[] = TaxSubtotal::copy($subtotal);
        }

        return $object;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        parent::xmlSerialize($writer, $a, $b);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        return parent::xmlDeserialize($reader, $a, $b);
    }

}
