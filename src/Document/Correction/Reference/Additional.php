<?php

namespace SJRoyd\PEF\BIS\Document\Correction\Reference;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property Attachment $attachment
 */
class Additional extends Document\Reference\Additional 
{
    /**
     * @return $this
     */
    public function attachment()
    {
        $this->attachment = new Attachment();
        return $this->attachment;
    }

    /**
     *
     * @param Document\Reference\Additional $additional
     * @return self
     */
    public static function copy(Document\Reference\Additional $additional)
    {
        $object = new self(
                $additional->id,
                $additional->schemeId
            );

        $object->documentDescription = $additional->documentDescription;
        $object->documentTypeCode    = $additional->documentTypeCode;
        $object->attachment &&
            $object->attachment      = Attachment::copy($additional->attachment);

        return $object;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        parent::xmlSerialize($writer, $a, $b);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        return parent::xmlDeserialize($reader, $a, $b);
    }

}
