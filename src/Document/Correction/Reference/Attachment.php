<?php

namespace SJRoyd\PEF\BIS\Document\Correction\Reference;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use SJRoyd\PEF\BIS\Document;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Schema as S;

class Attachment extends Document\Reference\Attachment 
{
    /**
     *
     * @param Document\Reference\Attachment $attachment
     * @return self
     */
    public static function copy(Document\Reference\Attachment $attachment)
    {
        $object = new self();
        $object->externalUri      = $attachment->externalUri;
        $object->embeddedDocument = $attachment->embeddedDocument;
        $object->embeddedMimeCode = $attachment->embeddedMimeCode;
        $object->embeddedFileName = $attachment->embeddedFileName;
        $object->embeddedSize     = $attachment->embeddedSize;

        return $object;
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        parent::xmlSerialize($writer, $a, $b);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        return parent::xmlDeserialize($reader, $a, $b);
    }
}
