<?php

namespace SJRoyd\PEF\BIS\Document\Correction;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Document as Doc;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\PEPPOL;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property AllowanceCharge[] $allowancesCharges obciążenia lub upusty / charges or allowances
 * @property TaxTotal[] $taxTotal podsumowanie podatku / tax totals
 * @property MonetaryTotal $total Podsumowanie dla faktury / Invoice document totals
 * @property InvoiceLine $lines
 */
class Invoice extends Doc\Document implements XmlSerializable, XmlDeserializable, Helper\Callback
{

    /**
     *
     * @param type $amount
     * @param type $percent
     * @param type $taxCategoryId
     * @return AllowanceCharge
     */
    public function addAllowanceCharge($amount, $percent = null, $taxCategoryId = null)
    {
        $allowanceCharge = new AllowanceCharge($amount, $percent, $taxCategoryId);
        $this->allowancesCharges[] = $allowanceCharge;
        return $allowanceCharge;
    }

    /**
     *
     * @return TaxTotal
     */
    public function taxTotal()
    {
        !$this->taxTotal && $this->taxTotal[] = new TaxTotal(self::$currencyCode);
        return $this->taxTotal[0];
    }

    /**
     *
     * @return MonetaryTotal
     */
    public function total()
    {
        !$this->total && $this->total = new MonetaryTotal();
        return $this->total;
    }


    /**
     *
     * @param string $name
     * @param float $unitPriceAmount
     * @param int $taxPercent
     * @param string $taxCode
     * @return InvoiceLine
     */
    public function line($name, $quantity, $quantityCode, $unitPriceAmount, $taxPercent, $taxCode)
    {
        static $id = 0;
        $id++;
        $line = new InvoiceLine($id, $quantity, $quantityCode);
        $line->setCallback($this);
//        $line->setNetAmount($netAmount);
        $line->item($name, $taxPercent, $taxCode);
        $line->price($unitPriceAmount);
        $this->lines[] = $line;
        $this->callback();
        return $line;
    }

    public function callback($item = null)
    {
        $this->total()->calculateAmounts($this->lines, $this->allowancesCharges);
        $this->taxTotal()->calculateAmounts($this->lines, $this->allowancesCharges);
    }

    protected function validate()
    {
        foreach($this->taxTotal[0]->subtotal as $index => $tax) {
            switch($index){
                case 0: break;
                case PEPPOL\TaxCategory::EXEMPT.'-0':
                case PEPPOL\TaxCategory::SERVICE_OUTSIDE_SCOPE.'-0':
                    if(!$tax->category->extemptionReason || !$tax->category->extemptionReasonCode){
                        throw new \InvalidArgumentException('No reason of extemption in tax category');
                    }
                    break;
            }
        }
    }

    /**
     *
     * @param \SJRoyd\PEF\BIS\Invoice $invoice
     * @return self
     */
    public static function copy(\SJRoyd\PEF\BIS\Invoice $invoice)
    {
        $object = new self();
        foreach($invoice->allowancesCharges as $allowanceCharge){
            $object->allowancesCharges[] = AllowanceCharge::copy($allowanceCharge);
        }
        foreach($invoice->taxTotal as $tax){
            $object->taxTotal[] = TaxTotal::copy($tax);
        }
        $object->total = MonetaryTotal::copy($invoice->total);
        foreach($invoice->lines as $line){
            $object->lines[] = InvoiceLine::copy($line);
        }

        return $object;
    }

    /**
     * The xmlSerialize method is called during xml writing.
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        $this->validate();
        $data = [];
        foreach($this->allowancesCharges as $allowanceCharge){
            $data[] = [
                'name' => $a.'AllowanceCharge',
                'value' => $allowanceCharge
            ];
        }
        foreach($this->taxTotal as $tax){
            $data[] = [
                'name' => $a.'TaxTotal',
                'value' => $tax
            ];
        }
        $data[$a.'LegalMonetaryTotal'] = $this->total;
        foreach($this->lines as $line){
            $data[] = [
                'name' => $a.'InvoiceLine',
                'value' => $line
            ];
        }

        $writer->write($data);
    }

    /**
     * The deserialize method is called during xml parsing.
     * @return mixed
     */
    public static function xmlDeserialize(Reader $reader, $a = S::CAC_PL, $b = S::CBC_PL)
    {
        D::prepare($reader);

        $object = new self();
        $object->allowancesCharges  = D::getArray($a.'AllowanceCharge');
        $object->taxTotal           = D::getArray($a.'TaxTotal');
        $object->total              = D::get($a.'LegalMonetaryTotal');

        $object->lines = D::getArray($a.'InvoiceLine');


        return $object;
    }

    public static function parse($xml)
    {

    }

}
