<?php

namespace SJRoyd\PEF\BIS\Document;

use SJRoyd\PEF\BIS\Helper\Schema;

class OrderSchema extends Schema
{
    const NS = '{urn:oasis:names:specification:ubl:schema:xsd:Order-2}';
}
