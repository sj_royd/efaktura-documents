<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 *
 * @property string $id kod kategorii opodatkowania obciążenia / charge tax category code
 * @property int $percent procent opodatkowania / tax percentage
 * @property string $extemptionReasonCode VAT exemption reason code
 * @property string $extemptionReason VAT exemption reason text
 * @property string $schemeId wskaźnik rodzaju podatku (VAT) / tax scheme code (VAT)
 */
class TaxCategory implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * kod kategorii opodatkowania obciążenia / charge tax category code
     * UNCL5305
     * @var string
     */
    protected $id;

    /**
     * procent opodatkowania / tax percentage
     * @var int
     */
    protected $percent;

    /**
     * VAT exemption reason code
     * @var string
     */
    protected $extemptionReasonCode;

    /**
     * VAT exemption reason text
     * @var string
     */
    protected $extemptionReason;

    /**
     * wskaźnik rodzaju podatku (VAT) / tax scheme code (VAT)
     * UN/ECE 5153
     * @var string
     */
    protected $schemeId = 'VAT';

    /**
     *
     * @param bool $indicator wskaźnik obciążenia / charge indicator
     * @param float $amount kwota obciążenia / charge amount
     * @param int $percent procent opodatkowania / tax percentage
     * @param string $taxCategoryId kod kategorii opodatkowania obciążenia / charge tax category code
     */
    public function __construct($id, $percent = 0)
    {
        $this->id = $id;
        // UNCL5305
        Helper\CodeList::validate(
            Helper\CodeList::getTaxCategory($id), $id
        );
        switch($id){
            case Helper\PEPPOL\TaxCategory::ITEM_FREE_EXPORT:
            case Helper\PEPPOL\TaxCategory::EXEMPT:
            case Helper\PEPPOL\TaxCategory::VAT_EXEMPT_EEA:
            case Helper\PEPPOL\TaxCategory::ZERO_RATED:
            case Helper\PEPPOL\TaxCategory::SERVICE_OUTSIDE_SCOPE:
                $percent = 0;
                break;
        }
        $this->percent = $percent;
    }

    /**
     * VAT exemption reason
     * @param string $extemptionReason
     * @return $this
     */
    public function setExtemptionReason($extemptionReason)
    {
        $this->extemptionReason = $extemptionReason;
        return $this;
    }

    /**
     * VAT exemption reason code
     * @param string $code
     * @return $this
     */
    public function setExtemptionReasonCode($code)
    {
        $this->extemptionReasonCode = $code;
        return $this;
    }

    /**
     * wskaźnik rodzaju podatku (VAT) / tax scheme code (VAT)
     * @param string $schemeId
     * @return $this
     */
    public function setSchemeId($schemeId)
    {
        $this->schemeId = $schemeId;
        return $this;
    }

    public function getIndex()
    {
        return "{$this->id}-{$this->percent}";
    }

    protected function validate()
    {
        if(is_null($this->id)){
            throw new Ex\MissingArgumentException('ID');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $b.'ID'                     => $this->id,
            $b.'Percent'                => $this->percent,
            $b.'TaxExemptionReasonCode' => $this->extemptionReasonCode,
            $b.'TaxExemptionReason'     => $this->extemptionReason,
            $a.'TaxScheme'              => [
                $b.'ID' => $this->schemeId
            ]
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($b.'ID'),
                D::cast(D::get($b.'Percent'), D::CAST_FLOAT)
            );
        $object->extemptionReasonCode = D::get($b.'TaxExemptionReasonCode');
        $object->extemptionReason     = D::get($b.'TaxExemptionReason');
        $object->schemeId             = D::get($a.'TaxScheme/'.$b.'ID');

        return $object;
    }

}
