<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * dane dla grupy opodatkowania / tax subtotals
 * @property float $netAmount podstawa opodatkowania / taxable amount
 * @property float $grossAmount wartość z podatkiem / amount with tax included
 * @property float $amount wartość podatku / tax total
 * @property string $currency
 * @property TaxCategory $category kategoria podatku / tax category
 */
class TaxSubtotal implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * podstawa opodatkowania / taxable amount
     * @var float
     */
    protected $netAmount = 0;

    /**
     * wartość z podatkiem / amount with tax included
     * @var float
     */
    protected $grossAmount = 0;

    /**
     * wartość podatku / tax amount
     * @var float
     */
    protected $amount = 0;

    /**
     * identyfikator waluty / tax currency
     * @var string
     */
    protected $currency;

    /**
     * kategoria podatku / tax category
     * @var TaxCategory
     */
    protected $category;

    public function __construct($percent, $code)
    {
        $this->category = new TaxCategory($code, $percent);
    }

    /**
     *
     * @param float $netAmount podstawa opodatkowania / taxable amount
     * @param float $taxAmount wartość podatku / tax amount
     */
    public function addAmounts($netAmount, $taxAmount = 0)
    {
        $this->netAmount += $netAmount;
        $this->amount += $taxAmount;
        $this->grossAmount += ($netAmount + $taxAmount);
    }

    /**
     * identyfikator waluty / tax currency
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }


    protected function validate()
    {
        if(is_nan($this->netAmount)){
            throw new Ex\WrongArgumentException('Subtotal Taxable Amount');
        }
        if(is_nan($this->amount)){
            throw new Ex\WrongArgumentException('Subtotal Tax Amount');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $writer->write([
            [
                'name'       => $b.'TaxableAmount',
                'value'      => Helper\amountFormat($this->netAmount),
                'attributes' => [
                    'currencyID' => $this->currency
                ]
            ],
            [
                'name'       => $b.'TaxAmount',
                'value'      => Helper\amountFormat($this->amount),
                'attributes' => [
                    'currencyID' => $this->currency
                ]
            ],
            $a.'TaxCategory' => $this->category
        ]);
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(0,'S');
        $object->netAmount     = D::cast(D::get($b.'TaxableAmount'), D::CAST_FLOAT);
        $object->amount        = D::cast(D::get($b.'TaxAmount'), D::CAST_FLOAT);
        $object->currency      = D::get($b.'TaxAmount/@currencyID');
        $object->category      = D::get($a.'TaxCategory');
        $object->grossAmount   = ($object->netAmount + $object->amount);

        return $object;
    }

}
