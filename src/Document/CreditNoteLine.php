<?php

namespace SJRoyd\PEF\BIS\Document;

use Sabre\Xml\Reader;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Exception as Ex;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\Getter;
use SJRoyd\PEF\BIS\Helper\Schema as S;

/**
 * @property int $id numer pozycji faktury / Line (position) number
 * @property string $note opis szczegółowy dla pozycji księgowanej
 * @property float $netAmount wartość pozycji
 * @property string $accountingCost stanowisko kosztów
 */
class CreditNoteLine implements XmlSerializable, XmlDeserializable
{
    use Getter;

    /**
     * numer pozycji faktury / Line (position) number
     * @var int
     */
    protected $id;

    /**
     * opis szczegółowy dla pozycji księgowanej
     * @var string
     */
    protected $note;

    /**
     * wartość pozycji
     * @var float
     */
    protected $netAmount;

    /**
     * stanowisko kosztów
     * @var float
     */
    protected $accountingCost;

    /**
     *
     * @param int $id numer pozycji faktury / Line (position) number
     * @param string $note opis szczegółowy dla pozycji księgowanej
     * @param float $amount wartość dla pozycji
     */
    public function __construct($id, $note, $amount)
    {
        $this->id = $id;
        $this->note = $note;
        $this->netAmount = $amount;
    }

    /**
     * pozycja kosztów u nabywcy / Invoice line Buyer accounting reference
     * @param string $accountingCost
     * @return $this
     */
    public function setAccountingCost($accountingCost)
    {
        $this->accountingCost = $accountingCost;
        return $this;
    }

    protected function validate()
    {
        if(!is_numeric($this->id)){
            throw new Ex\WrongArgumentException('ID');
        }
        if(!$this->note){
            throw new Ex\MissingArgumentException('Line Note');
        }
        if(!is_numeric($this->netAmount)){
            throw new Ex\WrongArgumentException('Line Extension Amount');
        }
    }

    public function xmlSerialize(Writer $writer, $a = S::CAC, $b = S::CBC)
    {
        $this->validate();

        $data = [
            $b.'ID' => $this->id,
            $b.'Note' => $this->note,
            [
                'name' => $b.'LineExtensionAmount',
                'value' => Helper\amountFormat($this->netAmount),
                'attributes' => [
                    'currencyID' => Document::$currencyCode
                ]
            ],
            $b.'AccountingCost' => $this->accountingCost
        ];

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $a = S::CAC, $b = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::cast(D::get($b.'ID'), D::CAST_INT),
                D::get($b.'Note'),
                D::cast(D::get($b.'LineExtensionAmount'), D::CAST_FLOAT)
            );
        $object->accountingCost = D::get($b.'AccountingCost');

        return $object;
    }

}
