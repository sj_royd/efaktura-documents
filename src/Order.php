<?php

namespace SJRoyd\PEF\BIS;

use Sabre\Xml\Reader;
use Sabre\Xml\Service;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Document as Doc;
use SJRoyd\PEF\BIS\Document\OrderSchema as S;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * @property string $salesOrderId Referencja do zamówienia
 * @property string $id numer faktury / invoice number
 * @property \DateTime $issueDate data wystawienia / issue date
 * @property \DateTime $issueTime czas wystawienia / issue time
 * @property int $typeCode identyfikator typu faktury / Invoice type code
 * @property string $note uwagi tekstowe do faktury / Invoice note
 * @property string $customerReference referencja klienta
 * @property string $accountingCost znak stanowiska kosztów u nabywcy / Buyer accounting reference
 * @property Doc\Period $period okres ważności / validity period
 * @property Doc\Reference\QuotationDocument $quotationReference referencje do promocji
 * @property Doc\Reference\Order $orderReference referencje do zamówienia
 * @property Doc\Reference\OriginatorDocument $originatorReference referencje do dokumentu zamawiającego
 * / buyer document reference
 * @property Doc\Reference\Additional[] $additionalReferences
 * @property Doc\Reference\Contract $contractReference referencje do umowy
 * @property Doc\Reference\Project $projectReference referencja do projektu / Project reference
 * @property Doc\Party\Order\BuyerCustomer $buyerCustomer dane odbiorcy
 * @property Doc\Party\Order\SellerSupplier $sellerSupplier dane dostawcy
 * @property Doc\Party\Order\OriginatorCustomer $originatorCustomer dane zamawiającego
 * @property Doc\Party\Order\AccountingCustomer $accountingCustomer dane nabywcy
 * @property Doc\OrderDelivery $delivery miejsce dostawy
 * @property Doc\DeliveryTerms $deliveryTerms Warunki dostawy
 * @property string $paymentTerms Warunki płatności
 * @property Doc\AllowanceCharge[] $allowancesCharges obciążenia lub upusty / charges or allowances
 * @property Doc\TaxTotal[] $taxTotal podsumowanie podatku / tax totals
 * @property Doc\MonetaryTotal $total przewidywane sumy dla zamówienia
 * @property Doc\OrderLine[] $lines
 */
class Order extends Document\Document implements XmlSerializable, XmlDeserializable, Helper\Callback
{
    /**
     * wersja komunikatu / message version
     * @var string
     */
    protected $customizationID = 'urn:fdc:peppol.eu:poacc:trns:order:3';

    /**
     * identyfikator procesu biznesowego / business process identifier
     * @var string
     */
    protected $profileID = 'urn:fdc:peppol.eu:poacc:bis:order_only:3';

    /**
     *
     * @param string $id                  numer komunikatu
     * @param \DateTime|string $issueDate data zamówienia
     * @param string $currency            Currency code
     */
    public function __construct($id, $issueDate, $currency = PEPPOL\Currency::PLN)
    {
        $this->id = $id;
        $this->issueDate = Helper\date($issueDate);
        if($this->issueDate->format('His')){
            $this->issueTime = $this->issueDate;
        }
//        $this->typeCode = PEPPOL\OrderTypeCode::ORDER;
        $this->currency = $currency;
        self::$currencyCode = $currency;
        Helper\CodeList::validate(
            Helper\CodeList::getCurrency($currency), $currency
        );
    }

    /**
     * Referencja do zamówienia
     * @param string $id
     * @return $this
     */
    public function setSalesOrderId($id)
    {
        $this->salesOrderId = $id;
        return $this;
    }

    public function setIssueTimeIndicator()
    {
        $this->issueTime = true;
        return $this;
    }

    /**
     * @param int $type PEPPOL\OrderTypeCode::ORDER
     * @return $this
     */
    public function setTypeCode($type)
    {
        $this->typeCode = $type;
        Helper\CodeList::validate(
            Helper\CodeList::getOrder($type), $type
        );
        return $this;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @param string $accountingCost
     * @return $this
     */
    public function setAccountingCost($accountingCost)
    {
        $this->accountingCost = $accountingCost;
        return $this;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setCustomerReference($reference)
    {
        $this->customerReference = $$reference;
        return $this;
    }

    /**
     * @param \DateTime|string $endDate
     * @return $this
     */
    public function setValidityPeriod($endDate)
    {
        $this->period = new Document\Period(null, $endDate);
        return $this;
    }

    /**
     * Referencja do promocji
     * @param string $reference
     * @return $this
     */
    public function setQuotationReference($reference)
    {
        $this->quotationReference = new Doc\Reference\QuotationDocument($reference);
        return $this;
    }

    /**
     * Referencja do zamówienia
     * @param string $reference
     * @return $this
     */
    public function setOrderReference($reference)
    {
        $this->orderReference = new Doc\Reference\Order($reference);
        return $this;
    }

    /**
     * Referencja do dokumentu zamawiającvego
     * @param string $reference
     * @return $this
     */
    public function setOriginatorReference($reference)
    {
        $this->originatorReference = new Doc\Reference\OriginatorDocument($reference);
        return $this;
    }

    /**
     *
     * @param string $id
     * @param string $schemeId
     * @return Doc\Reference\Additional
     */
    public function additionalReference($id, $schemeId)
    {
        $reference = new Doc\Reference\Additional($id, $schemeId);
        $this->additionalReferences[] = $reference;
        return $reference;
    }

    /**
     * Referencja do umowy
     * @param string $reference
     * @return $this
     */
    public function setContract($reference)
    {
        $this->contractReference = new Doc\Reference\Contract($reference);
        return $this;
    }

    /**
     * @param string $name
     * @return Doc\Party\Order\BuyerCustomer
     */
    public function buyer($name = null)
    {
        $this->buyerCustomer = new Doc\Party\Order\BuyerCustomer($name);
        return $this->buyerCustomer;
    }

    /**
     * @param string $name
     * @return Doc\Party\Order\SellerSupplier
     */
    public function seller($name = null)
    {
        $this->sellerSupplier = new Doc\Party\Order\SellerSupplier($name);
        return $this->sellerSupplier;
    }

    /**
     * @param string $name
     * @return Doc\Party\Order\OriginatorCustomer
     */
    public function originator($name = null)
    {
        $this->originatorCustomer = new Doc\Party\Order\OriginatorCustomer($name);
        return $this->originatorCustomer;
    }

    /**
     * @param string $name
     * @return Doc\Party\Order\AccountingCustomer
     */
    public function customer($name = null)
    {
        $this->accountingCustomer = new Doc\Party\Order\AccountingCustomer($name);
        return $this->accountingCustomer;
    }

    /**
     *
     * @param string $carrierName
     * @return Doc\OrderDelivery
     */
    public function delivery($carrierName)
    {
        $this->delivery = new Doc\OrderDelivery($carrierName);
        return $this->delivery;
    }

    /**
     *
     * @return Doc\DeliveryTerms
     */
    public function deliveryTerms()
    {
        !$this->deliveryTerms && $this->deliveryTerms = new Doc\DeliveryTerms();
        return $this->deliveryTerms;
    }

    /**
     *
     * @param string $paymentTerms
     * @return $this
     */
    public function setPaymentTerms($paymentTerms)
    {
        $this->paymentTerms = $paymentTerms;
        return $this;
    }

    /**
     *
     * @param float $amount
     * @param int $percent
     * @param string $taxCategoryId
     * @return Doc\AllowanceCharge
     */
    public function addAllowanceCharge($amount, $percent = null, $taxCategoryId = null)
    {
        $allowanceCharge = new Doc\AllowanceCharge($amount, $percent, $taxCategoryId);
        $this->allowancesCharges[] = $allowanceCharge;
        $this->callback();
        return $allowanceCharge;
    }

    /**
     *
     * @return Doc\TaxTotal
     */
    public function taxTotal()
    {
        !$this->taxTotal && $this->taxTotal[] = new Doc\TaxTotal(self::$currencyCode, false);
        return $this->taxTotal[0];
    }

    /**
     *
     * @return Doc\MonetaryTotal
     */
    public function total()
    {
        !$this->total && $this->total = new Doc\MonetaryTotal();
        return $this->total;
    }

    /**
     *
     * @param string $name
     * @param float $unitPriceAmount
     * @param int $taxPercent
     * @param string $taxCode
     * @return Doc\OrderLine
     */
    public function line($name, $quantity, $quantityCode, $unitPriceAmount, $taxPercent, $taxCode)
    {
        static $id = 0;
        $id++;
        $line = new Doc\OrderLine($id, $quantity, $quantityCode);
        $line->setCallback($this);
//        $line->setNetAmount($netAmount);
        $line->item($name, $taxPercent, $taxCode);
        $line->price($unitPriceAmount);
        $this->lines[] = $line;
        $this->callback();
        return $line;
    }

    public function callback($item = null)
    {
        $this->total()->calculateAmounts($this->lines, $this->allowancesCharges);
        $this->taxTotal()->calculateAmounts($this->lines, $this->allowancesCharges);
    }

    protected function validate()
    {
    }

    public function xmlSerialize(Writer $writer, $cac = S::CAC, $cbc = S::CBC)
    {
        $this->validate();
        $data = [
            $cbc.'CustomizationID'      => $this->customizationID,
            $cbc.'ProfileID'            => $this->profileID,
            $cbc.'ID'                   => $this->id,
            $cbc.'SalesOrderID'         => $this->salesOrderId,
            $cbc.'IssueDate'            => Helper\dateFormat($this->issueDate),
            $cbc.'IssueTime'            => Helper\dateFormat($this->issueTime, 'H:i:s'),
            $cbc.'OrderTypeCode'        => $this->typeCode,
            $cbc.'Note'                 => $this->note, //
            $cbc.'DocumentCurrencyCode' => self::$currencyCode,
            $cbc.'CustomerReference'    => $this->customerReference, //
            $cbc.'AccountingCost'       => $this->accountingCost, //
            $cac.'ValidityPeriod'       => $this->period, //

            $cac.'QuotationDocumentReference'   => $this->quotationReference,
            $cac.'OrderDocumentReference'       => $this->orderReference,
            $cac.'OriginatorDocumentReference'  => $this->originatorReference,
        ];
        foreach($this->additionalReferences as $ref){
            $data[] = [
                'name' => $cac.'AdditionalDocumentReference',
                'value' => $ref
            ];
        }
        $data += [
            $cac.'Contract'     => $this->contractReference,

            $cac.'BuyerCustomerParty'   => [
                $cac.'Party' => $this->buyerCustomer
            ],
            $cac.'SellerSupplierParty'      => [
                $cac.'Party' => $this->sellerSupplier
            ],
            $cac.'OriginatorCustomerParty'  => $this->originatorCustomer ? [
                $cac.'Party' => $this->originatorCustomer
            ] : null,
            $cac.'AccountingCustomerParty'  => $this->accountingCustomer ? [
                $cac.'Party' => $this->accountingCustomer
            ] : null,

            $cac.'Delivery'     => $this->delivery,
            $cac.'DeliveryTerms'=> $this->deliveryTerms,
            $cac.'PaymentTerms' => $this->paymentTerms ? [
                $cbc.'Note' => $this->paymentTerms
            ] : null,
        ];
        foreach($this->allowancesCharges as $allowanceCharge){
            $data[] = [
                'name' => $cac.'AllowanceCharge',
                'value' => $allowanceCharge
            ];
        }
        $data += [
            $cac.'TaxTotal' => $this->taxTotal,
            $cac.'AnticipatedMonetaryTotal' => $this->total
        ];
        foreach($this->lines as $line){
            $data[] = [
                'name' => $cac.'OrderLine',
                'value' => $line
            ];
        }

        $writer->write(Helper\cleanArray($data));
    }

    public static function xmlDeserialize(Reader $reader, $cac = S::CAC, $cbc = S::CBC)
    {
        D::prepare($reader);

        $date = D::get($cbc.'IssueDate');
        $time = D::get($cbc.'IssueTime');
        $oDate = new \DateTime($date.' '.$time);

        $object = new self(
                D::get($cbc.'ID'),
                $oDate,
                D::get($cbc.'DocumentCurrencyCode')
            );
        $object->parse           = true;
        $object->salesOrderId    = D::get($cbc.'SalesOrderID');
        $object->issueTime       = (bool)D::get($cbc.'IssueTime');
        $object->typeCode        = D::get($cbc.'OrderTypeCode');
        $object->note            = D::get($cbc.'Note');

        $object->customerReference    = D::get($cbc.'CustomerReference');
        $object->accountingCost       = D::get($cbc.'AccountingCost');
        $object->period               = D::get($cac.'ValidityPeriod');
        $object->quotationReference   = D::get($cac.'QuotationDocumentReference');
        $object->orderReference       = D::get($cac.'OrderDocumentReference');
        $object->originatorReference  = D::get($cac.'OriginatorDocumentReference');
        $object->additionalReferences = D::getArray($cac.'AdditionalDocumentReference'); // []
        $object->contractReference    = D::get($cac.'Contract');

        $object->buyerCustomer      = D::get($cac.'BuyerCustomerParty');
        $object->sellerSupplier     = D::get($cac.'SellerSupplierParty');
        $object->originatorCustomer = D::get($cac.'OriginatorCustomerParty');
        $object->accountingCustomer = D::get($cac.'AccountingCustomerParty');
        $object->delivery           = D::get($cac.'Delivery');
        $object->deliveryTerms      = D::get($cac.'DeliveryTerms');
        $object->paymentTerms       = D::get($cac.'PaymentTerms/'.$cbc.'Note');

        $object->allowancesCharges  = D::getArray($cac.'AllowanceCharge');
        $object->taxTotal           = D::get($cac.'TaxTotal');
        $object->total              = D::get($cac.'AnticipatedMonetaryTotal');
        $object->lines              = D::getArray($cac.'OrderLine');

        return $object;
    }

    /**
     * Generate Invoice object to HTML
     * @return string
     */
    public function html()
    {
//        $this->validate();
        return Preview\View::render('order', [
            'doc' => $this,
            'code' => new Helper\CodeList()
        ]);
    }

    /**
     * Generate Invoice object to XML
     * @return string
     */
    public function xml()
    {
        $xmlService = new Service();
        $xmlService->namespaceMap = [
            S::getNS(S::NS)   => '',
            S::getNS(S::CBC)  => 'cbc',
            S::getNS(S::CAC)  => 'cac',
            S::getNS(S::CCTS) => 'ccts',
            S::getNS(S::QDT)  => 'qdt',
            S::getNS(S::UDT)  => 'udt',
            S::getNS(S::XS)   => 'xs',
        ];
        return $xmlService->write('Order', [
            $this
        ]);
    }

    /**
     * Parse XML to Invoice object
     * @param string $xml
     * @return Invoice
     */
    public static function parse($xml)
    {
        if(!$xml){
            throw new Exception\EmptyDocumentException('Order');
        }
        $service = new Service();
        $service->elementMap = [
            S::NS. 'Order'       => Order::class,

            S::CAC.'ValidityPeriod'              => Doc\Period::class,
            S::CAC.'OrderDocumentReference'      => Doc\Reference\Order::class,
            S::CAC.'QuotationDocumentReference'  => Doc\Reference\QuotationDocument::class,
            S::CAC.'OriginatorDocumentReference' => Doc\Reference\OriginatorDocument::class,
            S::CAC.'AdditionalDocumentReference' => Doc\Reference\Additional::class,
            S::CAC.'Attachment'                  => Doc\Reference\Attachment::class,
            S::CAC.'Contract'                    => Doc\Reference\Contract::class,

            S::CAC.'BuyerCustomerParty'      => Doc\Party\Order\BuyerCustomer::class,
            S::CAC.'SellerSupplierParty'     => Doc\Party\Order\SellerSupplier::class,
            S::CAC.'OriginatorCustomerParty' => Doc\Party\Order\OriginatorCustomer::class,
            S::CAC.'AccountingCustomerParty' => Doc\Party\Order\AccountingCustomer::class,

            S::CAC.'PostalAddress'      => Doc\Party\Address::class,
            S::CAC.'PartyTaxScheme'     => Doc\Party\TaxScheme::class,
            S::CAC.'PartyLegalEntity'   => Doc\Party\LegalEntity::class,
            S::CAC.'Contact'            => Doc\Party\Contact::class,
            S::CAC.'RegistrationAddress'=> Doc\Party\Address::class,

            S::CAC.'Delivery'               => Doc\OrderDelivery::class,
            S::CAC.'RequestedDeliveryPeriod'=> Doc\Period::class,
            S::CAC.'Address'                => Doc\Party\Address::class,
            S::CAC.'DeliveryParty'          => Doc\Party\Order\Delivery::class,
            S::CAC.'DeliveryTerms'          => Doc\DeliveryTerms::class,

            S::CAC.'AllowanceCharge'          => Doc\AllowanceCharge::class,
            S::CAC.'TaxCategory'              => Doc\TaxCategory::class,
            S::CAC.'TaxTotal'                 => Doc\TaxTotal::class,
            S::CAC.'TaxSubtotal'              => Doc\TaxSubtotal::class,
            S::CAC.'AnticipatedMonetaryTotal' => Doc\MonetaryTotal::class,

            S::CAC.'OrderLine'              => Doc\OrderLine::class,
            S::CAC.'Item'                   => Doc\Line\OrderItem::class,
            S::CAC.'OriginatorParty'        => Doc\Party\Order\LineOriginator::class,
            S::CAC.'ClassifiedTaxCategory'  => Doc\TaxCategory::class,
            S::CAC.'AdditionalItemProperty' => Doc\Line\ItemProperty::class,
            S::CAC.'ItemInstance'           => Doc\Line\ItemInstance::class,
            S::CAC.'Price'                  => Doc\Line\Price::class,
        ];

        $object = $service->parse($xml);
        if($object instanceof Order){
            return $object;
        }

        throw new Exception\WrongDocumentException('Order');
    }

}
