<?php

namespace SJRoyd\PEF\BIS;

use Sabre\Xml\Reader;
use Sabre\Xml\Service;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Document as Doc;
use SJRoyd\PEF\BIS\Document\CreditNoteSchema as S;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * @property string $id numer faktury / invoice number
 * @property \DateTime $issueDate data wystawienia faktury / Invoice issue date
 * @property \DateTime $taxPointDate data powstania obowiązku podatkowego / Tax point date - the date when the VAT becomes accountable
 * @property int $typeCode identyfikator typu faktury / Invoice type code
 * @property string $note uwagi tekstowe do faktury / Invoice note
 * @property string $accountingCost znak stanowiska kosztów u nabywcy / Buyer accounting reference
 * @property string $buyerReference referencja kupującego / Buyer reference
 * @property Doc\Period $period okres fakturowany / invoice period
 * @property Doc\Reference\Order $orderReference referencje do zamówienia
 * @property Doc\Reference\Billing[] $billingReference referencje do faktury
 * @property Doc\Reference\DespatchDocument $despatchReference referencje do dokumentu awiza wysyłki
 * / Despatch Advice reference
 * @property Doc\Reference\ReceiptDocument $receiptReference referencje do dokumentu potwierdzenia odbioru
 * / Receiving advice reference
 * @property Doc\Reference\OriginatorDocument $originatorReference referencje do dokumentu zamawiającego
 * / buyer document reference
 * @property Doc\Reference\ContractorDocument $contractReference referencje do kontraktu
 * / contract reference
 * @property Doc\Reference\Additional[] $additionalReferences
 * @property Doc\Reference\Project $projectReference referencja do projektu / Project reference
 * @property Doc\Party\AccountingSupplier $accountingSupplier dane sprzedawcy / supplier data
 * @property Doc\Party\AccountingCustomer $accountingCustomer dane klienta / customer data
 * @property Doc\Party\Payee $payee dane odbiorcy płatności / Payee data
 * @property Doc\Party\TaxRepresentative $taxRepresentative dane przedstawiciela podatkowego
 * / Tax representative data
 * @property Doc\BillingDelivery $delivery dane odbiorcy faktury (jeśli inny niż Kupujący) i dostawy
 * / Delicer to and invoice receiver data (if different from customer)
 * @property Doc\PaymentMeans[] $paymentMeans sposób zapłaty / payment means
 * @property string $paymentTerms warunki płatności / Payment terms
 * @property Doc\AllowanceCharge[] $allowancesCharges obciążenia lub upusty / charges or allowances
 * @property Doc\TaxTotal[] $taxTotal podsumowanie podatku / tax totals
 * @property Doc\MonetaryTotal $total Podsumowanie dla faktury / Invoice document totals
 * @property Doc\InvoiceCorrectionLine[] $lines
 *
 * @property Doc\Correction\Invoice $originInvoiceData
 * @property Doc\Correction\InvoiceDifference $diffInvoiceData
 */
class InvoiceCorrection extends Invoice implements XmlSerializable, XmlDeserializable, Helper\Callback
{
    /**
     * wersja komunikatu / message version
     * @var string
     */
    protected $customizationID = 'urn:cen.eu:en16931:2017#compliant#urn:fdc:peppol.eu:2017:poacc:billing:3.0#extended#urn:fdc:www.efaktura.gov.pl:ver1.0';

    /**
     * identyfikator procesu biznesowego / business process identifier
     * @var string
     */
    protected $profileID = 'urn:fdc:www.efaktura.gov.pl:ver1.0:corr_inv:ver1.0';

    /**
     *
     * @var Invoice
     */
    private $originInvoice;

    /**
     *
     * @var Doc\Correction\Invoice
     */
    protected $originInvoiceData;

    /**
     *
     * @var Doc\Correction\InvoiceDifference
     */
    protected $diffInvoiceData;

    /**
     *
     * @param string $id                  Invoice number
     * @param \DateTime|string $issueDate Invoice issue date
     * @param string $currency            Currency code
     */
    public function __construct($id, $issueDate, $currency = PEPPOL\Currency::PLN)
    {
        parent::__construct($id, $issueDate, $currency);
        $this->typeCode = PEPPOL\CreditNoteTypeCode::CREDIT_NOTE;
        $this->diffInvoiceData = new Document\Correction\InvoiceDifference();
    }

    public function setOriginInvoice(Invoice $invoice)
    {
        $this->taxCurrency = $invoice->taxCurrency;
        $this::$taxCurrencyCode = $invoice::$taxCurrencyCode;
        $this->orderReference = $invoice->orderReference;
        $ref = new Document\Reference\Billing();
        $ref->documentReference($invoice->id, $invoice->issueDate);
        $this->billingReference[] = $ref;
        $this->contractReference = $invoice->contractReference;
        $this->projectReference = $invoice->projectReference;

        $this->accountingSupplier = $invoice->accountingSupplier;
        $this->accountingCustomer = $invoice->accountingCustomer;
        $this->payee = $invoice->payee;
        $this->taxRepresentative = $invoice->taxRepresentative;
        $this->delivery = $invoice->delivery;

        $this->originInvoice = $invoice;

        $this->originInvoiceData = Doc\Correction\Invoice::copy($invoice);

        $this->total()->setPrepaidAmount($invoice->total->prepaidAmount);

        return $this;
    }

    /**
     * Kopiuje sposoby płatności z faktury pierwotnej
     * @return $this
     */
    public function copyPaymentMeans()
    {
        $this->paymentMeans = $this->originInvoice->paymentMeans;
        $this->paymentTerms = $this->originInvoice->paymentTerms;

        return $this;
    }

    /**
     * Kopiuje upusty i obciążenia z faktury pierwotnej
     * @return $this
     */
    public function copyAllowancesCharges()
    {
        $this->allowancesCharges = $this->originInvoice->allowancesCharges;

        return $this;
    }

    /**
     * @deprecated
     */
    public function setDueDate($dueDate)
    {
        throw new \InvalidArgumentException('Due date not available in this document');
    }

    /**
     *
     * @param type $amount
     * @param type $percent
     * @param type $taxCategoryId
     * @return Doc\AllowanceCharge
     */
    public function addAllowanceCharge($amount, $percent = null, $taxCategoryId = null)
    {
        if(!$this->originInvoice){
            throw new \Exception('Set the Origin Invoice at first');
        }
        return parent::addAllowanceCharge($amount, $percent, $taxCategoryId);
    }

    /**
     *
     * @return Doc\TaxTotal
     */
    public function taxTotal()
    {
        if(!$this->parse && !$this->originInvoice){
            throw new \Exception('Set the Origin Invoice at first');
        }
        return parent::taxTotal();
    }

    /**
     *
     * @return Doc\MonetaryTotal
     */
    public function total()
    {
        if(!$this->parse && !$this->originInvoice){
            throw new \Exception('Set the Origin Invoice at first');
        }
        return parent::total();
    }

    /**
     *
     * @param string $name
     * @param float $unitPriceAmount
     * @param int $taxPercent
     * @param string $taxCode
     * @return Doc\InvoiceCorrectionLine
     */
    public function line($name, $quantity, $quantityCode, $unitPriceAmount, $taxPercent, $taxCode)
    {
        if(!$this->originInvoice){
            throw new \Exception('Set the Origin Invoice at first');
        }
        static $id = 0;
        $id++;
        $line = new Doc\InvoiceCorrectionLine($id, $quantity, $quantityCode);
        $line->setCallback($this);
//        $line->setNetAmount($netAmount);
        $line->item($name, $taxPercent, $taxCode);
        $line->price($unitPriceAmount);
        $this->lines[] = $line;
        $this->callback();
        return $line;
    }

    public function callback($item = null)
    {
        parent::callback();
        $this->diffInvoiceData->calculate($this);
    }


    /**
     * The xmlSerialize method is called during xml writing.
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer, $cac = S::CAC, $cbc = S::CBC)
    {
        $this->validate();
        $data = [
            S::EXT.'UBLExtensions' => [
                [
                    'name' => S::EXT.'UBLExtension',
                    'value' => [
                        [
                            'name' => S::EXT.'ExtensionContent',
                            'value' => [
                                [
                                    'name' => $cbc.'Comment',
                                    'value' => 'FAKTURA KORYGUJĄCA'
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'name' => S::EXT.'UBLExtension',
                    'value' => [
                        [
                            'name' => $cbc.'Name',
                            'value' => 'Dane faktury korygowanej'
                        ],
                        [
                            'name' => S::EXT.'ExtensionContent',
                            'value' => [
                                S::CAC_PL.'OriginalInvoiceData' => $this->originInvoiceData
                            ]
                        ]
                    ]
                ],
                [
                    'name' => S::EXT.'UBLExtension',
                    'value' => [
                        [
                            'name' => $cbc.'Name',
                            'value' => 'Różnice wartości faktury po uwzględnieniu korekty'
                        ],
                        [
                            'name' => S::EXT.'ExtensionContent',
                            'value' => [
                                S::CAC_PL.'InvoiceCorrection' => $this->diffInvoiceData
                            ]
                        ]
                    ]
                ]
            ],
            $cbc.'CustomizationID'      => $this->customizationID,
            $cbc.'ProfileID'            => $this->profileID,
            $cbc.'ID'                   => $this->id,
            $cbc.'IssueDate'            => Helper\dateFormat($this->issueDate),
            $cbc.'TaxPointDate'         => Helper\dateFormat($this->taxPointDate),
            $cbc.'CreditNoteTypeCode'   => $this->typeCode,
            $cbc.'Note'                 => $this->note, //
            $cbc.'DocumentCurrencyCode' => self::$currencyCode,
            $cbc.'TaxCurrencyCode'      => self::$taxCurrencyCode,
            $cbc.'AccountingCost'       => $this->accountingCost, //
            $cbc.'BuyerReference'       => $this->buyerReference, //
            $cac.'InvoicePeriod'        => $this->period, //

            $cac.'OrderReference'              => $this->orderReference,
        ];
        foreach($this->billingReference as $ref) {
            $data[] = [
                'name' => $cac.'BillingReference',
                'value' => $ref
            ];
        }
        $data += [
            $cac.'DespatchDocumentReference'   => $this->despatchReference,
            $cac.'ReceiptDocumentReference'    => $this->receiptReference,
            $cac.'OriginatorDocumentReference' => $this->originatorReference,
            $cac.'ContractDocumentReference'   => $this->contractReference,
        ];
        foreach($this->additionalReferences as $ref){
            $data[] = [
                'name' => $cac.'AdditionalDocumentReference',
                'value' => $ref
            ];
        }
        $data += [
            $cac.'ProjectReference'            => $this->projectReference,

            $cac.'AccountingSupplierParty' => [
                $cac.'Party' => $this->accountingSupplier
            ],
            $cac.'AccountingCustomerParty' => [
                $cac.'Party' => $this->accountingCustomer
            ],
            $cac.'PayeeParty'              => $this->payee,
            $cac.'TaxRepresentativeParty'  => $this->taxRepresentative,

            $cac.'Delivery'           => $this->delivery,
        ];
        foreach($this->paymentMeans as $paymentMean){
            $data[] = [
                'name' => $cac.'PaymentMeans',
                'value' => $paymentMean
            ];
        }
        $this->paymentTerms && $data[$cac.'PaymentTerms'] = [
            $cbc.'Note' => $this->paymentTerms
        ];
        foreach($this->allowancesCharges as $allowanceCharge){
            $data[] = [
                'name' => $cac.'AllowanceCharge',
                'value' => $allowanceCharge
            ];
        }
        foreach($this->taxTotal as $tax){
            $data[] = [
                'name' => $cac.'TaxTotal',
                'value' => $tax
            ];
        }
        $data[$cac.'LegalMonetaryTotal'] = $this->total;
        foreach($this->lines as $line){
            $data[] = [
                'name' => $cac.'CreditNoteLine',
                'value' => $line
            ];
        }

        $writer->write(Helper\cleanArray($data));
    }

    /**
     * The deserialize method is called during xml parsing.
     * @return mixed
     */
    public static function xmlDeserialize(Reader $reader, $cac = S::CAC, $cbc = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($cbc.'ID'),
                D::get($cbc.'IssueDate', '\DateTime'),
                D::get($cbc.'DocumentCurrencyCode')
            );
        $object->parse           = true;
        $object->taxPointDate    = D::get($cbc.'TaxPointDate', '\DateTime');
        $object->typeCode        = D::get($cbc.'CreditNoteTypeCode');
        $object->taxCurrency =
        self::$taxCurrencyCode   = D::get($cbc.'TaxCurrencyCode');
        $object->note            = D::get($cbc.'Note');
        $object->accountingCost  = D::get($cbc.'AccountingCost');

        $object->buyerReference       = D::get($cbc.'BuyerReference');
        $object->period               = D::get($cac.'InvoicePeriod');
        $object->orderReference       = D::get($cac.'OrderReference');
        $object->billingReference     = D::getArray($cac.'BillingReference'); // []
        $object->despatchReference    = D::get($cac.'DespatchDocumentReference');
        $object->receiptReference     = D::get($cac.'ReceiptDocumentReference');
        $object->originatorReference  = D::get($cac.'OriginatorDocumentReference');
        $object->contractReference    = D::get($cac.'ContractDocumentReference');
        $object->additionalReferences = D::getArray($cac.'AdditionalDocumentReference'); // []
        $object->projectReference     = D::get($cac.'ProjectReference');

        $object->accountingSupplier = D::get($cac.'AccountingSupplierParty');
        $object->accountingCustomer = D::get($cac.'AccountingCustomerParty');
        $object->payee              = D::get($cac.'PayeeParty');
        $object->taxRepresentative  = D::get($cac.'TaxRepresentativeParty');

        $object->delivery           = D::get($cac.'Delivery');
        $object->paymentMeans       = D::getArray($cac.'PaymentMeans');
        $object->paymentTerms       = D::get($cac.'PaymentTerms/'.$cbc.'Note');
        $object->allowancesCharges  = D::getArray($cac.'AllowanceCharge');
        $object->taxTotal           = D::getArray($cac.'TaxTotal');
        $object->total              = D::get($cac.'LegalMonetaryTotal');

        $object->lines = D::getArray($cac.'CreditNoteLine');

        /** @var Doc\Extension $ext */
        foreach(D::getArray(S::EXT.'UBLExtensions/'.S::EXT.'UBLExtension') as $ext){
            $ext->originalInvoiceData && $object->originInvoiceData = $ext->originalInvoiceData;
            $ext->invoiceDifference && $object->diffInvoiceData = $ext->invoiceDifference;
        }

        return $object;
    }

    /**
     * Generate Invoice object to HTML
     * @return string
     */
    public function html()
    {
        $this->validate();
        return Preview\View::render('invoice_correction', [
            'doc' => $this,
            'code' => new Helper\CodeList()
        ]);
    }


    /**
     * Generate Invoice object to XML
     * @return string
     */
    public function xml()
    {
        $xmlService = new Service();
        $xmlService->namespaceMap = [
            S::getNS(S::NS)     => '',
            S::getNS(S::CBC)    => 'cbc',
            S::getNS(S::CAC)    => 'cac',
            S::getNS(S::CBC_PL) => 'cbc-pl',
            S::getNS(S::CAC_PL) => 'cac-pl',
            S::getNS(S::EXT)    => 'ext',
            S::getNS(S::CCTS)   => 'ccts',
            S::getNS(S::QDT)    => 'qdt',
            S::getNS(S::UDT)    => 'udt',
            S::getNS(S::XS)     => 'xs',
        ];
        return $xmlService->write('CreditNote', [
            $this
        ]);
    }

    /**
     * Parse XML to Invoice object
     * @param string $xml
     * @return Invoice
     */
    public static function parse($xml)
    {
        if(!$xml){
            throw new Exception\EmptyDocumentException('Invoice Correction');
        }
        $service = new Service();
        $service->elementMap = [
            S::NS. 'CreditNote'    => InvoiceCorrection::class,
            S::CAC.'InvoicePeriod' => Doc\Period::class,

            S::CAC.'OrderReference'              => Doc\Reference\Order::class,
            S::CAC.'BillingReference'            => Doc\Reference\Billing::class,
            S::CAC.'DespatchDocumentReference'   => Doc\Reference\DespatchDocument::class,
            S::CAC.'ReceiptDocumentReference'    => Doc\Reference\ReceiptDocument::class,
            S::CAC.'OriginatorDocumentReference' => Doc\Reference\OriginatorDocument::class,
            S::CAC.'ContractDocumentReference'   => Doc\Reference\ContractorDocument::class,
            S::CAC.'AdditionalDocumentReference' => Doc\Reference\Additional::class,
            S::CAC.'Attachment'                  => Doc\Reference\Attachment::class,
            S::CAC.'ProjectReference'            => Doc\Reference\Project::class,
            S::CAC.'InvoiceDocumentReference'    => Doc\Reference\InvoiceDocument::class,

            S::CAC.'AccountingSupplierParty' => Doc\Party\Billing\AccountingSupplier::class,
            S::CAC.'AccountingCustomerParty' => Doc\Party\Billing\AccountingCustomer::class,
            S::CAC.'PayeeParty'              => Doc\Party\Billing\Payee::class,
            S::CAC.'TaxRepresentativeParty'  => Doc\Party\Billing\TaxRepresentative::class,

            S::CAC.'PostalAddress'    => Doc\Party\Address::class,
            S::CAC.'PartyTaxScheme'   => Doc\Party\TaxScheme::class,
            S::CAC.'PartyLegalEntity' => Doc\Party\LegalEntity::class,
            S::CAC.'Contact'          => Doc\Party\Contact::class,

            S::CAC.'Delivery'               => Doc\BillingDelivery::class,
            S::CAC.'Address'                => Doc\Party\Address::class,
            S::CAC.'DeliveryParty'          => Doc\Party\Billing\Delivery::class,

            S::CAC.'PaymentMeans'           => Doc\PaymentMeans::class,
            S::CAC.'CardAccount'            => Doc\PaymentMeans\Card::class,
            S::CAC.'PayeeFinancialAccount'  => Doc\PaymentMeans\Account::class,
            S::CAC.'PaymentMandate'         => Doc\PaymentMeans\Mandate::class,
            S::CAC.'PayerFinancialAccount'  => Doc\PaymentMeans\Account::class,

            S::CAC.'AllowanceCharge'        => Doc\AllowanceCharge::class,
            S::CAC.'TaxCategory'            => Doc\TaxCategory::class,
            S::CAC.'TaxTotal'               => Doc\TaxTotal::class,
            S::CAC.'TaxSubtotal'            => Doc\TaxSubtotal::class,
            S::CAC.'LegalMonetaryTotal'     => Doc\MonetaryTotal::class,

            S::CAC.'CreditNoteLine'         => Doc\InvoiceCorrectionLine::class,
            S::CAC.'DocumentReference'      => Doc\Reference\Additional::class,
            S::CAC.'Item'                   => Doc\Line\Item::class,
            S::CAC.'ClassifiedTaxCategory'  => Doc\TaxCategory::class,
            S::CAC.'Price'                  => Doc\Line\Price::class,


            S::EXT.'UBLExtension'               => Doc\Extension::class,
            S::CAC_PL.'OriginalInvoiceData'     => Doc\Correction\Invoice::class,
            S::CAC_PL.'InvoiceCorrection'       => Doc\Correction\InvoiceDifference::class,

            S::CAC_PL.'InvoicePeriod'           => Doc\Correction\Period::class,
            S::CAC_PL.'AllowanceCharge'         => Doc\Correction\AllowanceCharge::class,
            S::CAC_PL.'TaxCategory'             => Doc\Correction\TaxCategory::class,
            S::CAC_PL.'TaxTotal'                => Doc\Correction\TaxTotal::class,
            S::CAC_PL.'TaxSubtotal'             => Doc\Correction\TaxSubtotal::class,
            S::CAC_PL.'LegalMonetaryTotal'      => Doc\Correction\MonetaryTotal::class,

            S::CAC_PL.'InvoiceLine'             => Doc\Correction\InvoiceLine::class,
            S::CAC_PL.'DocumentReference'       => Doc\Correction\Reference\Additional::class,
            S::CAC_PL.'Attachment'              => Doc\Correction\Reference\Attachment::class,
            S::CAC_PL.'Item'                    => Doc\Correction\Line\Item::class,
            S::CAC_PL.'ClassifiedTaxCategory'   => Doc\Correction\TaxCategory::class,
            S::CAC_PL.'Price'                   => Doc\Correction\Line\Price::class,
        ];

        $object = $service->parse($xml);
        if($object instanceof InvoiceCorrection){
            return $object;
        }

        throw new Exception\WrongDocumentException('Invoice Correction');
    }

}
