<?php

namespace SJRoyd\PEF\BIS;

use Sabre\Xml\Reader;
use Sabre\Xml\Service;
use Sabre\Xml\Writer;
use Sabre\Xml\XmlDeserializable;
use Sabre\Xml\XmlSerializable;
use SJRoyd\PEF\BIS\Document as Doc;
use SJRoyd\PEF\BIS\Document\InvoiceSchema as S;
use SJRoyd\PEF\BIS\Helper;
use SJRoyd\PEF\BIS\Helper\Deserializer as D;
use SJRoyd\PEF\BIS\Helper\PEPPOL;

/**
 * @property string $id numer faktury / invoice number
 * @property \DateTime $issueDate data wystawienia faktury / Invoice issue date
 * @property \DateTime $dueDate termin płatności faktury / Payment due date
 * @property \DateTime $taxPointDate data powstania obowiązku podatkowego / Tax point date - the date when the VAT becomes accountable
 * @property int $typeCode identyfikator typu faktury / Invoice type code
 * @property string $note uwagi tekstowe do faktury / Invoice note
 * @property string $accountingCost znak stanowiska kosztów u nabywcy / Buyer accounting reference
 * @property string $buyerReference referencja kupującego / Buyer reference
 * @property Doc\Period $period okres fakturowany / invoice period
 * @property Doc\Reference\Order $orderReference referencje do zamówienia
 * @property Doc\Reference\Billing[] $billingReference referencje do faktury
 * @property Doc\Reference\DespatchDocument $despatchReference referencje do dokumentu awiza wysyłki
 * / Despatch Advice reference
 * @property Doc\Reference\ReceiptDocument $receiptReference referencje do dokumentu potwierdzenia odbioru
 * / Receiving advice reference
 * @property Doc\Reference\OriginatorDocument $originatorReference referencje do dokumentu zamawiającego
 * / buyer document reference
 * @property Doc\Reference\ContractorDocument $contractReference referencje do kontraktu
 * / contract reference
 * @property Doc\Reference\Additional[] $additionalReferences
 * @property Doc\Reference\Project $projectReference referencja do projektu / Project reference
 * @property Doc\Party\Billing\AccountingSupplier $accountingSupplier dane sprzedawcy / supplier data
 * @property Doc\Party\Billing\AccountingCustomer $accountingCustomer dane klienta / customer data
 * @property Doc\Party\Billing\Payee $payee dane odbiorcy płatności / Payee data
 * @property Doc\Party\Billing\TaxRepresentative $taxRepresentative dane przedstawiciela podatkowego
 * / Tax representative data
 * @property Doc\BillingDelivery $delivery dane odbiorcy faktury (jeśli inny niż Kupujący) i dostawy
 * / Delicer to and invoice receiver data (if different from customer)
 * @property Doc\PaymentMeans[] $paymentMeans sposób zapłaty / payment means
 * @property string $paymentTerms warunki płatności / Payment terms
 * @property Doc\AllowanceCharge[] $allowancesCharges obciążenia lub upusty / charges or allowances
 * @property Doc\TaxTotal[] $taxTotal podsumowanie podatku / tax totals
 * @property Doc\MonetaryTotal $total Podsumowanie dla faktury / Invoice document totals
 * @property Doc\InvoiceLine[] $lines
 */
class Invoice extends Doc\Document implements XmlSerializable, XmlDeserializable, Helper\Callback
{
    /**
     * wersja komunikatu / message version
     * @var string
     */
    protected $customizationID = 'urn:cen.eu:en16931:2017#compliant#urn:fdc:peppol.eu:2017:poacc:billing:3.0';

    /**
     * identyfikator procesu biznesowego / business process identifier
     * @var string
     */
    protected $profileID = 'urn:fdc:peppol.eu:2017:poacc:billing:01:1.0';

    /**
     *
     * @param string $id                  Invoice number
     * @param \DateTime|string $issueDate Invoice issue date
     * @param string $currency            Currency code
     */
    public function __construct($id, $issueDate, $currency = PEPPOL\Currency::PLN)
    {
        $this->id = $id;
        $this->issueDate = Helper\date($issueDate);
        $this->typeCode = PEPPOL\InvoiceTypeCode::INVOICE;
        $this->currency = $currency;
        self::$currencyCode = $currency;
        Helper\CodeList::validate(
            Helper\CodeList::getCurrency($currency), $currency
        );
    }

    /**
     * @param string|\DateTime $dueDate
     * @return $this
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = Helper\date($dueDate);
        return $this;
    }

    /**
     * @param string|\DateTime $taxPointDate
     * @return $this
     */
    public function setTaxPointDate($taxPointDate)
    {
        $this->taxPointDate = Helper\date($taxPointDate);
        return $this;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    /**
     * kod waluty podatku VAT i kwota / VAT accounting currency code and amount
     * @param string $taxCurrency
     * @return $this
     */
    public function setTaxOtherCurrency($taxCurrency, $amount)
    {
        $this->taxCurrency = $taxCurrency;
        self::$taxCurrencyCode = $taxCurrency;
        Helper\CodeList::validate(
            Helper\CodeList::getCurrency($taxCurrency), $taxCurrency
        );
        $this->taxTotal();
        $this->taxTotal[1] = new Doc\TaxTotal($taxCurrency);
        $this->taxTotal[1]->setAmount($amount);
        return $this;
    }

    /**
     * @param int $type PEPPOL\InvoiceTypeCode::INVOICE
     */
    public function setTypeCode($type)
    {
        $this->typeCode = $type;
        Helper\CodeList::validate(
            Helper\CodeList::getInvoice($type), $type
        );
    }

    /**
     * @param string $accountingCost
     * @return $this
     */
    public function setAccountingCost($accountingCost)
    {
        $this->accountingCost = $accountingCost;
        return $this;
    }

    /**
     * @param string $reference
     * @return $this
     */
    public function setBuyerReference($reference)
    {
        $this->buyerReference = $reference;
        return $this;
    }

    /**
     * @param \DateTime|string $startDate
     * @param \DateTime|string $endDate
     * @param int $code
     * @return $this
     */
    public function setPeriod($startDate = null, $endDate = null, $code = PEPPOL\VatDate::DELIVERY)
    {
        $this->period = new Doc\Period($startDate, $endDate);
        $this->period->setDescriptionCode($code);
        return $this;
    }

    /**
     * @param string $reference
     * @param string $salesOrderId
     * @return $this
     */
    public function setOrderReference($reference, $salesOrderId = null)
    {
        $this->orderReference = new Doc\Reference\Order($reference, $salesOrderId);
        return $this;
    }

    /**
     * @param string $id
     * @param string|\DateTime $issueDate
     * @return $this
     */
    public function addBillingReference($id, $issueDate = null)
    {
        $reference = new Doc\Reference\Billing();
        $reference->documentReference($id, $issueDate);
        $this->billingReference[] = $reference;
        return $this;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setDespatchReference($id)
    {
        $this->despatchReference = new Doc\Reference\DespatchDocument($id);
        return $this;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setReceiptReference($id)
    {
        $this->receiptReference = new Doc\Reference\ReceiptDocument($id);
        return $this;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setOriginatorReference($id)
    {
        $this->originatorReference = new Doc\Reference\OriginatorDocument($id);
        return $this;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setContractReference($id)
    {
        $this->contractReference = new Doc\Reference\ContractorDocument($id);
        return $this;
    }

    /**
     *
     * @param string $id
     * @param string $schemeId
     * @return Doc\Reference\Additional
     */
    public function additionalReference($id, $schemeId)
    {
        $reference = new Doc\Reference\Additional($id, $schemeId);
        $this->additionalReferences[] = $reference;
        return $reference;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setProjectReference($id)
    {
        $this->projectReference = new Doc\Reference\Project($id);
        return $this;
    }

    /**
     * @param string $name
     * @return Doc\Party\Billing\AccountingSupplier
     */
    public function supplier($name)
    {
        $this->accountingSupplier = new Doc\Party\Billing\AccountingSupplier($name);
        return $this->accountingSupplier;
    }

    /**
     * @param string $name
     * @return Doc\Party\Billing\AccountingCustomer
     */
    public function customer($name)
    {
        $this->accountingCustomer = new Doc\Party\Billing\AccountingCustomer($name);
        return $this->accountingCustomer;
    }

    /**
     * @param string $name
     * @return Doc\Party\Billing\Payee
     */
    public function payee($name)
    {
        $this->payee = new Doc\Party\Billing\Payee($name);
        return $this->payee;
    }

    /**
     *
     * @param string $name
     * @return Doc\Party\Billing\TaxRepresentative
     */
    public function taxRepresentative($name)
    {
        $this->taxRepresentative = new Doc\Party\Billing\TaxRepresentative($name);
        return $this->taxRepresentative;
    }

    /**
     *
     * @return Doc\BillingDelivery
     */
    public function delivery()
    {
        !$this->delivery && $this->delivery = new Doc\BillingDelivery();
        return $this->delivery;
    }

    /**
     *
     * @param string $code
     * @param string $codeName
     * @return Doc\PaymentMeans
     */
    public function addPaymentMeans($code, $codeName)
    {
        $paymentMeans = new Doc\PaymentMeans($code, $codeName);
        $this->paymentMeans[] = $paymentMeans;
        return $paymentMeans;
    }

    /**
     *
     * @param string $paymentTerms
     * @return $this
     */
    public function setPaymentTerms($paymentTerms)
    {
        $this->paymentTerms = $paymentTerms;
        return $this;
    }

    /**
     *
     * @param float $amount
     * @param int $percent
     * @param string $taxCategoryId
     * @return Doc\AllowanceCharge
     */
    public function addAllowanceCharge($amount, $percent = null, $taxCategoryId = null)
    {
        $allowanceCharge = new Doc\AllowanceCharge($amount, $percent, $taxCategoryId);
        $this->allowancesCharges[] = $allowanceCharge;
        $this->callback();
        return $allowanceCharge;
    }

    /**
     *
     * @return Doc\TaxTotal
     */
    public function taxTotal()
    {
        !$this->taxTotal && $this->taxTotal[] = new Doc\TaxTotal(self::$currencyCode);
        return $this->taxTotal[0];
    }

    /**
     *
     * @return Doc\MonetaryTotal
     */
    public function total()
    {
        !$this->total && $this->total = new Doc\MonetaryTotal();
        return $this->total;
    }

    /**
     *
     * @param string $name
     * @param float $unitPriceAmount
     * @param int $taxPercent
     * @param string $taxCode
     * @return Doc\InvoiceLine
     */
    public function line($name, $quantity, $quantityCode, $unitPriceAmount, $taxPercent, $taxCode)
    {
        static $id = 0;
        $id++;
        $line = new Doc\InvoiceLine($id, $quantity, $quantityCode);
        $line->setCallback($this);
//        $line->setNetAmount($netAmount);
        $line->item($name, $taxPercent, $taxCode);
        $line->price($unitPriceAmount);
        $this->lines[] = $line;
        $this->callback();
        return $line;
    }

    public function callback($item = null)
    {
        $this->total()->calculateAmounts($this->lines, $this->allowancesCharges);
        $this->taxTotal()->calculateAmounts($this->lines, $this->allowancesCharges);
    }

    protected function validate()
    {
        foreach($this->taxTotal[0]->subtotal as $index => $tax) {
            switch($index){
                case 0: break;
                case PEPPOL\TaxCategory::EXEMPT.'-0':
                case PEPPOL\TaxCategory::SERVICE_OUTSIDE_SCOPE.'-0':
                    if(!$tax->category->extemptionReason || !$tax->category->extemptionReasonCode){
                        throw new \InvalidArgumentException('No reason of extemption in tax category');
                    }
                    break;
            }
        }
    }

    /**
     * The xmlSerialize method is called during xml writing.
     * @param Writer $writer
     * @return void
     */
    public function xmlSerialize(Writer $writer, $cac = S::CAC, $cbc = S::CBC)
    {
        $this->validate();
        $data = [
            $cbc.'CustomizationID'      => $this->customizationID,
            $cbc.'ProfileID'            => $this->profileID,
            $cbc.'ID'                   => $this->id,
            $cbc.'IssueDate'            => Helper\dateFormat($this->issueDate),
            $cbc.'DueDate'              => Helper\dateFormat($this->dueDate), //
            $cbc.'TaxPointDate'         => Helper\dateFormat($this->taxPointDate), //
            $cbc.'InvoiceTypeCode'      => $this->typeCode,
            $cbc.'Note'                 => $this->note, //
            $cbc.'DocumentCurrencyCode' => self::$currencyCode,
            $cbc.'TaxCurrencyCode'      => self::$taxCurrencyCode,
            $cbc.'AccountingCost'       => $this->accountingCost, //
            $cbc.'BuyerReference'       => $this->buyerReference, //
            $cac.'InvoicePeriod'        => $this->period, //

            $cac.'OrderReference'              => $this->orderReference,
        ];
        foreach($this->billingReference as $ref) {
            $data[] = [
                'name' => $cac.'BillingReference',
                'value' => $ref
            ];
        }
        $data += [
            $cac.'DespatchDocumentReference'   => $this->despatchReference,
            $cac.'ReceiptDocumentReference'    => $this->receiptReference,
            $cac.'OriginatorDocumentReference' => $this->originatorReference,
            $cac.'ContractDocumentReference'   => $this->contractReference,
        ];
        foreach($this->additionalReferences as $ref){
            $data[] = [
                'name' => $cac.'AdditionalDocumentReference',
                'value' => $ref
            ];
        }
        $data += [
            $cac.'ProjectReference'            => $this->projectReference,

            $cac.'AccountingSupplierParty' => [
                $cac.'Party' => $this->accountingSupplier
            ],
            $cac.'AccountingCustomerParty' => [
                $cac.'Party' => $this->accountingCustomer
            ],
            $cac.'PayeeParty'              => $this->payee,
            $cac.'TaxRepresentativeParty'  => $this->taxRepresentative,

            $cac.'Delivery'           => $this->delivery,
        ];
        foreach($this->paymentMeans as $paymentMean){
            $data[] = [
                'name' => $cac.'PaymentMeans',
                'value' => $paymentMean
            ];
        }
        $this->paymentTerms && $data[$cac.'PaymentTerms'] = [
            $cbc.'Note' => $this->paymentTerms
        ];
        foreach($this->allowancesCharges as $allowanceCharge){
            $data[] = [
                'name' => $cac.'AllowanceCharge',
                'value' => $allowanceCharge
            ];
        }
        foreach($this->taxTotal as $tax){
            $data[] = [
                'name' => $cac.'TaxTotal',
                'value' => $tax
            ];
        }
        $data[$cac.'LegalMonetaryTotal'] = $this->total;
        foreach($this->lines as $line){
            $data[] = [
                'name' => $cac.'InvoiceLine',
                'value' => $line
            ];
        }

        $writer->write(Helper\cleanArray($data));
    }

    /**
     * The deserialize method is called during xml parsing.
     * @return mixed
     */
    public static function xmlDeserialize(Reader $reader, $cac = S::CAC, $cbc = S::CBC)
    {
        D::prepare($reader);

        $object = new self(
                D::get($cbc.'ID'),
                D::get($cbc.'IssueDate', '\DateTime'),
                D::get($cbc.'DocumentCurrencyCode')
            );
        $object->parse           = true;
        $object->dueDate         = D::get($cbc.'DueDate', '\DateTime');
        $object->taxPointDate    = D::get($cbc.'TaxPointDate', '\DateTime');
        $object->typeCode        = D::get($cbc.'InvoiceTypeCode');
        $object->taxCurrency =
        self::$taxCurrencyCode   = D::get($cbc.'TaxCurrencyCode');
        $object->note            = D::get($cbc.'Note');
        $object->accountingCost  = D::get($cbc.'AccountingCost');

        $object->buyerReference       = D::get($cbc.'BuyerReference');
        $object->period               = D::get($cac.'InvoicePeriod');
        $object->orderReference       = D::get($cac.'OrderReference');
        $object->billingReference     = D::getArray($cac.'BillingReference'); // []
        $object->despatchReference    = D::get($cac.'DespatchDocumentReference');
        $object->receiptReference     = D::get($cac.'ReceiptDocumentReference');
        $object->originatorReference  = D::get($cac.'OriginatorDocumentReference');
        $object->contractReference    = D::get($cac.'ContractDocumentReference');
        $object->additionalReferences = D::getArray($cac.'AdditionalDocumentReference'); // []
        $object->projectReference     = D::get($cac.'ProjectReference');

        $object->accountingSupplier = D::get($cac.'AccountingSupplierParty');
        $object->accountingCustomer = D::get($cac.'AccountingCustomerParty');
        $object->payee              = D::get($cac.'PayeeParty');
        $object->taxRepresentative  = D::get($cac.'TaxRepresentativeParty');
        $object->delivery           = D::get($cac.'Delivery');

        $object->paymentMeans       = D::getArray($cac.'PaymentMeans');
        $object->paymentTerms       = D::get($cac.'PaymentTerms/'.$cbc.'Note');

        $object->allowancesCharges  = D::getArray($cac.'AllowanceCharge');
        $object->taxTotal           = D::getArray($cac.'TaxTotal');
        $object->total              = D::get($cac.'LegalMonetaryTotal');
        $object->lines              = D::getArray($cac.'InvoiceLine');

        return $object;
    }

    /**
     * Generate Invoice object to HTML
     * @return string
     */
    public function html()
    {
        $this->validate();
        return Preview\View::render('invoice', [
            'doc' => $this,
            'code' => new Helper\CodeList()
        ]);
    }


    /**
     * Generate Invoice object to XML
     * @return string
     */
    public function xml()
    {
        $xmlService = new Service();
        $xmlService->namespaceMap = [
            S::getNS(S::NS)   => '',
            S::getNS(S::CBC)  => 'cbc',
            S::getNS(S::CAC)  => 'cac',
            S::getNS(S::CCTS) => 'ccts',
            S::getNS(S::QDT)  => 'qdt',
            S::getNS(S::UDT)  => 'udt',
            S::getNS(S::XS)   => 'xs',
        ];
        return $xmlService->write('Invoice', [
            $this
        ]);
    }

    /**
     * Parse XML to Invoice object
     * @param string $xml
     * @return Invoice
     */
    public static function parse($xml)
    {
        if(!$xml){
            throw new Exception\EmptyDocumentException('Invoice Correction');
        }
        $service = new Service();
        $service->elementMap = [
            S::NS. 'Invoice'       => Invoice::class,
            S::CAC.'InvoicePeriod' => Doc\Period::class,

            S::CAC.'OrderReference'              => Doc\Reference\Order::class,
            S::CAC.'BillingReference'            => Doc\Reference\Billing::class,
            S::CAC.'DespatchDocumentReference'   => Doc\Reference\DespatchDocument::class,
            S::CAC.'ReceiptDocumentReference'    => Doc\Reference\ReceiptDocument::class,
            S::CAC.'OriginatorDocumentReference' => Doc\Reference\OriginatorDocument::class,
            S::CAC.'ContractDocumentReference'   => Doc\Reference\ContractorDocument::class,
            S::CAC.'AdditionalDocumentReference' => Doc\Reference\Additional::class,
            S::CAC.'Attachment'                  => Doc\Reference\Attachment::class,
            S::CAC.'ProjectReference'            => Doc\Reference\Project::class,
            S::CAC.'InvoiceDocumentReference'    => Doc\Reference\InvoiceDocument::class,

            S::CAC.'AccountingSupplierParty' => Doc\Party\Billing\AccountingSupplier::class,
            S::CAC.'AccountingCustomerParty' => Doc\Party\Billing\AccountingCustomer::class,
            S::CAC.'PayeeParty'              => Doc\Party\Billing\Payee::class,
            S::CAC.'TaxRepresentativeParty'  => Doc\Party\Billing\TaxRepresentative::class,

            S::CAC.'PostalAddress'    => Doc\Party\Address::class,
            S::CAC.'PartyTaxScheme'   => Doc\Party\TaxScheme::class,
            S::CAC.'PartyLegalEntity' => Doc\Party\LegalEntity::class,
            S::CAC.'Contact'          => Doc\Party\Contact::class,

            S::CAC.'Delivery'               => Doc\BillingDelivery::class,
            S::CAC.'Address'                => Doc\Party\Address::class,
            S::CAC.'DeliveryParty'          => Doc\Party\Billing\Delivery::class,

            S::CAC.'PaymentMeans'           => Doc\PaymentMeans::class,
            S::CAC.'CardAccount'            => Doc\PaymentMeans\Card::class,
            S::CAC.'PayeeFinancialAccount'  => Doc\PaymentMeans\Account::class,
            S::CAC.'PaymentMandate'         => Doc\PaymentMeans\Mandate::class,
            S::CAC.'PayerFinancialAccount'  => Doc\PaymentMeans\Account::class,

            S::CAC.'AllowanceCharge'        => Doc\AllowanceCharge::class,
            S::CAC.'TaxCategory'            => Doc\TaxCategory::class,
            S::CAC.'TaxTotal'               => Doc\TaxTotal::class,
            S::CAC.'TaxSubtotal'            => Doc\TaxSubtotal::class,
            S::CAC.'LegalMonetaryTotal'     => Doc\MonetaryTotal::class,

            S::CAC.'InvoiceLine'            => Doc\InvoiceLine::class,
            S::CAC.'DocumentReference'      => Doc\Reference\Additional::class,
            S::CAC.'Item'                   => Doc\Line\InvoiceItem::class,
            S::CAC.'ClassifiedTaxCategory'  => Doc\TaxCategory::class,
            S::CAC.'AdditionalItemProperty' => Doc\Line\ItemProperty::class,
            S::CAC.'Price'                  => Doc\Line\Price::class,
        ];

        $object = $service->parse($xml);
        if($object instanceof Invoice){
            return $object;
        }

        throw new Exception\WrongDocumentException('Invoice');
    }

}
